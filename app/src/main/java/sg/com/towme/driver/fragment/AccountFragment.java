package sg.com.towme.driver.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.work.WorkManager;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import io.socket.client.Ack;
import io.socket.client.Socket;
import sg.com.towme.driver.Controller;
import sg.com.towme.driver.R;
import sg.com.towme.driver.constant.AppConstant;
import sg.com.towme.driver.enumeration.Screens;
import sg.com.towme.driver.listener.CheckNetworkListener;
import sg.com.towme.driver.listener.iDialogButtonClick;
import sg.com.towme.driver.listener.iPageActive;
import sg.com.towme.driver.model.BaseModel;
import sg.com.towme.driver.model.config_.ConfigDetails;
import sg.com.towme.driver.model.user.UserData;
import sg.com.towme.driver.network.IResponseCallback;
import sg.com.towme.driver.network.NetworkCall;
import sg.com.towme.driver.socket.SocketConstant;
import sg.com.towme.driver.socket.SocketIOClient;
import sg.com.towme.driver.utility.AppHelper;
import sg.com.towme.driver.utility.PreferanceHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import jp.wasabeef.glide.transformations.CropCircleTransformation;
import retrofit2.Call;


public class AccountFragment extends BaseFragment implements iPageActive {

    @BindView(R.id.imgProfilePic)
    CircleImageView imgProfilePic;
    @BindView(R.id.txtProfile)
    AppCompatTextView txtProfile;
    @BindView(R.id.txtNotification)
    AppCompatTextView txtNotification;
    @BindView(R.id.txtHistory)
    AppCompatTextView txtHistory;
    @BindView(R.id.txtInvite)
    AppCompatTextView txtInvite;
    @BindView(R.id.txtChangePassword)
    AppCompatTextView txtChangePassword;
    @BindView(R.id.txtLogout)
    AppCompatTextView txtLogout;
    @BindView(R.id.txtName)
    AppCompatTextView txtName;
    @BindView(R.id.txtSupport)
    AppCompatTextView txtSupport;
    @BindView(R.id.txtTAndC)
    AppCompatTextView txtTAndC;
    @BindView(R.id.txtPrivacyPolicy)
    AppCompatTextView txtPrivacyPolicy;
    Socket mSocket;

    public static AccountFragment newInstance() {
        AccountFragment fragment = new AccountFragment();
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSocket = SocketIOClient.getInstance();
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    private void initUI() {
     /*   homeActivity.setHomeIcon(R.drawable.ic_menu);
        homeActivity.setToolbarTitle("",0);*/
        UserData userData = AppHelper.getInstance().getUserDetails();

        Glide.with(getActivity())
                .load(userData.getUserImage())
                .transform(new CropCircleTransformation())
                .into(imgProfilePic);
        txtName.setText(userData.getName());

    }

    @Override
    public void onResume() {
        super.onResume();
        initUI();

    }

    @OnClick({R.id.imgProfilePic, R.id.txtProfile, R.id.txtNotification, R.id.txtHistory, R.id.txtInvite, R.id.txtSupport, R.id.txtChangePassword, R.id.txtTAndC, R.id.txtPrivacyPolicy, R.id.txtPriceChart, R.id.txtLogout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgProfilePic:
                break;
            case R.id.txtProfile:
                homeActivity.openProfileActivity();
                break;
            case R.id.txtNotification:
                homeActivity.openNotificationActivity(0);
                break;
            case R.id.txtHistory:
                homeActivity.openHistoryActivity(Screens.BOOKING_HISTORY, "");
                break;
            case R.id.txtInvite:
                homeActivity.openInviteActivity();
                break;
            case R.id.txtSupport:
                homeActivity.openHelpWebviewActivity();
                break;
            case R.id.txtChangePassword:
                homeActivity.openChangePasswordActivity();
                break;
            case R.id.txtTAndC:
                ConfigDetails configDetails = AppHelper.getInstance().getConfigDetails();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(configDetails.getDriverTermsAndCondition()));
                startActivity(i);
                break;
            case R.id.txtPrivacyPolicy:
                ConfigDetails configDetails1 = AppHelper.getInstance().getConfigDetails();
                Intent i2 = new Intent(Intent.ACTION_VIEW);
                i2.setData(Uri.parse(configDetails1.getPrivacyPolicy()));
                startActivity(i2);
                break;
            case R.id.txtPriceChart:
                ConfigDetails configDetails2 = AppHelper.getInstance().getConfigDetails();
                Intent i3 = new Intent(Intent.ACTION_VIEW);
                i3.setData(Uri.parse(configDetails2.getDriverPriceList()));
                startActivity(i3);
                break;
            case R.id.txtLogout:
                openLogoutDialog();
                break;
        }
    }


    private void openLogoutDialog() {
        iDialogButtonClick callBack = new iDialogButtonClick() {
            @Override
            public void negativeClick() {

            }

            @Override
            public void positiveClick(String s) {

                CheckNetworkListener callback1 = () -> {
                    homeActivity.showProgressDialog();
                    callLogoutApi();
                };
                if (homeActivity.isNetworkAvailable(txtLogout, callback1)) {
                    callback1.onRetryClick();
                }

            }
        };
        homeActivity.openConfirmationDialog(callBack, getString(R.string.are_you_sure_you_want_to_logout), getString(R.string.yes), getString(R.string.no));

    }

    @Override
    public void onPageActive(String str) {

    }

    private void callLogoutApi() {
        JSONObject object = new JSONObject();
        mSocket.emit(SocketConstant.LOGOUT, object, new Ack() {
            @Override
            public void call(Object... args) {
                Log.e("Logout success", args[0].toString());
            }
        });
        NetworkCall.getInstance().logout(new IResponseCallback<BaseModel>() {
            @Override
            public void success(BaseModel data) {
                homeActivity.hideProgressDialog();
                if (data.getCode() == 1) {
                    PreferanceHelper.getInstance().clearPreference();
                    WorkManager.getInstance(Controller.getInstance()).cancelAllWork();
                    homeActivity.openLoginActivity();

                } else {
                    showSnackBar(data.getMessage());
                }
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                homeActivity.hideProgressDialog();
                showSnackBar(baseModel.getMessage());
            }

            @Override
            public void onError(Call<BaseModel> responseCall, Throwable T) {
                homeActivity.hideProgressDialog();
                showSnackBar(getString(R.string.error_message));
            }
        });

    }
}
