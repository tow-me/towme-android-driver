package sg.com.towme.driver.listener;

/**
 * Created by dhara golakiya on 29-04-2019.
 */

public interface iGPSEnableListener {
    void onGPSEnableSuccess();
    void onGPSEnableCancel();
}
