package sg.com.towme.driver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OnOffResponse extends BaseModel {
    @SerializedName("data")
    @Expose
    private DriverStatus data;

    public DriverStatus getData() {
        return data;
    }

    public void setData(DriverStatus data) {
        this.data = data;
    }
}
