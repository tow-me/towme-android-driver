package sg.com.towme.driver.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import sg.com.towme.driver.PicImage.ImagePicker;
import sg.com.towme.driver.R;
import sg.com.towme.driver.constant.AppConstant;
import sg.com.towme.driver.enumeration.LoginType;
import sg.com.towme.driver.enumeration.Screens;
import sg.com.towme.driver.genericbottomsheet.GenericBottomModel;
import sg.com.towme.driver.genericbottomsheet.GenericBottomSheetDialog;
import sg.com.towme.driver.listener.CheckNetworkListener;
import sg.com.towme.driver.model.BaseModel;
import sg.com.towme.driver.model.SocialData;
import sg.com.towme.driver.model.brand.BrandData;
import sg.com.towme.driver.model.brand.BrandPojo;
import sg.com.towme.driver.model.config_.ConfigDetails;
import sg.com.towme.driver.model.user.UserPojo;
import sg.com.towme.driver.network.IResponseCallback;
import sg.com.towme.driver.network.NetworkCall;
import sg.com.towme.driver.network.Parameter;
import sg.com.towme.driver.utility.AppHelper;
import sg.com.towme.driver.utility.AppValidation;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import jp.wasabeef.glide.transformations.CropCircleTransformation;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

public class DriverSignupActivity extends ToolBarActivity {

    private static final int PICK_IMAGE = 1;
    private static final int PLACE_PICKER = 111;

    @BindView(R.id.imgProfilePic)
    CircleImageView imgProfilePic;
    @BindView(R.id.btnRegister)
    AppCompatButton btnRegister;
    @BindView(R.id.edtName)
    AppCompatEditText edtName;
    @BindView(R.id.edtCompanyName)
    AppCompatEditText edtCompanyName;

    @BindView(R.id.edtUEN)
    AppCompatEditText edtUEN;
    @BindView(R.id.edtPhoneNumber)
    AppCompatEditText edtPhoneNumber;
    @BindView(R.id.edtEmail)
    AppCompatEditText edtEmail;
    @BindView(R.id.edtPassword)
    AppCompatEditText edtPassword;
    @BindView(R.id.edtRetypePassword)
    AppCompatEditText edtRetypePassword;

    SocialData socialData;
    File filePath = null;

    @BindView(R.id.edtReferralCode)
    AppCompatEditText edtReferralCode;
    @BindView(R.id.edtAddress)
    AppCompatEditText edtAddress;
    @BindView(R.id.edtFlatebedNo)
    AppCompatEditText edtFlatebedNo;
    @BindView(R.id.edtFlatebedModel)
    AppCompatEditText edtFlatebedModel;
    @BindView(R.id.edtFlatebedBrand)
    AppCompatEditText edtFlatebedBrand;
    @BindView(R.id.edtTowtruckdNo)
    AppCompatEditText edtTowtruckdNo;
    @BindView(R.id.edtTowtruckModel)
    AppCompatEditText edtTowtruckModel;
    @BindView(R.id.edtTowtruckBrand)
    AppCompatEditText edtTowtruckBrand;
    @BindView(R.id.cardAddress)
    CardView cardAddress;

    Double latitude = 0.0;
    Double longitude = 0.0;

    List<BrandData> brandList;
    @BindView(R.id.txtTAndC)
    AppCompatTextView txtTAndC;
    @BindView(R.id.txtAddImage)
    AppCompatTextView txtAddImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_driver_signup);
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);

        Places.initialize(getApplicationContext(), getString(R.string.google_maps_key));
        PlacesClient placesClient = Places.createClient(this);

        iniUI();

//        getBrands();
    }

    private void iniUI() {
        setTermsAndCondition();
        setToolbarTitle(getString(R.string.signup));
        Intent intent = getIntent();

        if (intent != null)
            socialData = (SocialData) intent.getSerializableExtra(AppConstant.social_data);

        if (socialData.getLoginType() != LoginType.NORMAL) {
            edtPassword.setVisibility(View.GONE);
            edtRetypePassword.setVisibility(View.GONE);

            edtName.setText(socialData.getName());
            edtEmail.setText(socialData.getEmail());

            Glide.with(this)
                    .load(socialData.getProfilePicUrl())
                    .transform(new CropCircleTransformation())
                    .into(imgProfilePic);

            txtAddImage.setVisibility(View.GONE);

            Glide.with(this).asBitmap().load(socialData.getProfilePicUrl()).into(new CustomTarget<Bitmap>() {

                @Override
                public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                    filePath = AppHelper.saveToInternalStorage(DriverSignupActivity.this, resource);
                }

                @Override
                public void onLoadCleared(@Nullable Drawable placeholder) {

                }

            });

        }
    }

    private void setTermsAndCondition() {
        ConfigDetails configDetails = AppHelper.getInstance().getConfigDetails();
//        SpannableString spannableString = new SpannableString("Click here for more.");
        String text = "By proceeding, you agree to Terms and Conditions and acknowledge that you have read the Privacy Policy.";
        SpannableString spannableString = new SpannableString(text);
//        String url = AppConstant.TERMS_CONDITION;
        String url = configDetails.getTermsAndCondition();
        spannableString.setSpan(new URLSpan(url), 28, 48, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//        String url2 = AppConstant.PRIVACY_POLICY;
        String url2 = configDetails.getPrivacyPolicy();
        spannableString.setSpan(new URLSpan(url2), 88, text.length() - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        txtTAndC.setText(spannableString);
        txtTAndC.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick({R.id.imgProfilePic, R.id.btnRegister, R.id.cardAddress, R.id.edtAddress, R.id.edtTowtruckBrand, R.id.edtFlatebedBrand})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgProfilePic:
                if (isReadStorageAllowed(PICK_IMAGE))
                    pickImage();
                break;
            case R.id.btnRegister:
//                FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(instanceIdResult -> register(filePath, instanceIdResult.getToken(), socialData));
                register(filePath, "", socialData);
                /*if (isValidate())
                    openOTPActivity();*/
                break;
            case R.id.cardAddress:
            case R.id.edtAddress:
                openPlacePicker();
                break;
            case R.id.edtTowtruckBrand:
                if (brandList != null)
                    openBrandSelectionDialog(getString(R.string.select_brand), 1);
                break;
            case R.id.edtFlatebedBrand:
                if (brandList != null)
                    openBrandSelectionDialog(getString(R.string.select_brand), 2);
                break;
        }
    }

    private void openBrandSelectionDialog(String header, int which) {
        List<GenericBottomModel> modelList = new ArrayList<>();
        for (int i = 0; i < brandList.size(); i++) {
            GenericBottomModel model = new GenericBottomModel();
            model.setId(brandList.get(i).getId() + "");
            model.setItemText(brandList.get(i).getBrand());
            modelList.add(model);
        }
        openBottomSheet(header, modelList, new GenericBottomSheetDialog.RecyclerItemClick() {
            @Override
            public void onItemClick(GenericBottomModel genericBottomModel) {
                if (which == 1)
                    edtTowtruckBrand.setText(genericBottomModel.getItemText());
                else
                    edtFlatebedBrand.setText(genericBottomModel.getItemText());
            }
        });
    }

    private void pickImage() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);

    }

    private boolean isReadStorageAllowed(int permissionCode) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, permissionCode);
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == PICK_IMAGE)
                pickImage();
        } else {
            showSnackBar(imgProfilePic, getResources().getString(R.string.you_have_to_allow_storage_permission));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE) {
            if (resultCode == RESULT_OK) {
                if (data.getData() != null) {
                    Bitmap bitmap = ImagePicker.getImageFromResult(this, resultCode, data);
                    if (bitmap != null) {
                        filePath = AppHelper.saveToInternalStorage(this, bitmap);
                        AppHelper.loadImage(this, imgProfilePic, R.drawable.ic_user_placeholder, filePath.getPath());

                        txtAddImage.setVisibility(View.GONE);

                    }
                }
            }
        } else if (requestCode == PLACE_PICKER) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                latitude = place.getLatLng().latitude;
                longitude = place.getLatLng().longitude;
                edtAddress.setText(place.getAddress());

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.e("Place error", status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    private boolean isValidate() {

        if (AppValidation.isEmptyFieldValidate(edtName.getText().toString().trim())) {
            showSnackBar(edtName, getString(R.string.please_enter_name));
            return false;
        }
        if (AppValidation.isEmptyFieldValidate(edtCompanyName.getText().toString().trim())) {
            showSnackBar(edtCompanyName, getString(R.string.please_enter_company_name));
            return false;
        }
      /*  if (AppValidation.isEmptyFieldValidate(edtVehicleNo.getText().toString().trim())) {
            showSnackBar(edtVehicleNo, getString(R.string.please_enter_vehicle_no));
            return false;
        }*/
        if (AppValidation.isEmptyFieldValidate(edtUEN.getText().toString().trim())) {
            showSnackBar(edtUEN, getString(R.string.please_enter_uen));
            return false;
        }
        if (AppValidation.isEmptyFieldValidate(edtPhoneNumber.getText().toString().trim())) {
            showSnackBar(edtPhoneNumber, getString(R.string.please_enter_phone_number));
            return false;
        }
        if (AppValidation.isEmptyFieldValidate(edtEmail.getText().toString().trim())) {
            showSnackBar(edtEmail, getString(R.string.please_enter_email_address));
            return false;
        }
        if (!AppValidation.isEmailValidate(edtEmail.getText().toString().trim())) {
            showSnackBar(edtEmail, getString(R.string.please_enter_valid_email));
            return false;
        }
        if (socialData.getLoginType() == LoginType.NORMAL) {
            if (AppValidation.isEmptyFieldValidate(edtPassword.getText().toString().trim())) {
                showSnackBar(edtPassword, getString(R.string.please_enter_password));
                return false;
            }
            if (AppValidation.isEmptyFieldValidate(edtRetypePassword.getText().toString().trim())) {
                showSnackBar(edtRetypePassword, getString(R.string.please_retype_your_password));
                return false;
            }
            if (!edtPassword.getText().toString().trim().equals(edtRetypePassword.getText().toString().trim())) {
                showSnackBar(edtRetypePassword, getString(R.string.your_password_and_confirm_password_doesn_t_match));
                return false;
            }
        }
        return true;
    }

    private void register(File fileProfileImage, String token, SocialData socialData) {
        CheckNetworkListener callback = () -> {
            if (isValidate()) {
                showProgressDialog();
                callRegistrationAPI(fileProfileImage, token, socialData);
            }
        };

        if (isNetworkAvailable(btnRegister, callback)) {
            callback.onRetryClick();
        }
    }

    private void callRegistrationAPI(File fileProfileImage, String token, SocialData socialData) {

        MultipartBody.Part profilePic = null;
        if (fileProfileImage != null) {
            RequestBody requestFile =
                    RequestBody.create(
                            MediaType.parse("image/*"),
                            fileProfileImage
                    );
            profilePic = MultipartBody.Part.createFormData(Parameter.user_image, fileProfileImage.getName(), requestFile);
        }

        NetworkCall.getInstance().postRegistrationData(profilePic, getRegistrationParam(token, socialData), new IResponseCallback<UserPojo>() {
            @Override
            public void success(UserPojo data) {
                hideProgressDialog();
                if (data.getCode() == 1) {
                    AppHelper.getInstance().setUserDetails(data.getData());
                    AppHelper.getInstance().setUserToken(data.getToken());
                    if (socialData.getRegisterType().equalsIgnoreCase("0")) {
                        openOTPActivity(Screens.SIGNUP, data.getData());
                    } else {
                        AppHelper.getInstance().setLogin(true);
                        openHomeActivity();
                    }
                } else {
                    showSnackBar(btnRegister, data.getMessage());
                }
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                hideProgressDialog();
                showSnackBar(btnRegister, baseModel.getMessage());
            }

            @Override
            public void onError(Call<UserPojo> responseCall, Throwable T) {
                hideProgressDialog();
                showSnackBar(btnRegister, getString(R.string.error_message));
            }
        });
    }

    public HashMap<String, RequestBody> getRegistrationParam(String device_token, SocialData socialData) {

        HashMap<String, RequestBody> param = new HashMap<>();
        param.put(Parameter.register_type, RequestBody.create(MediaType.parse("text/plain"), socialData.getRegisterType()));
        param.put(Parameter.name, RequestBody.create(MediaType.parse("text/plain"), edtName.getText().toString()));
        param.put(Parameter.company_name, RequestBody.create(MediaType.parse("text/plain"), edtCompanyName.getText().toString()));
        param.put(Parameter.uen, RequestBody.create(MediaType.parse("text/plain"), edtUEN.getText().toString()));
        param.put(Parameter.mobile, RequestBody.create(MediaType.parse("text/plain"), edtPhoneNumber.getText().toString()));
        param.put(Parameter.email, RequestBody.create(MediaType.parse("text/plain"), edtEmail.getText().toString()));
//        param.put(Parameter.user_type, RequestBody.create(MediaType.parse("text/plain"), "1"));
        param.put(Parameter.device_id, RequestBody.create(MediaType.parse("text/plain"), device_token));
//        param.put(Parameter.vehicle_no, RequestBody.create(MediaType.parse("text/plain"), edtVehicleNo.getText().toString()));
        param.put(Parameter.mobile_type, RequestBody.create(MediaType.parse("text/plain"), AppConstant.MOBILE_TYPE));
        if (socialData.getLoginType() == LoginType.NORMAL)
            param.put(Parameter.password, RequestBody.create(MediaType.parse("text/plain"), edtPassword.getText().toString()));
        else
            param.put(Parameter.social_id, RequestBody.create(MediaType.parse("text/plain"), socialData.getSocialId()));
        param.put(Parameter.invite_code, RequestBody.create(MediaType.parse("text/plain"), edtReferralCode.getText().toString().trim()));
        param.put(Parameter.towtruck_vehicleno, RequestBody.create(MediaType.parse("text/plain"), edtTowtruckdNo.getText().toString().trim()));
        param.put(Parameter.towtruck_model, RequestBody.create(MediaType.parse("text/plain"), edtTowtruckModel.getText().toString().trim()));
        param.put(Parameter.towtruck_brand, RequestBody.create(MediaType.parse("text/plain"), edtTowtruckBrand.getText().toString().trim()));
        param.put(Parameter.flatebad_vehicleno, RequestBody.create(MediaType.parse("text/plain"), edtFlatebedNo.getText().toString().trim()));
        param.put(Parameter.flatebad_model, RequestBody.create(MediaType.parse("text/plain"), edtFlatebedModel.getText().toString().trim()));
        param.put(Parameter.flatebad_brand, RequestBody.create(MediaType.parse("text/plain"), edtFlatebedBrand.getText().toString().trim()));
        param.put(Parameter.address, RequestBody.create(MediaType.parse("text/plain"), edtAddress.getText().toString().trim()));
        param.put(Parameter.company_longitude, RequestBody.create(MediaType.parse("text/plain"), String.valueOf(longitude)));
        param.put(Parameter.company_latitude, RequestBody.create(MediaType.parse("text/plain"), String.valueOf(latitude)));
//        param.put(Parameter.country_code, RequestBody.create(MediaType.parse("text/plain"), "91"));
        param.put(Parameter.country_code, RequestBody.create(MediaType.parse("text/plain"), "65"));
        Log.e("param", param.toString() + "...");
        return param;
    }

    private void openPlacePicker() {

        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.PHONE_NUMBER, Place.Field.ADDRESS_COMPONENTS, Place.Field.ADDRESS, Place.Field.LAT_LNG);
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields)
                .build(this);
        startActivityForResult(intent, PLACE_PICKER);
    }

//    private void getBrands() {
//
//        showProgressDialog();
//        NetworkCall.getInstance().getBrands(new IResponseCallback<BrandPojo>() {
//            @Override
//            public void success(BrandPojo data) {
//                hideProgressDialog();
//                if (data.getCode() == 1) {
//                    if (brandList == null)
//                        brandList = new ArrayList<>();
//                    else
//                        brandList.clear();
//
//                    brandList.addAll(data.getData());
//
//                } else
//                    showSnackBar(btnRegister, data.getMessage());
//            }
//
//            @Override
//            public void onFailure(BaseModel baseModel) {
//                hideProgressDialog();
//                showSnackBar(btnRegister, baseModel.getMessage());
//            }
//
//            @Override
//            public void onError(Call<BrandPojo> responseCall, Throwable T) {
//                hideProgressDialog();
//                showSnackBar(btnRegister, getString(R.string.error_message));
//            }
//        });
//
//    }

}