package sg.com.towme.driver.network;



import sg.com.towme.driver.model.BaseModel;

import retrofit2.Call;

public interface IResponseCallback<T> {
    void success(T data);
    void onFailure(BaseModel baseModel);
    void onError(Call<T> responseCall, Throwable T);
}
