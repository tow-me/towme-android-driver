package sg.com.towme.driver.listener;

import android.view.View;

public interface RecyclerViewClickListener {
    void onClick(View view, int position, Object object);
}
