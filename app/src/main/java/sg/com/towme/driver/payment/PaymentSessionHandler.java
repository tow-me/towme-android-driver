package sg.com.towme.driver.payment;

import android.content.Intent;
import android.util.Log;

import androidx.annotation.Nullable;

import com.stripe.android.ApiResultCallback;
import com.stripe.android.CustomerSession;
import com.stripe.android.PaymentIntentResult;
import com.stripe.android.PaymentSession;
import com.stripe.android.PaymentSessionData;
import com.stripe.android.SetupIntentResult;
import com.stripe.android.Stripe;
import com.stripe.android.model.ConfirmPaymentIntentParams;
import com.stripe.android.model.ConfirmSetupIntentParams;
import com.stripe.android.model.PaymentIntent;
import com.stripe.android.model.PaymentMethod;
import com.stripe.android.model.SetupIntent;
import com.stripe.android.model.StripeIntent;
import sg.com.towme.driver.activity.AppNavigationActivity;
import sg.com.towme.driver.model.AbstractCallback;
import sg.com.towme.driver.network.StripeCall;
import sg.com.towme.driver.utility.AppHelper;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Objects;

import okhttp3.ResponseBody;

public class PaymentSessionHandler implements PaymentSession.PaymentSessionListener {
    private static final String STRIPE_RETURN_URL = "payments-towme://stripe-redirect";

    public static final String TYPE_ORDER = "order";
    public static final String TYPE_COMMITION = "order";

    private String destination_stripe_account_id = null;

    private AppNavigationActivity activity;
    private long amount;

    private String clientSecret;
    private String payment_method_id;
    private String type = TYPE_ORDER;
    private String description;
    private String title;
    private String bookingID,userID,driverID;

    private PaymentSession paymentSession;
    private PaymentSessionListener paymentSessionListener;

    private boolean user_initiated = false;

    PaymentSessionHandler(AppNavigationActivity activity) {
        this.activity = activity;
    }

    public void setActivity(AppNavigationActivity activity) {
        this.activity = activity;
    }

    public void setPaymentSessionListener(PaymentSessionListener paymentSessionListener) {
        this.paymentSessionListener = paymentSessionListener;
    }

    public void setDestination_stripe_account_id(String destination_stripe_account_id) {
        this.destination_stripe_account_id = destination_stripe_account_id;
    }

    public void setType(String type) {
        this.type = type;
        this.destination_stripe_account_id = null;
    }

    public void setBookingDetails(String bookingID,String userID,String driverID){
        this.bookingID=bookingID;
        this.userID=userID;
        this.driverID=driverID;
    }
    public void setTitle(String payment_description) {
        this.title = payment_description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void initTransaction(long amount) {
        this.amount = amount;
        StripeUtil.applyTheme(activity);
        activity.showProgressDialog();
        if (paymentSession != null) paymentSession.clearPaymentMethod();
        initPaymentSession();
    }

    private void initPaymentSession() {
        user_initiated = false;
        CustomerSession.initCustomerSession(activity, new CustomerKeyProvider((success, message) -> {
            activity.hideProgressDialog();
            if (success) {
                paymentSession = new PaymentSession(activity, StripeUtil.defaultPaymentSessionConfig());
                paymentSession.init(this);
                paymentSession.clearPaymentMethod();
                paymentSession.setCartTotal(amount);
                paymentSession.presentPaymentMethodSelection("");
            } else {
                activity.openDialog(message);
                paymentSessionListener.onPaymentFailed(message);
                clearSession();
            }
        }));
    }

    private Stripe getStripe() {
        return StripeUtil.getStripe(activity, destination_stripe_account_id);
    }

    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {
        if (intent == null) return;
        if (paymentSession == null) return;

        boolean isPaymentIntentResult = getStripe().onPaymentResult(requestCode, intent, new ApiResultCallback<PaymentIntentResult>() {
            @Override
            public void onSuccess(PaymentIntentResult paymentIntentResult) {
                activity.hideProgressDialog();
                processStripeIntent(paymentIntentResult.getIntent(), true);
            }

            @Override
            public void onError(@NotNull Exception e) {
                e.printStackTrace();
                activity.hideProgressDialog();
                activity.openDialog(e.getMessage());
            }
        });

        if (isPaymentIntentResult) {
            activity.showProgressDialog();
        } else {
            boolean isSetupIntentResult = getStripe().onSetupResult(requestCode, intent, new ApiResultCallback<SetupIntentResult>() {
                @Override
                public void onSuccess(SetupIntentResult setupIntentResult) {
                    activity.hideProgressDialog();
                    processStripeIntent(setupIntentResult.getIntent(), true);
                }

                @Override
                public void onError(@NotNull Exception e) {
                    e.printStackTrace();
                    activity.hideProgressDialog();
                    activity.openDialog(e.getMessage());

                }
            });

            if (!isSetupIntentResult) {
                paymentSession.handlePaymentData(requestCode, resultCode, intent);
            }
        }

    }


    private void processStripeIntent(StripeIntent stripeIntent, boolean isAfterConfirmation) {
        if (stripeIntent == null) return;
        if (stripeIntent.requiresAction()) {
            getStripe().handleNextActionForPayment(activity, Objects.requireNonNull(stripeIntent.getClientSecret()));
        } else if (stripeIntent.requiresConfirmation()) {
            confirmStripeIntent(stripeIntent.getId(), destination_stripe_account_id);
        } else if (stripeIntent.getStatus() == PaymentIntent.Status.Succeeded) {
            if (stripeIntent instanceof PaymentIntent) {
                PaymentIntent paymentIntent = (PaymentIntent) stripeIntent;
                paymentSessionListener.onPaymentSuccess(paymentIntent.getId(), true);
                clearSession();
            } else if (stripeIntent instanceof SetupIntent) {
                SetupIntent setupIntent = (SetupIntent) stripeIntent;
                paymentSessionListener.onPaymentSuccess(setupIntent.getId(), true);
                clearSession();
            }
        } else if (stripeIntent.getStatus() == PaymentIntent.Status.RequiresPaymentMethod) {
            if (isAfterConfirmation) {
                paymentSessionListener.onPaymentFailed("Payment method not authorised or payment has been declined, Please contact your payment bank for more info.");
                clearSession();
            } else {
                if (payment_method_id == null) return;
                if (stripeIntent instanceof PaymentIntent) {
                    getStripe().confirmPayment(activity, ConfirmPaymentIntentParams.createWithPaymentMethodId(payment_method_id, clientSecret));
                } else if (stripeIntent instanceof SetupIntent) {
                    getStripe().confirmSetupIntent(activity, ConfirmSetupIntentParams.create(payment_method_id, clientSecret));
                }
            }
        } else if (stripeIntent.getStatus() == PaymentIntent.Status.RequiresCapture) {
            paymentSessionListener.onPaymentSuccess(stripeIntent.getId(), false);
            clearSession();
            // We suppose to capture the payment, It will be kept reserved for maximum 7 days..
            // Capture intent will be done in end ride api.
        } else {
            activity.openDialog("Unhandled payment intent, Status - " + stripeIntent.getStatus().toString());
        }
    }


    private void confirmStripeIntent(String payment_intent_id, String stripeAccountId) {
        HashMap<String, Object> param = new HashMap<>();
        param.put("intent_id", payment_intent_id);
        param.put("payment_method", payment_method_id);
        param.put("destination", destination_stripe_account_id);

//        if (!TextUtil.isNullOrEmpty(stripeAccountId)) param.put("destination", stripeAccountId);

        System.out.println("Brij data: " + param);
        activity.showProgressDialog();
        StripeCall.getInstance().confirmIntent(param, new AbstractCallback<ResponseBody>() {
            @Override
            public void result(ResponseBody result) {
                activity.hideProgressDialog();
                try {
                    JSONObject response = new JSONObject(result.string());
                    if (response.has("success")) {
                        boolean success = response.getBoolean("success");
                        if (success) {
                            paymentSessionListener.onPaymentSuccess(payment_intent_id, true);
                            clearSession();
                        } else {
                            activity.openDialog("Payment failed");
                            paymentSessionListener.onPaymentFailed("Payment failed;");
                            clearSession();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void clearSession() {
        if (paymentSession == null) return;
        paymentSession.clearPaymentMethod();
        paymentSession = null;
    }

    @Override
    public void onCommunicatingStateChanged(boolean isCommunicating) {
        Log.e("PaymentSession", "onCommunicatingStateChanged : " + isCommunicating);

    }

    @Override
    public void onError(int errorCode, @NotNull String errorMessage) {
        Log.e("PaymentSession", "onError : " + errorMessage);
        activity.openDialog(errorMessage);
        paymentSessionListener.onPaymentFailed(errorMessage);
        clearSession();

    }

    @Override
    public void onPaymentSessionDataChanged(@NotNull PaymentSessionData data) {
        Log.e("PaymentSession", "onPaymentSessionDataChanged : " + data.toString());
        PaymentMethod paymentMethod = data.getPaymentMethod();
        if (paymentMethod == null || paymentMethod.id == null) return;
        if (!data.isPaymentReadyToCharge()) return;

        if (user_initiated) return;
        user_initiated = true;

        activity.showProgressDialog();

        payment_method_id = paymentMethod.id;
        HashMap<String, Object> param = new HashMap<>();

        param.put("customer", AppHelper.getStripeId());
        param.put("booking_id", bookingID);
        param.put("purchase_type", "commission");
        param.put("user_id", userID);
        param.put("amount", String.valueOf(amount));
        param.put("driver_id", driverID);
        param.put("destination", destination_stripe_account_id);
        param.put("user_type", "driver");
        param.put("currency", "SGD");
        param.put("source", payment_method_id);
        param.put("commission", String.valueOf(AppHelper.getInstance().getConfigDetails().getTowmeApplicationCommission()));
        param.put("payment_method_types[]", "card");
        param.put("statement_descriptor_suffix", title);
        param.put("description", description);


//      param.put("payment_method_types[]", "card");

        StripeCall.getInstance().paymentIntent(param, new AbstractCallback<ResponseBody>() {
            @Override
            public void result(ResponseBody result) {
//                activity.hideProgressDialog();
                try {
                    String response = result.string();
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.has("client_secret")) {
                        clientSecret = jsonObject.getString("client_secret");
                        payment_method_id = jsonObject.getString("payment_method");
                        getStripe().confirmPayment(activity, ConfirmPaymentIntentParams.createWithPaymentMethodId(payment_method_id, clientSecret, STRIPE_RETURN_URL));
                    } else {
                        activity.hideProgressDialog();
                        String message = jsonObject.getJSONObject("raw").getString("message");
                        activity.openDialog(message);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    activity.hideProgressDialog();
                }

            }
        });
    }

    private StripeIntent retrieveStripeIntent(String clientSecret) {
        StripeIntent stripeIntent = null;
        try {
            if (clientSecret.startsWith("pi_")) {
                stripeIntent = getStripe().retrievePaymentIntentSynchronous(clientSecret);
            } else if (clientSecret.startsWith("seti_")) {
                stripeIntent = getStripe().retrieveSetupIntentSynchronous(clientSecret);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stripeIntent;
    }

    private void finishPayment() {
        paymentSession.onCompleted();
        paymentSession.clearPaymentMethod();
        paymentSession = null;
    }

    private void finishSetup() {
        paymentSession.onCompleted();
        paymentSession.clearPaymentMethod();
        paymentSession = null;
    }

    public interface PaymentSessionListener {
        void onPaymentSuccess(String payment_intent_id, boolean captured);

        void onPaymentFailed(String message);
    }
}
