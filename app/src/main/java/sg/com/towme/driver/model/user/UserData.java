
package sg.com.towme.driver.model.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserData implements Serializable {

    @SerializedName("userid")
    @Expose
    private Integer userid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("user_image")
    @Expose
    private String userImage;
    @SerializedName("user_type")
    @Expose
    private String type;
    @SerializedName("country_code")
    @Expose
    private String countryCode;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("push_notification_subscribe")
    @Expose
    private String pushNotificationSubscribe;
    @SerializedName("email_subscribe")
    @Expose
    private String emailSubscribe;
    @SerializedName("sms_subscribe")
    @Expose
    private String smsSubscribe;
    @SerializedName("towtruck_vehicleno")
    @Expose
    private String towtruckVehicleno;
    @SerializedName("towtruck_model")
    @Expose
    private String towtruckModel;
    @SerializedName("towtruck_brand")
    @Expose
    private String towtruckBrand;
    @SerializedName("flatebad_vehicleno")
    @Expose
    private String flatebadVehicleno;
    @SerializedName("flatebad_model")
    @Expose
    private String flatebadModel;
    @SerializedName("flatebad_brand")
    @Expose
    private String flatebadBrand;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("online_offline_status")
    @Expose
    private String onlineOfflineStatus;
    @SerializedName("stripe_customer_id")
    @Expose
    private String stripe_customer_id;
    @SerializedName("stripe_account_id")
    @Expose
    private String stripe_account_id;

    @SerializedName("mobile_v_code")
    @Expose
    private String mobileVCode;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("company_code")
    @Expose
    private String companyCode;
    @SerializedName("uen")
    @Expose
    private String uen;
    @SerializedName("company_latitude")
    @Expose
    private String companyLatitude;
    @SerializedName("company_longitude")
    @Expose
    private String companyLongitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("latitude")
    @Expose
    private String latitude;

    @SerializedName("invite_code")
    @Expose
    private String inviteCode;
    @SerializedName("balance")
    @Expose
    private String balance;
    @SerializedName("customer_id")
    @Expose
    private String customer_id;
    @SerializedName("is_blocked")
    @Expose
    private Integer isBlocked;
    @SerializedName("commission")
    @Expose
    private Double commission;

    public Integer getIsBlocked() {
        return isBlocked;
    }

    public void setIsBlocked(Integer isBlocked) {
        this.isBlocked = isBlocked;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getInviteCode() {
        return inviteCode;
    }

    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getUen() {
        return uen;
    }

    public void setUen(String uen) {
        this.uen = uen;
    }

    public String getCompanyLatitude() {
        return companyLatitude;
    }

    public void setCompanyLatitude(String companyLatitude) {
        this.companyLatitude = companyLatitude;
    }

    public String getCompanyLongitude() {
        return companyLongitude;
    }

    public void setCompanyLongitude(String companyLongitude) {
        this.companyLongitude = companyLongitude;
    }

    public String getMobileVCode() {
        return mobileVCode;
    }

    public void setMobileVCode(String mobileVCode) {
        this.mobileVCode = mobileVCode;
    }


    public String getOnlineOfflineStatus() {
        return onlineOfflineStatus;
    }

    public void setOnlineOfflineStatus(String onlineOfflineStatus) {
        this.onlineOfflineStatus = onlineOfflineStatus;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPushNotificationSubscribe() {
        return pushNotificationSubscribe;
    }

    public void setPushNotificationSubscribe(String pushNotificationSubscribe) {
        this.pushNotificationSubscribe = pushNotificationSubscribe;
    }

    public String getEmailSubscribe() {
        return emailSubscribe;
    }

    public void setEmailSubscribe(String emailSubscribe) {
        this.emailSubscribe = emailSubscribe;
    }

    public String getSmsSubscribe() {
        return smsSubscribe;
    }

    public void setSmsSubscribe(String smsSubscribe) {
        this.smsSubscribe = smsSubscribe;
    }

    public String getTowtruckVehicleno() {
        return towtruckVehicleno;
    }

    public void setTowtruckVehicleno(String towtruckVehicleno) {
        this.towtruckVehicleno = towtruckVehicleno;
    }

    public String getTowtruckModel() {
        return towtruckModel;
    }

    public void setTowtruckModel(String towtruckModel) {
        this.towtruckModel = towtruckModel;
    }

    public String getTowtruckBrand() {
        return towtruckBrand;
    }

    public void setTowtruckBrand(String towtruckBrand) {
        this.towtruckBrand = towtruckBrand;
    }

    public String getFlatebadVehicleno() {
        return flatebadVehicleno;
    }

    public void setFlatebadVehicleno(String flatebadVehicleno) {
        this.flatebadVehicleno = flatebadVehicleno;
    }

    public String getFlatebadModel() {
        return flatebadModel;
    }

    public void setFlatebadModel(String flatebadModel) {
        this.flatebadModel = flatebadModel;
    }

    public String getFlatebadBrand() {
        return flatebadBrand;
    }

    public void setFlatebadBrand(String flatebadBrand) {
        this.flatebadBrand = flatebadBrand;
    }


    public String getStripe_customer_id() {
        return stripe_customer_id;
    }

    public void setStripe_customer_id(String stripe_customer_id) {
        this.stripe_customer_id = stripe_customer_id;
    }

    public String getStripe_account_id() {
        return stripe_account_id;
    }

    public void setStripe_account_id(String stripe_account_id) {
        this.stripe_account_id = stripe_account_id;
    }

    public boolean hasStripeAccountId() {
        return stripe_account_id != null && !stripe_account_id.isEmpty();
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public Double getCommission() {
        return commission;
    }

    public void setCommission(Double commission) {
        this.commission = commission;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
