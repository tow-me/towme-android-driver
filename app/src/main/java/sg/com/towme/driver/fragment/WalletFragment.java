package sg.com.towme.driver.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import sg.com.towme.driver.R;
import sg.com.towme.driver.adapter.CardDetailsAdapter;
import sg.com.towme.driver.listener.iDateTimePickerClick;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class WalletFragment extends BaseFragment {

    @BindView(R.id.txtYourCoupons)
    AppCompatTextView txtYourCoupons;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.edtCardNumber)
    AppCompatEditText edtCardNumber;
    @BindView(R.id.edtCvv)
    AppCompatEditText edtCvv;
    @BindView(R.id.txtExpiryDate)
    AppCompatTextView txtExpiryDate;
    @BindView(R.id.edtCardType)
    AppCompatEditText edtCardType;
    @BindView(R.id.btnSaveCardDetails)
    AppCompatButton btnSaveCardDetails;

    CardDetailsAdapter cardDetailsAdapter;

    public static WalletFragment newInstance() {
        WalletFragment fragment = new WalletFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_wallet, container, false);
        ButterKnife.bind(this, view);
        initUI();
        setAdapter();
        return view;
    }

    private void initUI() {
    }

    private void setAdapter() {
        recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        cardDetailsAdapter = new CardDetailsAdapter(getActivity());
        recyclerview.setAdapter(cardDetailsAdapter);
    }

    @OnClick({R.id.txtYourCoupons, R.id.txtExpiryDate, R.id.edtCardType, R.id.btnSaveCardDetails})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtYourCoupons:
                homeActivity.openCouponsActivity();
                break;
            case R.id.txtExpiryDate:
                iDateTimePickerClick iDateTimePickerClick = new iDateTimePickerClick() {
                    @Override
                    public void onPickDate(String dateTime) {
                        txtExpiryDate.setText(dateTime);
                    }

                    @Override
                    public void onPickDateTime(String s, String apiDataTimeFormat) {

                    }
                };
                homeActivity.datePicker(iDateTimePickerClick);
                break;
            case R.id.edtCardType:
                break;
            case R.id.btnSaveCardDetails:
                break;
        }
    }
}
