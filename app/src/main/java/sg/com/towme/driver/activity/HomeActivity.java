package sg.com.towme.driver.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import io.socket.client.Ack;
import io.socket.client.Socket;
import sg.com.towme.driver.Controller;
import sg.com.towme.driver.R;
import sg.com.towme.driver.adapter.VPagerAdapter;
import sg.com.towme.driver.constant.AppConstant;
import sg.com.towme.driver.enumeration.ChatType;
import sg.com.towme.driver.fragment.AccountFragment;
import sg.com.towme.driver.fragment.BaseFragment;
import sg.com.towme.driver.fragment.CalenderFragmentTwo;
import sg.com.towme.driver.fragment.MapFragment;
import sg.com.towme.driver.fragment.MessageFragment;
import sg.com.towme.driver.fragment.WalletMoneyFragment;
import sg.com.towme.driver.listener.CheckNetworkListener;
import sg.com.towme.driver.listener.iGPSEnableListener;
import sg.com.towme.driver.listener.iPageActive;
import sg.com.towme.driver.model.BaseModel;
import sg.com.towme.driver.model.messages.MessagesPojo;
import sg.com.towme.driver.model.user.UserData;
import sg.com.towme.driver.model.user.UserPojo;
import sg.com.towme.driver.network.IResponseCallback;
import sg.com.towme.driver.network.NetworkCall;
import sg.com.towme.driver.service.DataWorker;
import sg.com.towme.driver.socket.SocketConstant;
import sg.com.towme.driver.socket.SocketIOClient;
import sg.com.towme.driver.utility.AppHelper;
import sg.com.towme.driver.utility.PreferanceHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class HomeActivity extends ToolBarActivity implements BottomNavigationView.OnNavigationItemSelectedListener, iGPSEnableListener {

    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @BindView(R.id.bottomNavigation)
    BottomNavigationView bottomNavigation;
    public static final String EXTRA_SENDER_ID = "EXTRA_SENDER_ID";
    public static final String EXTRA_TYPE = "EXTRA_TYPE";
    public static final String EXTRA_BOOKING_ID = "EXTRA_BOOKING_ID";
    public static final String EXTRA_NOTIFICATION_ID = "EXTRA_NOTIFICATION_ID";

    private VPagerAdapter vPagerAdapter;
    private List<BaseFragment> fragmentList;
    private static final int LOCATION_PERMISSION_CODE = 11;
    Socket mSocket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);

        clearNotification();

        mSocket = SocketIOClient.getInstance();

        if (isLocationAllowed(LOCATION_PERMISSION_CODE))
            enablegps(HomeActivity.this, this);

        initUI();

//        callGetProfileApi();
        bottomNavigation.setOnNavigationItemSelectedListener(this);
//        setupViewPagerFragment();
//        callGetProfileApi();

    }

    public void startDataWorker() {
        WorkManager.getInstance(Controller.getInstance()).cancelAllWork();
        OneTimeWorkRequest refreshCpnWork = new OneTimeWorkRequest.Builder(DataWorker.class)
                .setInitialDelay(30, TimeUnit.SECONDS).build();
        WorkManager.getInstance(Controller.getInstance()).enqueue(refreshCpnWork);

    }

    private void initUI() {
        hideHomeIcon();
        setToolbarTitle(getString(R.string.home));
    }

    private void setupViewPagerFragment() {
        if (vPagerAdapter == null) {
            fragmentList = getFragmentList();
            vPagerAdapter = new VPagerAdapter(getSupportFragmentManager(), fragmentList);
        }

        viewpager.setAdapter(vPagerAdapter);
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setToolbarTitle(position);
                bottomNavigation.getMenu().getItem(position).setChecked(true);
                iPageActive currentPage = (iPageActive) fragmentList.get(position);
                currentPage.onPageActive(getString(R.string.app_name));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        if (getIntent().getStringExtra(EXTRA_TYPE) != null) {
            String type = getIntent().getStringExtra(EXTRA_TYPE);
            if (type.equals("notification")) {
                int notifId = Integer.parseInt(getIntent().getStringExtra(EXTRA_NOTIFICATION_ID));
                openNotificationActivity(notifId);
                viewpager.post(new Runnable() {
                    @Override
                    public void run() {
                        viewpager.setCurrentItem(4);
                    }
                });
            } else if (type.equals("chat")) {
                int senderId = Integer.parseInt(getIntent().getStringExtra(EXTRA_SENDER_ID));
                if (senderId != 0) {
                    getBookingList(senderId);
                }
            }
        }
    }

    private void getBookingList(final int senderId) {
        showProgressDialog();
        JSONObject object = new JSONObject();
        try {
            object.put(SocketConstant.USER_ID, AppHelper.getInstance().getUserDetails().getUserid());
            object.put(SocketConstant.user_type, AppConstant.USER_TYPE);
            if (AppHelper.getInstance().getUserToken() != null) {
                object.put(SocketConstant.TOKEN, AppHelper.getInstance().getUserToken());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("Booking list param", object.toString() + "...");
        mSocket.emit(SocketConstant.GET_CHAT_LIST_BOOKINGS, object, new Ack() {
            @Override
            public void call(Object... args) {
                int code = 400;

                JSONObject obj = null;
                try {
                    obj = new JSONObject(args[0].toString());
                    code = obj.getInt("status_code");

                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (code == 401) {
                    try {
                        if (getApplicationContext() != null) {
                            PreferanceHelper.getInstance().clearPreference();
                            WorkManager.getInstance(Controller.getInstance()).cancelAllWork();
                            openLoginActivity();
                        }
                    } catch (Exception e) {}
                } else {
                    Log.e("Booking list response", args[0].toString());

               /* Gson gson = new Gson();

                Type collectionType = new TypeToken<ArrayList<CreateBooking>>() {
                }.getType();
                List<CreateBooking> list = gson.fromJson(args[0].toString(), collectionType);*/

                    Gson gson = new Gson();
                    MessagesPojo messagesPojo = gson.fromJson(args[0].toString(), MessagesPojo.class);
                    runOnUiThread(() -> {
                        hideProgressDialog();
                        if (messagesPojo.getData().getCurrentRides().size() > 0) {
                            for (int i = 0; i < messagesPojo.getData().getCurrentRides().size(); i++) {
                                if (messagesPojo.getData().getCurrentRides().get(i).getUser().getUserid() == senderId) {
                                    String startedImages = "";
                                    String stoppedImages = "";
                                    if (messagesPojo.getData().getCurrentRides().get(i).getStartedImages() != null && messagesPojo.getData().getCurrentRides().get(i).getStartedImages().size() > 0)
                                        startedImages = messagesPojo.getData().getCurrentRides().get(i).getStartedImages().get(0);
                                    if (messagesPojo.getData().getCurrentRides().get(i).getStoppedImages() != null && messagesPojo.getData().getCurrentRides().get(i).getStoppedImages().size() > 0)
                                        stoppedImages = messagesPojo.getData().getCurrentRides().get(i).getStoppedImages().get(0);
                                    openChatActivity(messagesPojo.getData().getCurrentRides().get(i).getBookingId(), startedImages, stoppedImages, ChatType.CURRENT, messagesPojo.getData().getCurrentRides().get(i).getDriver(), AppConstant.REQUEST_CHAT);
                                    viewpager.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            viewpager.setCurrentItem(2);
                                        }
                                    });
                                    break;
                                }
                            }
                        } else if (messagesPojo.getData().getPreviousRides().size() > 0) {
                            for (int i = 0; i < messagesPojo.getData().getPreviousRides().size(); i++) {
                                if (messagesPojo.getData().getPreviousRides().get(i).getUser().getUserid() == senderId) {
                                    String startedImages = "";
                                    String stoppedImages = "";
                                    if (messagesPojo.getData().getPreviousRides().get(i).getStartedImages() != null && messagesPojo.getData().getPreviousRides().get(i).getStartedImages().size() > 0)
                                        startedImages = messagesPojo.getData().getPreviousRides().get(i).getStartedImages().get(0);
                                    if (messagesPojo.getData().getPreviousRides().get(i).getStoppedImages() != null && messagesPojo.getData().getPreviousRides().get(i).getStoppedImages().size() > 0)
                                        stoppedImages = messagesPojo.getData().getPreviousRides().get(i).getStoppedImages().get(0);
                                    openChatActivity(messagesPojo.getData().getPreviousRides().get(i).getBookingId(), startedImages, stoppedImages, ChatType.PREVIOUS, messagesPojo.getData().getPreviousRides().get(i).getDriver(), AppConstant.REQUEST_CHAT);
                                    viewpager.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            viewpager.setCurrentItem(2);
                                        }
                                    });
                                    break;
                                }
                            }
                        }
                    });
                }
            }
        });
    }

    private List<BaseFragment> getFragmentList() {
        List<BaseFragment> fragments = new ArrayList<>();
        fragments.add(MapFragment.newInstance());
        fragments.add(CalenderFragmentTwo.newInstance());
        fragments.add(MessageFragment.newInstance());
        fragments.add(WalletMoneyFragment.newInstance());
        fragments.add(AccountFragment.newInstance());

        return fragments;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        setToolbarTitle(menuItem.getGroupId());
        switch (menuItem.getItemId()) {
            case R.id.actionHome:

                viewpager.setCurrentItem(0);
                break;
            case R.id.actionSteeringWheel:
                viewpager.setCurrentItem(1);
                break;
            case R.id.actionMessages:
                viewpager.setCurrentItem(2);
                break;
            case R.id.actionWallet:
                viewpager.setCurrentItem(3);
                break;
            case R.id.actionProfile:
                viewpager.setCurrentItem(4);
                break;
        }

        return true;
    }

    private void setToolbarTitle(int position) {
        switch (position) {
            case 0:
                setToolbarTitle(getString(R.string.home));
                break;
            case 1:
                setToolbarTitle(getString(R.string.calendar));
                break;
            case 2:
                setToolbarTitle(getString(R.string.messages));
                break;
            case 3:
                setToolbarTitle(getString(R.string.wallet));
                break;
            case 4:
                setToolbarTitle(getString(R.string.profile));
                break;
        }

    }

    public void selectVehicle() {
        viewpager.setCurrentItem(1);
    }

    @Override
    public void onGPSEnableSuccess() {
        Log.e("onGPSEnableSuccess", "...");
//        startDataWorker();
        setupViewPagerFragment();
    }

    @Override
    public void onGPSEnableCancel() {
        Log.e("onGPSEnableCancel", "...");
        enablegps(HomeActivity.this, this);
    }

    private boolean isLocationAllowed(int permissionCode) {
        if (ContextCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, permissionCode);
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == LOCATION_PERMISSION_CODE)
                enablegps(HomeActivity.this, this);
        } else {
            showSnackBar(txtToolbarTitle, getResources().getString(R.string.you_have_to_allow_location_permission));
            if (isLocationAllowed(LOCATION_PERMISSION_CODE)) ;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("Home activity", "onActivityResult :" + requestCode);
        if (requestCode == AppConstant.REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case RESULT_OK:
                    Log.e("Gps enabled success", "..");
                    //Success Perform Task Here
//                    startDataWorker();
                    onGPSEnableSuccess();
                    break;
                case RESULT_CANCELED:
                    onGPSEnableCancel();
                    Log.e("GPS", "User denied to access location");
                    break;
            }
        } else if (requestCode == AppConstant.REQUEST_CHAT) {
            Log.e("Request code Chat", requestCode + "...");

            MessageFragment mapFragment = (MessageFragment) fragmentList.get(2);
            mapFragment.onActivityResult(requestCode, resultCode, data);
        } else {
            Log.e("Request code payment", requestCode + "...");
            MapFragment mapFragment = (MapFragment) fragmentList.get(0);
            mapFragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void callGetProfileApi() {
        NetworkCall.getInstance().userProfile(new IResponseCallback<UserPojo>() {
            @Override
            public void success(UserPojo data) {
                hideProgressDialog();
                AppHelper.getInstance().setUserDetails(data.getData());

                UserData userData = data.getData();
                if (userData.getIsBlocked() == 1) {
                    CheckNetworkListener callback1 = () -> {
                        callLogoutApi();
                    };
                    if (isNetworkAvailable(viewpager, callback1)) {
                        callback1.onRetryClick();
                    }
                }

               /* UserData userData = data.getData();
                if (!userData.hasStripeAccountId()) {
                    startActivity(new Intent(HomeActivity.this, PaymentSetupActivity.class));
                }*/

            }

            @Override
            public void onFailure(BaseModel baseModel) {
                if (baseModel.getCode() == 0) {
                    PreferanceHelper.getInstance().clearPreference();
                    WorkManager.getInstance(Controller.getInstance()).cancelAllWork();
                    openLoginActivity();
                }
            }

            @Override
            public void onError(Call<UserPojo> responseCall, Throwable T) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        hideKeyBoard();
    }

    @Override
    protected void onResume() {
        super.onResume();

        CheckNetworkListener callback = () -> {
            callGetProfileApi();
        };

        if (isNetworkAvailable(viewpager, callback)) {
            callback.onRetryClick();
        }

    }

    private void callLogoutApi() {

        NetworkCall.getInstance().logout(new IResponseCallback<BaseModel>() {
            @Override
            public void success(BaseModel data) {
                hideProgressDialog();
                if (data.getCode() == 1) {
                    PreferanceHelper.getInstance().clearPreference();
                    WorkManager.getInstance(Controller.getInstance()).cancelAllWork();
                    openLoginActivity();

                } else {
                    showSnackBar(viewpager, data.getMessage());
                }
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                hideProgressDialog();
                showSnackBar(viewpager, baseModel.getMessage());
            }

            @Override
            public void onError(Call<BaseModel> responseCall, Throwable T) {
                hideProgressDialog();
                showSnackBar(viewpager, getString(R.string.error_message));
            }
        });

    }
}