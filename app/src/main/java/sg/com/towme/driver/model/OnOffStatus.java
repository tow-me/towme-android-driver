
package sg.com.towme.driver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OnOffStatus {

    @SerializedName("driver_online_offline_id")
    @Expose
    private Integer driverOnlineOfflineId;
    @SerializedName("userid")
    @Expose
    private Integer userid;
    @SerializedName("service_id")
    @Expose
    private Integer serviceId;
    @SerializedName("online_offline_status")
    @Expose
    private Integer onlineOfflineStatus;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getDriverOnlineOfflineId() {
        return driverOnlineOfflineId;
    }

    public void setDriverOnlineOfflineId(Integer driverOnlineOfflineId) {
        this.driverOnlineOfflineId = driverOnlineOfflineId;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getServiceId() {
        return serviceId;
    }

    public void setServiceId(Integer serviceId) {
        this.serviceId = serviceId;
    }

    public Integer getOnlineOfflineStatus() {
        return onlineOfflineStatus;
    }

    public void setOnlineOfflineStatus(Integer onlineOfflineStatus) {
        this.onlineOfflineStatus = onlineOfflineStatus;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
