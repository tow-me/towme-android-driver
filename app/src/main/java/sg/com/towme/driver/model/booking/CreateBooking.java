
package sg.com.towme.driver.model.booking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CreateBooking extends BaseBooking {

    @SerializedName("started_images")
    @Expose
    private List<Object> startedImages = null;
    @SerializedName("stopped_images")
    @Expose
    private List<Object> stoppedImages = null;
    @SerializedName("start_ride_description")
    @Expose
    private Object startRideDescription;
    @SerializedName("end_ride_description")
    @Expose
    private Object endRideDescription;
    @SerializedName("end_ride_extra_charge")
    @Expose
    private Object endRideExtraCharge;
    @SerializedName("rating")
    @Expose
    private Object rating;
    @SerializedName("comment")
    @Expose
    private Object comment;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("driver")
    @Expose
    private Object driver = null;
    @SerializedName("tow_price")
    @Expose
    private String towPrice;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("tow_price_type")
    @Expose
    private String towPriceType;

    public String getTowPriceType() {
        return towPriceType;
    }

    public void setTowPriceType(String towPriceType) {
        this.towPriceType = towPriceType;
    }

    public String getTowPrice() {
        return towPrice;
    }

    public void setTowPrice(String towPrice) {
        this.towPrice = towPrice;
    }
    public List<Object> getStartedImages() {
        return startedImages;
    }

    public void setStartedImages(List<Object> startedImages) {
        this.startedImages = startedImages;
    }

    public List<Object> getStoppedImages() {
        return stoppedImages;
    }

    public void setStoppedImages(List<Object> stoppedImages) {
        this.stoppedImages = stoppedImages;
    }

    public Object getStartRideDescription() {
        return startRideDescription;
    }

    public void setStartRideDescription(Object startRideDescription) {
        this.startRideDescription = startRideDescription;
    }

    public Object getEndRideDescription() {
        return endRideDescription;
    }

    public void setEndRideDescription(Object endRideDescription) {
        this.endRideDescription = endRideDescription;
    }

    public Object getEndRideExtraCharge() {
        return endRideExtraCharge;
    }

    public void setEndRideExtraCharge(Object endRideExtraCharge) {
        this.endRideExtraCharge = endRideExtraCharge;
    }

    public Object getRating() {
        return rating;
    }

    public void setRating(Object rating) {
        this.rating = rating;
    }

    public Object getComment() {
        return comment;
    }

    public void setComment(Object comment) {
        this.comment = comment;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Object getDriver() {
        return driver;
    }

    public void setDriver(Object driver) {
        this.driver = driver;
    }

    public long getChargeableAmount() {
//        return (long) ((parseDouble(getTotalCost()) - parseDouble(getDiscountedAmount())) * 100);
        return (long) ((parseDouble(getTotalCost())) * 100);
    }

    private static double parseDouble(String raw) {
        try {
            return Double.parseDouble(raw);
        } catch (Exception e) {
            return 0;
        }
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
