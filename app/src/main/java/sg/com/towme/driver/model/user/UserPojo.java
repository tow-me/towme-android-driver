
package sg.com.towme.driver.model.user;

import sg.com.towme.driver.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserPojo extends BaseModel {


    @SerializedName("token")
    @Expose
    private String token;

    @SerializedName("data")
    @Expose
    private UserData data;
    @SerializedName("is_registred")
    @Expose
    private Boolean isRegistred;
    @SerializedName("is_verify")
    @Expose
    private Boolean isVerify;

    public Boolean getRegistred() {
        return isRegistred;
    }

    public void setRegistred(Boolean registred) {
        isRegistred = registred;
    }

    public Boolean getVerify() {
        return isVerify;
    }

    public void setVerify(Boolean verify) {
        isVerify = verify;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserData getData() {
        return data;
    }

    public void setData(UserData data) {
        this.data = data;
    }

}
