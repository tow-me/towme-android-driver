package sg.com.towme.driver.utility;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class DateTimeUtil {

    private static final SimpleDateFormat SDF_INPUT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
    private static final SimpleDateFormat SDF_OUTPUT = new SimpleDateFormat("dd/MM/yyyy h:mm a", Locale.ENGLISH);
    private static final SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);

    private static final SimpleDateFormat DDMMYY = new SimpleDateFormat("dd/MM/yy", Locale.ENGLISH);
    private static final SimpleDateFormat HHMM = new SimpleDateFormat("h:m a", Locale.ENGLISH);
    public static final SimpleDateFormat HistoryDateFormat = new SimpleDateFormat("dd MM yyyy", Locale.ENGLISH);

    public static boolean isSame(Date date, Date last_date) {
        return yyyyMMdd.format(date).equals(yyyyMMdd.format(last_date));
    }

    public static String getFormattedDateTime(String time) {
        Date date = parseDateTime(time);
        if (date == null) return "";
        return getFormattedDateTime(date);
    }

    public static Date parseDateTime(String time) {
        try {
            SDF_INPUT.setTimeZone(TimeZone.getTimeZone("UTC"));
            return SDF_INPUT.parse(time);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getDDMMYY(long time) {
        try {
            DDMMYY.setTimeZone(TimeZone.getDefault());
            return DDMMYY.format(new Date(time));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getHHMM(long time) {
        try {
            HHMM.setTimeZone(TimeZone.getDefault());
            return HHMM.format(new Date(time));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static long getTimeStamp(String time) {
        try {
            SDF_INPUT.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = SDF_INPUT.parse(time);
            return date == null ? 0 : date.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String getFormattedDateTime(long time) {
        return getFormattedDateTime(new Date(time));
    }

    public static String getFormattedDateTime(Date date) {
        SDF_OUTPUT.setTimeZone(TimeZone.getDefault());
        return SDF_OUTPUT.format(date);
    }

    public static String convertToHHMMSS(long seconds) {
        seconds = seconds / 1000;
        long s = seconds % 60;
        long m = (seconds / 60) % 60;
        long h = (seconds / (60 * 60)) % 24;
        long d = (seconds / (60 * 60 * 24)) % 365;

        if (h > 0) {
            return String.format(Locale.ENGLISH, "%d hour %02d mins", h, m);
        } else if (m > 0) {
            return String.format(Locale.ENGLISH, "%d mins %02d secs ", m, s);
        } else {
            return String.format(Locale.ENGLISH, "%d seconds", s);
        }

    }

    public static String milliToString(long millis) {
        long days = TimeUnit.MILLISECONDS.toDays(millis) % 365;
        long hrs = TimeUnit.MILLISECONDS.toHours(millis) % 24;
        long min = TimeUnit.MILLISECONDS.toMinutes(millis) % 60;
        long sec = TimeUnit.MILLISECONDS.toSeconds(millis) % 60;

        StringBuilder sb = new StringBuilder();
        if (days > 0) {
            sb.append(String.format(Locale.ENGLISH, "%d %s", days, days > 1 ? "days" : "day"));
        }
        if (hrs > 0) {
            boolean hasData = false;
            if (sb.length() > 0) {
                sb.append(" ");
                hasData = true;
            }
            sb.append(String.format(Locale.ENGLISH, "%d %s", hrs, hrs > 1 ? "hours" : "hour"));
            if (hasData) return sb.toString();
        }
        if (min > 0) {
            boolean hasData = false;
            if (sb.length() > 0) {
                sb.append(" ");
                hasData = true;
            }
            sb.append(String.format(Locale.ENGLISH, "%d %s", min, min > 1 ? "mins" : "min"));
            if (hasData) return sb.toString();

        }
        if (sec > 0) {
            if (sb.length() > 0) sb.append(" ");
            sb.append(String.format(Locale.ENGLISH, "%d %s", sec, sec > 1 ? "secs" : "sec"));
        }

        return sb.toString();
    }

    public static String convertWithoutChangingTimezone(String time, String inputFormate, String outputFormate) {

        String formattedDate = "";
        try {
            SimpleDateFormat formatter = new SimpleDateFormat(inputFormate);
            Date value = formatter.parse(time);

            SimpleDateFormat dateFormatter = new SimpleDateFormat(outputFormate); //this format changeable
            formattedDate = dateFormatter.format(value);

            //Log.d("ourDate", ourDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedDate;
    }

    public static String convertUTCIntoLocal(String time, String inputFormate, String outputFormate) {

        String formattedDate = "";
        try {
            SimpleDateFormat formatter = new SimpleDateFormat(inputFormate);
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date value = formatter.parse(time);

            SimpleDateFormat dateFormatter = new SimpleDateFormat(outputFormate); //this format changeable
            dateFormatter.setTimeZone(TimeZone.getTimeZone("Asia/Singapore"));
            formattedDate = dateFormatter.format(value);

            //Log.d("ourDate", ourDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedDate;
    }

    public static String convertDateFormate(String time, String inputFormate, String outputFormate) {

        String formattedDate = "";
        try {
            SimpleDateFormat formatter = new SimpleDateFormat(inputFormate);
            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date value = formatter.parse(time);

            SimpleDateFormat dateFormatter = new SimpleDateFormat(outputFormate); //this format changeable
            dateFormatter.setTimeZone(TimeZone.getTimeZone("Asia/Singapore"));
            formattedDate = dateFormatter.format(value);

            //Log.d("ourDate", ourDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedDate;
    }
}
