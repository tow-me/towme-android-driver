package sg.com.towme.driver.payment;

import androidx.annotation.NonNull;
import androidx.annotation.Size;


import com.stripe.android.EphemeralKeyProvider;
import com.stripe.android.EphemeralKeyUpdateListener;
import sg.com.towme.driver.model.AbstractCallback;
import sg.com.towme.driver.network.StripeCall;
import sg.com.towme.driver.utility.AppHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import okhttp3.ResponseBody;

public class CustomerKeyProvider implements EphemeralKeyProvider {
    private EphemeralListener ephemeralListener;
    private static String ephemeral;

    public CustomerKeyProvider(EphemeralListener ephemeralListener) {
        this.ephemeralListener = ephemeralListener;
    }

    @Override
    public void createEphemeralKey(@NonNull @Size(min = 4) String apiVersion, @NonNull final EphemeralKeyUpdateListener keyUpdateListener) {
//        if (ephemeral != null) {
//            keyUpdateListener.onKeyUpdate(ephemeral);
//            return;
//        }
        HashMap<String, String> param = new HashMap<>();
        param.put("api_version", apiVersion);
        param.put("customer_id", AppHelper.getStripeId());
        StripeCall.getInstance().createKey(param, new AbstractCallback<ResponseBody>() {
            @Override
            public void result(ResponseBody responseBody) {
                try {
                    CustomerKeyProvider.ephemeral = responseBody.string();
                    keyUpdateListener.onKeyUpdate(CustomerKeyProvider.ephemeral);
                    ephemeralListener.onEphemeralUpdate(true, ephemeral);
                } catch (Exception e) {
                    e.printStackTrace();
                    try {
                        JSONObject jsonObject = new JSONObject(CustomerKeyProvider.ephemeral);
                        int code = jsonObject.getInt("statusCode");
                        String message = jsonObject.getJSONObject("raw").getString("message");
                        keyUpdateListener.onKeyUpdateFailure(code, message);
                        ephemeralListener.onEphemeralUpdate(false, message);
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                        ephemeralListener.onEphemeralUpdate(false, e.getMessage());
                    }
                }

            }
        });
    }

    public interface EphemeralListener {
        void onEphemeralUpdate(boolean success, String message);
    }
}