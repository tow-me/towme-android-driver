package sg.com.towme.driver.network;



import android.util.Log;

import sg.com.towme.driver.utility.AppHelper;

import java.util.HashMap;

public class HeaderHelper {
    private static final HeaderHelper ourInstance = new HeaderHelper();

    public static HeaderHelper getInstance() {
        return ourInstance;
    }

    private HeaderHelper() {
    }

    public HashMap<String, String> getAuthTokenParam() {
        HashMap<String, String> param = new HashMap<>();

        if (AppHelper.getInstance().isLogin()) {
            param.put(Parameter.Authorization, "Bearer " + AppHelper.getInstance().getUserToken());
        }
//        if (AppHelper.getInstance().getCookie() != null && !"".equals(AppHelper.getInstance().getCookie())) {
//            Log.e("zxc","here");
//            param.put(Parameter.Cookie, AppHelper.getInstance().getCookie());
//        }
        return param;
    }


}
