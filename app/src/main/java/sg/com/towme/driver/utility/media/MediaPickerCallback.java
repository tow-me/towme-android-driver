package sg.com.towme.driver.utility.media;

public interface MediaPickerCallback {
    void onPickedSuccess(Media media);
    void onPickedError(String error);
    void showProgressBar(boolean enable);
}