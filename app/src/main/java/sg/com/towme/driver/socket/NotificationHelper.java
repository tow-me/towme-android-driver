package sg.com.towme.driver.socket;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;

import sg.com.towme.driver.Controller;
import sg.com.towme.driver.R;
import sg.com.towme.driver.activity.HomeActivity;


public class NotificationHelper {
    private static String TAG = NotificationHelper.class.getName();
    private static final String CHANNEL_MESSAGE_ID = "general_notification_channel_id";
    private static final String CHANNEL_NAME = "General Notification";

    private static int getUniqueId() {
        return (int) (System.currentTimeMillis() & 0xfffffff);
    }

    private static NotificationHelper instance;


    public static NotificationHelper getInstance() {
        if (instance == null) instance = new NotificationHelper();
        return instance;
    }

    public void show(int notification_id, String title, String contentText) {
        createChannel();

        Context context = Controller.getInstance();
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_MESSAGE_ID)
                .setStyle(new NotificationCompat.BigTextStyle().setBigContentTitle(title).bigText(contentText))
                .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                .setContentTitle(title)
                .setContentText(contentText)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(getPendingIntent(context, notification_id));
        NotificationManagerCompat.from(context).notify(notification_id, builder.build());
    }

    private PendingIntent getPendingIntent(Context context, int unique_id) {
        Intent intent = new Intent(context, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        return PendingIntent.getActivity(context, unique_id, intent, PendingIntent.FLAG_ONE_SHOT);
    }

    private void createChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_MESSAGE_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
            NotificationManagerCompat.from(Controller.getInstance()).createNotificationChannel(channel);
        }
    }

    public void cancelAll() {
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(Controller.getInstance());
        notificationManager.cancelAll();
    }

    public void cancel(int notification_id) {
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(Controller.getInstance());
        notificationManager.cancel(notification_id);
    }

    public void cancel(String tag, int notification_id) {
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(Controller.getInstance());
        notificationManager.cancel(tag, notification_id);
        cancel(notification_id);
    }

}
