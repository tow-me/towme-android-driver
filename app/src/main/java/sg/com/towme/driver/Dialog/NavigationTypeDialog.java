package sg.com.towme.driver.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import sg.com.towme.driver.R;
import sg.com.towme.driver.activity.HomeActivity;
import sg.com.towme.driver.enumeration.NavigationType;
import sg.com.towme.driver.listener.iNavigationTypeListner;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class NavigationTypeDialog extends BaseDialog {

    HomeActivity homeActivity;


    public void setCallback(iNavigationTypeListner callback) {
        this.callback = callback;
    }

    iNavigationTypeListner callback;

    public static NavigationTypeDialog newInstance(iNavigationTypeListner callback) {
        NavigationTypeDialog fragment = new NavigationTypeDialog();
        fragment.setCallback(callback);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

       /* Window window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.dimAmount = 0.8f;
        windowParams.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(windowParams);*/
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

     /*   Window window = dialog.getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);*/


        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_navigation_type, container, false);
        getDialog().requestWindowFeature(STYLE_NO_TITLE);
        setCancelable(false);
        ButterKnife.bind(this, view);

        return view;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof HomeActivity) {
            homeActivity = (HomeActivity) context;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }


    @OnClick({R.id.btnWazeMap, R.id.btnGoogleMap})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnWazeMap:
                callback.onNavigationTypeSelect(NavigationType.WAZE_MAP);
                break;
            case R.id.btnGoogleMap:
                callback.onNavigationTypeSelect(NavigationType.GOOGLE_MAP);
                break;
        }
        dismiss();

    }
}
