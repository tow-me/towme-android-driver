package sg.com.towme.driver.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import sg.com.towme.driver.R;
import sg.com.towme.driver.listener.RecyclerViewClickListener;
import sg.com.towme.driver.model.transaction.TransactionData;
import sg.com.towme.driver.utility.AppHelper;
import sg.com.towme.driver.utility.DateTimeUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TransactionHistoryAdapter extends RecyclerView.Adapter<TransactionHistoryAdapter.ViewHolder> {

    Context context;
    List<TransactionData> listTransaction;

    public void setCallback(RecyclerViewClickListener callback) {
        this.callback = callback;
    }

    RecyclerViewClickListener callback;

    public TransactionHistoryAdapter(Context context, List<TransactionData> listTransaction) {
        this.context = context;
        this.listTransaction = listTransaction;
    }

    @Override
    public TransactionHistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.row_transaction_history, parent, false);
        TransactionHistoryAdapter.ViewHolder viewHolder = new TransactionHistoryAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TransactionHistoryAdapter.ViewHolder holder, int position) {

        TransactionData data = listTransaction.get(position);
        double amount = 0;
        if (data.getCommission() != null) {
            amount = data.getCommission();
        }
        if (data.getUserCancellation() == 1) {
            holder.txtTitle.setText("Cancellation Fee");
        } else {
            holder.txtTitle.setText("Towing Service");
        }
        holder.txtAmount.setText(data.getCurrency()+" " + AppHelper.getInstance().formatCurrency(amount));
        holder.txtAmount.setTextColor(context.getResources().getColor(R.color.colorTextRed));
        if (data.getIsRedeem() == 1) {
            holder.txtDetail.setText("Paid");
        } else {
            holder.txtDetail.setText("Unpaid");
        }
        holder.txtDate.setText(DateTimeUtil.convertWithoutChangingTimezone(data.getCreatedAt(), "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "dd MMM yyyy, hh:mma"));
        holder.txtTime.setText(DateTimeUtil.convertWithoutChangingTimezone(data.getCreatedAt(), "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "hh:mm a"));


    }

    @Override
    public int getItemCount() {
        return listTransaction.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_title)
        AppCompatTextView txtTitle;
        @BindView(R.id.txt_detail)
        AppCompatTextView txtDetail;
        @BindView(R.id.txt_date)
        AppCompatTextView txtDate;
        @BindView(R.id.txt_time)
        AppCompatTextView txtTime;
        @BindView(R.id.txt_amount)
        AppCompatTextView txtAmount;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
