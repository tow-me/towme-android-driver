package sg.com.towme.driver.listener;

public interface iVehicleTypeeListner {

    void onCarClick();

    void onMotorbikeClick();

    void onVanMinibusClick();

}
