
package sg.com.towme.driver.model.notification;


import sg.com.towme.driver.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationPojo extends BaseModel {

    @SerializedName("data")
    @Expose
    private List<NotificationData> data = null;

    public List<NotificationData> getData() {
        return data;
    }

    public void setData(List<NotificationData> data) {
        this.data = data;
    }

}
