
package sg.com.towme.driver.model.booking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import sg.com.towme.driver.model.chat.Chat;

import java.util.List;

public class BookingData extends BaseBooking  {

    @SerializedName("started_images")
    @Expose
    private List<String> startedImages = null;
    @SerializedName("stopped_images")
    @Expose
    private List<String> stoppedImages = null;
    @SerializedName("start_ride_description")
    @Expose
    private String startRideDescription;
    @SerializedName("end_ride_description")
    @Expose
    private String endRideDescription;
    @SerializedName("last_message")
    @Expose
    private Chat lastMessage;
    @SerializedName("end_ride_extra_charge")
    @Expose
    private List<EndRideExtraCharge> endRideExtraCharge = null;
    @SerializedName("driver")
    @Expose
    private User driver;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("tow_price")
    @Expose
    private String towPrice;
    @SerializedName("country")
    @Expose
    private String country;

    public String getTowPrice() {
        return towPrice;
    }

    public void setTowPrice(String towPrice) {
        this.towPrice = towPrice;
    }

    public Chat getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(Chat lastMessage) {
        this.lastMessage = lastMessage;
    }

    public User getDriver() {
        return driver;
    }

    public void setDriver(User driver) {
        this.driver = driver;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<String> getStartedImages() {
        return startedImages;
    }

    public void setStartedImages(List<String> startedImages) {
        this.startedImages = startedImages;
    }

    public List<String> getStoppedImages() {
        return stoppedImages;
    }

    public void setStoppedImages(List<String> stoppedImages) {
        this.stoppedImages = stoppedImages;
    }


    public String getStartRideDescription() {
        return startRideDescription;
    }

    public void setStartRideDescription(String startRideDescription) {
        this.startRideDescription = startRideDescription;
    }

    public String getEndRideDescription() {
        return endRideDescription;
    }

    public void setEndRideDescription(String endRideDescription) {
        this.endRideDescription = endRideDescription;
    }

    public List<EndRideExtraCharge> getEndRideExtraCharge() {
        return endRideExtraCharge;
    }

    public void setEndRideExtraCharge(List<EndRideExtraCharge> endRideExtraCharge) {
        this.endRideExtraCharge = endRideExtraCharge;
    }

    public long getChargeableAmount() {
//        return (long) ((parseDouble(getTotalCost()) - parseDouble(getDiscountedAmount())) * 100);
        return (long) ((parseDouble(getTotalCost())) * 100);
    }

    private static double parseDouble(String raw) {
        try {
            return Double.parseDouble(raw);
        } catch (Exception e) {
            return 0;
        }
    }

    public Double getCalculatedFinalCost() {
        Double totalExtraCharge = 0.0;

        if (getEndRideExtraCharge() != null)
            for (int i = 0; i < endRideExtraCharge.size(); i++) {
                totalExtraCharge += Double.parseDouble(endRideExtraCharge.get(i).getPrice().trim());
            }

        Double totalCharge = Double.parseDouble(getTowPrice().trim()) + totalExtraCharge - Double.parseDouble(getDiscountedAmount().trim());

        return totalCharge;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
