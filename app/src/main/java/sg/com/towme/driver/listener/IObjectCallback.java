package sg.com.towme.driver.listener;

public interface IObjectCallback<T> {
    void response(T object);
}
