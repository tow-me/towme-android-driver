package sg.com.towme.driver.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.WorkManager;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import io.socket.client.Ack;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import sg.com.towme.driver.Controller;
import sg.com.towme.driver.PicImage.ImagePicker;
import sg.com.towme.driver.R;
import sg.com.towme.driver.adapter.ChatAdapter;
import sg.com.towme.driver.constant.AppConstant;
import sg.com.towme.driver.enumeration.ChatType;
import sg.com.towme.driver.model.BaseModel;
import sg.com.towme.driver.model.UploadPojo;
import sg.com.towme.driver.model.booking.User;
import sg.com.towme.driver.model.chat.Chat;
import sg.com.towme.driver.model.chat.ChatPojo;
import sg.com.towme.driver.network.IResponseCallback;
import sg.com.towme.driver.network.NetworkCall;
import sg.com.towme.driver.network.Parameter;
import sg.com.towme.driver.socket.NotificationHelper;
import sg.com.towme.driver.socket.SocketConstant;
import sg.com.towme.driver.socket.SocketIOClient;
import sg.com.towme.driver.utility.AppHelper;
import sg.com.towme.driver.utility.PreferanceHelper;
import sg.com.towme.driver.utility.media.BottomSheetFilePicker;
import sg.com.towme.driver.utility.media.Media;
import sg.com.towme.driver.utility.media.MediaPickerCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import jp.wasabeef.glide.transformations.CropCircleTransformation;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

import static com.facebook.FacebookSdk.getApplicationContext;

public class ChatActivity extends ToolBarActivity implements MediaPickerCallback {

    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;

    ChatAdapter adapter;
    Socket mSocket;
    String bookingID;
    List<Chat> chatList;
    @BindView(R.id.imgProfilePic)
    CircleImageView imgProfilePic;
    @BindView(R.id.txtName)
    AppCompatTextView txtName;
    @BindView(R.id.txtCompanyName)
    AppCompatTextView txtCompanyName;
    @BindView(R.id.edtMessage)
    AppCompatEditText edtMessage;
    @BindView(R.id.imgSend)
    AppCompatImageView imgSend;
    @BindView(R.id.imgCamera)
    AppCompatImageView imgCamera;
    User user;
    private static final int PICK_IMAGE = 1;
    File filePath = null;

    int page = 1;
    int pageLimit = 10;
    int totalPage = 0;
    int currentPage = 0;
    int lastVisibleIteam;
    String startedImages;
    String stoppedImages;
    @BindView(R.id.imgCall)
    AppCompatImageView imgCall;
    @BindView(R.id.llMain)
    LinearLayout llMain;
    @BindView(R.id.llChat)
    LinearLayout llChat;

    ChatType chatType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);

        llMain.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                //r will be populated with the coordinates of your view that area still visible.
                llMain.getWindowVisibleDisplayFrame(r);

                int heightDiff = llMain.getRootView().getHeight() - (r.bottom - r.top);
                if (heightDiff > 500) { // if more than 100 pixels, its probably a keyboard...
                    Log.e("Keyboard", "open");
                    if (chatType == ChatType.PREVIOUS)
                        llChat.setVisibility(View.GONE);
                } else {
                    Log.e("Keyboard", "close");
                    llChat.setVisibility(View.VISIBLE);
                }
            }
        });

        clearNotification();

        if (getIntent() != null) {
            bookingID = getIntent().getStringExtra(AppConstant.booking_id);
            startedImages = getIntent().getStringExtra("started_images");
            stoppedImages = getIntent().getStringExtra("stoped_images");
            chatType = (ChatType) getIntent().getSerializableExtra("chat_type");
            user = (User) getIntent().getSerializableExtra("user_data");

            Glide.with(ChatActivity.this)
                    .load(user.getProfilePic())
                    .transform(new CropCircleTransformation())
                    .into(imgProfilePic);
            txtName.setText(user.getName());
//                            txtCompanyName.setText(data.getCompanyName());
            Log.e("asd", user.getCountryCode().contains("65")+"");
            imgCall.setOnClickListener(view -> {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                if (user.getCountryCode() != null) {
                    if (user.getCountryCode().contains("65")) {
                        intent.setData(Uri.parse("tel:" + user.getCountryCode() + user.getMobile()));
                    } else if (user.getCountryCode().contains("60")) {
                        intent.setData(Uri.parse("tel:0" + user.getMobile()));
                    } else {
                        intent.setData(Uri.parse("tel:" + user.getMobile()));
                    }
                } else {
                    intent.setData(Uri.parse("tel:" + user.getMobile()));
                }
                startActivity(intent);
            });

        }

        if (chatType == ChatType.PREVIOUS) {
            edtMessage.setEnabled(false);
            imgCamera.setEnabled(false);
            imgSend.setEnabled(false);
            imgCall.setVisibility(View.GONE);
        }

        mSocket = SocketIOClient.getInstance();

        mSocket.on(SocketConstant.GET_MESSAGE, onNewMessage);

        NotificationHelper.getInstance().cancelAll();
        iniUI();

        if (adapter == null || chatList == null) {
            setAdapter();
            getChatHistory();
        } else {
            setAdapter();
        }

    }

    private void iniUI() {
        setToolbarTitle(getString(R.string.messages));
    }

    Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.e("New message", args[0].toString());

            Gson gson = new Gson();
            List<Chat> list = Arrays.asList(gson.fromJson(args[0].toString(), Chat[].class));
            Log.e("chat list size", list.size() + "...");

            runOnUiThread(() -> {
                clearNotification();
                chatList.add(list.get(0));
                adapter.notifyDataSetChanged();
                recyclerview.scrollToPosition(chatList.size() - 1);

            });

        }
    };

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
    }

    private void setAdapter() {

        if (chatList == null)
            chatList = new ArrayList<>();
        if (adapter == null)
            adapter = new ChatAdapter(this, chatList, startedImages, stoppedImages);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        recyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (layoutManager.findFirstCompletelyVisibleItemPosition() == 0) {
                    // beginning of the recycler
                    Log.e("Loadmore", "beginning of the recycler...");

                    if (currentPage < totalPage)
                        getChatHistory();
                }

                lastVisibleIteam = layoutManager.findLastCompletelyVisibleItemPosition();

            }
        });

        recyclerview.setLayoutManager(layoutManager);
        recyclerview.setAdapter(adapter);
        adapter.notifyDataSetChanged();
//        recyclerview.scrollToPosition(10 - 1);
    }

    private void getChatHistory() {

        Log.e("Booking_id", bookingID + "...");

        JSONObject param = new JSONObject();
        try {
            param.put(SocketConstant.BOOKING_ID, bookingID);
            param.put(SocketConstant.LIMIT, pageLimit);
            param.put(SocketConstant.PAGE, page);
            param.put(SocketConstant.RECEIVER_ID, AppHelper.getInstance().getUserDetails().getUserid());
            if (AppHelper.getInstance().getUserToken() != null) {
                param.put(SocketConstant.TOKEN, AppHelper.getInstance().getUserToken());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (page == 1)
            showProgressDialog();
//        JSONObject param = SocketUtils.getInstance().getParameter(SocketConstant.BOOKING_ID, bookingID);

        mSocket.emit(SocketConstant.MESSAGES, param, new Ack() {
            @Override
            public void call(Object... args) {
                int code = 400;

                JSONObject obj = null;
                try {
                    obj = new JSONObject(args[0].toString());
                    code = obj.getInt("status_code");

                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (code == 401) {
                    try {
                        if (getApplicationContext() != null) {
                            PreferanceHelper.getInstance().clearPreference();
                            WorkManager.getInstance(Controller.getInstance()).cancelAllWork();
                            openLoginActivity();
                        }
                    } catch (Exception e) {}
                } else {
                    Log.e("Chat History", args[0].toString() + "...");

                    Gson gson = new Gson();
                    ChatPojo list = new ChatPojo();
                    try {
                        list = gson.fromJson(args[0].toString(), ChatPojo.class);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Log.e("chat list size", list.getRow().size() + "...");

                    totalPage = list.getPages();
                    currentPage = list.getCurrentPage();
                    List<Chat> finalList = list.getRow();
                    Collections.reverse(finalList);
                    runOnUiThread(() -> {

                        if (page == 1)
                            hideProgressDialog();

                        if (finalList.size() > 0) {
                            if (page == 1) {

                                User data;
                                if (finalList.get(0).getSender().getUserid() == AppHelper.getInstance().getUserDetails().getUserid())
                                    data = finalList.get(0).getReceiver();
                                else
                                    data = finalList.get(0).getSender();

                                Glide.with(ChatActivity.this)
                                        .load(data.getProfilePic())
                                        .transform(new CropCircleTransformation())
                                        .into(imgProfilePic);
                                txtName.setText(data.getName());
//                            txtCompanyName.setText(data.getCompanyName());

                            }

//                        chatList.clear();

                            if (page == 1)
                                chatList.addAll(finalList);
                            else
                                chatList.addAll(0, finalList);

                            adapter.notifyDataSetChanged();

                            if (page == 1)
                                recyclerview.scrollToPosition(chatList.size() - 1);
                            else
                                recyclerview.scrollToPosition((lastVisibleIteam + pageLimit) - 2);

                            page++;

                        }
                    });
                }
            }
        });

    }

    private void sendMessage(String strMessage, String strType) {

        Log.e("Booking_id", bookingID + "...");
//        showProgressDialog();
        String sendID = String.valueOf(AppHelper.getInstance().getUserDetails().getUserid());
        JSONObject param = new JSONObject();
        try {
            param.put(SocketConstant.BOOKING_ID, bookingID);
            param.put(SocketConstant.SENDER_ID, sendID);
            param.put(SocketConstant.MESSAGE, strMessage);
            param.put(SocketConstant.TYPE, strType);
            if (AppHelper.getInstance().getUserToken() != null) {
                param.put(SocketConstant.TOKEN, AppHelper.getInstance().getUserToken());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mSocket.emit(SocketConstant.MESSAGE, param, new Ack() {
            @Override
            public void call(Object... args) {
                int code = 400;

                JSONObject obj = null;
                try {
                    obj = new JSONObject(args[0].toString());
                    code = obj.getInt("status_code");

                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (code == 401) {
                    try {
                        if (getApplicationContext() != null) {
                            PreferanceHelper.getInstance().clearPreference();
                            WorkManager.getInstance(Controller.getInstance()).cancelAllWork();
                            openLoginActivity();
                        }
                    } catch (Exception e) {}
                } else {
                    Log.e("Send Message", args[0].toString() + "...");

                    Gson gson = new Gson();
                    List<Chat> list = Arrays.asList(gson.fromJson(args[0].toString(), Chat[].class));
                    Log.e("chat list size", list.size() + "...");

                    runOnUiThread(() -> {

                        edtMessage.setText("");
//                    hideProgressDialog();
                        chatList.add(list.get(0));
                        adapter.notifyDataSetChanged();
                        recyclerview.scrollToPosition(chatList.size() - 1);

                    });
                }
            }
        });

    }

    @Override
    public void onPickedSuccess(Media media) {
        if (media == null) return;
//        imageView.setUrl(media, 0);
        filePath = media.getLocalFile();
        callUploadAPI(filePath);
    }

    @Override
    public void onPickedError(String error) {

    }

    @Override
    public void showProgressBar(boolean enable) {

    }

    @OnClick({R.id.imgCamera, R.id.imgSend})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgCamera:
                if (isReadStorageAllowed(PICK_IMAGE)) {
                    final CharSequence[] items = {"Take Image", "Choose Image from Gallery"};

                    AlertDialog.Builder builder = new AlertDialog.Builder(ChatActivity.this);
                    builder.setTitle("Add Photo");
                    builder.setTitle(null);

                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {
                            if (items[item].equals("Take Image")) {
                                openCamera();
                            } else if (items[item].equals("Choose Image from Gallery")) {
                                pickImage();
                            }
                        }
                    });
                    builder.show();
                }

                break;
            case R.id.imgSend:
                if (edtMessage.getText().toString().trim().length() > 0)
                    sendMessage(edtMessage.getText().toString().trim(), SocketConstant.TEXT);
                break;
        }
    }

    private boolean isReadStorageAllowed(int permissionCode) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, permissionCode);
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == PICK_IMAGE)
                pickImage();
        } else {
            showSnackBar(imgProfilePic, getResources().getString(R.string.you_have_to_allow_storage_permission));
        }
    }

    private void openCamera() {
//        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
//        StrictMode.setVmPolicy(builder.build());
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        startActivityForResult(Intent.createChooser(intent, "Take Picture"), PICK_IMAGE);
    }

    private void pickImage() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE) {
            if (resultCode == RESULT_OK) {
                if (data.getData() != null) {
                    Bitmap bitmap = ImagePicker.getImageFromResult(this, resultCode, data);
                    if (bitmap != null) {
                        filePath = AppHelper.saveToInternalStorage(this, bitmap);
//                        imgVehiclePhoto.setVisibility(View.VISIBLE);
//                        imgVehiclePhoto.setImageBitmap(bitmap);
                        callUploadAPI(filePath);
                    }
                } else {
                    Bitmap image = (Bitmap) data.getExtras().get("data");
                    if (image != null) {
                        filePath = AppHelper.saveToInternalStorage(this, image);
//                        imgVehiclePhoto.setVisibility(View.VISIBLE);
//                        imgVehiclePhoto.setImageBitmap(bitmap);
                        callUploadAPI(filePath);
                    }
                }
            }
        }
    }

    private void callUploadAPI(File fileImage) {

        showProgressDialog();

        MultipartBody.Part profilePic = null;
        if (fileImage != null) {
            RequestBody requestFile =
                    RequestBody.create(
                            MediaType.parse("image/*"),
                            fileImage
                    );
            profilePic = MultipartBody.Part.createFormData(Parameter.files, fileImage.getName(), requestFile);
        }

        NetworkCall.getInstance().upload(profilePic, new IResponseCallback<UploadPojo>() {
            @Override
            public void success(UploadPojo data) {
                hideProgressDialog();
                if (data.getStatusCode() == 200) {
                    sendMessage(data.getData().get(0), SocketConstant.IMAGE);
                } else {
                    showSnackBar(imgSend, data.getMessage());
                }
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                hideProgressDialog();
                showSnackBar(imgSend, baseModel.getMessage());
            }

            @Override
            public void onError(Call<UploadPojo> responseCall, Throwable T) {
                hideProgressDialog();
                showSnackBar(imgSend, getString(R.string.error_message));
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSocket.off(SocketConstant.GET_MESSAGE, onNewMessage);
    }


}
