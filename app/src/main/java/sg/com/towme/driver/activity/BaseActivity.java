package sg.com.towme.driver.activity;


import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import sg.com.towme.driver.R;
import sg.com.towme.driver.constant.AppConstant;
import sg.com.towme.driver.genericbottomsheet.GenericBottomModel;
import sg.com.towme.driver.genericbottomsheet.GenericBottomSheetDialog;
import sg.com.towme.driver.listener.CheckNetworkListener;
import sg.com.towme.driver.listener.iGPSEnableListener;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

public class BaseActivity extends AppCompatActivity {

    /*void fragmentChange(Fragment fragment, FragmentState fragmentState) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (fragmentState == FragmentState.ADD) {
            transaction.add(R.id.fragment_container, fragment);
        } else {
            transaction.replace(R.id.fragment_container, fragment);
        }
        transaction.addToBackStack(fragment.getClass().getSimpleName());
        transaction.commit();

    }*/

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
    }

    public void hideKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);
    }

    public void showKeyboard(EditText editText) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInputFromWindow(editText.getApplicationWindowToken(),
                InputMethodManager.SHOW_FORCED, 0);
    }

    public void showSnackBar(View v, String str) {
        Snackbar snackbar = Snackbar.make(v, str, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    private List<String> getDummyList(int numOfRecord) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < numOfRecord; i++) {
            list.add("test" + i);
        }
        return list;
    }

    public void openBottomSheet(@Nullable String header, List<GenericBottomModel> modelList, GenericBottomSheetDialog.RecyclerItemClick callback) {
        final GenericBottomSheetDialog dialog = new GenericBottomSheetDialog();
        dialog.setDatalist(modelList);
        if (header != null) {
            dialog.setHeader(header);
        }
        dialog.setCallback(callback);
        dialog.show(getSupportFragmentManager(), dialog.getClass().getSimpleName());
    }

    private ConnectivityManager connectivityManager;

    public boolean isNetworkAvailable(View view, CheckNetworkListener checkNetwork) {
        boolean connected = false;
        try {
            connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            connected = networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
            if (!connected) {
                showSnackbarRetry(view, checkNetwork);
            }
            return connected;
        } catch (Exception e) {
            Log.e("connectivity", e.toString());
        }
        return connected;
    }

    void showSnackbarRetry(final View view, final CheckNetworkListener checkNetwork) {

        final Snackbar snackbar = Snackbar.make(view, "", Snackbar.LENGTH_INDEFINITE);

        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
        TextView textView = (TextView) layout.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setVisibility(View.INVISIBLE);
        // Inflate our custom view

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View snackView = inflater.inflate(R.layout.layout_snackbar_retry, null);

        TextView textViewTop = (TextView) snackView.findViewById(R.id.txtMessage);
        textViewTop.setText(getString(R.string.no_internet_connection));

        TextView textRetry = (TextView) snackView.findViewById(R.id.txtRetry);

        textRetry.setOnClickListener(v -> {
            snackbar.dismiss();
            if (isNetworkAvailable(view, checkNetwork)) {
                checkNetwork.onRetryClick();
            }
        });

        //If the view is not covering the whole snackbar layout, add this line
        layout.setPadding(0, 0, 0, 0);

        layout.addView(snackView, 0);
        snackbar.show();


    }

    private SettingsClient mSettingsClient;
    private LocationSettingsRequest mLocationSettingsRequest;


    void enablegps(Activity activity, iGPSEnableListener callback) {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(new LocationRequest().setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY));
        builder.setAlwaysShow(true);
        mLocationSettingsRequest = builder.build();

        mSettingsClient = LocationServices.getSettingsClient(activity);

        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        callback.onGPSEnableSuccess();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                try {
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(activity, AppConstant.REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.e("GPS", "Unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                Log.e("GPS", "Location settings are inadequate, and cannot be fixed here. Fix in Settings.");
                        }
                    }
                })
                .addOnCanceledListener(new OnCanceledListener() {
                    @Override
                    public void onCanceled() {
                        callback.onGPSEnableCancel();
                        Log.e("GPS", "checkLocationSettings -> onCanceled");
                    }
                });


    }

    public void clearNotification() {
        NotificationManager nMgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nMgr.cancelAll();
    }

}
