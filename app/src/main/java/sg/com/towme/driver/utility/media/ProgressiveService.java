package sg.com.towme.driver.utility.media;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Random;

public abstract class ProgressiveService extends IntentService {
    public static final String ACTION_PROGRESS_STATUS = "action_progress_status";
    public static final String ACTION_DOWNLOAD_PROGRESS_STATUS = "action_progress_status";
    public static final String ACTION_UPLOAD_PROGRESS_STATUS = "action_progress_status";
    public static final String ACTION_DELETE_PROGRESS_STATUS = "action_delete_status";

    public static String KEY_MEDIA = "media";
    public static String KEY_MESSAGES = "message_ids";
    public static String KEY_ROOM = "room_id";
    public static String KEY_ACTION = "action";

    /* Input Action */
    public static final int ACTION_START = 0;
    public static final int ACTION_PAUSE = 1;
    public static final int ACTION_CANCEL = 2;
    public static final int ACTION_RETRY = 3;
    public static final int ACTION_DELETE = 4;


    /* Progress Action */
    public static final int STATUS_PENDING = 0;
    public static final int STATUS_PROGRESS = 1;
    public static final int STATUS_PAUSED = 2;
    public static final int STATUS_FINISHED = 3;
    public static final int STATUS_ERROR = 4;
    public static final int STATUS_CONNECTION_LOST = 5;


    public ProgressiveService(String name) {
        super(name);
    }

    abstract public void onStarted(long taskId);

    abstract public void onPaused(long taskId);

    abstract public void onProcess(long taskId, float percent, long progressLength, long totalLength);

    abstract public void onFinished(long taskId);

    abstract public void onFailed(long taskId);

    public void OnRebuildStart(long taskId) {
    }

    public void OnRebuildFinished(long taskId) {
    }

    public void connectionLost(long taskId) {
    }

    public int getRandomNotificationId() {
        return new Random().nextInt(10000 - 1) + 1;
    }


    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

    }

    public void broadCastEvent(String action, int resultCode, Bundle bundle) {
        Intent intent = new Intent(action);
        bundle.putInt("result_code", resultCode);
        intent.putExtras(bundle);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public static String getStringSizeLengthFile(long size) {
        DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance(Locale.ENGLISH);
        df.applyPattern("0.00");

        float sizeKb = 1024.0f;
        float sizeMo = sizeKb * sizeKb;
        float sizeGo = sizeMo * sizeKb;
        float sizeTerra = sizeGo * sizeKb;

        if (size < sizeMo) return df.format(size / sizeKb) + " KB";
        else if (size < sizeGo) return df.format(size / sizeMo) + " MB";
        else if (size < sizeTerra) return df.format(size / sizeGo) + " GB";
        return "";
    }
}
