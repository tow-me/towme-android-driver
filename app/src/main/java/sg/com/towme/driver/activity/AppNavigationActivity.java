package sg.com.towme.driver.activity;


import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.TimePicker;

import sg.com.towme.driver.Dialog.BaseDialog;
import sg.com.towme.driver.Dialog.BookingRemarkDialog;
import sg.com.towme.driver.Dialog.ConfirmationDialog;
import sg.com.towme.driver.Dialog.DateTimePickerDialog;
import sg.com.towme.driver.Dialog.InfoDialog;
import sg.com.towme.driver.Dialog.NavigationTypeDialog;
import sg.com.towme.driver.Dialog.PersonalBusinessDialog;
import sg.com.towme.driver.Dialog.VehicleTypeDialog;
import sg.com.towme.driver.R;
import sg.com.towme.driver.constant.AppConstant;
import sg.com.towme.driver.enumeration.ChatType;
import sg.com.towme.driver.enumeration.Info;
import sg.com.towme.driver.enumeration.Screens;
import sg.com.towme.driver.listener.iDatePickerListner;
import sg.com.towme.driver.listener.iDateTimePickerClick;
import sg.com.towme.driver.listener.iDialogButtonClick;
import sg.com.towme.driver.listener.iInfoDialogListner;
import sg.com.towme.driver.listener.iNavigationTypeListner;
import sg.com.towme.driver.listener.iPersonalBusinessListner;
import sg.com.towme.driver.listener.iVehicleTypeeListner;
import sg.com.towme.driver.model.SocialData;
import sg.com.towme.driver.model.booking.BookingData;
import sg.com.towme.driver.model.booking.User;
import sg.com.towme.driver.model.user.UserData;
import sg.com.towme.driver.utility.TextUtil;

import java.util.Calendar;


public class AppNavigationActivity extends BaseActivity {

    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
    }

    public void showProgressDialog() {
        if (dialog == null) {
            dialog = ProgressDialog.show(this, null, null, true);
            dialog.setContentView(R.layout.progressbar);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        } else {
            dialog.show();
        }
    }


    public void hideProgressDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }


    public void openHomeActivity() {
        Intent i = new Intent(this, HomeActivity.class);
        startActivity(i);
        finishAffinity();
    }

    public void openSplashActivity() {
        startActivity(new Intent(this, SplashActivity.class));
        finish();
    }


    public void openLoginActivity() {
        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);
        finishAffinity();
    }


    public void openSignupActivity(SocialData data) {
        Intent intent = new Intent(this, DriverSignupActivity.class);
        intent.putExtra(AppConstant.social_data, data);
        startActivity(intent);
    }

    public void openForgotPasswordActivity() {
        Intent intent = new Intent(this, ForgotPasswordActivity.class);
        startActivity(intent);
    }

    public void openChatActivity(Integer bookingId, String startedImages, String stoppedImages, ChatType chatType, User user, int requestCode) {
        Intent i = new Intent(this, ChatActivity.class);
        i.putExtra(AppConstant.booking_id, String.valueOf(bookingId));
        i.putExtra("started_images", startedImages);
        i.putExtra("stoped_images", stoppedImages);
        i.putExtra("chat_type", chatType);
        i.putExtra("user_data", user);
        startActivityForResult(i, requestCode);

//        startActivity(new Intent(this, ChatActivity.class));
    }

    public void openProfileActivity() {
        startActivity(new Intent(this, ProfileActivity.class));
    }

    public void openHistoryActivity(Screens screens, String apiDate) {
        Intent i = new Intent(this, BookingHistoryActivity.class);
        i.putExtra(AppConstant.SCREEN, screens);
        i.putExtra("api_date", apiDate);
        startActivity(i);
//        startActivity(new Intent(this, BookingHistoryActivity.class));
    }

    public void openBookingDetailsActivity() {
        startActivity(new Intent(this, BookingDetailsActivity.class));
    }

    public void openChangePasswordActivity() {
        startActivity(new Intent(this, ChangePasswordActivity.class));
    }


    public void openCouponsActivity() {
        startActivity(new Intent(this, CouponsActivity.class));
    }

    public void openInviteActivity() {
        startActivity(new Intent(this, InviteActivity.class));
    }

    public void openHelpActivity() {
        startActivity(new Intent(this, HelpActivity.class));
    }

    public void openOTPActivity(Screens screen, UserData data) {

        Intent intent = new Intent(this, OTPActivity.class);
        intent.putExtra(AppConstant.SCREEN, screen);
        intent.putExtra(AppConstant.user_data, data);
        startActivity(intent);

    }

    public void openResetPasswordActivity(UserData data) {
        Intent intent = new Intent(this, ResetPasswordActivity.class);
        intent.putExtra(AppConstant.user_data, data);
        startActivity(intent);
    }

    public void openDateTimePickerDialog(iDatePickerListner callback) {
        DateTimePickerDialog.newInstance(callback).show(getSupportFragmentManager(), DateTimePickerDialog.class.getSimpleName());
    }

    public void openPersonalBusinessDialog(iPersonalBusinessListner callback) {
        PersonalBusinessDialog.newInstance(callback).show(getSupportFragmentManager(), PersonalBusinessDialog.class.getSimpleName());
    }

    public void openVehicleTypeDialog(iVehicleTypeeListner callback) {
        VehicleTypeDialog.newInstance(callback).show(getSupportFragmentManager(), VehicleTypeDialog.class.getSimpleName());
    }

    public void openConfirmationDialog(iDialogButtonClick callback, String message, String positiveText, String nagativeText) {
        ConfirmationDialog.newInstance(callback, message, positiveText, nagativeText).show(getSupportFragmentManager(), ConfirmationDialog.class.getSimpleName());
    }

    public void openInfoDialog(iInfoDialogListner callback, String message, Info info) {
        InfoDialog.newInstance(callback, message, info).show(getSupportFragmentManager(), InfoDialog.class.getSimpleName());
    }

    public void openBookingRemarkDialog(String message) {
        BookingRemarkDialog.newInstance(message).show(getSupportFragmentManager(), BookingRemarkDialog.class.getSimpleName());
    }

    public void datePicker(iDateTimePickerClick iDateTimePickerClick) {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

//        DatePickerDialog datePickerDialog = new DatePickerDialog(this, AlertDialog.THEME_DEVICE_DEFAULT_DARK, new DatePickerDialog.OnDateSetListener() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String date_time = String.format("%02d", (dayOfMonth)) + "/" + String.format("%02d", (monthOfYear + 1)) + "/" + year;
                iDateTimePickerClick.onPickDate(date_time);
            }
        }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();

    }

    public void dateTimePicker(iDateTimePickerClick iDateTimePickerClick) {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String date_time = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                String apiDateFormat = year + "-" + String.format("%02d", (monthOfYear + 1)) + "-" + String.format("%02d", dayOfMonth); //2020-01-20
                tiemPicker(iDateTimePickerClick, date_time, apiDateFormat);
            }
        }, mYear, mMonth, mDay);
//        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();

    }

    public void openEndJobctivity(BookingData bookingData) {
        Intent intent = new Intent(this, EndJobActivity.class);
        intent.putExtra(AppConstant.booking_data, bookingData);
        startActivity(intent);
    }

    public void openStartJobctivity(BookingData bookingData) {
        Intent intent = new Intent(this, StartJobActivity.class);
        intent.putExtra(AppConstant.booking_data, bookingData);
        startActivity(intent);
    }

    public void openJobDetailsActivity(BookingData bookingData) {
        Intent intent = new Intent(this, JobDetailsActivity.class);
        intent.putExtra(AppConstant.booking_data, bookingData);
        startActivity(intent);
    }

    public void openNotificationActivity(int id) {
        Intent intent = new Intent(this, NotificationActivity.class);
        if (id != 0) {
            intent.putExtra(AppConstant.notif_id, id);
        }
        startActivity(intent);
    }

    public void openTransactionHistoryActivity() {
        startActivity(new Intent(this, TransactionHistoryActivity.class));
    }

    public void openHelpWebviewActivity() {
        Intent intent = new Intent(this, HelpWebviewActivity.class);
        startActivity(intent);
    }

    private void tiemPicker(iDateTimePickerClick callback, String date_time, String apiDateFormat) {
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                        String strHrsToShow = (hourOfDay == 0) ? "12" : hourOfDay + "";
                        c.set(Calendar.HOUR_OF_DAY, Integer.parseInt(strHrsToShow));
                        c.set(Calendar.MINUTE, minute);

                        String am_pm = "";
                        if (c.get(Calendar.AM_PM) == 1) am_pm = "PM";
                        else if (c.get(Calendar.AM_PM) == 0) am_pm = "AM";

                        String apiDataTimeFormat = apiDateFormat + " " + String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute) + ":" + String.format("%02d", c.get(Calendar.SECOND)); // 18:34:04
                        callback.onPickDateTime(date_time + " " + ((hourOfDay > 12) ? (hourOfDay - 12) + "" : hourOfDay) + "" + ":" + c.get(Calendar.MINUTE) + " " + am_pm, apiDataTimeFormat);
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }


    public void openNavigationTypeDialog(iNavigationTypeListner callback) {
        NavigationTypeDialog.newInstance(callback).show(getSupportFragmentManager(), NavigationTypeDialog.class.getSimpleName());
    }

    BaseDialog baseDialog = null;

    public void openDialog(String message) {
        if (TextUtil.isNullOrEmpty(message)) return;
        if (baseDialog != null && baseDialog.isShowing()) return;
        baseDialog = ConfirmationDialog.newInstance(null, message, getString(R.string.ok), null).show(this);
    }

}