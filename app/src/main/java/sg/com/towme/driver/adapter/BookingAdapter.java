package sg.com.towme.driver.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import sg.com.towme.driver.R;
import sg.com.towme.driver.constant.AppConstant;
import sg.com.towme.driver.enumeration.Screens;
import sg.com.towme.driver.listener.RecyclerViewClickListener;
import sg.com.towme.driver.model.booking.BookingData;
import sg.com.towme.driver.utility.AppHelper;
import sg.com.towme.driver.utility.DateTimeUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class BookingAdapter extends RecyclerView.Adapter<BookingAdapter.ViewHolder> {

    Context context;
    List<BookingData> listBooking;
    Screens screens;

    public void setCallback(RecyclerViewClickListener callback) {
        this.callback = callback;
    }

    RecyclerViewClickListener callback;

    public BookingAdapter(Context context, List<BookingData> listBooking, Screens screens) {
        this.context = context;
        this.listBooking = listBooking;
        this.screens = screens;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.row_my_jobs, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if (listBooking.size() > 0) {
            BookingData data = listBooking.get(position);

            Glide.with(context)
                    .load(data.getUser().getUserImage().trim())
                    .transform(new CropCircleTransformation())
                    .placeholder(R.drawable.ic_user_placeholder)
                    .into(holder.imgProfilePic);

            holder.txtName.setText(data.getUser().getName());
            if (data.getBookingStatus() == 0 || data.getBookingStatus() == 5)
                holder.btnAccept.setVisibility(View.VISIBLE);
            else
                holder.btnAccept.setVisibility(View.GONE);

            holder.txtSource.setText(data.getSource());
            holder.txtDestination.setText(data.getDestination());
            holder.txtTime.setText(DateTimeUtil.convertWithoutChangingTimezone(data.getBookingDate(), "yyyy-MM-dd HH:mm:ss", "EEE, dd MMM hh:mm a"));
            holder.txtPrice.setText(String.format("%s%s", AppConstant.CURRENCY, data.getTotalCost()));

            holder.txtStatus.setTextColor(Color.parseColor(AppHelper.getInstance().getStatusColor(data.getBookingStatus(), context)));
            holder.txtStatus.setText(AppHelper.getInstance().getStatusText(data.getBookingStatus(), context));

            holder.llRow.setOnClickListener(view -> {
                callback.onClick(holder.llRow, position, null);
            });
            holder.btnAccept.setOnClickListener(view -> {
                callback.onClick(holder.btnAccept, position, null);
            });

        }

    }

    @Override
    public int getItemCount() {
        return listBooking.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.llRow)
        LinearLayout llRow;
        @BindView(R.id.txt_time)
        AppCompatTextView txtTime;
        @BindView(R.id.txtSource)
        AppCompatTextView txtSource;
        @BindView(R.id.txtDestination)
        AppCompatTextView txtDestination;
        @BindView(R.id.txtPrice)
        AppCompatTextView txtPrice;
        @BindView(R.id.txtName)
        AppCompatTextView txtName;
        @BindView(R.id.txtStatus)
        AppCompatTextView txtStatus;
        @BindView(R.id.btnAccept)
        Button btnAccept;
        @BindView(R.id.imgProfilePic)
        CircleImageView imgProfilePic;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

}