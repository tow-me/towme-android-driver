package sg.com.towme.driver.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import sg.com.towme.driver.R;
import sg.com.towme.driver.listener.RecyclerViewClickListener;

import butterknife.ButterKnife;

public class CardDetailsAdapter extends RecyclerView.Adapter<CardDetailsAdapter.ViewHolder> {

    Context context;


    public void setCallback(RecyclerViewClickListener callback) {
        this.callback = callback;
    }

    RecyclerViewClickListener callback;

    public CardDetailsAdapter(Context context) {
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.row_card, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
     /*   holder.rbReviews.setChecked(true);
        holder.llMain.setOnClickListener(view -> {
            if (holder.expandableLayout.isExpanded()) {
                holder.expandableLayout.collapse();
            } else {
                holder.expandableLayout.expand();
            }
        });

        holder.btnBookNow.setOnClickListener(view -> {
            callback.onClick(holder.btnBookNow,position);
        });*/
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}
