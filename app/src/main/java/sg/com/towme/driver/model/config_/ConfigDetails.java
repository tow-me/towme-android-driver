
package sg.com.towme.driver.model.config_;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConfigDetails {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("radius")
    @Expose
    private Integer radius;
    @SerializedName("towme_application_commission")
    @Expose
    private Double towmeApplicationCommission;
    @SerializedName("towme_cash_payment_commission")
    @Expose
    private Integer towmeCashPaymentCommission;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("environment")
    @Expose
    private String environment;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("booking_type_info")
    @Expose
    private String bookingTypeInfo;
    @SerializedName("booking_accident_info")
    @Expose
    private String bookingAccidentInfo;
    @SerializedName("booking_breakdown_info")
    @Expose
    private String bookingBreakdownInfo;
    @SerializedName("booking_contact_info")
    @Expose
    private String bookingContactInfo;
    @SerializedName("invite_heading_driver")
    @Expose
    private String inviteHeading;
    @SerializedName("invite_text_driver")
    @Expose
    private String inviteText;
    @SerializedName("invite_coupon_amount")
    @Expose
    private String inviteCouponAmount;
    @SerializedName("privacy_policy")
    @Expose
    private String privacyPolicy;
    @SerializedName("terms_and_condition")
    @Expose
    private String termsAndCondition;
    @SerializedName("helpUser")
    @Expose
    private String helpUser;
    @SerializedName("helpDriver")
    @Expose
    private String helpDriver;
    @SerializedName("driver_terms_and_condition")
    @Expose
    private String driverTermsAndCondition;
    @SerializedName("driver_price_list")
    @Expose
    private String driverPriceList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRadius() {
        return radius;
    }

    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    public Double getTowmeApplicationCommission() {
        return towmeApplicationCommission;
    }

    public void setTowmeApplicationCommission(Double towmeApplicationCommission) {
        this.towmeApplicationCommission = towmeApplicationCommission;
    }

    public Integer getTowmeCashPaymentCommission() {
        return towmeCashPaymentCommission;
    }

    public void setTowmeCashPaymentCommission(Integer towmeCashPaymentCommission) {
        this.towmeCashPaymentCommission = towmeCashPaymentCommission;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getBookingTypeInfo() {
        return bookingTypeInfo;
    }

    public void setBookingTypeInfo(String bookingTypeInfo) {
        this.bookingTypeInfo = bookingTypeInfo;
    }

    public String getBookingAccidentInfo() {
        return bookingAccidentInfo;
    }

    public void setBookingAccidentInfo(String bookingAccidentInfo) {
        this.bookingAccidentInfo = bookingAccidentInfo;
    }

    public String getBookingBreakdownInfo() {
        return bookingBreakdownInfo;
    }

    public void setBookingBreakdownInfo(String bookingBreakdownInfo) {
        this.bookingBreakdownInfo = bookingBreakdownInfo;
    }

    public String getBookingContactInfo() {
        return bookingContactInfo;
    }

    public void setBookingContactInfo(String bookingContactInfo) {
        this.bookingContactInfo = bookingContactInfo;
    }

    public String getInviteHeading() {
        return inviteHeading;
    }

    public void setInviteHeading(String inviteHeading) {
        this.inviteHeading = inviteHeading;
    }

    public String getInviteText() {
        return inviteText;
    }

    public void setInviteText(String inviteText) {
        this.inviteText = inviteText;
    }

    public String getInviteCouponAmount() {
        return inviteCouponAmount;
    }

    public void setInviteCouponAmount(String inviteCouponAmount) {
        this.inviteCouponAmount = inviteCouponAmount;
    }

    public String getPrivacyPolicy() {
        return privacyPolicy;
    }

    public void setPrivacyPolicy(String privacyPolicy) {
        this.privacyPolicy = privacyPolicy;
    }

    public String getTermsAndCondition() {
        return termsAndCondition;
    }

    public void setTermsAndCondition(String termsAndCondition) {
        this.termsAndCondition = termsAndCondition;
    }

    public String getHelpUser() {
        return helpUser;
    }

    public void setHelpUser(String helpUser) {
        this.helpUser = helpUser;
    }

    public String getHelpDriver() {
        return helpDriver;
    }

    public void setHelpDriver(String helpDriver) {
        this.helpDriver = helpDriver;
    }

    public String getDriverTermsAndCondition() {
        return driverTermsAndCondition;
    }

    public void setDriverTermsAndCondition(String driverTermsAndCondition) {
        this.driverTermsAndCondition = driverTermsAndCondition;
    }

    public String getDriverPriceList() {
        return driverPriceList;
    }

    public void setDriverPriceList(String driverPriceList) {
        this.driverPriceList = driverPriceList;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }
}