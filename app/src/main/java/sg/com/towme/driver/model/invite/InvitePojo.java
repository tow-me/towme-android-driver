
package sg.com.towme.driver.model.invite;


import sg.com.towme.driver.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InvitePojo extends BaseModel {

    @SerializedName("data")
    @Expose
    private InviteData data;

    public InviteData getData() {
        return data;
    }

    public void setData(InviteData data) {
        this.data = data;
    }
}
