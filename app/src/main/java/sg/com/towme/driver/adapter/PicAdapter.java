package sg.com.towme.driver.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import sg.com.towme.driver.R;
import sg.com.towme.driver.listener.RecyclerViewClickListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PicAdapter extends RecyclerView.Adapter<PicAdapter.ViewHolder> {

    Context context;
    boolean isAdd = false;

    public void setCallback(RecyclerViewClickListener callback) {
        this.callback = callback;
    }

    RecyclerViewClickListener callback;
    List<String> picList;

    public PicAdapter(Context context, List<String> picList, boolean isAdd) {
        this.context = context;
        this.picList = picList;
        this.isAdd = isAdd;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.row_pic, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Glide.with(context).load(picList.get(position)).placeholder(R.drawable.placeholder).into(holder.imageview);
        if (isAdd) {
            holder.imgClose.setVisibility(View.VISIBLE);
            holder.imgClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callback.onClick(holder.imgClose, position, null);
                }
            });
        } else {
            holder.imgClose.setVisibility(View.GONE);
            holder.imageview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callback.onClick(holder.imageview, position, null);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return picList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageview)
        ImageView imageview;
        @BindView(R.id.imgClose)
        ImageView imgClose;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

}
