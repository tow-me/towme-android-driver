package sg.com.towme.driver.Dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;

import sg.com.towme.driver.R;
import sg.com.towme.driver.listener.iDatePickerListner;
import sg.com.towme.driver.listener.iDateTimePickerClick;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by vibhiksha on 21/2/20
 */
public class DateTimePickerDialog extends BaseDialog {

    iDatePickerListner callback;
    @BindView(R.id.btnSave)
    AppCompatButton btnSave;

    private void setCallback(iDatePickerListner callback) {
        this.callback = callback;
    }

    public static DateTimePickerDialog newInstance(iDatePickerListner callback) {
        DateTimePickerDialog fragment = new DateTimePickerDialog();
        fragment.setCallback(callback);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
//        getDialog().getWindow().setLayout((R.dimen._100sdp), ViewGroup.LayoutParams.WRAP_CONTENT);
//        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.90);
        int height = ViewGroup.LayoutParams.WRAP_CONTENT;
//        int width = ViewGroup.LayoutParams.WRAP_CONTENT;
//        int height = (int)(getResources().getDisplayMetrics().heightPixels*0.80);

        getDialog().getWindow().setLayout(width, height);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_date_time_picker, container, false);
        ButterKnife.bind(this, view);

        initUI();
        return view;
    }

    private void initUI() {
    }

    public void openDateTimePicker() {
        iDateTimePickerClick iDateTimePickerClick = new iDateTimePickerClick() {
            @Override
            public void onPickDate(String dateTime) {
            }

            @Override
            public void onPickDateTime(String s, String apiDataTimeFormat) {

            }
        };
        appNavigationActivity.dateTimePicker(iDateTimePickerClick);
    }
}
