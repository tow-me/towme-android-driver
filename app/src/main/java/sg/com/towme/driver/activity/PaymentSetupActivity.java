package sg.com.towme.driver.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import sg.com.towme.driver.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class PaymentSetupActivity extends ToolBarActivity {

    private static final int REQUEST_CODE_PAYMENT_SETUP = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_payment_setup);
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);
        iniUI();
    }

    private void iniUI() {
        setToolbarTitle(getString(R.string.payment_setup));
    }


    @OnClick({R.id.btn_connect})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_connect:
                startActivityForResult(new Intent(this, ConnectAccountActivity.class), REQUEST_CODE_PAYMENT_SETUP);
                break;
        }
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) return;
        if (requestCode != REQUEST_CODE_PAYMENT_SETUP) return;
        this.finish();
    }
}
