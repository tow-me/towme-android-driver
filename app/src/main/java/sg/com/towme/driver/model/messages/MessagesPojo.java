
package sg.com.towme.driver.model.messages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MessagesPojo {

    @SerializedName("status_code")
    @Expose
    private Integer statusCode;

    @SerializedName("data")
    @Expose
    private MessagesData data = null;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public MessagesData getData() {
        return data;
    }

    public void setData(MessagesData data) {
        this.data = data;
    }

}
