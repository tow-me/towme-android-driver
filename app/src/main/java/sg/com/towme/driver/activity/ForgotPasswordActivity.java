package sg.com.towme.driver.activity;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;

import sg.com.towme.driver.R;
import sg.com.towme.driver.constant.AppConstant;
import sg.com.towme.driver.enumeration.Screens;
import sg.com.towme.driver.listener.CheckNetworkListener;
import sg.com.towme.driver.model.BaseModel;
import sg.com.towme.driver.model.user.UserPojo;
import sg.com.towme.driver.network.IResponseCallback;
import sg.com.towme.driver.network.NetworkCall;
import sg.com.towme.driver.network.Parameter;
import sg.com.towme.driver.utility.AppValidation;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

public class ForgotPasswordActivity extends ToolBarActivity {

    @BindView(R.id.edtEmail)
    AppCompatEditText edtEmail;
    @BindView(R.id.btnGo)
    AppCompatButton btnGo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);

        iniUI();
    }

    private void iniUI() {
        setToolbarTitle(getString(R.string.forgot_password_));
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean isValidate() {
        if (AppValidation.isEmptyFieldValidate(edtEmail.getText().toString().trim())) {
            showSnackBar(edtEmail, getString(R.string.please_enter_phone_number));
            return false;
        }
        return true;
    }

    @OnClick(R.id.btnGo)
    public void onViewClicked() {
        hideKeyBoard();
        CheckNetworkListener callback = () -> {
            if (isValidate()) {
                showProgressDialog();
                callForgotPasswordApi();
            }
        };

        if (isNetworkAvailable(btnGo, callback)) {
            callback.onRetryClick();
        }
    }


    private void callForgotPasswordApi() {

        NetworkCall.getInstance().forgotPassword(getParam(), new IResponseCallback<UserPojo>() {
            @Override
            public void success(UserPojo data) {
                hideProgressDialog();
                showSnackBar(btnGo, data.getMessage());
                if (data.getCode() == 1)
                    openOTPActivity(Screens.FORGOT_PASSWORD, data.getData());
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                hideProgressDialog();
                showSnackBar(btnGo, baseModel.getMessage());
            }

            @Override
            public void onError(Call<UserPojo> responseCall, Throwable T) {
                hideProgressDialog();
                showSnackBar(btnGo, getString(R.string.error_message));
            }
        });

    }

    private HashMap<String, String> getParam() {
        HashMap<String, String> param = new HashMap<>();
        param.put(Parameter.mobile_no, edtEmail.getText().toString().trim());
        param.put(Parameter.user_type, AppConstant.USER_TYPE);
        return param;
    }
}
