
package sg.com.towme.driver.model.facebook;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FacebookPicture {

    @SerializedName("data")
    @Expose
    private FacebookPictureData data;

    public FacebookPictureData getData() {
        return data;
    }

    public void setData(FacebookPictureData data) {
        this.data = data;
    }

}
