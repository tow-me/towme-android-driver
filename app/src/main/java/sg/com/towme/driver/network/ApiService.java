package sg.com.towme.driver.network;


import retrofit2.http.Path;
import retrofit2.http.Query;
import sg.com.towme.driver.model.BaseModel;
import sg.com.towme.driver.model.OnOffResponse;
import sg.com.towme.driver.model.UploadPojo;
import sg.com.towme.driver.model.booking.BookingListPojo;
import sg.com.towme.driver.model.brand.BrandPojo;
import sg.com.towme.driver.model.calendar.CalendarPojo;
import sg.com.towme.driver.model.config_.ConfigPojo;
import sg.com.towme.driver.model.invite.InvitePojo;
import sg.com.towme.driver.model.linkedin.LinkedinEmailPojo;
import sg.com.towme.driver.model.linkedin.LinkedinProfilePojo;
import sg.com.towme.driver.model.notification.NotificationPojo;
import sg.com.towme.driver.model.stripe.Balance;
import sg.com.towme.driver.model.transaction.TransactionData;
import sg.com.towme.driver.model.user.UserPojo;

import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.QueryMap;

public interface ApiService {

    @POST(NetworkURL.LOGIN)
    @FormUrlEncoded
    Call<UserPojo> postLoginData(@FieldMap HashMap<String, String> params);

    @Multipart
    @POST(NetworkURL.REGISTER)
    Call<UserPojo> registration(
            @Part MultipartBody.Part file,
            @PartMap HashMap<String, RequestBody> params);

    @Multipart
    @POST(NetworkURL.UPDATE_PROFILE)
    Call<UserPojo> updateProfile(
            @HeaderMap HashMap<String, String> header,
            @Part MultipartBody.Part file,
            @PartMap HashMap<String, RequestBody> params);

    @POST(NetworkURL.CHANGE_PASSWORD)
    @FormUrlEncoded
    Call<BaseModel> changePassword(@HeaderMap HashMap<String, String> header, @FieldMap HashMap<String, String> params);

    @POST(NetworkURL.CHANGE_COUNTRY)
    @FormUrlEncoded
    Call<BaseModel> changeCountry(@HeaderMap HashMap<String, String> header, @FieldMap HashMap<String, String> params);

    @GET(NetworkURL.LINKEDIN_PROFILE_DETAILS)
    Call<LinkedinProfilePojo> getLinkedinProfile(@HeaderMap HashMap<String, String> header);

    @GET(NetworkURL.LINKEDIN_EMAIL)
    Call<LinkedinEmailPojo> getLinkedinEmail(@HeaderMap HashMap<String, String> header);

    @POST(NetworkURL.LOGOUT)
    Call<BaseModel> logout(@HeaderMap HashMap<String, String> header);

    @POST(NetworkURL.UPDATE_LOCATION)
    @FormUrlEncoded
    Call<BaseModel> updateLocation(@HeaderMap HashMap<String, String> header, @FieldMap HashMap<String, String> params);

    @POST(NetworkURL.GET_NOTIFICATION)
    @FormUrlEncoded
    Call<NotificationPojo> getNotification(@HeaderMap HashMap<String, String> header, @FieldMap HashMap<String, String> params);

    @POST(NetworkURL.OTP_VERIFICATION)
    @FormUrlEncoded
    Call<UserPojo> OTPVerification(@FieldMap HashMap<String, String> params);

    @POST(NetworkURL.RESEND_OTP)
    @FormUrlEncoded
    Call<BaseModel> resendOTP(@FieldMap HashMap<String, String> params);

    @POST(NetworkURL.FORGOT_PASSWORD)
    @FormUrlEncoded
    Call<UserPojo> forgotPassword(@FieldMap HashMap<String, String> params);

    @POST(NetworkURL.RESET_PASSWORD)
    @FormUrlEncoded
    Call<UserPojo> resetPassword(@FieldMap HashMap<String, String> params);

    @GET(NetworkURL.GET_PROFILE)
    Call<UserPojo> getProfile(@HeaderMap HashMap<String, String> header);

    @GET(NetworkURL.USER_PROFILE)
    Call<UserPojo> userProfile(@HeaderMap HashMap<String, String> header);

    @POST(NetworkURL.UPDATE_TOWTRUCK)
    @FormUrlEncoded
    Call<BaseModel> updateTowtruck(@HeaderMap HashMap<String, String> header, @FieldMap HashMap<String, String> params);

    @POST(NetworkURL.UPDATE_FLATEBED)
    @FormUrlEncoded
    Call<BaseModel> updateFlatebed(@HeaderMap HashMap<String, String> header, @FieldMap HashMap<String, String> params);

    @GET(NetworkURL.GET_INVITE_CODE)
    Call<InvitePojo> getInviteCode(@HeaderMap HashMap<String, String> header);

    @POST(NetworkURL.BOOKING_LIST)
    @FormUrlEncoded
    Call<BookingListPojo> bookingList(@Field(Parameter.booking_status) List<Integer> status, @FieldMap HashMap<String, String> params, @HeaderMap HashMap<String, String> header);

    @Multipart
    @POST(NetworkURL.UPLOAD)
    Call<UploadPojo> upload(
            @Part MultipartBody.Part file,
            @HeaderMap HashMap<String, String> header);

    @GET(NetworkURL.GET_CALENDAR)
    Call<CalendarPojo> getCalendar(@HeaderMap HashMap<String, String> header, @QueryMap HashMap<String, String> params);


    @POST(NetworkURL.BALANCE)
    Call<Balance> balance(@HeaderMap HashMap<String, String> header);

    @POST(NetworkURL.TRANSFER)
    @FormUrlEncoded
    Call<BaseModel> transfer(@HeaderMap HashMap<String, String> header, @FieldMap HashMap<String, String> params);

    @POST(NetworkURL.HISTORY)
    Call<BaseModel> history(@HeaderMap HashMap<String, String> header);

    @GET(NetworkURL.TRANSACTION_HISTORY)
    Call<List<TransactionData>> getTransactionHistory(@HeaderMap HashMap<String, String> header, @QueryMap HashMap<String, String> params);

    @GET(NetworkURL.GET_BRANDS)
    Call<BrandPojo> getBrandList(@Query("category") String categoryId);

    @POST(NetworkURL.REFRESH_DEVICE)
    @FormUrlEncoded
    Call<BaseModel> refreshDevice(@HeaderMap HashMap<String, String> header, @FieldMap HashMap<String, String> params);


    @GET(NetworkURL.CONFIGURATION)
    Call<ConfigPojo> getConfiguration();

    @POST(NetworkURL.CHANGE_TOW_TYPE)
    @FormUrlEncoded
    Call<BaseModel> changeTowType(@HeaderMap HashMap<String, String> header, @FieldMap HashMap<String, String> params);

    @POST(NetworkURL.EPHEMERAL_KEY)
    Call<ResponseBody> createKey(@Body HashMap<String, String> param);

    @POST(NetworkURL.PAYMENT_INTENT)
    Call<ResponseBody> paymentIntent(@Body HashMap<String, Object> param);

    @POST(NetworkURL.CAPTURE_INTENT)
    Call<ResponseBody> captureIntent(@Body HashMap<String, Object> param);

    @POST(NetworkURL.CONFIRM_INTENT)
    Call<ResponseBody> confirmIntent(@Body HashMap<String, Object> param);

    @POST(NetworkURL.CHANGE_ONLINE_STATUS)
    @FormUrlEncoded
    Call<OnOffResponse> changeOnlineStatus(@HeaderMap HashMap<String, String> header, @FieldMap HashMap<String, String> params);

    @POST(NetworkURL.ACCEPT_MALAYSIA)
    Call<ResponseBody> acceptMalaysia(@HeaderMap HashMap<String, String> header, @Path("booking") String booking);

    @POST(NetworkURL.REJECT_MALAYSIA)
    Call<ResponseBody> rejectMalaysia(@HeaderMap HashMap<String, String> header, @Path("booking") String booking);

    @POST(NetworkURL.CANCEL_MALAYSIA)
    Call<ResponseBody> cancelMalaysia(@HeaderMap HashMap<String, String> header, @Path("booking") String booking);

    @POST(NetworkURL.START_MALAYSIA)
    @FormUrlEncoded
    Call<ResponseBody> startMalaysia(@HeaderMap HashMap<String, String> header, @Path("booking") String booking, @FieldMap HashMap<String, String> params);

    @POST(NetworkURL.COMPLETE_MALAYSIA)
    @FormUrlEncoded
    Call<ResponseBody> completeMalaysia(@HeaderMap HashMap<String, String> header, @Path("booking") String booking, @FieldMap HashMap<String, String> params);

    @POST(NetworkURL.ACCEPT_SG)
    Call<ResponseBody> acceptSG(@HeaderMap HashMap<String, String> header, @Path("booking") String booking);

    @POST(NetworkURL.REJECT_SG)
    Call<ResponseBody> rejectSG(@HeaderMap HashMap<String, String> header, @Path("booking") String booking);

    @POST(NetworkURL.CANCEL_SG)
    Call<ResponseBody> cancelSG(@HeaderMap HashMap<String, String> header, @Path("booking") String booking);

    @POST(NetworkURL.START_SG)
    @FormUrlEncoded
    Call<ResponseBody> startSG(@HeaderMap HashMap<String, String> header, @Path("booking") String booking, @FieldMap HashMap<String, String> params);

    @POST(NetworkURL.COMPLETE_SG)
    @FormUrlEncoded
    Call<ResponseBody> completeSG(@HeaderMap HashMap<String, String> header, @Path("booking") String booking, @FieldMap HashMap<String, String> params);
}


