package sg.com.towme.driver.Dialog;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import sg.com.towme.driver.R;
import sg.com.towme.driver.listener.iDialogButtonClick;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Dhara Golakiya on 17-04-2018.
 */

public class ConfirmationDialog extends BaseDialog {

    @BindView(R.id.txtMessage)
    TextView txtMessage;
    @BindView(R.id.txtYes)
    TextView txtYes;
    @BindView(R.id.txtNo)
    TextView txtNo;
    Unbinder unbinder;
    private iDialogButtonClick callBack;
    private String positiveText;
    private String negative;
    private String message;

    public void setPositiveText(String positiveText) {
        this.positiveText = positiveText;
    }

    public void setNegative(String negative) {
        this.negative = negative;
    }

    public void setCallBack(iDialogButtonClick callBack) {
        this.callBack = callBack;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static ConfirmationDialog newInstance(iDialogButtonClick callBack, String message, String positiveText, String nagativeText) {
        ConfirmationDialog fragment = new ConfirmationDialog();
        fragment.setMessage(message);
        fragment.setCallBack(callBack);
        fragment.setPositiveText(positiveText);
        fragment.setNegative(nagativeText);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_confirmation, container, false);

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        txtMessage.setText(message);
        txtYes.setText(positiveText);
        if (negative == null || negative.isEmpty())
            txtNo.setVisibility(View.GONE);
        else
            txtNo.setText(negative);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.txtYes, R.id.txtNo})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtYes:
                if (callBack != null)
                    callBack.positiveClick(null);
                dismiss();
                break;
            case R.id.txtNo:
                if (callBack != null)
                    callBack.negativeClick();
                dismiss();
                break;
        }
    }
}
