package sg.com.towme.driver.activity;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import sg.com.towme.driver.R;
import sg.com.towme.driver.constant.AppConstant;
import sg.com.towme.driver.listener.CheckNetworkListener;
import sg.com.towme.driver.model.BaseModel;
import sg.com.towme.driver.model.user.UserPojo;
import sg.com.towme.driver.network.IResponseCallback;
import sg.com.towme.driver.network.NetworkCall;
import sg.com.towme.driver.utility.AppHelper;
import sg.com.towme.driver.utility.AppValidation;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

public class TransferMoneyActivity extends ToolBarActivity {


    @BindView(R.id.edt_amount)
    AppCompatEditText edt_amount;
    @BindView(R.id.btn_transfer)
    AppCompatButton btn_transfer;
    @BindView(R.id.imgArrow)
    AppCompatImageView imgArrow;
    @BindView(R.id.lblFrom)
    AppCompatTextView lblFrom;
    @BindView(R.id.lblCashWallet)
    AppCompatTextView lblCashWallet;
    @BindView(R.id.txtBalance)
    AppCompatTextView txtBalance;
    @BindView(R.id.rlBalance)
    RelativeLayout rlBalance;
    @BindView(R.id.lblTo)
    AppCompatTextView lblTo;
    @BindView(R.id.lblBank)
    AppCompatTextView lblBank;


    //    private double available_balance = 0;
    private Double balance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_transfer_money);
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);
        iniUI();

        setBalance();

    }

    private void setBalance() {
        balance = Double.parseDouble(AppHelper.getInstance().getUserDetails().getBalance().trim()) / 100;
        txtBalance.setText(balance + "");
    }

    private void iniUI() {
        setToolbarTitle(getString(R.string.transfer));
//        getBalance();
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean isValidate() {
        if (AppValidation.isEmptyFieldValidate(edt_amount.getText().toString().trim())) {
            showSnackBar(edt_amount, getString(R.string.please_enter_valid_amount));
            return false;
        }
        return true;
    }

    private static double parseDouble(String raw) {
        try {
            return Double.parseDouble(raw);
        } catch (Exception e) {
            // Exception goes here... Handle if you want.
        }
        return 0;
    }

    @OnClick(R.id.btn_transfer)
    public void onClick(View view) {
        hideKeyBoard();
        double amount = parseDouble(edt_amount.getText().toString().trim());
        if (balance == 0 || balance < amount) {
            openConfirmationDialog(null, getString(R.string.transfer_balance_could_not_be_greater_than_available_balance), getString(R.string.ok), null);
        } else if (amount < 50) {
            openConfirmationDialog(null, "Minimum amount for payment request is 50 SGD.", getString(R.string.ok), null);
        } else {
            if (edt_amount.getText().toString().trim().length() > 0) {
                CheckNetworkListener callback = () -> {
                    if (isValidate()) {
                        showProgressDialog();
                        transfer();
                    }
                };
                if (isNetworkAvailable(btn_transfer, callback)) {
                    callback.onRetryClick();
                }
            }
        }
    }

   /* private void getBalance() {
        NetworkCall.getInstance().getBalance(new IResponseCallback<Balance>() {
            @Override
            public void success(Balance data) {
                if (data == null) {
                    txt_available_balance.setText("SGD 0.00");
                    txt_pending_balance.setText("SGD 0.00");
                } else {
                    TransferMoneyActivity.this.balance = data;
                    available_balance = data.availableBalance();
                    txt_available_balance.setText(String.format(Locale.ENGLISH, "%.2f", data.availableBalance()));
                    txt_pending_balance.setText(String.format(Locale.ENGLISH, "%.2f", data.pendingBalance()));
                }
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                txt_available_balance.setText("SGD 0.00");
                txt_pending_balance.setText("SGD 0.00");
            }


            @Override
            public void onError(Call<Balance> responseCall, Throwable T) {
                txt_available_balance.setText("SGD 0.00");
                txt_pending_balance.setText("SGD 0.00");
            }
        });
    }*/

    private void transfer() {
        HashMap<String, String> param = new HashMap<>();
//        param.put("driver_id", String.valueOf(AppHelper.getInstance().getUserDetails().getUserid()));
//        param.put("destination", AppHelper.getInstance().getUserDetails().getStripe_account_id());

        double amount = Double.parseDouble(edt_amount.getText().toString().trim()) * 100;
        int amt = (int) amount;
        param.put("amount", String.valueOf(amt));
        param.put("currency", AppConstant.CURRENCY);

        NetworkCall.getInstance().transfer(param, new IResponseCallback<BaseModel>() {
            @Override
            public void success(BaseModel data) {
                callGetProfileApi();
                showSnackBar(btn_transfer, data.getMessage());
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                hideProgressDialog();
                showSnackBar(btn_transfer, baseModel.getMessage());
            }

            @Override
            public void onError(Call<BaseModel> responseCall, Throwable T) {
                hideProgressDialog();
                showSnackBar(btn_transfer, getString(R.string.error_message));
            }
        });

    }

    private void callGetProfileApi() {
        NetworkCall.getInstance().getProfile(new IResponseCallback<UserPojo>() {
            @Override
            public void success(UserPojo data) {
                hideProgressDialog();
                AppHelper.getInstance().setUserDetails(data.getData());

                setBalance();
            }

            @Override
            public void onFailure(BaseModel baseModel) {

            }

            @Override
            public void onError(Call<UserPojo> responseCall, Throwable T) {

            }
        });
    }

}
