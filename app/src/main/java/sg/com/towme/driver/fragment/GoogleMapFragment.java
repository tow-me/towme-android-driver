package sg.com.towme.driver.fragment;


import android.animation.ValueAnimator;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.Polyline;
import sg.com.towme.driver.R;
import sg.com.towme.driver.mapNavigation.FetchUrl;
import sg.com.towme.driver.mapNavigation.GPSTracker;
import sg.com.towme.driver.mapNavigation.MapNavigationPathCallback;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import sg.com.towme.driver.model.booking.CreateBooking;
import sg.com.towme.driver.utility.DebugLog;

import java.util.ArrayList;
import java.util.List;

import static com.facebook.FacebookSdk.getApplicationContext;

public class GoogleMapFragment extends BaseFragment {

    GoogleMap map;

    private ArrayList<LatLng> currentPoints;

   /* Double sourceLatitude = 22.019706;
    Double sourceLongitude = 71.704056;
    Double destLatitude = 21.9690312;
    Double destLongitude = 71.5611568;*/

    Double sourceLatitude;
    Double sourceLongitude;
    Double destLatitude;
    Double destLongitude;

    LatLng oldlocation;
    Marker mk = null;
    protected Location mLastLocation;

    Double driverlat;
    Double driverlong;
    Boolean isMarkerRotating = false;
    LatLng lastLatLng;
    GPSTracker gpsTracker;
    Polyline currentLine;
    Polyline firstLine;
    Double currentLatitude = 0.0;
    Double currentLongitude = 0.0;

    protected boolean isRecenter = true, isForceMapMove = false;

    protected void initMap(OnMapReadyCallback callback) {
        currentPoints = new ArrayList<LatLng>();

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(callback);
    }

    public void drawPath(Double sourceLatitude, Double sourceLongitude, Double destLatitude, Double destLongitude, int sourceMarker, int destMarker) {

        removePolyline();
//        map.clear();

        this.sourceLatitude = sourceLatitude;
        this.sourceLongitude = sourceLongitude;
        this.destLatitude = destLatitude;
        this.destLongitude = destLongitude;

        Log.e("draw path", "...");
        try {
            new FetchUrl(getApplicationContext(),
                    new LatLng(sourceLatitude, sourceLongitude),
                    new LatLng(destLatitude, destLongitude),
                    new MapNavigationPathCallback() {
                        @Override
                        public void onSuccessGetPolyLine(PolylineOptions lineOptions, ArrayList<LatLng> points) {
                            Log.e("Add polyline", "...");
//                            lineOptions.color(Color.RED);
                            lineOptions.color(Color.parseColor("#46aa50"));
                            firstLine = map.addPolyline(lineOptions);
                            currentPoints = points;
                            setUpLocationPointer(sourceMarker, destMarker);
                        }

                        @Override
                        public void onFailedToGetPolyLine() {

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void setUpLocationPointer(int sourceMarker, int destMarker) {
        LatLngBounds.Builder bld = new LatLngBounds.Builder();

        LatLng latLngSource = new LatLng(sourceLatitude, sourceLongitude);

        lastLatLng = latLngSource;
        bld.include(latLngSource);
        mk = map.addMarker(new MarkerOptions().position(latLngSource)
                .icon(BitmapDescriptorFactory.fromResource(sourceMarker)).flat(true));
//        map.addMarker(new MarkerOptions().position(latLngSource).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_driver_marker)).title("Ningala"));

        LatLng latLngDestination = new LatLng(destLatitude, destLongitude);
        bld.include(latLngDestination);
        map.addMarker(new MarkerOptions().position(latLngDestination).icon(BitmapDescriptorFactory.fromResource(destMarker)).title(""));

        LatLngBounds bounds = bld.build();
        map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 16));
        oldlocation = latLngSource;

    }

    public void againAndAgain(Location location) {
        driverlat = location.getLatitude();
        driverlong = location.getLongitude();

        if (mk != null) {
            mk.remove();
        }

        if (null != driverlat && null != driverlong && null != oldlocation) {
            LatLng latlong = new LatLng(driverlat, driverlong);
            float bearing = (float) bearingBetweenLocations(oldlocation, latlong);
            if (mk != null)
                rotateMarker(mk, bearing);
            mk = map.addMarker(new MarkerOptions().position(new LatLng(driverlat, driverlong))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_driver_marker)));
            isMarkerRotating = true;
/*
            showSpeed(Double.valueOf(loadModel.getData().getSpeed()));
            showDestinationDistance(String.valueOf(loadModel.getData().getLatitude()),
                    String.valueOf(loadModel.getData().getLongitude()),
                    String.valueOf(loadModel.getData().getToLat()),
                    String.valueOf(loadModel.getData().getToLong()),
                    Double.valueOf(loadModel.getData().getSpeed()));*/

            lastLatLng = latlong;

            currentPoints.add(lastLatLng);
            redrawLine();

            if (mk != null) {
                animateMarker(mLastLocation, mk);
            }

//            mapRecenter();
            // clear and add new service providers
            if (isRecenter) {
                mapRecenter();
//                addAndRemoveServiceProviders(loadModel.getData().getServiceProvider());
            }
        }
    }

    // add service providers markers
    private void addAndRemoveServiceProviders(Double latitude, Double longitude) {
        // remove all markers
   /*     for (Marker marker : spMarkers) {
            marker.remove();
        }

        // add new marker

        Marker mk = map.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .title("Market update"));
        //.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin3))
        spMarkers.add(mk);*/


    }

    protected void mapRecenter() {
        DebugLog.e("mapRecenter : isForceMove - " + isForceMapMove + "  isRecenter - " + isRecenter);
        isForceMapMove = false;
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(lastLatLng, 16));
    }

    private double bearingBetweenLocations(LatLng latLng1, LatLng latLng2) {

        DebugLog.e("bearingBetweenLocations" + latLng1.toString() + "    " + latLng2.toString());
        double PI = 3.14159;
        double lat1 = latLng1.latitude * PI / 180;
        double long1 = latLng1.longitude * PI / 180;
        double lat2 = latLng2.latitude * PI / 180;
        double long2 = latLng2.longitude * PI / 180;

        double dLon = (long2 - long1);

        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
                * Math.cos(lat2) * Math.cos(dLon);

        double brng = Math.atan2(y, x);

        brng = Math.toDegrees(brng);
        brng = (brng + 360) % 360;

        return brng;
    }

    private void rotateMarker(final Marker marker, final float toRotation) {
        if (!isMarkerRotating) {
            final Handler handler = new Handler();
            final long start = SystemClock.uptimeMillis();
            final float startRotation = marker.getRotation();
            final long duration = 2000;

            final Interpolator interpolator = new LinearInterpolator();

            handler.post(new Runnable() {
                @Override
                public void run() {
                    isMarkerRotating = true;

                    long elapsed = SystemClock.uptimeMillis() - start;
                    float t = interpolator.getInterpolation((float) elapsed / duration);

                    float rot = t * toRotation + (1 - t) * startRotation;

                    float bearing = -rot > 180 ? rot / 2 : rot;

                    marker.setRotation(bearing);

                    if (t < 1.0) {
                        // Post again 16ms later.
                        handler.postDelayed(this, 16);
                    } else {
                        isMarkerRotating = false;
                    }
                }
            });
        }
    }

    private void redrawLine() {

        new FetchUrl(getApplicationContext(),
                new LatLng(sourceLatitude, sourceLongitude),
                new LatLng(destLatitude, destLongitude),
                new LatLng(driverlat, driverlong),
                new MapNavigationPathCallback() {
                    @Override
                    public void onSuccessGetPolyLine(PolylineOptions lineOptions, ArrayList<LatLng> points) {
                        if (currentLine != null)
                            currentLine.remove();

                        Log.d("Redraw line", "onSuccessGetPolyLine");
//                        lineOptions.color(Color.BLUE);
                        lineOptions.color(Color.parseColor("#46aa50"));
                        currentLine = map.addPolyline(lineOptions);
                    }

                    @Override
                    public void onFailedToGetPolyLine() {
                        Log.d("Redraw line", "onFailedToGetPolyLine");
                    }
                });


    }

    public void animateMarker(final Location destination, final Marker marker) {
        if (marker != null) {
            final LatLng startPosition = marker.getPosition();
//            final LatLng endPosition = new LatLng(destination.getLatitude(), destination.getLongitude());
            final LatLng endPosition = lastLatLng;

            final float startRotation = marker.getRotation();

            final LatLngInterpolator latLngInterpolator = new LatLngInterpolator.LinearFixed();
            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(1000); // duration 1 second
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    try {
                        float v = animation.getAnimatedFraction();
                        LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition);
                        marker.setPosition(newPosition);
                        marker.setRotation(computeRotation(v, startRotation, destination.getBearing()));
                    } catch (Exception ex) {
                        // I don't care atm..
                    }
                }
            });

            valueAnimator.start();
        }
    }

    private interface LatLngInterpolator {
        LatLng interpolate(float fraction, LatLng a, LatLng b);

        class LinearFixed implements LatLngInterpolator {
            @Override
            public LatLng interpolate(float fraction, LatLng a, LatLng b) {
                double lat = (b.latitude - a.latitude) * fraction + a.latitude;
                double lngDelta = b.longitude - a.longitude;
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360;
                }
                double lng = lngDelta * fraction + a.longitude;
                return new LatLng(lat, lng);
            }
        }
    }

    private static float computeRotation(float fraction, float start, float end) {
        float normalizeEnd = end - start; // rotate start to 0
        float normalizedEndAbs = (normalizeEnd + 360) % 360;

        float direction = (normalizedEndAbs > 180) ? -1 : 1; // -1 = anticlockwise, 1 = clockwise
        float rotation;
        if (direction > 0) {
            rotation = normalizedEndAbs;
        } else {
            rotation = normalizedEndAbs - 360;
        }

        float result = fraction * rotation + start;
        return (result + 360) % 360;
    }

    Marker markerCurrentLocation;

    public void addMarker(Double latitude, Double longitude) {
        if (markerCurrentLocation != null)
            markerCurrentLocation.remove();
        LatLng sydney = new LatLng(latitude, longitude);
        lastLatLng = sydney;
        markerCurrentLocation = map.addMarker(new MarkerOptions()
                .position(sydney)
                .title("You are Here"));
        markerCurrentLocation.setTag("Current Location");
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 16));
    }

    public void removeCurrentLocationMarker() {
        if (markerCurrentLocation != null)
            markerCurrentLocation.remove();
    }

    public void removePolyline() {
        if (currentLine != null)
            currentLine.remove();
        if (firstLine != null)
            firstLine.remove();
        map.clear();
    }

    public void openWazeMap() {
      /*  String uri = "geo: latitude,longtitude ?q= latitude,longtitude";
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(uri)));*/

        try {
            // Launch Waze
            String mapRequest = "https://waze.com/ul?q=" + destLatitude + "," + destLongitude + "&navigate=yes&zoom=17";
            Uri gmmIntentUri = Uri.parse(mapRequest);
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.waze");
            startActivity(mapIntent);

        } catch (ActivityNotFoundException e) {
            // If Waze is not installed, open it in Google Play
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.waze"));
            startActivity(intent);
        }
    }

    public void openGoogelMap() {
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?saddr=" + sourceLatitude + "," + sourceLongitude + "&daddr=" + destLatitude + "," + destLongitude));
        startActivity(intent);
    }

    protected void addAvailableJobsMarker(List<CreateBooking> list) {
        try {

            map.clear();
            LatLngBounds.Builder bld = new LatLngBounds.Builder();

            for (int i = 0; i < list.size(); i++) {
                try {

                    LatLng latLng = new LatLng(Double.parseDouble(list.get(i).getLatitude()), Double.parseDouble(list.get(i).getLongitude()));
                    bld.include(latLng);
                    Marker marker = map.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_red_pin)).title(""));
                    marker.setTag(i);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            if (currentLatitude > 0 || currentLongitude > 0) {
                LatLng latLng = new LatLng(currentLatitude, currentLongitude);
                bld.include(latLng);
                markerCurrentLocation = map.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title("You are Here"));
                markerCurrentLocation.setTag("Current Location");
            }

            LatLngBounds bounds = bld.build();
            map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 16));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}