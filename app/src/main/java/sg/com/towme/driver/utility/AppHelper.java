package sg.com.towme.driver.utility;

import android.app.AlertDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Space;

import com.bumptech.glide.Glide;

import sg.com.towme.driver.R;
import sg.com.towme.driver.constant.SharedPrefConstant;
import sg.com.towme.driver.model.Environment;
import sg.com.towme.driver.model.config_.ConfigDetails;
import sg.com.towme.driver.model.user.UserData;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashSet;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Dhara on 01/01/2019.
 */

public class AppHelper {

    private static final AppHelper ourInstance = new AppHelper();

    public static AppHelper getInstance() {
        return ourInstance;
    }

    private AppHelper() {
    }

    public boolean isLogin() {
        return PreferanceHelper.getInstance().getBoolean(SharedPrefConstant.IS_LOGIN);
    }

    public void setLogin(boolean isLogin) {
        PreferanceHelper.getInstance().putBoolean(SharedPrefConstant.IS_LOGIN, isLogin);
    }

    public void setCountry(String country) {
        PreferanceHelper.getInstance().putString(SharedPrefConstant.COUNTRY, country);
    }

    public String getCountry() {
        return PreferanceHelper.getInstance().getString(SharedPrefConstant.COUNTRY);
    }


    public String formatCurrency(Double input) {
        NumberFormat formatter = new DecimalFormat("#0.00");
        String output = formatter.format(input);
        return output;
    }

    public void showAlertDialog(Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setMessage(message)
                .setCancelable(false)
                .setNeutralButton(context.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        AlertDialog alert = builder.create();
        alert.setTitle(title);
        alert.show();

        final Button neutralButton = alert.getButton(AlertDialog.BUTTON_NEUTRAL);

        LinearLayout view = (LinearLayout) neutralButton.getParent();
        Space space = (Space) view.getChildAt(1);
        space.setVisibility(View.GONE);

        neutralButton.setTextColor(context.getResources().getColor(R.color.colorTextBlack));
        LinearLayout.LayoutParams neutralButtonLL = (LinearLayout.LayoutParams) neutralButton.getLayoutParams();
        neutralButtonLL.width = LinearLayout.LayoutParams.MATCH_PARENT;
        neutralButtonLL.gravity = Gravity.END;
        neutralButton.setLayoutParams(neutralButtonLL);

    }

    // save image in local storage
    public static File saveToInternalStorage(Context context, Bitmap bitmapImage) {
        ContextWrapper cw = new ContextWrapper(context);
// path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("ImageDir", Context.MODE_PRIVATE);
// Create imageDir
        File mypath = new File(directory, "image_" + System.currentTimeMillis() + ".jpg");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
// Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return mypath;
    }

    public static void loadImage(Context context, CircleImageView imgProfilePic, int placeholder, String image) {
        Glide.with(context).load(image).placeholder(placeholder).into(imgProfilePic);
    }

    public void setCookie(HashSet<String> cookie) {
        PreferanceHelper.getInstance().putStringSet(SharedPrefConstant.COOKIE, cookie);
    }

    public void setEnvironment(Environment userData) {
        Gson gson = new Gson();
        PreferanceHelper.getInstance().putString(SharedPrefConstant.ENV_DETAILS, gson.toJson(userData));
    }

    public Environment getEnvironment() {
        Gson gson = new Gson();
        return gson.fromJson(PreferanceHelper.getInstance().getString(SharedPrefConstant.ENV_DETAILS), Environment.class);
    }

    public void setUserDetails(UserData userData) {
        Gson gson = new Gson();
        PreferanceHelper.getInstance().putString(SharedPrefConstant.USER_DETAILS, gson.toJson(userData));
    }

    public HashSet<String> getCookie() {
        return PreferanceHelper.getInstance().getStringset(SharedPrefConstant.COOKIE);
    }

    public UserData getUserDetails() {
        Gson gson = new Gson();
        return gson.fromJson(PreferanceHelper.getInstance().getString(SharedPrefConstant.USER_DETAILS), UserData.class);
    }

    public void setUserToken(String token) {
        PreferanceHelper.getInstance().putString(SharedPrefConstant.USER_TOKEN, token);
    }

    public String getUserToken() {
        return PreferanceHelper.getInstance().getString(SharedPrefConstant.USER_TOKEN);
    }

    public void setLatitude(String latitude) {
        PreferanceHelper.getInstance().putString(SharedPrefConstant.LATITUDE, latitude);
    }

    public String getLatitude() {
        return PreferanceHelper.getInstance().getString(SharedPrefConstant.LATITUDE);
    }

    public void setLongitude(String longitude) {
        PreferanceHelper.getInstance().putString(SharedPrefConstant.LONGITUDE, longitude);
    }

    public String getLongitude() {
        return PreferanceHelper.getInstance().getString(SharedPrefConstant.LONGITUDE);
    }

    public String getStatusText(int status, Context context) {
/*
                0 = PENDING ,
                1 = CONFIRM BOOK BY DRIVER,
                2 = START,
                3 = COMPLETE,
                4 = CANCEL,
                5 = QUEUE,
                6 = NOT PERFORM,
                7 = Payment Done,
                8 = Navigation start*/

        String strStatus = "";
        switch (status) {
            case -1:
                strStatus = context.getString(R.string.failed);
                break;
            case 0:
                strStatus = context.getString(R.string.pending);
                break;
            case 1:
                strStatus = context.getString(R.string.confirmed);
                break;
            case 2:
                strStatus = context.getString(R.string.started);
                break;
            case 3:
                strStatus = context.getString(R.string.payment_pending);
                break;
            case 4:
                strStatus = context.getString(R.string.cancelled);
                break;
            case 5:
                strStatus = context.getString(R.string.in_queue);
                break;
            case 6:
//                strStatus = context.getString(R.string.not_performed);
                strStatus = context.getString(R.string.cancelled_by_driver);
                break;
            case 7:
                strStatus = context.getString(R.string.completed);
                break;
            case 8:
                strStatus = context.getString(R.string.refunded);
                break;
            case 9:
                strStatus = context.getString(R.string.user_cancelled);
                break;
            case 10:
                strStatus = context.getString(R.string.failed_payment);
                break;
        }

        return strStatus;

    }

    public void setConfigDetails(ConfigDetails userData) {
        Gson gson = new Gson();
        PreferanceHelper.getInstance().putString(SharedPrefConstant.CONFIG_DETAILS, gson.toJson(userData));
    }

    public ConfigDetails getConfigDetails() {
        Gson gson = new Gson();
        return gson.fromJson(PreferanceHelper.getInstance().getString(SharedPrefConstant.CONFIG_DETAILS), ConfigDetails.class);
    }

    public static String getStripeId() {
        UserData user = AppHelper.getInstance().getUserDetails();
        if (user == null) return "";
        return String.valueOf(user.getCustomer_id());
    }

    public String getStatusColor(int status, Context context) {
        String strStatus = "#46aa50";
        switch (status) {
            case -1:
                strStatus = "#FF0000";
                break;
            case 0:
                strStatus = "#555555";
                break;
            case 1:
                strStatus = "#FF9100";
                break;
            case 2:
                strStatus = "#46aa50";
                break;
            case 3:
                strStatus = "#FF0000";
                break;
            case 4:
                strStatus = "#FF0000";
                break;
            case 5:
                strStatus = "#0000FF";
                break;
            case 6:
                strStatus = "#FF0000";
                break;
            case 7:
                strStatus = "#46aa50";
                break;
            case 8:
                strStatus = "#FF9100";
                break;
            case 9:
                strStatus = "#FF0000";
                break;
            case 10:
                strStatus = "#e88d0e";
                break;
        }

        return strStatus;

    }
}
