package sg.com.towme.driver.network;



import sg.com.towme.driver.model.AbstractCallback;

import java.util.HashMap;

import okhttp3.ResponseBody;

public class StripeCall {

    private static StripeCall instance = new StripeCall();
    private ApiService apiService;

    private StripeCall() {
        apiService = RetroClient.getApiServiceStripe();
    }

    public static StripeCall getInstance() {
        return instance;
    }


    public void createKey(HashMap<String, String> param, AbstractCallback<ResponseBody> callback) {
        apiService.createKey(param).enqueue(new AbstractCallback<ResponseBody>() {
            @Override
            public void result(ResponseBody result) {
                System.out.println("Brij get response: " + result);
                if (callback != null) callback.result(result);
            }
        });
    }

    public void paymentIntent(HashMap<String, Object> param, AbstractCallback<ResponseBody> callback) {
        apiService.paymentIntent(param).enqueue(new AbstractCallback<ResponseBody>() {
            @Override
            public void result(ResponseBody result) {
                System.out.println("Brij get response: 111" + result);
                if (callback != null) callback.result(result);
            }
        });
    }

    public void confirmIntent(HashMap<String, Object> param, AbstractCallback<ResponseBody> callback) {
        apiService.confirmIntent(param).enqueue(new AbstractCallback<ResponseBody>() {
            @Override
            public void result(ResponseBody result) {
                if (callback != null) callback.result(result);
            }
        });
    }

    public void captureIntent(HashMap<String, Object> param, AbstractCallback<ResponseBody> callback) {
        apiService.captureIntent(param).enqueue(new AbstractCallback<ResponseBody>() {
            @Override
            public void result(ResponseBody result) {
                if (callback != null) callback.result(result);
            }
        });
    }

   /* public void addCard(Card card, AbstractCallback<ObjectBaseModel<CardBase>> callback) {
        HashMap<String, String> param = new HashMap<>();
        param.put("user_id", String.valueOf(AppHelper.getInstance().getUserDetails().getUserid()));
        param.put("number", card.getNumber());
        param.put("exp_month", String.valueOf(card.getExp_month()));
        param.put("exp_year", String.valueOf(card.getExp_year()));
        param.put("cvc", card.getCvc());
        if (TextUtil.isNullOrEmpty(card.getId())) {
            apiService.addCard(param).enqueue(callback);
        } else {
            param.put("card_id", card.getId());
            apiService.updateCard(param).enqueue(callback);
        }
    }

    public void setDefault(Card card, AbstractCallback<ObjectBaseModel<CardBase>> callback) {
        HashMap<String, String> param = new HashMap<>();
        param.put("user_id", String.valueOf(AppHelper.getInstance().getUserDetails().getUserid()));
        param.put("card_id", card.getId());
        apiService.setDefault(param).enqueue(new AbstractCallback<ObjectBaseModel<CardBase>>() {
            @Override
            public void result(ObjectBaseModel<CardBase> result) {
                if (callback != null) callback.result(result);
            }
        });
    }

    public void getCustomer(AbstractCallback<ObjectBaseModel<Customer>> callback) {
        HashMap<String, String> param = new HashMap<>();
        param.put("user_id", String.valueOf(AppHelper.getInstance().getUserDetails().getUserid()));
        apiService.getCustomer(param).enqueue(new AbstractCallback<ObjectBaseModel<Customer>>() {
            @Override
            public void result(ObjectBaseModel<Customer> result) {
                if (callback != null) callback.result(result);
            }
        });
    }

    public void deleteCard(Card card, AbstractCallback<ObjectBaseModel<CardBase>> callback) {
        HashMap<String, String> param = new HashMap<>();
        param.put("user_id", String.valueOf(AppHelper.getInstance().getUserDetails().getUserid()));
        param.put("cardId", card.getId());
        apiService.deleteCard(param).enqueue(callback);
    }

    public void getCards(AbstractCallback<ObjectBaseModel<Customer>> callback) {
        HashMap<String, String> param = new HashMap<>();
        param.put("user_id", String.valueOf(AppHelper.getInstance().getUserDetails().getUserid()));
//        apiService.listCard(param).enqueue(callback);
        apiService.getCustomer(param).enqueue(new AbstractCallback<ObjectBaseModel<Customer>>() {
            @Override
            public void result(ObjectBaseModel<Customer> result) {
                if (callback != null) callback.result(result);
            }
        });
    }



    public void getTransactions(int page, int limit, IResponseCallback<ListBaseModel<Transaction>> callback) {
        HashMap<String, Object> param = new HashMap<>();
        param.put(Parameter.user_id, String.valueOf(AppHelper.getInstance().getUserDetails().getUserid()));
        param.put("page", page);
        param.put("limit", limit);

        Call<ListBaseModel<Transaction>> call = apiService.getTransactions(param);
        call.enqueue(new Callback<ListBaseModel<Transaction>>() {
            @Override
            public void onResponse(@NotNull Call<ListBaseModel<Transaction>> call, @NotNull Response<ListBaseModel<Transaction>> response) {
                if (callback != null) callback.success(response.body());
            }

            @Override
            public void onFailure(@NotNull Call<ListBaseModel<Transaction>> call, @NotNull Throwable t) {
                t.printStackTrace();
                if (callback != null) callback.onError(call, t);
            }
        });
    }
*/

}
