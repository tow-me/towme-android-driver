package sg.com.towme.driver.listener;


public interface iDialogButtonClick {

    void negativeClick();
    void positiveClick(String str);
}
