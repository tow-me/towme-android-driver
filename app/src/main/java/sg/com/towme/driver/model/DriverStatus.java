package sg.com.towme.driver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DriverStatus {
    @SerializedName("driver")
    @Expose
    private Status driver;

    public Status getDriver() {
        return driver;
    }

    public void setDriver(Status driver) {
        this.driver = driver;
    }
}
