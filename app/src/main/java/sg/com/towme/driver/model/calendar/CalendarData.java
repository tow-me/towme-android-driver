package sg.com.towme.driver.model.calendar;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CalendarData {

    @SerializedName("booking_id")
    @Expose
    private Integer bookingId;
    @SerializedName("userid")
    @Expose
    private Integer userid;
    @SerializedName("driver_userid")
    @Expose
    private Integer driverUserid;
    @SerializedName("vehicle_id")
    @Expose
    private Integer vehicleId;
    @SerializedName("booking_date")
    @Expose
    private String bookingDate;
    @SerializedName("sub_services_id")
    @Expose
    private Integer subServicesId;
    @SerializedName("service_id")
    @Expose
    private Integer serviceId;
    @SerializedName("service_charge")
    @Expose
    private String serviceCharge;
    @SerializedName("booking_charge")
    @Expose
    private String bookingCharge;
    @SerializedName("total_cost")
    @Expose
    private String totalCost;
    @SerializedName("payment_type")
    @Expose
    private Integer paymentType;
    @SerializedName("payment_offline_confirm")
    @Expose
    private Object paymentOfflineConfirm;
    @SerializedName("payment_offline_confirm_date")
    @Expose
    private Object paymentOfflineConfirmDate;
    @SerializedName("payment_offline_confirm_time")
    @Expose
    private Object paymentOfflineConfirmTime;
    @SerializedName("account_type")
    @Expose
    private Integer accountType;
    @SerializedName("card_id")
    @Expose
    private Object cardId;
    @SerializedName("accident_type")
    @Expose
    private String accidentType;
    @SerializedName("breakdown_type")
    @Expose
    private String breakdownType;
    @SerializedName("booking_type")
    @Expose
    private Integer bookingType;
    @SerializedName("booking_type_later")
    @Expose
    private Object bookingTypeLater;
    @SerializedName("booking_status")
    @Expose
    private Integer bookingStatus;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("transaction_id")
    @Expose
    private Object transactionId;
    @SerializedName("transaction_amount")
    @Expose
    private Object transactionAmount;
    @SerializedName("transaction_status")
    @Expose
    private Object transactionStatus;
    @SerializedName("booking_image")
    @Expose
    private Object bookingImage;
    @SerializedName("extra_notes")
    @Expose
    private String extraNotes;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("destination")
    @Expose
    private String destination;
    @SerializedName("dest_latitude")
    @Expose
    private String destLatitude;
    @SerializedName("dest_longitude")
    @Expose
    private String destLongitude;
    @SerializedName("started_at")
    @Expose
    private String startedAt;
    @SerializedName("stopped_at")
    @Expose
    private String stoppedAt;
    @SerializedName("started_images")
    @Expose
    private List<String> startedImages;
    @SerializedName("stopped_images")
    @Expose
    private List<String> stoppedImages;
    @SerializedName("start_ride_description")
    @Expose
    private String startRideDescription;
    @SerializedName("end_ride_description")
    @Expose
    private String endRideDescription;
    @SerializedName("end_ride_extra_charge")
    @Expose
    private List<EndRideExtraCharge> endRideExtraCharge;
    @SerializedName("coupon_id")
    @Expose
    private Object couponId;
    @SerializedName("discounted_amount")
    @Expose
    private String discountedAmount;
    @SerializedName("rating")
    @Expose
    private String rating;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("tow_price")
    @Expose
    private String towPrice;

    public String getTowPrice() {
        return towPrice;
    }

    public void setTowPrice(String towPrice) {
        this.towPrice = towPrice;
    }

    public Integer getBookingId() {
        return bookingId;
    }

    public void setBookingId(Integer bookingId) {
        this.bookingId = bookingId;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getDriverUserid() {
        return driverUserid;
    }

    public void setDriverUserid(Integer driverUserid) {
        this.driverUserid = driverUserid;
    }

    public Integer getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Integer vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public Integer getSubServicesId() {
        return subServicesId;
    }

    public void setSubServicesId(Integer subServicesId) {
        this.subServicesId = subServicesId;
    }

    public Integer getServiceId() {
        return serviceId;
    }

    public void setServiceId(Integer serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceCharge() {
        return serviceCharge;
    }

    public void setServiceCharge(String serviceCharge) {
        this.serviceCharge = serviceCharge;
    }

    public String getBookingCharge() {
        return bookingCharge;
    }

    public void setBookingCharge(String bookingCharge) {
        this.bookingCharge = bookingCharge;
    }

    public String getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(String totalCost) {
        this.totalCost = totalCost;
    }

    public Integer getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Integer paymentType) {
        this.paymentType = paymentType;
    }

    public Object getPaymentOfflineConfirm() {
        return paymentOfflineConfirm;
    }

    public void setPaymentOfflineConfirm(Object paymentOfflineConfirm) {
        this.paymentOfflineConfirm = paymentOfflineConfirm;
    }

    public Object getPaymentOfflineConfirmDate() {
        return paymentOfflineConfirmDate;
    }

    public void setPaymentOfflineConfirmDate(Object paymentOfflineConfirmDate) {
        this.paymentOfflineConfirmDate = paymentOfflineConfirmDate;
    }

    public Object getPaymentOfflineConfirmTime() {
        return paymentOfflineConfirmTime;
    }

    public void setPaymentOfflineConfirmTime(Object paymentOfflineConfirmTime) {
        this.paymentOfflineConfirmTime = paymentOfflineConfirmTime;
    }

    public Integer getAccountType() {
        return accountType;
    }

    public void setAccountType(Integer accountType) {
        this.accountType = accountType;
    }

    public Object getCardId() {
        return cardId;
    }

    public void setCardId(Object cardId) {
        this.cardId = cardId;
    }

    public String getAccidentType() {
        return accidentType;
    }

    public void setAccidentType(String accidentType) {
        this.accidentType = accidentType;
    }

    public String getBreakdownType() {
        return breakdownType;
    }

    public void setBreakdownType(String breakdownType) {
        this.breakdownType = breakdownType;
    }

    public Integer getBookingType() {
        return bookingType;
    }

    public void setBookingType(Integer bookingType) {
        this.bookingType = bookingType;
    }

    public Object getBookingTypeLater() {
        return bookingTypeLater;
    }

    public void setBookingTypeLater(Object bookingTypeLater) {
        this.bookingTypeLater = bookingTypeLater;
    }

    public Integer getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(Integer bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Object getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Object transactionId) {
        this.transactionId = transactionId;
    }

    public Object getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(Object transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public Object getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(Object transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public Object getBookingImage() {
        return bookingImage;
    }

    public void setBookingImage(Object bookingImage) {
        this.bookingImage = bookingImage;
    }

    public String getExtraNotes() {
        return extraNotes;
    }

    public void setExtraNotes(String extraNotes) {
        this.extraNotes = extraNotes;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDestLatitude() {
        return destLatitude;
    }

    public void setDestLatitude(String destLatitude) {
        this.destLatitude = destLatitude;
    }

    public String getDestLongitude() {
        return destLongitude;
    }

    public void setDestLongitude(String destLongitude) {
        this.destLongitude = destLongitude;
    }

    public String getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(String startedAt) {
        this.startedAt = startedAt;
    }

    public String getStoppedAt() {
        return stoppedAt;
    }

    public void setStoppedAt(String stoppedAt) {
        this.stoppedAt = stoppedAt;
    }

    public List<String> getStartedImages() {
        return startedImages;
    }

    public void setStartedImages(List<String> startedImages) {
        this.startedImages = startedImages;
    }

    public List<String> getStoppedImages() {
        return stoppedImages;
    }

    public void setStoppedImages(List<String> stoppedImages) {
        this.stoppedImages = stoppedImages;
    }

    public String getStartRideDescription() {
        return startRideDescription;
    }

    public void setStartRideDescription(String startRideDescription) {
        this.startRideDescription = startRideDescription;
    }

    public String getEndRideDescription() {
        return endRideDescription;
    }

    public void setEndRideDescription(String endRideDescription) {
        this.endRideDescription = endRideDescription;
    }

    public List<EndRideExtraCharge> getEndRideExtraCharge() {
        return endRideExtraCharge;
    }

    public void setEndRideExtraCharge(List<EndRideExtraCharge> endRideExtraCharge) {
        this.endRideExtraCharge = endRideExtraCharge;
    }

    public Object getCouponId() {
        return couponId;
    }

    public void setCouponId(Object couponId) {
        this.couponId = couponId;
    }

    public String getDiscountedAmount() {
        return discountedAmount;
    }

    public void setDiscountedAmount(String discountedAmount) {
        this.discountedAmount = discountedAmount;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Double getTowAmount() {
        return parseDouble(towPrice) + parseDouble(serviceCharge) - parseDouble(discountedAmount);
    }

    private static double parseDouble(String raw) {
        try {
            return Double.parseDouble(raw);
        } catch (Exception e) {
            return 0;
        }
    }
}
