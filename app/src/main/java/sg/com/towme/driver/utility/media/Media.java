package sg.com.towme.driver.utility.media;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.format.DateUtils;

import sg.com.towme.driver.utility.TextUtil;

import java.io.File;
import java.util.Objects;

public class Media implements Parcelable {

    public static final okhttp3.MediaType MEDIA_TYPE_TEXT = okhttp3.MediaType.parse("text/plain");
    public static final okhttp3.MediaType MEDIA_TYPE_IMAGE = okhttp3.MediaType.parse("image/*");
    public static final okhttp3.MediaType MEDIA_TYPE_VIDEO = okhttp3.MediaType.parse("video/*");

    private long id;
    private String message_id;
    private String local_url;
    private String local_thumb_url;
    private String remote_url;
    private String remote_thumb_url;
    private long size;
    private long duration;
    private MediaType mediaType;
    private float progress;
    private long downloaded;
    private byte[] thumb;

    private String latitude;
    private String longitude;
    private String description;

    private ActionState actionState;

    private boolean isLocal = false;
    private int playback_duration;
    private int position;

    public Media() {
        id = System.currentTimeMillis();
    }

    protected Media(Parcel in) {
        id = in.readLong();
        message_id = in.readString();
        position = in.readInt();
        local_url = in.readString();
        local_thumb_url = in.readString();
        remote_url = in.readString();
        remote_thumb_url = in.readString();
        size = in.readLong();
        duration = in.readLong();
        playback_duration = in.readInt();
        mediaType = (MediaType) in.readSerializable();
        actionState = (ActionState) in.readSerializable();
        progress = in.readFloat();
        downloaded = in.readLong();
        thumb = in.createByteArray();
        isLocal = in.readByte() == 1;
        latitude = in.readString();
        longitude = in.readString();
        description = in.readString();
    }

    public static final Creator<Media> CREATOR = new Creator<Media>() {
        @Override
        public Media createFromParcel(Parcel in) {
            return new Media(in);
        }

        @Override
        public Media[] newArray(int size) {
            return new Media[size];
        }
    };


    public String getLocal_url() {
        return local_url;
    }

    public void setLocal_url(String local_url) {
        this.local_url = local_url;
    }

    public String getLocal_thumb_url() {
        return local_thumb_url;
    }

    public void setLocal_thumb_url(String local_thumb_url) {
        this.local_thumb_url = local_thumb_url;
    }

    public String getRemote_url() {
        return remote_url;
    }

    public void setRemote_url(String remote_url) {
        this.remote_url = remote_url;
    }

    public String getRemote_thumb_url() {
        if (TextUtil.isNullOrEmpty(remote_thumb_url)) {
            return getRemote_url();
        }
        return remote_thumb_url;
    }

    public void setRemote_thumb_url(String remote_thumb_url) {
        this.remote_thumb_url = remote_thumb_url;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }


    public int getTotalDuration() {
        return (int) (duration / 1000);
    }

    public int getPlayback_duration() {
        return playback_duration;
    }

    public void setPlayback_duration(int playback_duration) {
        this.playback_duration = playback_duration;
    }

    public String getUrl() {
        if (TextUtil.isNullOrEmpty(remote_url)) return getLocalUrl();
        else return remote_url;
    }

    public String getThumbUrl() {
        if (TextUtil.isNullOrEmpty(remote_thumb_url)) return getLocal_thumb_url();
        else return remote_thumb_url;
    }

    public String getLocalUrl() {
        if (TextUtil.isNullOrEmpty(local_url)) return getRemote_url();
        else return local_url;
    }

    public boolean hasRemoteUrl() {
        return !TextUtil.isNullOrEmpty(remote_url);
    }

    public boolean hasRemoteThumbUrl() {
        return !TextUtil.isNullOrEmpty(remote_thumb_url);
    }


    public boolean hasLocalUrl() {
        return !TextUtil.isNullOrEmpty(local_url);
    }


    public ActionState getActionState() {
        if (actionState == null) return actionState = ActionState.NONE;
        return actionState;
    }

    public void setActionState(ActionState actionState) {
        this.actionState = actionState;
    }

    public File getLocalFile() {
        return new File(local_url);
    }

    public File getLocalThumbFile() {
        if (local_thumb_url == null) return null;
        return new File(local_thumb_url);
    }

    public boolean hasLocalThumb() {
        if (TextUtil.isNullOrEmpty(local_thumb_url)) return false;
        return new File(local_thumb_url).exists();
    }

    public void setProgress(float progress) {
        this.progress = progress;
    }

    public float getProgress() {
        return progress;
    }

    public long getDownloaded() {
        return downloaded;
    }

    public void setDownloaded(long downloaded) {
        this.downloaded = downloaded;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMessage_id() {
        return message_id;
    }

    public void setMessage_id(String message_id) {
        this.message_id = message_id;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public boolean isLocal() {
        return isLocal;
    }

    public void setLocal(boolean local) {
        isLocal = local;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Media copy() {
        Media media = new Media();
        media.setId(id);
        media.setMessage_id(message_id);
        media.setLocal_url(local_url);
        media.setLocal_thumb_url(local_thumb_url);
        media.setRemote_url(remote_url);
        media.setRemote_thumb_url(remote_thumb_url);
        media.setSize(size);
        media.setDuration(duration);
        media.setMediaType(mediaType);
        media.setPlayback_duration(playback_duration);
        media.setActionState(ActionState.NONE);
        media.setDownloaded(0);
        media.setProgress(progress);
        media.setLocal(isLocal);
        media.setLatitude(latitude);
        media.setLongitude(longitude);
        media.setDescription(description);

        return media;
    }

    public Media clearLocal() {
        setLocal_thumb_url(null);
        setLocal_url(null);
        setProgress(0);
        setLocal(false);
        setDownloaded(0);
        setActionState(ActionState.NONE);
        return this;
    }


    public boolean isExist(Context context) {
        return FileUtil.isAvailable(context, this);
    }

    public boolean isExist(Context context, String path) {
        return FileUtil.isAvailable(context, this);
    }

    @Override
    public int hashCode() {
        return Objects.hash(remote_url, local_url);
    }

    public String getRootDirectory(Context context) {
        return MediaType.getRootDirectory(context, getMediaType());
    }

    public String getFilename() {
        return isLocal() ? FileUtil.getFileName(getLocalUrl()) : FileUtil.getFileName(getRemote_url());
    }

    public String getFormattedFileName() {
      return FileUtil.getFormattedFilename(getFilename());

    }

    public String getExtension() {
        return FileUtil.getExtensionName(getFilename());
    }

    public String getDownloadPath(Context context) {
        return getRootDirectory(context) + getFilename();
    }


    public File getDownloadFile(Context context) {
        return new File(getDownloadPath(context));
    }


    public boolean isDownloadableFileExist(Context context) {
        return new File(getDownloadPath(context)).exists();
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(message_id);
        dest.writeInt(position);
        dest.writeString(local_url);
        dest.writeString(local_thumb_url);
        dest.writeString(remote_url);
        dest.writeString(remote_thumb_url);
        dest.writeLong(size);
        dest.writeLong(duration);
        dest.writeInt(playback_duration);
        dest.writeSerializable(mediaType);
        dest.writeSerializable(actionState);
        dest.writeFloat(progress);
        dest.writeLong(downloaded);
        dest.writeByteArray(thumb);
        dest.writeByte((byte) (isLocal ? 1 : 0));
        dest.writeString(latitude);
        dest.writeString(longitude);
        dest.writeString(description);
    }


    public Bitmap getDecodedBitmap() {
        return BitmapFactory.decodeByteArray(thumb, 0, thumb.length);
    }

    public byte[] getThumb() {
        return thumb;
    }

    public void setThumb(byte[] thumb) {
        this.thumb = thumb;
    }


    public static String getFormattedDuration(long seconds) {
        return DateUtils.formatElapsedTime(seconds);
    }


    public static Media create(Address address) {
        Media media = new Media();
        media.setMediaType(MediaType.LOCATION);
        media.setLatitude(String.valueOf(address.getLatitude()));
        media.setLongitude(String.valueOf(address.getLongitude()));
        media.setDescription(address.getFullAddress());
        return media;
    }

    public static Media create(String vCard) {
        Media media = new Media();
        media.setMediaType(MediaType.CONTACT);
        media.setDescription(vCard);
        return media;
    }

    public static Media create(Thumb thumb) {
        Media media = new Media();
        media.setMediaType(thumb.getMediaType());
        media.setLocal_url(thumb.getFile().getPath());
        media.setSize(thumb.getFile().length());
        media.setLocal(true);
        if (thumb.getThumb() != null) {
            media.setLocal_thumb_url(thumb.getThumb().getPath());
            media.setThumb(thumb.getBytes());
        }
        return media;
    }

    public boolean isValid() {
        return getSize() > 0;
    }

    public static Media create(Thumb thumb, long duration) {
        Media media = new Media();
        media.setMediaType(thumb.getMediaType());
        media.setLocal_url(thumb.getFile().getPath());
        media.setSize(thumb.getFile().length());
        media.setDuration(duration);
        media.setLocal(true);
        if (thumb.getThumb() != null) {
            media.setLocal_thumb_url(thumb.getThumb().getPath());
            media.setThumb(thumb.getBytes());
        }
        return media;
    }


    public boolean isActionInProgress() {
        ActionState actionState = getActionState();
        return actionState == ActionState.PROGRESS || actionState == ActionState.PROCESSING;
    }



}
