
package sg.com.towme.driver.model.linkedin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfilePicture {

    @SerializedName("displayImage~")
    @Expose
    private DisplayImage displayImage;

    public DisplayImage getDisplayImage() {
        return displayImage;
    }

    public void setDisplayImage(DisplayImage displayImage) {
        this.displayImage = displayImage;
    }

}
