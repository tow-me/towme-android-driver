package sg.com.towme.driver.listener;


import sg.com.towme.driver.enumeration.Info;

public interface iInfoDialogListner {

    void onOkClick(Info info);

}
