package sg.com.towme.driver.listener;

public interface iPersonalBusinessListner {

    void onBusinessClick();

    void onPersonalClick();
}
