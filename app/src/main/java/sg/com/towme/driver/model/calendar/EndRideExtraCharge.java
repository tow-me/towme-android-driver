package sg.com.towme.driver.model.calendar;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EndRideExtraCharge {
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("price")
    @Expose
    private String price;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
