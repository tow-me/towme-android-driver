package sg.com.towme.driver.network;

import sg.com.towme.driver.model.BaseModel;
import sg.com.towme.driver.utility.DebugLog;
import com.google.gson.Gson;


public class ParsingHelper {
    private static final ParsingHelper ourInstance = new ParsingHelper();

    private ParsingHelper() {

    }

    public static ParsingHelper getInstance() {
        return ourInstance;
    }

    public BaseModel baseModelParsing(String str) throws NullPointerException {
        Gson gson = new Gson();
        try {

            // Convert JSON to Java Object
            DebugLog.e("Base Model String " + str);
            BaseModel baseModel = gson.fromJson(str, BaseModel.class);
            return baseModel;

            // Convert JSON to JsonElement, and later to String
            /*JsonElement json = gson.fromJson(reader, JsonElement.class);
            String jsonInString = gson.toJson(json);
            System.out.println(jsonInString);*/

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


}
