package sg.com.towme.driver.adapter;

import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import sg.com.towme.driver.R;
import sg.com.towme.driver.listener.RecyclerViewClickListener;
import sg.com.towme.driver.model.notification.NotificationData;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import sg.com.towme.driver.utility.DateTimeUtil;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    Context context;
    List<NotificationData> listNotification;

    public void setCallback(RecyclerViewClickListener callback) {
        this.callback = callback;
    }

    RecyclerViewClickListener callback;

    public NotificationAdapter(Context context, List<NotificationData> listNotification) {
        this.context = context;
        this.listNotification = listNotification;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.row_notification, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        NotificationData data = listNotification.get(position);
        holder.txtTitle.setText(data.getNotificationTitle());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            holder.txtDescription.setText(Html.fromHtml(data.getNotificationDescription(), Html.FROM_HTML_MODE_COMPACT));
        } else {
            holder.txtDescription.setText(Html.fromHtml(data.getNotificationDescription()));
        }
        holder.txtDate.setText(DateTimeUtil.convertWithoutChangingTimezone(data.getUpdatedAt(), "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy, hh:mma"));
        holder.llRow.setOnClickListener(view -> {
            callback.onClick(holder.llRow, position, null);
        });
    }

    @Override
    public int getItemCount() {
        return listNotification.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.llRow)
        LinearLayout llRow;
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.txtDescription)
        TextView txtDescription;
        @BindView(R.id.txtDate)
        TextView txtDate;
        @BindView(R.id.imgArrow)
        ImageView imgArrow;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }


}
