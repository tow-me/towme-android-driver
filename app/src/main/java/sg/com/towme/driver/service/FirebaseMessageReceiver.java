package sg.com.towme.driver.service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import sg.com.towme.driver.R;
import sg.com.towme.driver.activity.HomeActivity;
import sg.com.towme.driver.activity.SplashActivity;


public class FirebaseMessageReceiver
        extends FirebaseMessagingService {

    // Override onMessageReceived() method to extract the
    // title and
    // body from the message passed in FCM
    @Override
    public void
    onMessageReceived(RemoteMessage remoteMessage) {
        String channel_id = "general_notification_channel_id";
        if (remoteMessage.getNotification() != null) {
            if(remoteMessage.getData().size() > 0){
                Intent intent = new Intent(this, HomeActivity.class);
                if (remoteMessage.getData().get("notification_type").equals("chat")) {
                    intent.putExtra(HomeActivity.EXTRA_SENDER_ID, remoteMessage.getData().get("sender_id"));
                } else if (remoteMessage.getData().get("notification_type").equals("notification")) {
                    intent.putExtra(HomeActivity.EXTRA_NOTIFICATION_ID, remoteMessage.getData().get("notification_id"));
                }
                intent.putExtra(HomeActivity.EXTRA_TYPE, remoteMessage.getData().get("notification_type"));
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                PendingIntent pendingIntent
                        = PendingIntent.getActivity(
                        this, 0, intent,
                        PendingIntent.FLAG_ONE_SHOT);

                // Create a Builder object using NotificationCompat
                // class. This will allow control over all the flags
                NotificationCompat.Builder builder
                        = new NotificationCompat
                        .Builder(getApplicationContext(),
                        channel_id)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentIntent(pendingIntent);

                // A customized design for the notification can be
                // set only for Android versions 4.1 and above. Thus
                // condition for the same is checked here.
                builder = builder.setContentTitle(remoteMessage.getNotification().getTitle())
                        .setContentText(remoteMessage.getNotification().getBody())
                        .setSmallIcon(R.mipmap.ic_launcher);
                // Create an object of NotificationManager class to
                // notify the
                // user of events that happen in the background.
                NotificationManager notificationManager
                        = (NotificationManager) getSystemService(
                        Context.NOTIFICATION_SERVICE);
                // Check if the Android Version is greater than Oreo
                if (Build.VERSION.SDK_INT
                        >= Build.VERSION_CODES.O) {
                    NotificationChannel notificationChannel
                            = new NotificationChannel(
                            channel_id, "web_app",
                            NotificationManager.IMPORTANCE_HIGH);
                    notificationManager.createNotificationChannel(
                            notificationChannel);
                }

                notificationManager.notify(0, builder.build());
            }
        } else {
            {
                if(remoteMessage.getData().size() > 0){
                    Intent intent = new Intent(this, HomeActivity.class);
                    if (remoteMessage.getData().get("notification_type").equals("chat")) {
                        intent.putExtra(HomeActivity.EXTRA_SENDER_ID, remoteMessage.getData().get("sender_id"));
                    } else if (remoteMessage.getData().get("notification_type").equals("notification")) {
                        intent.putExtra(HomeActivity.EXTRA_NOTIFICATION_ID, remoteMessage.getData().get("notification_id"));
                    }
                    intent.putExtra(HomeActivity.EXTRA_TYPE, remoteMessage.getData().get("notification_type"));
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    PendingIntent pendingIntent
                            = PendingIntent.getActivity(
                            this, 0, intent,
                            PendingIntent.FLAG_UPDATE_CURRENT);

                    // Create a Builder object using NotificationCompat
                    // class. This will allow control over all the flags
                    NotificationCompat.Builder builder
                            = new NotificationCompat
                            .Builder(getApplicationContext(),
                            channel_id)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentIntent(pendingIntent);

                    // A customized design for the notification can be
                    // set only for Android versions 4.1 and above. Thus
                    // condition for the same is checked here.
                    builder = builder.setContentTitle(remoteMessage.getData().get("title"))
                            .setContentText(remoteMessage.getData().get("body"))
                            .setSmallIcon(R.mipmap.ic_launcher);
                    // Create an object of NotificationManager class to
                    // notify the
                    // user of events that happen in the background.
                    NotificationManager notificationManager
                            = (NotificationManager) getSystemService(
                            Context.NOTIFICATION_SERVICE);
                    // Check if the Android Version is greater than Oreo
                    if (Build.VERSION.SDK_INT
                            >= Build.VERSION_CODES.O) {
                        NotificationChannel notificationChannel
                                = new NotificationChannel(
                                channel_id, "web_app",
                                NotificationManager.IMPORTANCE_HIGH);
                        notificationManager.createNotificationChannel(
                                notificationChannel);
                    }

                    notificationManager.notify(0, builder.build());
                }
            }
        }
    }
}
