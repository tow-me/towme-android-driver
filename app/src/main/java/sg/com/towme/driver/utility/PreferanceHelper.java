package sg.com.towme.driver.utility;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashSet;

import sg.com.towme.driver.Controller;
import sg.com.towme.driver.constant.SharedPrefConstant;
import sg.com.towme.driver.model.Environment;
import sg.com.towme.driver.model.config_.ConfigDetails;

public class PreferanceHelper {

    Context context;
    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;

    private static final PreferanceHelper ourInstance = new PreferanceHelper();

    public static PreferanceHelper getInstance() {
        return ourInstance;
    }

    private PreferanceHelper() {
        this.context = Controller.getInstance().getApplicationContext();
        sharedPref = context.getSharedPreferences(SharedPrefConstant.prefName, Context.MODE_PRIVATE);
        editor = sharedPref.edit();
    }

    public void putString(String key, String value) {

        editor.putString(key, value);
        editor.commit();
    }

    public void putStringSet(String key, HashSet<String> value) {

        editor.putStringSet(key, value);
        editor.commit();
    }


    public String getString(String key) {
        String str = sharedPref.getString(key, "");
        return str;
    }

    public HashSet<String> getStringset(String key) {
        HashSet<String> str = (HashSet<String>) sharedPref.getStringSet(key, new HashSet<String>());
        return str;
    }

    public void putBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public boolean getBoolean(String key) {
        return sharedPref.getBoolean(key, false);
    }

    public void clearPreference() {
        ConfigDetails configDetails = AppHelper.getInstance().getConfigDetails();
        Environment environment = AppHelper.getInstance().getEnvironment();
        editor.clear();
        editor.commit();

        AppHelper.getInstance().setConfigDetails(configDetails);
        AppHelper.getInstance().setEnvironment(environment);
    }

}
