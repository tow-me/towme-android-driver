package sg.com.towme.driver.utility.media;

import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.webkit.MimeTypeMap;

import androidx.core.content.FileProvider;

import sg.com.towme.driver.BuildConfig;
import sg.com.towme.driver.utility.TextUtil;
import sg.com.towme.driver.utility.Util;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.DecimalFormat;
import java.util.Objects;


public class FileUtil {
    public static final String BULLETIN = "\u2022";
    private static final String DOT = ".";
    private static final String UNDER_SCORE = "_";

    private static final String ROOT_DIRECTORY = "/Frekis/";
    private static final String IMAGE_DIRECTORY = "Images/";
    private static final String VIDEO_DIRECTORY = "Videos/";
    private static final String AUDIO_DIRECTORY = "Audios/";
    private static final String DOCUMENT_DIRECTORY = "Documents/";

    private static final String THUMB_DIRECTORY = IMAGE_DIRECTORY + "Thumbs/";
    private static final String PROFILE_PHOTO_DIRECTORY = IMAGE_DIRECTORY + "Profile Photos/";

    public static final String IMAGE_PREFIX = "IMG_";
    public static final String FILE_PREFIX = "FILE_";

    public static final String EXTENSION_IMAGE = ".jpg";


    private static String getExternalStoragePath(Context context) {
        return Environment.getExternalStorageDirectory() + ROOT_DIRECTORY;
//        return Objects.requireNonNull(getExternalStoragePath(null)).getPath() + "/";
    }


    public static File getRootDirectory(Context context) {
        File root = new File(getExternalStoragePath(context));
        boolean isCreated = root.mkdirs();
        return root;
    }

    public static File getDirectory(Context context, String sub_directory) {
        File root = new File(getExternalStoragePath(context) + sub_directory);
        boolean isCreated = root.mkdirs();
        return root;
    }


    public static File getImageDirectory(Context context) {
        return getDirectory(context, IMAGE_DIRECTORY);
    }

    public static File getThumbDirectory(Context context) {
        return getDirectory(context, THUMB_DIRECTORY);
    }

    public static File getVideoDirectory(Context context) {
        return getDirectory(context, VIDEO_DIRECTORY);
    }

    public static File getAudioDirectory(Context context) {
        return getDirectory(context, AUDIO_DIRECTORY);
    }

    public static File getDocumentDirectory(Context context) {
        return getDirectory(context, DOCUMENT_DIRECTORY);
    }

    public static Uri getURI(Context context, File file) {
        return FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", file);
    }

    public static File createNewFile(Context context, MediaType mediaType) {
        String root = MediaType.getRootDirectory(context, mediaType);
        String extension = MediaType.getExtension(mediaType);
        return new File(root, (mediaType.getName() + UNDER_SCORE + System.currentTimeMillis() + extension));
    }

    public static File createNewFile(Context context, MediaType mediaType, String extension) {
        String root = MediaType.getRootDirectory(context, mediaType);
        extension = prefixIsNotThere(extension, DOT);
        extension = extension == null ? MediaType.getExtension(mediaType) : extension;
        return new File(root, (mediaType.getName() + UNDER_SCORE + System.currentTimeMillis() + extension));
    }

    public static File createNewFile(Context context, String filename, MediaType mediaType, String extension) {
        String root = MediaType.getRootDirectory(context, mediaType);
        if (hasExtension(filename)) {
            filename = addUniqueness(filename);
            return new File(root, filename);
        } else
            return new File(root, (filename + UNDER_SCORE + System.currentTimeMillis() + prefixIsNotThere(extension, DOT)));

    }

    private static String addUniqueness(String filename) {
        String name = filename.substring(0, filename.lastIndexOf("."));
        String extension = filename.substring(filename.lastIndexOf("."));
        return name + UNDER_SCORE + System.currentTimeMillis() + extension;
    }


    public static String prefixIsNotThere(String extension, String prefix) {
        if (extension == null) return extension;
        if (extension.contains(prefix)) return extension;
        return prefix + extension;
    }


    public static File createNewRandomFile(Context context, String prefix, String extension) {
        if (prefix == null || prefix.isEmpty()) prefix = IMAGE_PREFIX;
        File root = getRootDirectory(context);
        if (extension != null) {
            return new File(root, (prefix + System.currentTimeMillis() + extension));
        } else {
            return new File(root, (prefix + System.currentTimeMillis()));
        }
    }

    public static File createNewFile(Context context, String fileName, String extension) {
        File root = getRootDirectory(context);
        if (extension != null) {
            return new File(root, fileName + extension);
        } else {
            return new File(root, fileName);
        }
    }


    public static String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }

    public static String getStringFromFile(File file) throws Exception {
        FileInputStream fin = new FileInputStream(file);
        String ret = convertStreamToString(fin);
        fin.close();
        return ret;
    }

    public static boolean isAvailable(Context context, Media media) {
        if (media == null) return false;
        String base = MediaType.getRootDirectory(context, media.getMediaType());
        return isFileExist(new File(base, media.getFilename()));

    }

//    public static File createNewFile(Context context, ImageProxy image) {
//        ByteBuffer buffer = image.getPlanes()[0].getBuffer();
//        byte[] bytes = new byte[buffer.remaining()];
//        buffer.get(bytes);
//        return saveBitmapImage(context, BitmapFactory.decodeByteArray(bytes, 0, bytes.length, null));
//    }


    public static File createNewFile(String path, String prefix, String extension) {
        if (prefix == null || prefix.isEmpty()) {
            prefix = "IMG_";
        }

        if (extension != null) {
            return new File(path, (prefix + System.currentTimeMillis() + extension));
        } else {
            return new File(path, (prefix + System.currentTimeMillis()));
        }
    }

    public static File createNewFile(Context context, byte[] raw_data, String prefix, String extension) {
        try {
            File file = createNewFile(context, prefix, extension);
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
            bos.write(raw_data);
            bos.flush();
            bos.close();
            return file;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static File getNewPath(Context context, Uri uri, MediaType mediaType) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
        if (cursor == null) return null;
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
        cursor.moveToFirst();
        String path = cursor.getString(column_index);
        cursor.close();

        if (path == null) {
            return null;
        } else {
            return copy(new File(path), createNewFile(context, mediaType));
        }
    }

    public static File copy(File src, File dst) {
        InputStream in = null;
        try {
            in = new FileInputStream(src);
            OutputStream out = new FileOutputStream(dst);

            // Transfer bytes from in to out
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
            return dst;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }


    public static void add_no_media(Context context) {
        File no_media = createNewFile(context, ".nomedia", null);
        try {
            if (no_media.mkdirs()) {
                boolean isCreated = no_media.createNewFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static Thumb getThumb(Context context, MediaType mediaType, File file) {
        try {
            Bitmap bitmap = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                bitmap = mediaType == MediaType.VIDEO ?
                        ThumbnailUtils.createVideoThumbnail(file, Thumb.SIZE, null) :
                        ThumbnailUtils.createImageThumbnail(file, Thumb.SIZE, null);
            } else {
                bitmap = mediaType == MediaType.VIDEO ?
                        ThumbnailUtils.createVideoThumbnail(file.getPath(), MediaStore.Video.Thumbnails.MINI_KIND) :
                        ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(file.getPath()), Thumb.THUMB_SIZE, Thumb.THUMB_SIZE);
            }
            File thumb = saveBitmapImage(context, bitmap);
            return new Thumb(mediaType, file, thumb, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Thumb(mediaType, file, null, null);

//        Bitmap compressed_bitmap = compress(thumb, THUMB_SIZE, THUMB_SIZE, 50);
//        byte[] bytes = getCompressedByte(compressed_bitmap == null ? bitmap : compressed_bitmap);
    }

    public static File getFileFromUri(Context context, Uri uri, MediaType mediaType) {
        if (uri == null || uri.getPath() == null) return null;
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(uri);
            String filename = getFileName(context, uri);
            String extension = getExtensionName(filename);
            if (extension == null || extension.isEmpty()) {
                extension = getMimeType(context, uri);
            }
            File file = filename == null ? createNewFile(context, mediaType, extension) : createNewFile(context, filename, mediaType, extension);
            writeStreamToFile(Objects.requireNonNull(inputStream), file);
            return file;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static File addContentToFile(Context context, Uri uri, File outputFile) {
        if (uri == null || uri.getPath() == null) return null;
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(uri);
            writeStreamToFile(Objects.requireNonNull(inputStream), outputFile);
            return outputFile;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }


    private static boolean hasExtension(String filename) {
        if (filename == null) return false;
        if (filename.contains(".")) return true;
        return false;
    }


    private static String getFileName(Context context, Uri uri) {
        String result = null;
        if (uri.getScheme() == null) return null;
        if (Objects.equals(uri.getScheme(), ContentResolver.SCHEME_CONTENT)) {
            try {
                Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
                if (cursor != null && cursor.moveToFirst())
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                if (cursor != null) cursor.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (Objects.equals(uri.getScheme(), ContentResolver.SCHEME_FILE)) {
            result = uri.getPath();
        }
        if (result == null) return null;
        int cut = result.lastIndexOf('/');
        if (cut != -1) result = result.substring(cut + 1);
        return result;
    }

    private static void writeStreamToFile(InputStream inputStream, File file) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            try {
                Files.copy(inputStream, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
                if (file.length() > 0) return;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        OutputStream out = null;
        try {
            out = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while ((len = inputStream.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) out.close();
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String getMimeType(Context context, Uri uri) {
        String extension;
        if (Objects.equals(uri.getScheme(), ContentResolver.SCHEME_CONTENT)) {
            final MimeTypeMap mime = MimeTypeMap.getSingleton();
            extension = mime.getExtensionFromMimeType(context.getContentResolver().getType(uri));
        } else {
            extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(Objects.requireNonNull(uri.getPath()))).toString());
        }

        return extension;
    }

//    public static Bitmap compress(File file, int width, int height, int quality) {
//        try {
//            return new Compressor(Controller.getInstance())
//                    .setMaxWidth(width).setMaxHeight(height)
//                    .setQuality(quality).setCompressFormat(Bitmap.CompressFormat.WEBP)
//                    .compressToBitmap(file);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }

    public static File saveBitmapImage(Context context, Bitmap bitmap) {
        if (bitmap == null) return null;
        try {
            File file = FileUtil.createNewFile(context, MediaType.THUMB);
            FileOutputStream fos = new FileOutputStream(file);

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, bos);
            fos.write(bos.toByteArray());
            fos.flush();
            fos.close();
            return file;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Thumb getThumbnail(Context context, MediaType mediaType, File file) {
        return getThumb(context, mediaType, file);
    }


    public static byte[] getCompressedByte(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 0, stream);
        return stream.toByteArray();
    }

    public static byte[] getCompressedByte(File file) {
        return getCompressedByte(BitmapFactory.decodeFile(file.getPath()));
    }


    public static boolean isFileExist(Context context, String name) {
        return FileUtil.createNewFile(context, name, null).exists();
    }

    public static boolean isFileExist(File file) {
        return file.exists();
    }


    public static String getFileName(String url) {
        if (url == null || url.isEmpty())
            return String.valueOf(System.currentTimeMillis());
        else if (url.contains("/")) return url.substring(url.lastIndexOf("/") + 1);
        return url;
    }

    public static String getExtensionName(String url) {
        if (url == null || url.isEmpty() || !url.contains(".")) return "";
        else if (url.contains(".")) return url.substring(url.lastIndexOf(".") + 1);
        return url;
    }

    public static String getExtensionWithDot(String url) {
        if (url == null || url.isEmpty() || !url.contains(".")) return "";
        else if (url.contains(".")) return url.substring(url.lastIndexOf("."));
        return url;
    }


    public static String fileNameWithOutExtension(String url) {
        if (url == null || url.isEmpty()) {
            return String.valueOf(System.currentTimeMillis());
        } else if (url.contains("/")) {
            if (url.contains(".")) return url.substring(url.lastIndexOf("/"), url.lastIndexOf('.'));
            else return url.substring(url.lastIndexOf("/"));
        } else {
            if (url.contains(".")) return url.substring(0, url.lastIndexOf('.'));
            else return url;

        }
    }

    private final static DecimalFormat DECIMAL_SIZE_FORMAT = new DecimalFormat("#,##0.#");
    private final static String[] units = new String[]{"B", "KB", "MB", "GB", "TB"};

    public static String getFileSize(long size) {
        if (size <= 0) return "0";
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return "Size " + DECIMAL_SIZE_FORMAT.format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }


    public static void intentMedia(Context context, Media media) {
        try {
            Uri uri = FileUtil.getURI(context, media.getDownloadFile(context));
            String mime = context.getContentResolver().getType(uri);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(uri, mime);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            context.startActivity(intent);
        } catch (Exception e) {
            Util.TOAST("Unable to open file, Couldn't find any installed app to open this media");
        }

    }

    public static void intentMap(Context context, Media media) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("geo:" + media.getLatitude() + "," + media.getLongitude() + "?q=" + media.getLatitude() + "," + media.getLongitude()));
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Util.TOAST("Unable to open file, Couldn't find any installed app to open this media");
            e.printStackTrace();
        }
    }


    public static void initGalleryScanner(Context context, File file) {
        new MediaScannerClient(context, file).connect();
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri contentUri = Uri.fromFile(getRootDirectory(context));
        mediaScanIntent.setData(contentUri);
        context.sendBroadcast(mediaScanIntent);
    }


    public static String getFormattedFilename(String name) {
        if (TextUtil.isNullOrEmpty(name)) return name;
        String extension = FileUtil.getExtensionWithDot(name);
        if (name.contains(UNDER_SCORE)) {
            name = name.substring(0, name.lastIndexOf("_"));
            return name + extension;
        }
        return name;
    }
}