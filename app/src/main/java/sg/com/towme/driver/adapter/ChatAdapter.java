package sg.com.towme.driver.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import sg.com.towme.driver.R;
import sg.com.towme.driver.activity.ImageActivity;
import sg.com.towme.driver.listener.RecyclerViewClickListener;
import sg.com.towme.driver.model.chat.Chat;
import sg.com.towme.driver.socket.SocketConstant;
import sg.com.towme.driver.utility.AppHelper;
import sg.com.towme.driver.utility.DateTimeUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.LayoutInflater.from;

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<Chat> chatList;
    String startedImages;
    String stoppedImages;

    public ChatAdapter(Context context, List<Chat> chatList,String startedImages, String stoppedImages) {
        this.context = context;
        this.chatList = chatList;
        this.startedImages=startedImages;
        this.stoppedImages=stoppedImages;
    }

    public void setCallback(RecyclerViewClickListener callback) {
        this.callback = callback;
    }

    RecyclerViewClickListener callback;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == 0) {
            View view = from(parent.getContext()).inflate(R.layout.row_chat_right, parent, false);
            return new SenderHolder(view);
        } else if (viewType == 1) {
            View view = from(parent.getContext()).inflate(R.layout.row_chat_left, parent, false);
            return new ReceiverHolder(view);
        } else if (viewType == 2) {
            View view = from(parent.getContext()).inflate(R.layout.row_chat_start_ride, parent, false);
            return new StartRideHolder(view);
        } else {
            View view = from(parent.getContext()).inflate(R.layout.row_chat_end_ride, parent, false);
            return new EndRideHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (getItemViewType(position) == 1)// Receiver View Holder
        {
            ReceiverHolder baseHolder = (ReceiverHolder) holder;
            baseHolder.bind(chatList.get(position));
        } else if (getItemViewType(position) == 0) {//Sender View Holder
            SenderHolder baseHolder = (SenderHolder) holder;
            baseHolder.bind(chatList.get(position));
        } else if (getItemViewType(position) == 2) {
            StartRideHolder baseHolder = (StartRideHolder) holder;
            baseHolder.bind(chatList.get(position));
        } else {
            EndRideHolder baseHolder = (EndRideHolder) holder;
            baseHolder.bind(chatList.get(position));

        }
    }

    @Override
    public int getItemCount() {
        return chatList.size();
    }

    @Override
    public int getItemViewType(int position) {
        Integer userID = AppHelper.getInstance().getUserDetails().getUserid();
       /* if (userID == chatList.get(position).getSenderId())
            return 0;
        else
            return 1;*/

        if (chatList.get(position).getType().equalsIgnoreCase("start_ride")) {
            return 2;
        } else if (chatList.get(position).getType().equalsIgnoreCase("end_ride")) {
            return 3;
        } else {
            if (userID == chatList.get(position).getSenderId())
                return 0;
            else
                return 1;
        }
    }

    class SenderHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtTime)
        TextView txtTime;
        @BindView(R.id.txtMessage)
        TextView txtMessage;
        @BindView(R.id.imageview)
        ImageView imageview;

        SenderHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(Chat chat) {
            if (chat.getType().equalsIgnoreCase(SocketConstant.TEXT)) {
                imageview.setVisibility(View.GONE);
                txtMessage.setVisibility(View.VISIBLE);
                txtMessage.setText(chat.getMessage());
            } else if (chat.getType().equalsIgnoreCase(SocketConstant.IMAGE)) {
                imageview.setVisibility(View.VISIBLE);
                txtMessage.setVisibility(View.GONE);
                Glide.with(context).load(chat.getMessage()).placeholder(R.drawable.placeholder).into(imageview);
                imageview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, ImageActivity.class);
                        intent.putExtra(ImageActivity.EXTRA_PHOTO, chat.getMessage());
                        context.startActivity(intent);
                    }
                });
            }

            txtTime.setText(DateTimeUtil.convertUTCIntoLocal(chat.getCreatedAt(), "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "EEE, dd MMM yyyy hh:mm a"));

        }

    }

    class ReceiverHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtTime)
        TextView txtTime;
        @BindView(R.id.txtMessage)
        TextView txtMessage;
        @BindView(R.id.imageview)
        ImageView imageview;

        ReceiverHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(Chat chat) {

            if (chat.getType().equalsIgnoreCase(SocketConstant.TEXT)) {
                imageview.setVisibility(View.GONE);
                txtMessage.setVisibility(View.VISIBLE);
                txtMessage.setText(chat.getMessage());
            } else if (chat.getType().equalsIgnoreCase(SocketConstant.IMAGE)) {
                imageview.setVisibility(View.VISIBLE);
                txtMessage.setVisibility(View.GONE);
                Glide.with(context).load(chat.getMessage()).placeholder(R.drawable.placeholder).into(imageview);
                imageview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, ImageActivity.class);
                        intent.putExtra(ImageActivity.EXTRA_PHOTO, chat.getMessage());
                        context.startActivity(intent);
                    }
                });
            }

            txtTime.setText(DateTimeUtil.convertUTCIntoLocal(chat.getCreatedAt(), "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "EEE, dd MMM yyyy hh:mm a"));

        }

    }

   /* class RatingHolder extends RecyclerView.ViewHolder {

        RatingHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }*/

    class EndRideHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtTime)
        TextView txtTime;
        @BindView(R.id.txtDriverName)
        TextView txtDriverName;
        @BindView(R.id.txtDriverCompany)
        TextView txtDriverCompany;
        @BindView(R.id.txtMessage)
        TextView txtMessage;
        @BindView(R.id.imhVehicle)
        ImageView imhVehicle;
        @BindView(R.id.imgDriverProfilePic)
        ImageView imgDriverProfilePic;
        @BindView(R.id.ratingbar)
        RatingBar ratingbar;
        @BindView(R.id.edtReview)
        EditText edtReview;
        @BindView(R.id.btnSubmit)
        Button btnSubmit;

        EndRideHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(Chat chat) {
            txtMessage.setText(chat.getMessage());
            Glide.with(context).load(stoppedImages).placeholder(R.drawable.placeholder).into(imhVehicle);
            imhVehicle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ImageActivity.class);
                    intent.putExtra(ImageActivity.EXTRA_PHOTO, chat.getBooking().getStoppedImages().get(0));
                    context.startActivity(intent);
                }
            });
            Glide.with(context).load(chat.getSender().getProfilePic()).placeholder(R.drawable.placeholder).into(imgDriverProfilePic);
            txtDriverName.setText(chat.getSender().getName());
            txtDriverCompany.setText(chat.getSender().getCompanyName());
            txtTime.setText(DateTimeUtil.convertUTCIntoLocal(chat.getCreatedAt(), "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "EEE, dd MMM yyyy hh:mm a"));


        }

    }

    class StartRideHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtTime)
        TextView txtTime;
        @BindView(R.id.txtMessage)
        TextView txtMessage;
        @BindView(R.id.imhVehicle)
        ImageView imhVehicle;

        StartRideHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(Chat chat) {

            txtMessage.setText(chat.getMessage());
            Glide.with(context).load(startedImages).placeholder(R.drawable.placeholder).into(imhVehicle);
            imhVehicle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ImageActivity.class);
                    intent.putExtra(ImageActivity.EXTRA_PHOTO, chat.getBooking().getStartedImages().get(0));
                    context.startActivity(intent);
                }
            });
            txtTime.setText(DateTimeUtil.convertUTCIntoLocal(chat.getCreatedAt(), "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "EEE, dd MMM yyyy hh:mm a"));

        }

    }


}
