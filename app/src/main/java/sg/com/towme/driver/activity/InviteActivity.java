package sg.com.towme.driver.activity;

import android.content.ClipboardManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;

import sg.com.towme.driver.R;

import sg.com.towme.driver.model.BaseModel;
import sg.com.towme.driver.model.config_.ConfigDetails;
import sg.com.towme.driver.model.invite.InvitePojo;
import sg.com.towme.driver.network.IResponseCallback;
import sg.com.towme.driver.network.NetworkCall;
import sg.com.towme.driver.utility.AppHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

public class InviteActivity extends ToolBarActivity {

    @BindView(R.id.txtTitle)
    AppCompatTextView txtTitle;
    @BindView(R.id.txtDescription)
    AppCompatTextView txtDescription;
    @BindView(R.id.txtInviteCode)
    AppCompatTextView txtInviteCode;
    @BindView(R.id.txtCopy)
    AppCompatTextView txtCopy;
    @BindView(R.id.txtTAndC)
    AppCompatTextView txtTAndC;

    ConfigDetails configDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_invite);
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);

        configDetails = AppHelper.getInstance().getConfigDetails();

        iniUI();

      /*  CheckNetworkListener callback = () -> {
            showProgressDialog();
            getInvitationDetails();
        };

        if (isNetworkAvailable(txtCopy, callback)) {
            callback.onRetryClick();
        }*/


    }

    private void iniUI() {
//        setHomeIcon(R.drawable.ic_back_white);
        setToolbarTitle(getString(R.string.invite));

        txtTitle.setText(configDetails.getInviteHeading());
        txtDescription.setText(configDetails.getInviteText());
        if (AppHelper.getInstance().getUserDetails() != null)
            txtInviteCode.setText(AppHelper.getInstance().getUserDetails().getInviteCode());
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getInvitationDetails() {

        NetworkCall.getInstance().getInviteCode(new IResponseCallback<InvitePojo>() {
            @Override
            public void success(InvitePojo data) {
                hideProgressDialog();
                if (data.getCode() == 1) {
                    txtInviteCode.setText(data.getData().getInviteCode() + "");
                } else {
                    showSnackBar(txtCopy, data.getMessage());
                }
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                hideProgressDialog();
                showSnackBar(txtCopy, baseModel.getMessage());
            }

            @Override
            public void onError(Call<InvitePojo> responseCall, Throwable T) {
                hideProgressDialog();
                showSnackBar(txtCopy, getString(R.string.error_message));
            }
        });

    }


    @OnClick({R.id.txtTAndC, R.id.txtCopy})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtTAndC:
                openTermsAndCondition();
                break;
            case R.id.txtCopy:
                ClipboardManager cm = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                cm.setText(txtInviteCode.getText());
                Toast.makeText(this, R.string.copied_to_clipboard, Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void openTermsAndCondition() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(configDetails.getTermsAndCondition().trim()));
        startActivity(intent);
    }
}
