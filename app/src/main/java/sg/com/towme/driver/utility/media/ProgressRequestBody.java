package sg.com.towme.driver.utility.media;

import androidx.annotation.NonNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.BufferedSink;

public class ProgressRequestBody extends RequestBody {


    private static final int DEFAULT_BUFFER_SIZE = 2048;

    private long requestCode;
    private File mFile;
    private MediaType mediaType;
    private ProgressiveService progressiveService;
    private boolean updateProgress = false;


    public ProgressRequestBody(long requestCode, File file, MediaType mediaType, ProgressiveService listener, boolean updateEnable) {
        this.mFile = file;
        this.mediaType = mediaType;
        this.requestCode = requestCode;
        this.progressiveService = listener;
        this.updateProgress = updateEnable;
    }


    public ProgressRequestBody(long requestCode, File file, ProgressiveService listener) {
        this.mFile = file;
        this.requestCode = requestCode;
        this.progressiveService = listener;
    }


    @Override
    public MediaType contentType() {
        if (mediaType == null) mediaType = Media.MEDIA_TYPE_IMAGE;
        return mediaType;
    }

    @Override
    public long contentLength() throws IOException {
        return mFile.length();
    }

    @Override
    public void writeTo(@NonNull BufferedSink sink) throws IOException {
        if (progressiveService != null && updateProgress) {
            progressiveService.onStarted(requestCode);
        }

        long fileLength = mFile.length();
        byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
        long uploaded = 0;


        try (FileInputStream in = new FileInputStream(mFile)) {
            int read;
            while ((read = in.read(buffer)) != -1) {
                uploaded += read;
                sink.write(buffer, 0, read);
                if (progressiveService != null && updateProgress) {
                    progressiveService.onProcess(requestCode, 100f * uploaded / fileLength, uploaded, fileLength);
                }
            }
        }
    }
}