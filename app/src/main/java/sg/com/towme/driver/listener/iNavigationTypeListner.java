package sg.com.towme.driver.listener;


import sg.com.towme.driver.enumeration.NavigationType;

public interface iNavigationTypeListner {

    void onNavigationTypeSelect(NavigationType accountType);

}
