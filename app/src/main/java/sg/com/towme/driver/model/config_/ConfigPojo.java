
package sg.com.towme.driver.model.config_;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import sg.com.towme.driver.model.BaseModel;

public class ConfigPojo extends BaseModel {

    @SerializedName("data")
    @Expose
    private ConfigDetails data;

    public ConfigDetails getData() {
        return data;
    }

    public void setData(ConfigDetails data) {
        this.data = data;
    }

}
