package sg.com.towme.driver.Dialog;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import sg.com.towme.driver.R;
import sg.com.towme.driver.enumeration.Info;

public class BookingRemarkDialog extends BaseDialog {


    Unbinder unbinder;
    @BindView(R.id.txtMessage)
    AppCompatTextView txtMessage;
    @BindView(R.id.txtOk)
    AppCompatTextView txtOk;

    private String message;

    public void setMessage(String message) {
        this.message = message;
    }

    public static BookingRemarkDialog newInstance(String message) {
        BookingRemarkDialog fragment = new BookingRemarkDialog();
        fragment.setMessage(message);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_booking_remark, container, false);

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        txtMessage.setText(message);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.txtOk})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtOk:
                dismiss();
                break;

        }
    }
}