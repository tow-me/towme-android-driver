package sg.com.towme.driver.mapNavigation;

import android.Manifest;
import android.content.Context;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.Looper;
import android.widget.Toast;

import sg.com.towme.driver.Controller;
import sg.com.towme.driver.utility.DebugLog;

import java.util.List;
import java.util.Locale;

public class GPSTracker implements LocationListener {

    private static final String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 2; // 10 meters
    //        private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
    private static final long MIN_TIME_BW_UPDATES = 1000 * 10; // 1 minute
//    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute

    private Context context;
    private Location location;
    private boolean canGetLocation = false;
    iLocationChangeListner iLocationChangeListner;

    public GPSTracker(Context context, iLocationChangeListner iLocationChangeListner) {
        this.context = context;
        this.iLocationChangeListner = iLocationChangeListner;
        location = getLocation();
    }

//    @SuppressLint("MissingPermission")
    public Location getLocation() {

        try {
            LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

            if (locationManager == null) return location;

            boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) return location;

            canGetLocation = true;

            HandlerThread handlerThread = new HandlerThread("MyHandlerThread");
            handlerThread.start();

            Looper looper = handlerThread.getLooper();

           /* if (isGPSEnabled) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this, looper);
                location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }
            if (isNetworkEnabled) {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this, looper);
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }*/

            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_FINE);
            criteria.setAltitudeRequired(false);
            criteria.setBearingRequired(true);
            criteria.setCostAllowed(true);
            String strLocationProvider = locationManager.getBestProvider(criteria, true);

            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this, looper);
             location = locationManager.getLastKnownLocation(strLocationProvider);


         /*   Criteria criteria = new Criteria();
            criteria.setPowerRequirement(Criteria.POWER_MEDIUM); // Chose your desired power consumption level.
            criteria.setAccuracy(Criteria.ACCURACY_FINE); // Choose your accuracy requirement.
            criteria.setSpeedRequired(true); // Chose if speed for first location fix is required.
            criteria.setAltitudeRequired(false); // Choose if you use altitude.
            criteria.setBearingRequired(true); // Choose if you use bearing.
            criteria.setCostAllowed(false); // Choose if this provider can waste money :-)

            DebugLog.e("Critearea ::" + locationManager.getBestProvider(criteria, true));*/
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
        }

        return location;
    }


    public void setLocation(Location location) {
        this.location = location;
    }

    public double getLatitude() {
        if (location == null) return 0;
        return location.getLatitude();
    }

    public double getLongitude() {
        if (location == null) return 0;
        return location.getLongitude();
    }

    public String getAddress() {
        if (location == null) return "";
        return getCompleteAddressString(location.getLatitude(), location.getLongitude());
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String full_address = "";
        try {
            Geocoder geocoder = new Geocoder(Controller.getInstance(), Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null && addresses.size() > 0) {
                Address address = addresses.get(0);
                StringBuilder sb = new StringBuilder("");
                for (int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
                    sb.append(address.getAddressLine(i)).append("\n");
                }
                full_address = sb.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return full_address;
    }

    public boolean canGetLocation() {
        return this.canGetLocation;
    }


    @Override
    public void onLocationChanged(Location location) {
        DebugLog.e("onLocationChanged............");
        this.location = location;
        iLocationChangeListner.onLocationChange(getAddress(), getLatitude(), getLongitude(),location);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
