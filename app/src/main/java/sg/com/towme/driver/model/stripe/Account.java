package sg.com.towme.driver.model.stripe;


import sg.com.towme.driver.utility.TextUtil;

public class Account {
    private String id;
    private String email;
    private String type;
    private Settings settings;
    private boolean details_submitted;
    private String default_currency;
    private String country;
    private boolean charges_enabled;
    private Capabilities capabilities;
    private Business_profile business_profile;
    private String object;
    private boolean payouts_enabled;

    public String getType() {
        return type;
    }

    public Settings getSettings() {
        return settings;
    }

    public boolean getPayouts_enabled() {
        return payouts_enabled;
    }

    public String getEmail() {
        if (TextUtil.isNullOrEmpty(email)) return "Not Specified";
        return email;
    }

    public boolean getDetails_submitted() {
        return details_submitted;
    }

    public String getDefault_currency() {
        return default_currency;
    }

    public String getCountry() {
        return country;
    }

    public boolean getCharges_enabled() {
        return charges_enabled;
    }

    public Capabilities getCapabilities() {
        return capabilities;
    }

    public Business_profile getBusiness_profile() {
        return business_profile;
    }

    public String getObject() {
        return object;
    }

    public String getId() {
        return id;
    }

    public static class Settings {
        private Payments payments;
        private Dashboard dashboard;
        private Card_payments card_payments;
        private Branding branding;

        public Payments getPayments() {
            return payments;
        }

        public Dashboard getDashboard() {
            return dashboard;
        }

        public Card_payments getCard_payments() {
            return card_payments;
        }

        public Branding getBranding() {
            return branding;
        }
    }

    public static class Payments {
        private String statement_descriptor;

        public String getStatement_descriptor() {
            return statement_descriptor;
        }
    }

    public static class Dashboard {
        private String timezone;
        private String display_name;

        public String getTimezone() {
            return timezone;
        }

        public String getDisplay_name() {
            return display_name;
        }
    }

    public static class Card_payments {
    }

    public static class Branding {
    }

    public static class Capabilities {
        private String legacy_payments;

        public String getLegacy_payments() {
            return legacy_payments;
        }
    }

    public static class Business_profile {
        private String url;
        private String support_url;
        private String support_phone;
        private String support_email;
        private Support_address support_address;
        private String name;
        private String mcc;

        public String getUrl() {
            return url;
        }

        public String getSupport_url() {
            return support_url;
        }

        public String getSupport_phone() {
            return support_phone;
        }

        public String getSupport_email() {
            return support_email;
        }

        public Support_address getSupport_address() {
            return support_address;
        }

        public String getName() {
            return name;
        }

        public String getMcc() {
            return mcc;
        }
    }

    public static class Support_address {
    }
}
