package sg.com.towme.driver.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import sg.com.towme.driver.R;

import sg.com.towme.driver.adapter.HistoryAdapter;
import sg.com.towme.driver.constant.AppConstant;
import sg.com.towme.driver.enumeration.Screens;
import sg.com.towme.driver.model.BaseModel;
import sg.com.towme.driver.model.booking.BookingData;
import sg.com.towme.driver.model.booking.BookingListPojo;
import sg.com.towme.driver.network.IResponseCallback;
import sg.com.towme.driver.network.NetworkCall;
import sg.com.towme.driver.network.Parameter;
import sg.com.towme.driver.utility.AppHelper;
import sg.com.towme.driver.utility.DateTimeUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;


public class BookingHistoryActivity extends ToolBarActivity {

    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    HistoryAdapter historyAdapter;
    List<BookingData> listBooking;
    Screens screens;
    String apiDate = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_booking_history);
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);

        iniUI();

        if (getIntent() != null) {
            screens = (Screens) getIntent().getSerializableExtra(AppConstant.SCREEN);
            if (screens == Screens.CALENDER) {
                apiDate = getIntent().getStringExtra("api_date");

                String title = DateTimeUtil.convertWithoutChangingTimezone(apiDate, "yyyy-MM-dd", "EEE, dd MMM yyyy");
                setToolbarTitle(title);
            } else {
                setToolbarTitle(getString(R.string.history));
            }
        }

        if (listBooking == null || historyAdapter == null) {
            setAdapter();
            callBookingListApi();
        } else
            setAdapter();

    }

    private void iniUI() {

//        setHomeIcon(R.drawable.ic_back_white);


    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setAdapter() {

        if (listBooking == null)
            listBooking = new ArrayList<>();

        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        if (historyAdapter == null)
            historyAdapter = new HistoryAdapter(this, listBooking, Screens.BOOKING_HISTORY);

//        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerview.getContext(), DividerItemDecoration.VERTICAL);

//        recyclerview.addItemDecoration(dividerItemDecoration);

        historyAdapter.setCallback((view, position, object) -> {
            switch (view.getId()) {
                case R.id.llRow:
                    openJobDetailsActivity(listBooking.get(position));
                    break;
            }
        });

        recyclerview.setAdapter(historyAdapter);

    }

    private void callBookingListApi() {
        showProgressDialog();
        List<Integer> listStatus = new ArrayList<>();
        if (screens == Screens.CALENDER) {
            listStatus.add(1);
            listStatus.add(2);
            listStatus.add(3);
            listStatus.add(4);
            listStatus.add(5);
            listStatus.add(6);
            listStatus.add(7);
        } else {
            listStatus.add(3);
            listStatus.add(4);
            listStatus.add(6);
            listStatus.add(7);
        }

        NetworkCall.getInstance().bookingList(listStatus, getParam(), new IResponseCallback<BookingListPojo>() {
            @Override
            public void success(BookingListPojo data) {
                hideProgressDialog();

                if (data.getCode() == 1) {
                    listBooking.clear();
                    listBooking.addAll(data.getData());
                    historyAdapter.notifyDataSetChanged();
                } else {
                    showSnackBar(recyclerview, data.getMessage());
                }

            }

            @Override
            public void onFailure(BaseModel baseModel) {
                hideProgressDialog();
                showSnackBar(recyclerview, baseModel.getMessage());
            }

            @Override
            public void onError(Call<BookingListPojo> responseCall, Throwable T) {
                hideProgressDialog();
                showSnackBar(recyclerview, getString(R.string.error_message));
            }
        });

    }

    private HashMap<String, String> getParam() {
        HashMap<String, String> param = new HashMap<>();
        param.put(Parameter.user_id, String.valueOf(AppHelper.getInstance().getUserDetails().getUserid()));
        param.put(Parameter.user_type, AppConstant.USER_TYPE);

        if (screens == Screens.CALENDER)
            param.put(Parameter.booking_date, apiDate.trim());
        Log.e("zxc", apiDate);
      /*  param.put("booking_status[]", "3");
        param.put("booking_status[]", "4");
        param.put("booking_status[]", "5");
        param.put("booking_status[]", "6");
        param.put("booking_status[]", "7");*/

        return param;
    }


}
