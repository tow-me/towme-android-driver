package sg.com.towme.driver.genericbottomsheet;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import sg.com.towme.driver.R;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class GenericBottomSheetDialog extends BottomSheetDialogFragment {
    @BindView(R.id.tvHeader)
    TextView tvHeader;
    @BindView(R.id.recycleItemList)
    RecyclerView recycleItemList;
    Unbinder unbinder;

    private RecyclerItemClick callback;
    private List<GenericBottomModel> datalist;
    private String header = "";

    public void setCallback(RecyclerItemClick callback) {
        this.callback = callback;
    }

    public void setDatalist(List<GenericBottomModel> datalist) {
        this.datalist = datalist;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    BottomSheetAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bottomdialog_generic_item_picker, container);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        tvHeader.setText(header);

        setAdapter();

    }

    private void setAdapter() {
        recycleItemList.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new BottomSheetAdapter(getContext(),datalist);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);
        recycleItemList.addItemDecoration(dividerItemDecoration);
        adapter.setCallback((view, position,object) -> {
            switch (view.getId()) {
                case R.id.lrMain:
                    dismiss();
                    callback.onItemClick(datalist.get(position));
                    break;
            }

        });

        recycleItemList.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public interface RecyclerItemClick {
        void onItemClick(GenericBottomModel genericBottomModel);
    }


}
