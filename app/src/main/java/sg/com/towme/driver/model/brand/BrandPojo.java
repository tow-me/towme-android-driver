
package sg.com.towme.driver.model.brand;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import sg.com.towme.driver.model.BaseModel;

public class BrandPojo extends BaseModel {

    @SerializedName("data")
    @Expose
    private List<BrandData> data = null;

    public List<BrandData> getData() {
        return data;
    }

    public void setData(List<BrandData> data) {
        this.data = data;
    }

}
