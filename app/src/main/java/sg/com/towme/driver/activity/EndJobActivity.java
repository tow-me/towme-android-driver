package sg.com.towme.driver.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.WorkManager;

import com.bumptech.glide.Glide;

import okhttp3.ResponseBody;
import sg.com.towme.driver.Controller;
import sg.com.towme.driver.PicImage.ImagePicker;
import sg.com.towme.driver.R;

import sg.com.towme.driver.adapter.PicAdapter;
import sg.com.towme.driver.constant.AppConstant;
import sg.com.towme.driver.genericbottomsheet.GenericBottomModel;
import sg.com.towme.driver.genericbottomsheet.GenericBottomSheetDialog;
import sg.com.towme.driver.model.BaseModel;

import sg.com.towme.driver.model.UploadPojo;
import sg.com.towme.driver.model.booking.BookingData;
import sg.com.towme.driver.model.booking.User;
import sg.com.towme.driver.model.booking.VehicleData;
import sg.com.towme.driver.network.IResponseCallback;
import sg.com.towme.driver.network.NetworkCall;
import sg.com.towme.driver.network.Parameter;
import sg.com.towme.driver.socket.SocketConstant;
import sg.com.towme.driver.socket.SocketIOClient;
import sg.com.towme.driver.utility.AppHelper;

import io.socket.client.Ack;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import jp.wasabeef.glide.transformations.CropCircleTransformation;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import sg.com.towme.driver.utility.PreferanceHelper;

public class EndJobActivity extends ToolBarActivity {

    @BindView(R.id.imgProfilePic)
    CircleImageView imgProfilePic;
    /* @BindView(R.id.recyclerview)
     RecyclerView recyclerview;*/
   /* @BindView(R.id.imgVehiclePhoto)
    AppCompatImageView imgVehiclePhoto;*/
    @BindView(R.id.btnEnd)
    AppCompatButton btnEnd;
    @BindView(R.id.imgCamera)
    AppCompatImageView imgCamera;
    /*  @BindView(R.id.txtSelectExtraCharges)
      AppCompatTextView txtSelectExtraCharges;*/
    @BindView(R.id.txtName)
    AppCompatTextView txtName;
    @BindView(R.id.txtPrice)
    AppCompatTextView txtPrice;
    @BindView(R.id.txtVehicleDetails)
    AppCompatTextView txtVehicleDetails;
    @BindView(R.id.txtTowType)
    AppCompatTextView txtTowType;
    @BindView(R.id.txtSource)
    AppCompatTextView txtSource;
    @BindView(R.id.txtDestination)
    AppCompatTextView txtDestination;
    @BindView(R.id.txtImageCount)
    AppCompatTextView txtImageCount;
    @BindView(R.id.rvEndPhoto)
    RecyclerView rvEndPhoto;
    /*  @BindView(R.id.txtAddExtraCharges)
      AppCompatImageView txtAddExtraCharges;*/
    @BindView(R.id.llTakePhoto)
    RelativeLayout llTakePhoto;
    @BindView(R.id.edtDescription)
    AppCompatEditText edtDescription;
    /*  @BindView(R.id.txtTotalExtraCharge)
      AppCompatTextView txtTotalExtraCharge;*/
    BookingData bookingData;
    String imageURL = "";
    Socket mSocket;
    //    Double totalExtraCharge = 0.0;
//    List<ExtraCharges> extraChargesList;
//    String selectedTowType = "";
    private static final int PICK_IMAGE = 1;
//    ExtraChargesAdapter adapter;

    List<String> listEndPic;
    PicAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_end_job);
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);

        mSocket = SocketIOClient.getInstance();
        iniUI();

        setAdapter();

//        setAdapter();
    }

    private void iniUI() {
//        setHomeIcon(R.drawable.ic_back_white);
        setToolbarTitle(getString(R.string.end_job));

        if (getIntent() != null) {
            bookingData = (BookingData) getIntent().getSerializableExtra(AppConstant.booking_data);

            User user = bookingData.getUser();

            Glide.with(this)
                    .load(user.getUserImage().trim())
                    .transform(new CropCircleTransformation())
                    .into(imgProfilePic);

            txtName.setText(user.getName());

            String curr = "SGD";
            if (bookingData.getCountry().contains("MY")){
                curr = "RM";
            }
            txtPrice.setText(curr + " " + bookingData.getTotalCost() + "");

            VehicleData vehicleData = bookingData.getVehicle();
            txtVehicleDetails.setText(vehicleData.getVehicleTypeName() + " - " + vehicleData.getVehicleColor() + " - " + vehicleData.getVehicleBrand() + "(" + vehicleData.getVehicleNo() + ")");

            String type = "";
//            if (bookingData.getAccidentType().length() > 0) {
//                type = getString(R.string.accident) + "(" + bookingData.getAccidentType() + ")";
//            } else {
//                type = getString(R.string.breakdown) + "(" + bookingData.getBreakdownType() + ")";
//            }

            txtTowType.setText(type);

            txtSource.setText(bookingData.getSource());
            txtDestination.setText(bookingData.getDestination());

//            selectedTowType = bookingData.getBookingTowType().trim();
//            txtSelectExtraCharges.setText(selectedTowType);
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

 /*   private void setAdapter() {
        if (extraChargesList == null) {
            extraChargesList = new ArrayList<>();
            extraChargesList.add(new ExtraCharges());
            extraChargesList.add(new ExtraCharges());
        }
        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        if (adapter == null)
            adapter = new ExtraChargesAdapter(this, extraChargesList);

        adapter.setCallback((view, position, object) -> {
            ExtraCharges extraCharges = (ExtraCharges) object;
            switch (view.getId()) {
                case R.id.edtDescriptiob:
                    extraChargesList.get(position).setDescription(extraCharges.getDescription());
                    break;
                case R.id.edtCost:
                    extraChargesList.get(position).setCost(extraCharges.getCost());

                    setTotalExtraCharge();
                    break;
            }

        });

        recyclerview.setAdapter(adapter);
    }*/

    private void openCamera() {
//        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
//        StrictMode.setVmPolicy(builder.build());
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        startActivityForResult(Intent.createChooser(intent, "Take Picture"), PICK_IMAGE);
    }

    private void pickImage() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);

    }

    private boolean isReadStorageAllowed(int permissionCode) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, permissionCode);
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == PICK_IMAGE) {
                final CharSequence[] items = {"Take Image", "Choose Image from Gallery"};

                AlertDialog.Builder builder = new AlertDialog.Builder(EndJobActivity.this);
                builder.setTitle("Add Photo");
                builder.setTitle(null);

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (items[item].equals("Take Image")) {
                            openCamera();
                        } else if (items[item].equals("Choose Image from Gallery")) {
                            pickImage();
                        }
                    }
                });
                builder.show();
            }
        } else {
            showSnackBar(imgProfilePic, getResources().getString(R.string.you_have_to_allow_storage_permission));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE) {
            if (resultCode == RESULT_OK) {
                if (data.getData() != null) {
                    Bitmap bitmap = ImagePicker.getImageFromResult(this, resultCode, data);
                    if (bitmap != null) {
                        File filePath = AppHelper.saveToInternalStorage(this, bitmap);
//                        imgVehiclePhoto.setVisibility(View.VISIBLE);
//                        imgVehiclePhoto.setImageBitmap(bitmap);
                        callUploadAPI(filePath);
                    }
                } else {
                    Bitmap image = (Bitmap) data.getExtras().get("data");
                    if (image != null) {
                        File filePath = AppHelper.saveToInternalStorage(this, image);
//                        imgVehiclePhoto.setVisibility(View.VISIBLE);
//                        imgVehiclePhoto.setImageBitmap(bitmap);
                        callUploadAPI(filePath);
                    }
                }
            }
        }
    }

    @OnClick({R.id.imgCamera, R.id.btnEnd, /*R.id.txtSelectExtraCharges, R.id.txtAddExtraCharges*/})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgCamera:
                if (listEndPic == null || listEndPic.size() < 8) {
                    if (isReadStorageAllowed(PICK_IMAGE)) {
                        final CharSequence[] items = {"Take Image", "Choose Image from Gallery"};

                        AlertDialog.Builder builder = new AlertDialog.Builder(EndJobActivity.this);
                        builder.setTitle("Add Photo");
                        builder.setTitle(null);

                        builder.setItems(items, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int item) {
                                if (items[item].equals("Take Image")) {
                                    openCamera();
                                } else if (items[item].equals("Choose Image from Gallery")) {
                                    pickImage();
                                }
                            }
                        });
                        builder.show();
                    }
                } else {
                    showSnackBar(llTakePhoto, "You can not upload more then 8 images");
                }
                break;
            case R.id.btnEnd:
                if (isValidate()) {
                    btnEnd.setEnabled(false);
                    if (bookingData.getCountry().contains("MY")) {
                        completeBookingMalaysia(bookingData.getBookingId());
                    } else {
                        completeBookingSG(bookingData.getBookingId());
//                        endRide(null);
                    }
                   /* JSONArray extraCharge = new JSONArray();

                    try {
                        for (int i = 0; i < extraChargesList.size(); i++) {
                            if (extraChargesList.get(i).getCost().length() > 0) {
                                totalExtraCharge += Double.parseDouble(extraChargesList.get(i).getCost());
                                JSONObject charge = new JSONObject();
                                charge.put(SocketConstant.DESC, extraChargesList.get(i).getDescription());
                                charge.put(SocketConstant.PRICE, extraChargesList.get(i).getCost());
                                extraCharge.put(charge);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    changeTowType(extraCharge);*/


                }
                break;
          /*  case R.id.txtSelectExtraCharges:
                openBottomSheetDialog(getString(R.string.select_extra_charges));
                break;*/
          /*  case R.id.txtAddExtraCharges:
                extraChargesList.add(new ExtraCharges());
                adapter.notifyDataSetChanged();
                break;*/
        }
    }

 /*   private void setTotalExtraCharge() {
        Double total = 0.0;
        for (int i = 0; i < extraChargesList.size(); i++) {
            if (extraChargesList.get(i).getCost().length() > 0) {
                total += Double.parseDouble(extraChargesList.get(i).getCost());

            }
        }

        txtTotalExtraCharge.setText(AppConstant.CURRENCY + " " + total);
    }*/

    private void openBottomSheetDialog(String header) {
        List<GenericBottomModel> modelList = new ArrayList<>();
        List<String> listStatusItem = Arrays.asList(getResources().getStringArray(R.array.extraChargies));
        for (int i = 0; i < listStatusItem.size(); i++) {
            GenericBottomModel model = new GenericBottomModel();
            model.setId(i + "");
            model.setItemText(listStatusItem.get(i));
            modelList.add(model);
        }
        openBottomSheet(header, modelList, new GenericBottomSheetDialog.RecyclerItemClick() {
            @Override
            public void onItemClick(GenericBottomModel genericBottomModel) {
//                selectedTowType = genericBottomModel.getItemText().trim();
//                txtSelectExtraCharges.setText(selectedTowType);
            }
        });
    }

    private void callUploadAPI(File fileImage) {

        showProgressDialog();

        MultipartBody.Part profilePic = null;
        if (fileImage != null) {
            RequestBody requestFile =
                    RequestBody.create(
                            MediaType.parse("image/*"),
                            fileImage
                    );
            profilePic = MultipartBody.Part.createFormData(Parameter.files, fileImage.getName(), requestFile);
        }

        NetworkCall.getInstance().upload(profilePic, new IResponseCallback<UploadPojo>() {
            @Override
            public void success(UploadPojo data) {
                hideProgressDialog();
                if (data.getStatusCode() == 200) {
                    imageURL = data.getData().get(0);
                    if (listEndPic == null)
                        listEndPic = new ArrayList<>();
                    listEndPic.add(imageURL);
                    adapter.notifyDataSetChanged();

                    txtImageCount.setText("(" + listEndPic.size() + ")");

                } else {
                    showSnackBar(llTakePhoto, data.getMessage());
                }
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                hideProgressDialog();
                showSnackBar(llTakePhoto, baseModel.getMessage());
            }

            @Override
            public void onError(Call<UploadPojo> responseCall, Throwable T) {
                hideProgressDialog();
                showSnackBar(llTakePhoto, getString(R.string.error_message));
            }
        });

    }

    private boolean isValidate() {
       /* if (AppValidation.isEmptyFieldValidate(edtDescription.getText().toString().trim())) {
            showSnackBar(btnEnd, getString(R.string.please_enter_description));
            return false;
        }*/
        if (listEndPic == null || listEndPic.size() <= 3) {
            showSnackBar(btnEnd, getString(R.string.please_upload_vehicle_photo));
            return false;
        }

        return true;
    }

    private HashMap<String, String> getParam() {
        HashMap<String, String> param = new HashMap<>();
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < listEndPic.size(); i++) {
            jsonArray.put(listEndPic.get(i));
        }
        param.put(Parameter.images, jsonArray.toString());
        param.put(SocketConstant.END_RIDE_DESCRIPTION, edtDescription.getText().toString().trim());
        return param;
    }

    private void completeBookingMalaysia(int id) {
        showProgressDialog();
        NetworkCall.getInstance().completeMalaysia(id+"", getParam(), new IResponseCallback<ResponseBody>() {
            @Override
            public void success(ResponseBody data) {
                try {
                    JSONObject json = new JSONObject(data.string());
                    if (json.getBoolean("status")) {
                        endRide(json.getJSONObject("data").getJSONObject("booking").toString());
                    } else {
                        hideProgressDialog();
                        showSnackBar(btnEnd, json.getString("message"));
                    }
                } catch (Exception e) {}
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                hideProgressDialog();
                showSnackBar(btnEnd, baseModel.getMessage());
            }

            @Override
            public void onError(Call<ResponseBody> responseCall, Throwable T) {
                hideProgressDialog();
                showSnackBar(btnEnd, getString(R.string.error_message));
            }
        });
    }

    private void completeBookingSG(int id) {
        showProgressDialog();
        NetworkCall.getInstance().completeSG(id+"", getParam(), new IResponseCallback<ResponseBody>() {
            @Override
            public void success(ResponseBody data) {
                try {
                    JSONObject json = new JSONObject(data.string());
                    if (json.getBoolean("status")) {
                        endRide(json.getJSONObject("data").getJSONObject("booking").toString());
                    } else {
                        hideProgressDialog();
                        showSnackBar(btnEnd, json.getString("message"));
                    }
                } catch (Exception e) {}
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                hideProgressDialog();
                showSnackBar(btnEnd, baseModel.getMessage());
            }

            @Override
            public void onError(Call<ResponseBody> responseCall, Throwable T) {
                hideProgressDialog();
                showSnackBar(btnEnd, getString(R.string.error_message));
            }
        });
    }

    private void endRide(/*JSONArray extraCharge*/String booking) {
        showProgressDialog();
        JSONObject param = new JSONObject();
        try {
            param.put(SocketConstant.BOOKING_ID, bookingData.getBookingId());
            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < listEndPic.size(); i++) {
//                jsonArray.put(imageURL);
                jsonArray.put(listEndPic.get(i));
            }
            if (booking != null) {
                param.put(SocketConstant.booking, booking);
            }
            param.put(SocketConstant.IMAGES, jsonArray);
            param.put(SocketConstant.END_RIDE_DESCRIPTION, edtDescription.getText().toString().trim());
            if (AppHelper.getInstance().getUserToken() != null) {
                param.put(SocketConstant.TOKEN, AppHelper.getInstance().getUserToken());
            }
           /* param.put(SocketConstant.END_RIDE_EXTRA_CHARGE, extraCharge);
            param.put(SocketConstant.BOOKING_CHARGE, 0);
            param.put(SocketConstant.SERVICE_CHARGE, String.valueOf(totalExtraCharge));//extra charge
            param.put(SocketConstant.TOTAL_COST, bookingData.getTotalCost().trim());//only total*/

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("Stop ride param", param.toString());

        mSocket.emit(SocketConstant.STOP_RIDE, param, new Ack() {
            @Override
            public void call(Object... args) {
                int code = 400;

                JSONObject obj = null;
                try {
                    obj = new JSONObject(args[0].toString());
                    code = obj.getInt("status_code");

                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (code == 401) {
                    try {
                        if (getApplicationContext() != null) {
                            hideProgressDialog();
                            btnEnd.setEnabled(true);
                            PreferanceHelper.getInstance().clearPreference();
                            WorkManager.getInstance(Controller.getInstance()).cancelAllWork();
                            openLoginActivity();
                        }
                    } catch (Exception e) {}
                } else {
                    Log.e("Stop Ride success", args[0].toString() + "...");

              /*  runOnUiThread(() -> {
                    hideProgressDialog();
                    openHomeActivity();
                });*/

                    sendEndRideMessage("Your " + bookingData.getVehicle().getVehicleTypeName() + " has been dropped off at your destination.", SocketConstant.end_ride);
                }
            }
        });

    }

    private void sendEndRideMessage(String strMessage, String strType) {
        Log.e("Booking_id", bookingData.getBookingId() + "...");
//        showProgressDialog();
        String sendID = String.valueOf(AppHelper.getInstance().getUserDetails().getUserid());
        JSONObject param = new JSONObject();
        try {
            param.put(SocketConstant.BOOKING_ID, bookingData.getBookingId());
            param.put(SocketConstant.SENDER_ID, sendID);
            param.put(SocketConstant.MESSAGE, strMessage);
            param.put(SocketConstant.TYPE, strType);
            if (AppHelper.getInstance().getUserToken() != null) {
                param.put(SocketConstant.TOKEN, AppHelper.getInstance().getUserToken());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mSocket.emit(SocketConstant.MESSAGE, param, new Ack() {
            @Override
            public void call(Object... args) {
                int code = 400;

                JSONObject obj = null;
                try {
                    obj = new JSONObject(args[0].toString());
                    code = obj.getInt("status_code");

                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (code == 401) {
                    try {
                        if (getApplicationContext() != null) {
                            hideProgressDialog();
                            btnEnd.setEnabled(true);
                            PreferanceHelper.getInstance().clearPreference();
                            WorkManager.getInstance(Controller.getInstance()).cancelAllWork();
                            openLoginActivity();
                        }
                    } catch (Exception e) {}
                } else {
                    Log.e("Send Message", args[0].toString() + "...");

            /*    Gson gson = new Gson();
                List<Chat> list = Arrays.asList(gson.fromJson(args[0].toString(), Chat[].class));
                Log.e("chat list size", list.size() + "...");*/

                    runOnUiThread(() -> {
                        hideProgressDialog();
                        btnEnd.setEnabled(true);
                        openHomeActivity();
                    });
                }
            }
        });

    }

    private void setAdapter() {

        if (listEndPic == null)
            listEndPic = new ArrayList<>();

        if (adapter == null)
            adapter = new PicAdapter(this, listEndPic,true);

        adapter.setCallback((view, position, object) -> {
            switch (view.getId()) {
                case R.id.imgClose:
                    listEndPic.remove(position);
                    adapter.notifyDataSetChanged();
                    txtImageCount.setText("(" + listEndPic.size() + ")");
                    break;
            }

        });

        rvEndPhoto.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rvEndPhoto.setAdapter(adapter);
    }


    /*private void changeTowType(JSONArray extraCharge) {
        HashMap<String, String> param = new HashMap<>();
        param.put("booking_id", String.valueOf(bookingData.getBookingId()));
        param.put("type", selectedTowType);
        if (selectedTowType.trim().equalsIgnoreCase(bookingData.getBookingTowType().trim()))
            param.put("flag", "0");//no change
        else
            param.put("flag", "1");// change
        param.put("user_id", String.valueOf(bookingData.getUser().getUserid()));
        param.put("driver_id", String.valueOf(AppHelper.getInstance().getUserDetails().getUserid()));
        double total = Double.parseDouble(bookingData.getTotalCost().trim()) + totalExtraCharge - Double.parseDouble(bookingData.getDiscountedAmount().trim());
        double amount = total * 100;
        int finalAmount = (int) amount;
        param.put("amount", String.valueOf(finalAmount));
        param.put("currency", AppConstant.CURRENCY);

        NetworkCall.getInstance().changeTowType(param, new IResponseCallback<BaseModel>() {
            @Override
            public void success(BaseModel data) {

                if (data.getCode() == 1) {
                    endRide(extraCharge);
                } else {
                    hideProgressDialog();
                    showSnackBar(btnEnd, data.getMessage());
                }
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                hideProgressDialog();
                showSnackBar(btnEnd, baseModel.getMessage());
            }

            @Override
            public void onError(Call<BaseModel> responseCall, Throwable T) {
                hideProgressDialog();
                showSnackBar(btnEnd, getString(R.string.error_message));
            }
        });

    }*/
}