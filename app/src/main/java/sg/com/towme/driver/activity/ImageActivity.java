package sg.com.towme.driver.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import sg.com.towme.driver.R;
import uk.co.senab.photoview.PhotoView;
import uk.co.senab.photoview.PhotoViewAttacher;

public class ImageActivity extends AppNavigationActivity {
    @BindView(R.id.photoView)
    PhotoView photoView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private PhotoViewAttacher attacher;
    public static final String EXTRA_PHOTO = "EXTRA_PHOTO";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_image);
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);
        initUI();
    }

    private void initUI() {
        progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary), android.graphics.PorterDuff.Mode.MULTIPLY);
        attacher = new PhotoViewAttacher(photoView);
        Glide.with(photoView.getContext())
                .load(getIntent().getStringExtra(EXTRA_PHOTO))
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        attacher.update();
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(photoView);
        attacher.setOnSingleFlingListener(new PhotoViewAttacher.OnSingleFlingListener() {
            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                btnClose();
                return false;
            }
        });
    }

    @OnClick(R.id.btnClose)
    void btnClose() {
        onBackPressed();
    }
}