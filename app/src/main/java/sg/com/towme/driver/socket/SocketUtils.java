package sg.com.towme.driver.socket;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Dhara Golakiya (email:dharagolakiya4@gmail.com) on 03/July/2020
 */
public class SocketUtils {

    private static final SocketUtils ourInstance = new SocketUtils();

    public static SocketUtils getInstance() {
        return ourInstance;
    }

    private SocketUtils() {
    }

    public JSONObject getParameter(String key, String value) {
        JSONObject object = new JSONObject();
        try {
            object.put(key, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return object;
    }
}
