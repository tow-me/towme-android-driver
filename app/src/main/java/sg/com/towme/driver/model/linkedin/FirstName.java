
package sg.com.towme.driver.model.linkedin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FirstName {

    @SerializedName("localized")
    @Expose
    private Localized localized;
    @SerializedName("preferredLocale")
    @Expose
    private PreferredLocale preferredLocale;

    public Localized getLocalized() {
        return localized;
    }

    public void setLocalized(Localized localized) {
        this.localized = localized;
    }

    public PreferredLocale getPreferredLocale() {
        return preferredLocale;
    }

    public void setPreferredLocale(PreferredLocale preferredLocale) {
        this.preferredLocale = preferredLocale;
    }

}
