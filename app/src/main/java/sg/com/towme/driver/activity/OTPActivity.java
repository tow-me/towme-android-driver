package sg.com.towme.driver.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;


import sg.com.towme.driver.R;
import sg.com.towme.driver.constant.AppConstant;
import sg.com.towme.driver.enumeration.Screens;
import sg.com.towme.driver.listener.CheckNetworkListener;
import sg.com.towme.driver.model.BaseModel;
import sg.com.towme.driver.model.user.UserData;
import sg.com.towme.driver.model.user.UserPojo;
import sg.com.towme.driver.network.IResponseCallback;
import sg.com.towme.driver.network.NetworkCall;
import sg.com.towme.driver.network.Parameter;
import sg.com.towme.driver.utility.AppHelper;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

public class OTPActivity extends ToolBarActivity {

    @BindView(R.id.btnSubmit)
    AppCompatButton btnSubmit;
    @BindView(R.id.edt1)
    AppCompatEditText edt1;
    @BindView(R.id.edt2)
    AppCompatEditText edt2;
    @BindView(R.id.edt3)
    AppCompatEditText edt3;
    @BindView(R.id.edt4)
    AppCompatEditText edt4;
    @BindView(R.id.edt5)
    AppCompatEditText edt5;
    @BindView(R.id.edt6)
    AppCompatEditText edt6;
    @BindView(R.id.edtPhoneNumber)
    AppCompatEditText edtPhoneNumber;
    @BindView(R.id.imgEdit)
    AppCompatImageView imgEdit;
    @BindView(R.id.txtResendOTP)
    AppCompatTextView txtResendOTP;

    UserData userData;
    Screens screens;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_otp);
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);

        iniUI();
    }

    private void iniUI() {

        setToolbarTitle(getString(R.string.otp_verification));

        Intent intent = getIntent();
        screens = (Screens) intent.getSerializableExtra(AppConstant.SCREEN);
        userData = (UserData) intent.getSerializableExtra(AppConstant.user_data);

        edtPhoneNumber.setText(userData.getMobile());

        if (userData != null && userData.getMobileVCode() != null)
            if (!userData.getMobileVCode().isEmpty()) {
                String otp = userData.getMobileVCode();
                char[] otpAr = new char[otp.length()];
                for (int i = 0; i < otp.length(); i++) {
                    otpAr[i] = otp.charAt(i);
                }

//                edt1.setText(String.valueOf(otpAr[0]));
//                edt2.setText(String.valueOf(otpAr[1]));
//                edt3.setText(String.valueOf(otpAr[2]));
//                edt4.setText(String.valueOf(otpAr[3]));
//                edt5.setText(String.valueOf(otpAr[4]));
//                edt6.setText(String.valueOf(otpAr[5]));
            }


        edt1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 0) {

                } else {
                    edt2.setEnabled(true);
                    edt2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        edt2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.length() == 0) {
                    edt1.requestFocus();
                } else {
                    edt3.setEnabled(true);
                    edt3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        edt3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 0) {
                    edt2.requestFocus();
                } else {
                    edt4.setEnabled(true);
                    edt4.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        edt4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 0) {
                    edt3.requestFocus();
                } else {
                    edt5.setEnabled(true);
                    edt5.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        edt5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 0) {
                    edt4.requestFocus();
                } else {
                    edt6.setEnabled(true);
                    edt6.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        edt6.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 0) {
                    btnSubmit.setEnabled(false);
                    edt5.requestFocus();
                } else {
                    btnSubmit.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });


        edtPhoneNumber.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    edtPhoneNumber.setEnabled(false);
                    edtPhoneNumber.getBackground().mutate().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.SRC_ATOP);
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick({R.id.imgEdit, R.id.txtResendOTP, R.id.btnSubmit})
    public void onViewClicked(View view) {
        hideKeyBoard();
        switch (view.getId()) {
            case R.id.imgEdit:
                edtPhoneNumber.setEnabled(true);
                edtPhoneNumber.requestFocus();
                edtPhoneNumber.getBackground().mutate().setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_ATOP);
                edtPhoneNumber.setSelection(edtPhoneNumber.getText().toString().trim().length());
                break;
            case R.id.txtResendOTP:
                edt1.setText("");
                edt2.setText("");
                edt3.setText("");
                edt4.setText("");
                edt5.setText("");
                edt6.setText("");

                CheckNetworkListener callback1 = () -> {
                    showProgressDialog();
                    callResendOtpAPI();
                };

                if (isNetworkAvailable(btnSubmit, callback1)) {
                    callback1.onRetryClick();
                }

                break;
            case R.id.btnSubmit:
                CheckNetworkListener callback = () -> {
                    showProgressDialog();
                    callOTPVerificationAPI();
                };

                if (isNetworkAvailable(btnSubmit, callback)) {
                    callback.onRetryClick();
                }

                break;
        }
    }

    private void callOTPVerificationAPI() {

        NetworkCall.getInstance().OTPVerification(getOTPVerificationParam(), new IResponseCallback<UserPojo>() {
            @Override
            public void success(UserPojo data) {
                hideProgressDialog();
                if (data.getCode() == 1) {
                    if (screens == Screens.SIGNUP) {
                        AppHelper.getInstance().setLogin(true);
                        AppHelper.getInstance().setUserDetails(data.getData());
//                        AppHelper.getInstance().setUserToken(data.getToken());
                        openHomeActivity();
                    } else {
                        //reset password
                        openResetPasswordActivity(data.getData());
                    }
                } else {
                    showSnackBar(btnSubmit, data.getMessage());
                }
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                hideProgressDialog();
                showSnackBar(btnSubmit, baseModel.getMessage());
            }

            @Override
            public void onError(Call<UserPojo> responseCall, Throwable T) {
                hideProgressDialog();
                showSnackBar(btnSubmit, getString(R.string.error_message));
            }
        });

    }

    private HashMap<String, String> getOTPVerificationParam() {
        HashMap<String, String> param = new HashMap<>();
        param.put(Parameter.userid, String.valueOf(userData.getUserid()));

        String strOtp = edt1.getText().toString().trim() +
                edt2.getText().toString().trim() +
                edt3.getText().toString().trim() +
                edt4.getText().toString().trim() +
                edt5.getText().toString().trim() +
                edt6.getText().toString().trim();

        param.put(Parameter.otp, strOtp);
        param.put(Parameter.user_type, AppConstant.USER_TYPE);
        return param;
    }

    private void callResendOtpAPI() {

        NetworkCall.getInstance().resendOTP(getResendOtpParam(), new IResponseCallback<BaseModel>() {
            @Override
            public void success(BaseModel data) {
                hideProgressDialog();
                showSnackBar(btnSubmit, data.getMessage());
                if (data.getCode() == 1) {
                }
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                hideProgressDialog();
                showSnackBar(btnSubmit, baseModel.getMessage());
            }

            @Override
            public void onError(Call<BaseModel> responseCall, Throwable T) {
                hideProgressDialog();
                showSnackBar(btnSubmit, getString(R.string.error_message));
            }
        });

    }

    private HashMap<String, String> getResendOtpParam() {
        HashMap<String, String> param = new HashMap<>();
        param.put(Parameter.userid, String.valueOf(userData.getUserid()));
        param.put(Parameter.user_type, AppConstant.USER_TYPE);
        return param;
    }

}
