package sg.com.towme.driver.activity;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import sg.com.towme.driver.R;
import sg.com.towme.driver.adapter.TransactionHistoryAdapter;
import sg.com.towme.driver.listener.CheckNetworkListener;
import sg.com.towme.driver.model.BaseModel;
import sg.com.towme.driver.model.transaction.TransactionData;
import sg.com.towme.driver.network.IResponseCallback;
import sg.com.towme.driver.network.NetworkCall;
import sg.com.towme.driver.network.Parameter;
import sg.com.towme.driver.utility.AppHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

public class TransactionHistoryActivity extends ToolBarActivity {

    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;

    TransactionHistoryAdapter adapter;
    List<TransactionData> listTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_transaction_history);
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);

        iniUI();

        if (listTransaction == null) {
            listTransaction = new ArrayList<>();
            setAdapter();
            CheckNetworkListener callback = () -> {
                showProgressDialog();
                callTransactionHistoryAPI();
            };

            if (isNetworkAvailable(recyclerview, callback)) {
                callback.onRetryClick();
            }
        } else
            setAdapter();

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void iniUI() {
//        setHomeIcon(R.drawable.ic_back_white);
        setToolbarTitle(getString(R.string.transaction_history));
    }

    private void setAdapter() {
        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        if (adapter == null)
            adapter = new TransactionHistoryAdapter(this, listTransaction);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.line_horizontal));
        recyclerview.addItemDecoration(dividerItemDecoration);
        recyclerview.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void callTransactionHistoryAPI() {

        NetworkCall.getInstance().getTransactionHistory(getParam(), new IResponseCallback<List<TransactionData>>() {
            @Override
            public void success(List<TransactionData> data) {
                hideProgressDialog();
//                if (data.getCode() == 1) {
                    listTransaction.clear();
                    listTransaction.addAll(data);
                    adapter.notifyDataSetChanged();
//                } else {
//                    showSnackBar(recyclerview, data.getMessage());
//                }
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                hideProgressDialog();
                showSnackBar(recyclerview, baseModel.getMessage());
            }

            @Override
            public void onError(Call<List<TransactionData>> responseCall, Throwable T) {
                hideProgressDialog();
                showSnackBar(recyclerview, getString(R.string.error_message));
            }
        });
    }

    private HashMap<String, String> getParam() {
        HashMap<String, String> param = new HashMap<>();
        param.put(Parameter.driver_id, String.valueOf(AppHelper.getInstance().getUserDetails().getUserid()));
        return param;
    }


}