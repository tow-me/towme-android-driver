package sg.com.towme.driver.network;

import android.content.Context;

import com.google.gson.GsonBuilder;

import okhttp3.CookieJar;
import okhttp3.JavaNetCookieJar;
import sg.com.towme.driver.BuildConfig;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import sg.com.towme.driver.utility.AppHelper;

public class RetroClient {

    private static final OkHttpClient client = new OkHttpClient.Builder()
            .connectTimeout(100, TimeUnit.SECONDS)
            .writeTimeout(100, TimeUnit.SECONDS)
            .cookieJar(getCookie())
            .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .readTimeout(100, TimeUnit.SECONDS)
            .build();

    private static CookieJar getCookie() {
        CookieManager cookieManager = new CookieManager(new PersistentCookieStore(), CookiePolicy.ACCEPT_ALL);
        CookieJar cookieJar = new JavaNetCookieJar(cookieManager);
//        CookieManager cookieManager = new CookieManager();
//        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        return cookieJar;
    }

    private static Retrofit getRetrofitInstance() {
        String url = "https://app.towme.com.sg/api/v1/";
        if (AppHelper.getInstance().getEnvironment() != null) {
            url = AppHelper.getInstance().getEnvironment().getRootUrl();
        }
        return new Retrofit.Builder()
                .baseUrl(url).client(client)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().create()))
                .build();
    }

    private static Retrofit getRetrofitInstanceLinkedin() {
        return new Retrofit.Builder()
                .baseUrl(NetworkURL.ROOT_URL_LINKEDIN).client(client)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().create()))
                .build();
    }

    private static Retrofit getRetrofitInstanceStripe() {
        String url = "https://api.towme.com.sg/api/";
        if (AppHelper.getInstance().getEnvironment() != null) {
            url = AppHelper.getInstance().getEnvironment().getStripeUrl();
        }
        return new Retrofit.Builder()
                .baseUrl(url).client(client)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().create()))
                .build();
    }


    public static ApiService getApiService() {
        return getRetrofitInstance().create(ApiService.class);
    }

    public static ApiService getApiServiceLinkedin() {
        return getRetrofitInstanceLinkedin().create(ApiService.class);
    }

    public static ApiService getApiServiceStripe() {
        return getRetrofitInstanceStripe().create(ApiService.class);
    }

    public static Interceptor getInterceptor() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG) {
            logging.level(HttpLoggingInterceptor.Level.BODY);
        }
        return logging;
    }


}