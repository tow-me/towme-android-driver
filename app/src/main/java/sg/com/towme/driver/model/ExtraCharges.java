package sg.com.towme.driver.model;

/**
 * Created by Dhara Golakiya (email:dharagolakiya4@gmail.com) on 13/July/2020
 */
public class ExtraCharges {
    String description = "";
    String cost = "";

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }
}
