package sg.com.towme.driver.activity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.appcompat.widget.AppCompatTextView;

import sg.com.towme.driver.R;
import sg.com.towme.driver.listener.iDateTimePickerClick;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BookingDetailsActivity extends ToolBarActivity {

    @BindView(R.id.txtAccident)
    AppCompatTextView txtAccident;
    @BindView(R.id.chbAccident)
    AppCompatRadioButton chbAccident;
    @BindView(R.id.chbBreakDown)
    AppCompatRadioButton chbBreakDown;
    @BindView(R.id.imgInfo)
    ImageView imgInfo;
    @BindView(R.id.imgDetailInfo)
    ImageView imgDetailInfo;
    @BindView(R.id.txtMajor)
    AppCompatTextView txtMajor;
    @BindView(R.id.chbMajor)
    AppCompatRadioButton chbMajor;
    @BindView(R.id.txtMinor)
    AppCompatTextView txtMinor;
    @BindView(R.id.chbMnor)
    AppCompatRadioButton chbMnor;
    @BindView(R.id.imgEdit)
    AppCompatImageView imgEdit;
    @BindView(R.id.rbCash)
    RadioButton rbCash;
    @BindView(R.id.rbCredirCard)
    RadioButton rbCredirCard;
    @BindView(R.id.llTowmeNow)
    LinearLayout llTowmeNow;
    @BindView(R.id.llTowmeLater)
    LinearLayout llTowmeLater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_booking_details);
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);

        iniUI();
    }

    private void iniUI() {
        setHomeIcon(R.drawable.ic_back_white);
        setToolbarTitle(getResources().getString(R.string.booking_details));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
//        menu.findItem(R.id.action_overflow).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick({R.id.chbAccident, R.id.chbBreakDown, R.id.imgInfo, R.id.imgDetailInfo, R.id.chbMajor, R.id.chbMnor, R.id.imgEdit, R.id.rbCash, R.id.rbCredirCard, R.id.llTowmeNow, R.id.llTowmeLater})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.chbAccident:
                if (chbAccident.isChecked())
                    chbBreakDown.setChecked(false);
                else
                    chbAccident.setChecked(true);
                setAccidentDetail();
                break;
            case R.id.chbBreakDown:
                if (chbBreakDown.isChecked())
                    chbAccident.setChecked(false);
                else
                    chbBreakDown.setChecked(true);
                setAccidentDetail();
                break;
            case R.id.imgInfo:
                break;
            case R.id.imgDetailInfo:
                break;
            case R.id.chbMajor:
                if (chbMajor.isChecked())
                    chbMnor.setChecked(false);
                else
                    chbMajor.setChecked(true);
                break;
            case R.id.chbMnor:
                if (chbMnor.isChecked())
                    chbMajor.setChecked(false);
                else
                    chbMnor.setChecked(true);
                break;
            case R.id.imgEdit:
                break;
            case R.id.rbCash:
                break;
            case R.id.rbCredirCard:
                break;
            case R.id.llTowmeNow:
                finish();
                break;
            case R.id.llTowmeLater:

                openDateTimePicker();
               /* iDatePickerListner callBack = new iDatePickerListner() {

                    @Override
                    public void onSelectedTime() {

                    }
                };
                openDateTimePickerDialog(callBack);*/
                break;
        }
    }

    public void openDateTimePicker() {
        iDateTimePickerClick iDateTimePickerClick = new iDateTimePickerClick() {
            @Override
            public void onPickDate(String dateTime) {
            }

            @Override
            public void onPickDateTime(String s, String apiDataTimeFormat) {
                finish();
            }
        };
        dateTimePicker(iDateTimePickerClick);
    }
   /* private SlideDateTimeListener listener = new SlideDateTimeListener() {

        @Override
        public void onDateTimeSet(Date date)
        {
            // Do something with the date. This Date object contains
            // the date and time that the user has selected.
        }

        @Override
        public void onDateTimeCancel()
        {
            // Overriding onDateTimeCancel() is optional.
        }
    };
    private void openDateTimePicker() {
        new SlideDateTimePicker.Builder(getSupportFragmentManager())
                .setListener(listener)
                .setInitialDate(new Date())
                .build()
                .show();

        *//* SlideDateTimeListener listener = new SlideDateTimeListener() {

            @Override
            public void onDateTimeSet(Date date) {
                // Do something with the date. This Date object contains
                // the date and time that the user has selected.
            }

            @Override
            public void onDateTimeCancel() {
                // Overriding onDateTimeCancel() is optional.
            }
        };
        new SlideDateTimePicker.Builder(getSupportFragmentManager())
                .setListener(listener)
                .setInitialDate(new Date())
                .build()
                .show();*//*


     *//* SlideDateTimeListener listener = new SlideDateTimeListener() {
            @Override
            public void onDateTimeSet(Date date) {
                // Do something with the date. This Date object contains
                // the date and time that the user has selected.
            }

            @Override
            public void onDateTimeCancel() {
                // Overriding onDateTimeCancel() is optional.
            }
        };*//*
    }*/

    private void setAccidentDetail() {

        if (chbBreakDown.isChecked()) {
            txtMajor.setText(getResources().getString(R.string.towtruck));
            txtMinor.setText(getResources().getString(R.string.flatbed));
        } else {
            txtMajor.setText(getResources().getString(R.string.major));
            txtMinor.setText(getResources().getString(R.string.minor));
        }
    }


}
