package sg.com.towme.driver.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.WorkManager;

import io.socket.client.Ack;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import sg.com.towme.driver.Controller;
import sg.com.towme.driver.R;
import sg.com.towme.driver.adapter.BookingAdapter;
import sg.com.towme.driver.constant.AppConstant;
import sg.com.towme.driver.enumeration.Screens;
import sg.com.towme.driver.listener.iDialogButtonClick;
import sg.com.towme.driver.listener.iPageActive;
import sg.com.towme.driver.model.AbstractCallback;
import sg.com.towme.driver.model.BaseModel;
import sg.com.towme.driver.model.booking.BookingData;
import sg.com.towme.driver.model.booking.BookingListPojo;
import sg.com.towme.driver.network.IResponseCallback;
import sg.com.towme.driver.network.NetworkCall;
import sg.com.towme.driver.network.Parameter;
import sg.com.towme.driver.network.StripeCall;
import sg.com.towme.driver.payment.PaymentSessionHandler;
import sg.com.towme.driver.payment.StripeUtil;
import sg.com.towme.driver.socket.SocketConstant;
import sg.com.towme.driver.socket.SocketIOClient;
import sg.com.towme.driver.utility.AppHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;
import retrofit2.Call;
import sg.com.towme.driver.utility.PreferanceHelper;

import static com.facebook.FacebookSdk.getApplicationContext;

public class MyJobsVpagerFragment extends BaseFragment implements iPageActive {

    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    BookingAdapter messagesAdapter;
    List<BookingData> listBooking;
    @BindView(R.id.lblPreviousTrip)
    AppCompatTextView lblPreviousTrip;
    @BindView(R.id.rvPreviousTrip)
    RecyclerView rvPreviousTrip;

    Socket mSocket;

    public static MyJobsVpagerFragment newInstance() {
        MyJobsVpagerFragment fragment = new MyJobsVpagerFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (mSocket == null)
            mSocket = SocketIOClient.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_messages, container, false);
        ButterKnife.bind(this, view);
        initUI();



        return view;
    }

    @Override
    public void onPageActive(String str) {
        if (listBooking == null || messagesAdapter == null) {
            setAdapter();
            callBookingListApi();
        } else
            setAdapter();

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    private void initUI() {
        rvPreviousTrip.setVisibility(View.GONE);
        lblPreviousTrip.setVisibility(View.GONE);
    }

    private void setAdapter() {

        if (listBooking == null)
            listBooking = new ArrayList<>();

        recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        if (messagesAdapter == null)
            messagesAdapter = new BookingAdapter(getActivity(), listBooking, Screens.MY_JOBS);

//        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerview.getContext(), DividerItemDecoration.VERTICAL);

//        recyclerview.addItemDecoration(dividerItemDecoration);

        messagesAdapter.setCallback((view, position, object) -> {
            switch (view.getId()) {
                case R.id.llRow:
                    if (listBooking.get(position).getBookingStatus() == 1)
                        appNavigationActivity.openStartJobctivity(listBooking.get(position));
                    else if (listBooking.get(position).getBookingStatus() == 2)
                        appNavigationActivity.openEndJobctivity(listBooking.get(position));
                    break;
                case R.id.btnAccept:
                    if (AppHelper.getInstance().getConfigDetails().getTowmeApplicationCommission() > 0) {
                        iDialogButtonClick callBack = new iDialogButtonClick() {
                            @Override
                            public void negativeClick() {
                            }

                            @Override
                            public void positiveClick(String s) {

                                startPaymentSession(listBooking.get(position));
                            }
                        };
                        appNavigationActivity.openConfirmationDialog(callBack, getString(R.string.are_you_sure_you_want_to_pay_this_booking_request), getString(R.string.yes), getString(R.string.no));
                    } else {
                        bookingActionByDriver(listBooking.get(position).getBookingId(), 1);
                    }
                    break;
            }
        });

        recyclerview.setAdapter(messagesAdapter);

    }

    private void startPaymentSession(BookingData createBooking) {
        PaymentSessionHandler paymentSessionHandler = StripeUtil.getPaymentSessionHandler(appNavigationActivity);
        paymentSessionHandler.setType(PaymentSessionHandler.TYPE_ORDER);
        paymentSessionHandler.setTitle("Towme booking charges");
        paymentSessionHandler.setDescription("Order payment");
        paymentSessionHandler.setBookingDetails(String.valueOf(createBooking.getBookingId()), String.valueOf(AppHelper.getInstance().getUserDetails().getUserid()), String.valueOf(AppHelper.getInstance().getUserDetails().getUserid()));
        paymentSessionHandler.setDestination_stripe_account_id("");
        paymentSessionHandler.initTransaction(createBooking.getChargeableAmount());
        paymentSessionHandler.setPaymentSessionListener(new PaymentSessionHandler.PaymentSessionListener() {
            @Override
            public void onPaymentSuccess(String payment_intent_id, boolean captured) {
                Log.e("PaymentSessionHandler", "Success : " + payment_intent_id + " Captured - " + captured);

                callPaymentApi(payment_intent_id, createBooking);


            }

            @Override
            public void onPaymentFailed(String message) {
                Log.e("PaymentSessionHandler", message);
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void callPaymentApi(String payment_intent_id, BookingData createBooking) {
        appNavigationActivity.showProgressDialog();
        HashMap<String, Object> param = new HashMap<>();
        param.put("intent_id", payment_intent_id);
        param.put("amount", "0");

        StripeCall.getInstance().captureIntent(param, new AbstractCallback<ResponseBody>() {
            @Override
            public void result(ResponseBody result) {
                appNavigationActivity.hideProgressDialog();
                Log.e("Payment done", "...");

                bookingActionByDriver(createBooking.getBookingId(), 1);

            }
        });
    }

    private void bookingActionByDriver(int bookingId, int status) {
//        0 = pending (default), 1 = accept booking, 2 = reject booking (Booking Status)

        appNavigationActivity.showProgressDialog();
        JSONObject param = new JSONObject();
        try {
            param.put(SocketConstant.DRIVER_ID, AppHelper.getInstance().getUserDetails().getUserid());
            param.put(SocketConstant.BOOKING_ID, bookingId);
            param.put(SocketConstant.BOOKING_STATUS, status);
            if (AppHelper.getInstance().getUserToken() != null) {
                param.put(SocketConstant.TOKEN, AppHelper.getInstance().getUserToken());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("Accept Reject param", param.toString());

        mSocket.emit(SocketConstant.BOOKING_ACTION_BY_DRIVER, param, new Ack() {
            @Override
            public void call(Object... args) {

                getActivity().runOnUiThread(() -> {

                    appNavigationActivity.hideProgressDialog();
                    int code = 400;

                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(args[0].toString());
                        code = obj.getInt("status_code");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (code == 401) {
                        try {
                            if (getApplicationContext() != null) {
                                PreferanceHelper.getInstance().clearPreference();
                                WorkManager.getInstance(Controller.getInstance()).cancelAllWork();
                                appNavigationActivity.openLoginActivity();
                            }
                        } catch (Exception e) {}
                    } else {
                        Log.e("Accept Reject success", args[0].toString() + "...");
                        int bookingId = 0;
                        try {
                            JSONObject jsnJsonObject = new JSONObject(args[0].toString());
                            JSONObject data = jsnJsonObject.getJSONObject("data");
                            bookingId = data.getInt("booking_id");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        callBookingListApi();
                        Log.e("Booking id ", bookingId + "...");
                    }
                });

            }
        });

    }


    private void callBookingListApi() {
        appNavigationActivity.showProgressDialog();
        List<Integer> listStatus = new ArrayList<>();
        listStatus.add(0);
        listStatus.add(1);
        listStatus.add(2);
        listStatus.add(5);

        NetworkCall.getInstance().bookingList(listStatus, getParam(), new IResponseCallback<BookingListPojo>() {
            @Override
            public void success(BookingListPojo data) {
                appNavigationActivity.hideProgressDialog();

                if (data.getCode() == 1) {
                    listBooking.clear();
                    listBooking.addAll(data.getData());
                    messagesAdapter.notifyDataSetChanged();
                } else {
                    showSnackBar(data.getMessage());
                }

            }

            @Override
            public void onFailure(BaseModel baseModel) {
                appNavigationActivity.hideProgressDialog();
                showSnackBar(baseModel.getMessage());
            }

            @Override
            public void onError(Call<BookingListPojo> responseCall, Throwable T) {
                appNavigationActivity.hideProgressDialog();
                showSnackBar(getString(R.string.error_message));
            }
        });

    }

    private HashMap<String, String> getParam() {
        HashMap<String, String> param = new HashMap<>();
        param.put(Parameter.user_id, String.valueOf(AppHelper.getInstance().getUserDetails().getUserid()));
        param.put(Parameter.user_type, AppConstant.USER_TYPE);
        param.put(Parameter.max_distance, String.valueOf(AppHelper.getInstance().getConfigDetails().getRadius()));
        param.put(Parameter.latitude, AppHelper.getInstance().getLatitude().trim());
        param.put(Parameter.longitude, AppHelper.getInstance().getLongitude().trim());


        return param;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        StripeUtil.getPaymentSessionHandler(appNavigationActivity).onActivityResult(requestCode, resultCode, data);
    }
}