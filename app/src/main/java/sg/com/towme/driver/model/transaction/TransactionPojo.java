package sg.com.towme.driver.model.transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import sg.com.towme.driver.model.BaseModel;

import java.util.List;

public class TransactionPojo  extends BaseModel {

    @SerializedName("data")
    @Expose
    private List<TransactionData> data = null;

    public List<TransactionData> getData() {
        return data;
    }

    public void setData(List<TransactionData> data) {
        this.data = data;
    }
}
