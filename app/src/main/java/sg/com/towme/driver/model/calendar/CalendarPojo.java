package sg.com.towme.driver.model.calendar;

import sg.com.towme.driver.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CalendarPojo extends BaseModel {

    @SerializedName("earning")
    @Expose
    private Double earning;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("data")
    @Expose
    private List<CalendarData> data = null;

    public List<CalendarData> getData() {
        return data;
    }

    public void setData(List<CalendarData> data) {
        this.data = data;
    }

    public Double getEarning() {
        return earning;
    }

    public void setEarning(Double earning) {
        this.earning = earning;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
