package sg.com.towme.driver.service;


import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import retrofit2.Call;
import sg.com.towme.driver.Controller;
import sg.com.towme.driver.R;
import sg.com.towme.driver.activity.LoginActivity;
import sg.com.towme.driver.model.BaseModel;
import sg.com.towme.driver.network.IResponseCallback;
import sg.com.towme.driver.network.NetworkCall;
import sg.com.towme.driver.network.Parameter;
import sg.com.towme.driver.socket.SocketConstant;
import sg.com.towme.driver.socket.SocketIOClient;
import sg.com.towme.driver.utility.AppHelper;
import io.socket.client.Ack;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import sg.com.towme.driver.utility.PreferanceHelper;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FindCurrentPlaceRequest;
import com.google.android.libraries.places.api.net.FindCurrentPlaceResponse;
import com.google.android.libraries.places.api.net.PlacesClient;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.facebook.FacebookSdk.getApplicationContext;

public class DataWorker extends Worker {

    Context context;
    Socket mSocket;

    public DataWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);
        this.context = context;
    }

    @NotNull
    @Override
    public Result doWork() {
//        Log.e("DataWorker", "doWork...........");

        if (mSocket == null)
            mSocket = SocketIOClient.getInstance();

        updateLocation();

        OneTimeWorkRequest refreshCpnWork = new OneTimeWorkRequest.Builder(DataWorker.class)
                .setInitialDelay(60, TimeUnit.SECONDS).build();
        WorkManager.getInstance(Controller.getInstance()).enqueue(refreshCpnWork);

        return Result.success();
    }


    AutocompleteSessionToken autocompleteSessionToken;
    PlacesClient placesClient;

    private void updateLocation() {

        Places.initialize(getApplicationContext(), Controller.getInstance().getApplicationContext().getString(R.string.google_api_key));
        placesClient = Places.createClient(Controller.getInstance().getApplicationContext());
        autocompleteSessionToken = AutocompleteSessionToken.newInstance();

        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);

        FindCurrentPlaceRequest request =
                FindCurrentPlaceRequest.newInstance(fields);

        Task<FindCurrentPlaceResponse> placeResponse = placesClient.findCurrentPlace(request);
        placeResponse.addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                FindCurrentPlaceResponse response = task.getResult();
                Place place = response.getPlaceLikelihoods().get(0).getPlace();
                updateLocationAPI(place);
//                callUpdateLocationApi(place);
               /* for (PlaceLikelihood placeLikelihood : response.getPlaceLikelihoods()) {

                    Place place = placeLikelihood.getPlace();

                    callUpdateLocationApi(place);

                }*/
            } else {
                Exception exception = task.getException();
                if (exception instanceof ApiException) {
                    ApiException apiException = (ApiException) exception;
                    Log.e("Current location", "Place not found: " + apiException.getStatusCode());
                }
            }
        });
    }

    private HashMap<String, String> getParam(Place place) {
        HashMap<String, String> param = new HashMap<>();
        param.put(Parameter.latitude, place.getLatLng().latitude+"");
        param.put(Parameter.longitude, place.getLatLng().longitude+"");
        return param;
    }

    private void updateLocationAPI(Place place) {
        NetworkCall.getInstance().updateLocation(getParam(place), new IResponseCallback<BaseModel>() {
            @Override
            public void success(BaseModel data) {

            }

            @Override
            public void onFailure(BaseModel baseModel) {
            }

            @Override
            public void onError(Call<BaseModel> responseCall, Throwable T) {
            }
        });
    }

    private void callUpdateLocationApi(Place place) {

        Log.e("Worker Update location", place.getName() + "latitude:" + place.getLatLng().latitude + "..." + "longitude:" + place.getLatLng().longitude + "...");
        String userID = String.valueOf(AppHelper.getInstance().getUserDetails().getUserid());
        JSONObject param = new JSONObject();
        try {
            param.put(SocketConstant.USER_ID, userID);
            param.put(SocketConstant.LATITUDE, String.valueOf(place.getLatLng().latitude));
            param.put(SocketConstant.LONGITUDE, String.valueOf(place.getLatLng().longitude));
            if (AppHelper.getInstance().getUserToken() != null) {
                param.put(SocketConstant.TOKEN, AppHelper.getInstance().getUserToken());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mSocket.emit(SocketConstant.UPDATE_LOCATION, param, new Ack() {
            @Override
            public void call(Object... args) {
                int code = 400;

                JSONObject obj = null;
                try {
                    obj = new JSONObject(args[0].toString());
                    code = obj.getInt("status_code");

                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (code == 401) {
                    try {
                        if (getApplicationContext() != null) {
                            PreferanceHelper.getInstance().clearPreference();
                            WorkManager.getInstance(Controller.getInstance()).cancelAllWork();
                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            getApplicationContext().startActivity(i);
                        }
                    } catch (Exception e) {}
                } else {
                    Log.e("Worker Update location", "success " + place.getName() + "latitude:" + place.getLatLng().latitude + "..." + "longitude:" + place.getLatLng().longitude + "...");
                }
            }
        });

    }

}