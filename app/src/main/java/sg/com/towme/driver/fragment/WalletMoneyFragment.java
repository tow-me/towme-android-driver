package sg.com.towme.driver.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import sg.com.towme.driver.R;
import sg.com.towme.driver.activity.TransferMoneyActivity;
import sg.com.towme.driver.constant.AppConstant;
import sg.com.towme.driver.listener.iPageActive;
import sg.com.towme.driver.model.BaseModel;
import sg.com.towme.driver.model.stripe.Balance;
import sg.com.towme.driver.network.IResponseCallback;
import sg.com.towme.driver.network.NetworkCall;
import sg.com.towme.driver.utility.AppHelper;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;


public class WalletMoneyFragment extends BaseFragment implements iPageActive {

    @BindView(R.id.txt_transfer)
    TextView txt_transfer;
    @BindView(R.id.txt_history)
    TextView txt_history;
    @BindView(R.id.txt_balance)
    TextView txt_balance;
    @BindView(R.id.txt_currency)
    TextView txt_currency;

    double balance = 0f;

    public static WalletMoneyFragment newInstance() {
        return new WalletMoneyFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_wallet_money, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
//        getBalance();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (AppHelper.getInstance().getUserDetails().getCommission() != null) {
            balance = AppHelper.getInstance().getUserDetails().getCommission();
        }
        txt_balance.setText(AppHelper.getInstance().formatCurrency(balance));
        String curr = "SGD";
        if (AppHelper.getInstance().getUserDetails().getCountry() != null && AppHelper.getInstance().getUserDetails().getCountry().contains("MY")) {
            curr = "RM";
        }
        txt_currency.setText(curr);
    }

    //    private Balance balance;

    @OnClick({R.id.txt_transfer, R.id.txt_history})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_transfer:
                if (balance <= 0) {
                    homeActivity.openConfirmationDialog(null, getString(R.string.you_do_not_have_sufficient_balance_to_transfer), getString(R.string.ok), null);
                } else
                    startActivity(new Intent(activity, TransferMoneyActivity.class));
                break;
            case R.id.txt_history:
                homeActivity.openTransactionHistoryActivity();
                break;
        }
    }

    @Override
    public void onPageActive(String str) {

    }

    private void getBalance() {
        NetworkCall.getInstance().getBalance(new IResponseCallback<Balance>() {
            @Override
            public void success(Balance data) {
                if (data == null) {
                    txt_balance.setText("0.00");
                } else {
//                    WalletMoneyFragment.this.balance = data;
                    txt_balance.setText(String.format(Locale.ENGLISH, "%.2f", data.totalBalance()));
                    txt_currency.setText(data.getCurrency());
                }
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                txt_balance.setText("0.00");
            }

            @Override
            public void onError(Call<Balance> responseCall, Throwable T) {
                txt_balance.setText("0.00");
            }
        });
    }

}
