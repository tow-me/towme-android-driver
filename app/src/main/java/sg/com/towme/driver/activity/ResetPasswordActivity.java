package sg.com.towme.driver.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Space;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;

import sg.com.towme.driver.R;
import sg.com.towme.driver.constant.AppConstant;
import sg.com.towme.driver.enumeration.Info;
import sg.com.towme.driver.listener.CheckNetworkListener;
import sg.com.towme.driver.listener.iInfoDialogListner;
import sg.com.towme.driver.model.BaseModel;
import sg.com.towme.driver.model.user.UserData;
import sg.com.towme.driver.model.user.UserPojo;
import sg.com.towme.driver.network.IResponseCallback;
import sg.com.towme.driver.network.NetworkCall;
import sg.com.towme.driver.network.Parameter;
import sg.com.towme.driver.utility.AppValidation;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

public class ResetPasswordActivity extends ToolBarActivity implements iInfoDialogListner {

    @BindView(R.id.edtOldPassword)
    AppCompatEditText edtOldPassword;
    @BindView(R.id.edtNewPassword)
    AppCompatEditText edtNewPassword;
    @BindView(R.id.edtRetypePassword)
    AppCompatEditText edtRetypePassword;
    @BindView(R.id.btnUpdate)
    AppCompatButton btnUpdate;

    UserData userData;
    @BindView(R.id.space)
    Space space;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);

        iniUI();
    }

    private void iniUI() {
        setHomeIcon(R.drawable.ic_back_white);
        setToolbarTitle(getString(R.string.reset_password));
        edtOldPassword.setVisibility(View.GONE);
        space.setVisibility(View.VISIBLE);

        Intent intent = getIntent();
        userData = (UserData) intent.getSerializableExtra(AppConstant.user_data);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void showMessage(String message) {
        openInfoDialog(this, message, Info.RESET_PASSWORD);
    }

    private void callResetPasswordApi() {

        NetworkCall.getInstance().resetPassword(getParam(), new IResponseCallback<UserPojo>() {
            @Override
            public void success(UserPojo data) {
                hideProgressDialog();
                showMessage(data.getMessage());
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                hideProgressDialog();
                showSnackBar(btnUpdate, baseModel.getMessage());
            }

            @Override
            public void onError(Call<UserPojo> responseCall, Throwable T) {
                hideProgressDialog();
                showSnackBar(btnUpdate, getString(R.string.error_message));
            }
        });

    }

    private HashMap<String, String> getParam() {
        HashMap<String, String> param = new HashMap<>();
        param.put(Parameter.password, edtNewPassword.getText().toString().trim());
//        param.put(Parameter.confirm_password, edtRetypePassword.getText().toString().trim());
        param.put(Parameter.userid, String.valueOf(userData.getUserid()));
        param.put(Parameter.user_type, AppConstant.USER_TYPE);
        return param;
    }

    @OnClick(R.id.btnUpdate)
    public void onViewClicked() {
        hideKeyBoard();
        CheckNetworkListener callback = () -> {
            if (isValidate()) {
                showProgressDialog();
                callResetPasswordApi();

            }
        };

        if (isNetworkAvailable(btnUpdate, callback)) {
            callback.onRetryClick();
        }
    }

    private boolean isValidate() {

        if (AppValidation.isEmptyFieldValidate(edtNewPassword.getText().toString().trim())) {
            showSnackBar(edtOldPassword, getString(R.string.please_enter_new_password));
            return false;
        }
        if (AppValidation.isEmptyFieldValidate(edtRetypePassword.getText().toString().trim())) {
            showSnackBar(edtRetypePassword, getString(R.string.please_retype_your_password));
            return false;
        }
        if (!edtNewPassword.getText().toString().trim().equals(edtRetypePassword.getText().toString().trim())) {
            showSnackBar(edtRetypePassword, getString(R.string.your_new_password_and_confirm_password_doesn_t_match));
            return false;
        }


        return true;
    }

    @Override
    public void onOkClick(Info info) {
        switch (info) {
            case RESET_PASSWORD:
                openLoginActivity();
                break;
        }
    }
}
