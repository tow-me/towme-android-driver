package sg.com.towme.driver.listener;


public interface iDateTimePickerClick {

    void onPickDate(String dateTime);

    void onPickDateTime(String s, String apiDataTimeFormat);
}
