
package sg.com.towme.driver.model.messages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import sg.com.towme.driver.model.booking.BookingData;

import java.util.List;

public class MessagesData {

    @SerializedName("previous_rides")
    @Expose
    private List<BookingData> previousRides = null;

    @SerializedName("current_rides")
    @Expose
    private List<BookingData> currentRides = null;

    public List<BookingData> getPreviousRides() {
        return previousRides;
    }

    public void setPreviousRides(List<BookingData> previousRides) {
        this.previousRides = previousRides;
    }

    public List<BookingData> getCurrentRides() {
        return currentRides;
    }

    public void setCurrentRides(List<BookingData> currentRides) {
        this.currentRides = currentRides;
    }
}
