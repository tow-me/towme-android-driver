package sg.com.towme.driver.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.recyclerview.widget.RecyclerView;

import sg.com.towme.driver.R;
import sg.com.towme.driver.listener.RecyclerViewClickListener;
import sg.com.towme.driver.model.ExtraCharges;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExtraChargesAdapter extends RecyclerView.Adapter<ExtraChargesAdapter.ViewHolder> {

    Context context;

    public void setCallback(RecyclerViewClickListener callback) {
        this.callback = callback;
    }

    RecyclerViewClickListener callback;
    List<ExtraCharges> extraChargesList;

    public ExtraChargesAdapter(Context context, List<ExtraCharges> extraChargesList) {
        this.context = context;
        this.extraChargesList = extraChargesList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.row_extra_charges, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ExtraCharges data = extraChargesList.get(holder.getAdapterPosition());
        holder.edtDescriptiob.setText(data.getDescription());
        holder.edtCost.setText(data.getCost());

        holder.edtDescriptiob.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                data.setDescription(charSequence.toString());
                callback.onClick(holder.edtDescriptiob, holder.getAdapterPosition(), data);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        holder.edtCost.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                data.setCost(charSequence.toString());
                callback.onClick(holder.edtCost, holder.getAdapterPosition(), data);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    @Override
    public int getItemCount() {
        return extraChargesList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.edtDescriptiob)
        EditText edtDescriptiob;
        @BindView(R.id.edtCost)
        EditText edtCost;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }


}
