package sg.com.towme.driver.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import sg.com.towme.driver.R;
import sg.com.towme.driver.adapter.PicAdapter;
import sg.com.towme.driver.adapter.SummeryChargesAdapter;
import sg.com.towme.driver.constant.AppConstant;
import sg.com.towme.driver.model.booking.BookingData;
import sg.com.towme.driver.model.booking.EndRideExtraCharge;
import sg.com.towme.driver.model.booking.User;
import sg.com.towme.driver.model.booking.VehicleData;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import jp.wasabeef.glide.transformations.CropCircleTransformation;
import sg.com.towme.driver.utility.AppHelper;
import sg.com.towme.driver.utility.DateTimeUtil;

public class JobDetailsActivity extends ToolBarActivity {


    @BindView(R.id.imgProfilePic)
    CircleImageView imgProfilePic;
    @BindView(R.id.txtName)
    AppCompatTextView txtName;
    @BindView(R.id.txtPrice)
    AppCompatTextView txtPrice;
    @BindView(R.id.txtVehicleDetails)
    AppCompatTextView txtVehicleDetails;
    @BindView(R.id.txtTowType)
    AppCompatTextView txtTowType;
    @BindView(R.id.txtSource)
    AppCompatTextView txtSource;
    @BindView(R.id.txtDestination)
    AppCompatTextView txtDestination;
    @BindView(R.id.txtServiceType)
    AppCompatTextView txtServiceType;
    @BindView(R.id.txtTotal)
    AppCompatTextView txtTotal;
    @BindView(R.id.txtSubTotal)
    AppCompatTextView txtSubTotal;
    @BindView(R.id.txtDiscount)
    AppCompatTextView txtDiscount;
    @BindView(R.id.txtTotalCharge)
    AppCompatTextView txtTotalCharge;
    /*  @BindView(R.id.imgStartPhoto)
      AppCompatImageView imgStartPhoto;*/
  /*  @BindView(R.id.imgEndPhoto)
    AppCompatImageView imgEndPhoto;*/
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;

    BookingData bookingData;
    List<EndRideExtraCharge> endRideExtraCharge;
    SummeryChargesAdapter adapter;
    Double totalExtraCharge = 0.0;
    @BindView(R.id.txtStartNote)
    AppCompatTextView txtStartNote;
    @BindView(R.id.txtEndNote)
    AppCompatTextView txtEndNote;
    @BindView(R.id.txtStatus)
    AppCompatTextView txtStatus;
    @BindView(R.id.txtDescription)
    AppCompatTextView txtDescription;
    @BindView(R.id.llSubTotal)
    LinearLayout llSubTotal;
    @BindView(R.id.llDiscount)
    LinearLayout llDiscount;
    @BindView(R.id.viewSubtotal)
    View viewSubtotal;
    @BindView(R.id.viewDiscount)
    View viewDiscount;
    @BindView(R.id.txtBookingDate)
    AppCompatTextView txtBookingDate;
    @BindView(R.id.lblStartDate)
    AppCompatTextView lblStartDate;
    @BindView(R.id.lblEndDate)
    AppCompatTextView lblEndDate;
    @BindView(R.id.lblStartRideDetails)
    TextView lblStartRideDetails;
    @BindView(R.id.lblEndRideDetails)
    TextView lblEndRideDetails;
    @BindView(R.id.rlStartPhoto)
    RelativeLayout rlStartPhoto;
    @BindView(R.id.rlEndRide)
    RelativeLayout rlEndRide;
    @BindView(R.id.rvEndPhoto)
    RecyclerView rvEndPhoto;
    @BindView(R.id.rvStartPhoto)
    RecyclerView rvStartPhoto;

    PicAdapter startPicAdapter;
    PicAdapter endPicAdapter;

    List<String> listEndPic;
    List<String> listStartPic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_job_details);
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);
        iniUI();
    }

    private void setStartPicAdapter() {

        if (listStartPic == null)
            listStartPic = new ArrayList<>();

        if (startPicAdapter == null)
            startPicAdapter = new PicAdapter(this, listStartPic, false);

        startPicAdapter.setCallback((view, position, object) -> {
            switch (view.getId()) {
                case R.id.imageview:
                    Intent intent = new Intent(JobDetailsActivity.this, ImageActivity.class);
                    intent.putExtra(ImageActivity.EXTRA_PHOTO, listStartPic.get(position));
                    startActivity(intent);
                    break;
            }

        });

        rvStartPhoto.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rvStartPhoto.setAdapter(startPicAdapter);
    }

    private void setEndPicAdapter() {

        if (listEndPic == null)
            listEndPic = new ArrayList<>();

        if (endPicAdapter == null)
            endPicAdapter = new PicAdapter(this, listEndPic, false);

        endPicAdapter.setCallback((view, position, object) -> {
            switch (view.getId()) {
                case R.id.imageview:
                    Intent intent = new Intent(JobDetailsActivity.this, ImageActivity.class);
                    intent.putExtra(ImageActivity.EXTRA_PHOTO, listEndPic.get(position));
                    startActivity(intent);
                    break;
            }

        });

        rvEndPhoto.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rvEndPhoto.setAdapter(endPicAdapter);
    }

    private void showFullScreenDialog(String imageURL) {
        Dialog dialog = new Dialog(this, R.style.Theme_Design_NoActionBar);
        dialog.setContentView(R.layout.fullscreen_image);
        ImageView imageView = dialog.findViewById(R.id.imageview);
        Glide.with(this).load(imageURL).placeholder(R.drawable.placeholder).into(imageView);
        dialog.show();
    }

    private void iniUI() {

        setToolbarTitle(getString(R.string.job_details));

        if (getIntent() != null) {
            bookingData = (BookingData) getIntent().getSerializableExtra(AppConstant.booking_data);

            User user = bookingData.getUser();

            Glide.with(this)
                    .load(user.getUserImage().trim())
                    .transform(new CropCircleTransformation())
                    .into(imgProfilePic);

            txtName.setText(user.getName());
            String curr = "SGD";
            if (bookingData.getCountry().contains("MY")) {
                curr = "RM";
            }
            txtPrice.setText(curr + " " + AppHelper.getInstance().formatCurrency(bookingData.getCalculatedFinalCost()) + "");

            VehicleData vehicleData = bookingData.getVehicle();
            if (vehicleData != null)
                txtVehicleDetails.setText(vehicleData.getVehicleType() + " - " + vehicleData.getVehicleColor() + " - " + vehicleData.getVehicleBrand() + "(" + vehicleData.getVehicleNo() + ")");

           /* String type = "";
            if (bookingData.getAccidentType().length() > 0) {
                type = getString(R.string.accident) + "(" + bookingData.getAccidentType() + ")";
            } else {
                type = getString(R.string.breakdown) + "(" + bookingData.getBreakdownType() + ")";
            }
*/
//            txtTowType.setText(bookingData.getTowType());
            txtStatus.setText(AppHelper.getInstance().getStatusText(bookingData.getBookingStatus(), JobDetailsActivity.this));
            txtSource.setText(bookingData.getSource());
            txtBookingDate.setText(DateTimeUtil.convertWithoutChangingTimezone(bookingData.getBookingDate(), "yyyy-MM-dd HH:mm:ss", "EEE, dd MMM yyyy hh:mm a"));
            txtDestination.setText(bookingData.getDestination());
            txtDescription.setText(bookingData.getExtraNotes());
            endRideExtraCharge = new ArrayList<>();
            if (bookingData.getEndRideExtraCharge() != null && bookingData.getEndRideExtraCharge().size() > 0) {
                endRideExtraCharge.addAll(bookingData.getEndRideExtraCharge());
                setAdapter();
            }

            for (int i = 0; i < endRideExtraCharge.size(); i++) {
                totalExtraCharge += Double.parseDouble(endRideExtraCharge.get(i).getPrice().trim());
            }

            double total = Double.parseDouble(bookingData.getTowPrice().trim());

            txtTotal.setText(curr + AppHelper.getInstance().formatCurrency(total) + "");
            txtServiceType.setText(bookingData.getTowType());
            Double subTotal = Double.parseDouble(bookingData.getTowPrice().trim()) + totalExtraCharge;

            Double totalCharge = Double.parseDouble(bookingData.getTowPrice().trim()) + totalExtraCharge - Double.parseDouble(bookingData.getDiscountedAmount().trim());
            txtTotalCharge.setText(curr + AppHelper.getInstance().formatCurrency(totalCharge) + "");


            if (Double.parseDouble(bookingData.getDiscountedAmount().trim()) > 0) {
                txtSubTotal.setText(curr + AppHelper.getInstance().formatCurrency(subTotal));
                txtDiscount.setText(curr + bookingData.getDiscountedAmount() + "");
            } else {
                llDiscount.setVisibility(View.GONE);
                llSubTotal.setVisibility(View.GONE);
                viewDiscount.setVisibility(View.GONE);
                viewSubtotal.setVisibility(View.GONE);
            }

            if (bookingData.getBookingStatus() == 3 || bookingData.getBookingStatus() == 7 || bookingData.getBookingStatus() == 8) {
                if (bookingData.getEndRideDescription() != null)
                    txtEndNote.setText(getString(R.string.end_note) + bookingData.getEndRideDescription());
                lblEndDate.setVisibility(View.VISIBLE);
                lblEndDate.setText(DateTimeUtil.convertWithoutChangingTimezone(bookingData.getStoppedAt(), "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy hh:mm a"));
                lblEndRideDetails.setVisibility(View.VISIBLE);
                /*Glide.with(this)
                        .load(bookingData.getStoppedImages().get(0))
                        .into(imgEndPhoto);*/
                setEndPicAdapter();
                if (bookingData.getStoppedImages() != null) {
                    listEndPic.addAll(bookingData.getStoppedImages());
                }
                endPicAdapter.notifyDataSetChanged();

            } else {
                lblEndRideDetails.setVisibility(View.GONE);
                txtEndNote.setVisibility(View.GONE);
                rlEndRide.setVisibility(View.GONE);
            }

            if (bookingData.getBookingStatus() == 2 || bookingData.getBookingStatus() == 3 || bookingData.getBookingStatus() == 7 || bookingData.getBookingStatus() == 8) {
                if (bookingData.getStartRideDescription() != null)
                    txtStartNote.setText(getString(R.string.start_note) + bookingData.getStartRideDescription());
                lblStartDate.setVisibility(View.VISIBLE);
                lblStartDate.setText(DateTimeUtil.convertWithoutChangingTimezone(bookingData.getStartedAt(), "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy hh:mm a"));
                lblStartRideDetails.setVisibility(View.VISIBLE);
             /*   Glide.with(this)
                        .load(bookingData.getStartedImages().get(0))
                        .into(imgStartPhoto);*/

                setStartPicAdapter();
                if (bookingData.getStartedImages() != null) {
                    listStartPic.addAll(bookingData.getStartedImages());
                }
                startPicAdapter.notifyDataSetChanged();

            } else {
                lblStartRideDetails.setVisibility(View.GONE);
                txtStartNote.setVisibility(View.GONE);
                rlStartPhoto.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setAdapter() {
        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        String curr = "SGD";
        if (bookingData.getCountry().contains("MY")) {
            curr = "RM";
        }
        if (adapter == null)
            adapter = new SummeryChargesAdapter(this, endRideExtraCharge, curr);

        adapter.setCallback((view, position, object) -> {

        });

        recyclerview.setAdapter(adapter);
    }


}