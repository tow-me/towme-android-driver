package sg.com.towme.driver.constant;

/**
 * Created by Dhara on 13-03-2018.
 */

public interface SharedPrefConstant {

    String prefName = "towmedriver";
    String IS_LOGIN = "is_login";
    String USER_DETAILS = "user_details";
    String ENV_DETAILS = "environments";
    String COOKIE = "COOKIE";
    String COUNTRY = "country";
    String USER_TOKEN = "user_token";
    String CONFIG_DETAILS = "config_details";
    String LATITUDE = "latitude";
    String LONGITUDE = "longitude";


}
