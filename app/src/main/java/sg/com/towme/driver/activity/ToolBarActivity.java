package sg.com.towme.driver.activity;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.appbar.AppBarLayout;
import sg.com.towme.driver.R;

import butterknife.BindView;


public class ToolBarActivity extends AppNavigationActivity implements FragmentManager.OnBackStackChangedListener {

    @BindView(R.id.txtTitleToolbar)
    AppCompatTextView txtToolbarTitle;
    @BindView(R.id.toolBar)
    Toolbar toolBar;
    @BindView(R.id.appbar)
    AppBarLayout appbar;
    private Toolbar toolbar;
    private ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        setToolBar();
    }

    public void setToolBar() {
        toolbar = findViewById(R.id.toolBar);
        txtToolbarTitle = (AppCompatTextView) toolbar.findViewById(R.id.txtTitleToolbar);

        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        if (actionBar == null) return;
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_back_white);
        getSupportFragmentManager().addOnBackStackChangedListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.action_menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackStackChanged() {

    }

    public void setToolbarTitle(String title) {
        super.setTitle(null);
        if (actionBar == null) return;
        txtToolbarTitle.setText(title);
        setSubTitle(null);
//        actionTitles.push(new ActionTitle(title));

    }

    public void setToolbarTitleDrawable(int drawable) {
        txtToolbarTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, drawable, 0);
    }

    public void setHomeIcon(int icon) {
        actionBar.setHomeAsUpIndicator(icon);
    }

    public void hideHomeIcon() {
        actionBar.setDisplayHomeAsUpEnabled(false);
    }

    public void setSubTitle(String subTitle) {
        if (actionBar == null) return;
        actionBar.setSubtitle(subTitle);
    }

}
