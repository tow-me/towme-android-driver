package sg.com.towme.driver.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import sg.com.towme.driver.R;
import sg.com.towme.driver.constant.AppConstant;
import sg.com.towme.driver.enumeration.LoginType;
import sg.com.towme.driver.enumeration.Screens;
import sg.com.towme.driver.listener.CheckNetworkListener;
import sg.com.towme.driver.model.BaseModel;
import sg.com.towme.driver.model.Environment;
import sg.com.towme.driver.model.SocialData;
import sg.com.towme.driver.model.config_.ConfigPojo;
import sg.com.towme.driver.model.facebook.FacebookData;
import sg.com.towme.driver.model.linkedin.LinkedinEmailPojo;
import sg.com.towme.driver.model.linkedin.LinkedinProfilePojo;
import sg.com.towme.driver.model.user.UserPojo;
import sg.com.towme.driver.network.IResponseCallback;
import sg.com.towme.driver.network.NetworkCall;
import sg.com.towme.driver.network.Parameter;
import sg.com.towme.driver.utility.AppHelper;
import sg.com.towme.driver.utility.AppValidation;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.stripe.android.PaymentConfiguration;

import sg.com.omralcorut.linkedinsignin.Linkedin;
import sg.com.omralcorut.linkedinsignin.LinkedinLoginViewResponseListener;
import sg.com.omralcorut.linkedinsignin.model.LinkedinToken;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

public class LoginActivity extends AppNavigationActivity {

    @BindView(R.id.txtSignup)
    AppCompatTextView txtSignup;
    @BindView(R.id.btnGo)
    AppCompatButton btnGo;
    @BindView(R.id.imgLinkedIn)
    AppCompatImageView imgLinkedIn;
    @BindView(R.id.imgGoogle)
    AppCompatImageView imgGoogle;
    @BindView(R.id.imgFacebook)
    AppCompatImageView imgFacebook;
    @BindView(R.id.imgLogo)
    AppCompatImageView imgLogo;
    @BindView(R.id.edtemail)
    AppCompatEditText edtemail;
    @BindView(R.id.edtPassword)
    AppCompatEditText edtPassword;
    @BindView(R.id.btnFacebookLogin)
    LoginButton btnFacebookLogin;
    @BindView(R.id.txtForgotPassword)
    AppCompatTextView txtForgotPassword;

    private CallbackManager callbackManager = null;
    private GoogleApiClient mGoogleApiClient = null;
    private int RC_SIGN_IN_GOOGLE = 2222;
    private int count = 0;
    private boolean isDev = false;
    String accessToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);

        iniUI();
    }

    private void iniUI() {
        if (AppHelper.getInstance().getEnvironment() != null) {
            if (AppHelper.getInstance().getEnvironment().getRootUrl().contains("dev")) {
                isDev = true;
            } else {
                isDev = false;
            }
        }
    }

    private void callGetConfigurationData() {

        showProgressDialog();
        NetworkCall.getInstance().getConfiguration(new IResponseCallback<ConfigPojo>() {
            @Override
            public void success(ConfigPojo data) {
                hideProgressDialog();
                if (data.getCode() == 1) {
                    AppHelper.getInstance().setConfigDetails(data.getData());
                } else {
                    showSnackBar(btnGo, data.getMessage());
                }

            }

            @Override
            public void onFailure(BaseModel baseModel) {
                hideProgressDialog();
                showSnackBar(btnGo, baseModel.getMessage());
            }

            @Override
            public void onError(Call<ConfigPojo> responseCall, Throwable T) {
                hideProgressDialog();
                showSnackBar(btnGo, getString(R.string.error_message));
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick({R.id.txtSignup, R.id.imgLinkedIn, R.id.imgGoogle, R.id.imgFacebook, R.id.btnGo, R.id.txtForgotPassword, R.id.imgLogo})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtSignup:
                SocialData data = new SocialData();
                data.setLoginType(LoginType.NORMAL);
                data.setRegisterType("0");
//                openUserSignupActivity(data);
                openSignupActivity(data);
                break;
            case R.id.imgLinkedIn:
                initLinkedInLogin();
                break;
            case R.id.imgGoogle:
                initGoogleLogin();
                break;
            case R.id.imgFacebook:
                initFacebookSDK();
                break;
            case R.id.txtForgotPassword:
                openForgotPasswordActivity();
                break;
            case R.id.imgLogo:
                count++;
                if (count == 10) {
                    count = 0;
                    if (isDev) {
                        isDev = false;
                        Environment environment = new Environment();
                        environment.setRootUrl("https://app.towme.com.sg/api/v1/");
                        environment.setSocketUrl("https://api.towme.com.sg");
                        environment.setStripeKey("pk_live_dtkwEhkGEsaXLjZlH4qMhJlK00k8ktUWu6");
                        environment.setStripeUrl("https://api.towme.com.sg/api/");
                        AppHelper.getInstance().setEnvironment(environment);
                        String key = "pk_live_dtkwEhkGEsaXLjZlH4qMhJlK00k8ktUWu6";
                        if (AppHelper.getInstance().getEnvironment() != null) {
                            key = AppHelper.getInstance().getEnvironment().getStripeKey();
                        }
                        PaymentConfiguration.init(this, key);
                        Toast.makeText(LoginActivity.this, "Switched to production", Toast.LENGTH_SHORT).show();
                    } else {
                        isDev = true;
                        Environment environment = new Environment();
                        environment.setRootUrl("https://devapp.towme.com.sg/api/v1/");
                        environment.setSocketUrl("https://devapi.towme.com.sg");
                        environment.setStripeKey("pk_test_asEfactFvsDRRWILkKcn9EJk00GGDsKM2V");
                        environment.setStripeUrl("https://devapi.towme.com.sg/api/");
                        AppHelper.getInstance().setEnvironment(environment);
                        String key = "pk_live_dtkwEhkGEsaXLjZlH4qMhJlK00k8ktUWu6";
                        if (AppHelper.getInstance().getEnvironment() != null) {
                            key = AppHelper.getInstance().getEnvironment().getStripeKey();
                        }
                        PaymentConfiguration.init(this, key);
                        Toast.makeText(LoginActivity.this, "Switched to development", Toast.LENGTH_SHORT).show();
                    }
                    callGetConfigurationData();
                }
                break;
            case R.id.btnGo:
                SocialData socialData = new SocialData();
                socialData.setLoginType(LoginType.NORMAL);
                socialData.setRegisterType("0");
                FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(instanceIdResult -> callNormalLoginApi(instanceIdResult.getToken(), socialData));
//                callNormalLoginApi("", socialData);

                break;
        }
    }

    private boolean isValidate() {
        if (AppValidation.isEmptyFieldValidate(edtemail.getText().toString().trim())) {
            showSnackBar(edtemail, getString(R.string.please_enter_email_address));
            return false;
        }
        if (AppValidation.isEmptyFieldValidate(edtPassword.getText().toString().trim())) {
            showSnackBar(edtPassword, getString(R.string.please_enter_password));
            return false;
        }
        return true;
    }

    private void callNormalLoginApi(String token, SocialData socialData) {
        CheckNetworkListener callback = () -> {
            if (isValidate()) {
                showProgressDialog();
                callLoginAPI(token, socialData);
            }
        };

        if (isNetworkAvailable(btnGo, callback)) {
            callback.onRetryClick();
        }
    }

    private void callSocialLoginApi(String token, SocialData socialData) {
        CheckNetworkListener callback = () -> {
            showProgressDialog();
            callLoginAPI(token, socialData);
        };

        if (isNetworkAvailable(btnGo, callback)) {
            callback.onRetryClick();
        }
    }

    private void callLoginAPI(String token, SocialData socialData) {

        NetworkCall.getInstance().postLoginData(getLoginParam(token, socialData), new IResponseCallback<UserPojo>() {
            @Override
            public void success(UserPojo data) {
                hideProgressDialog();
                if (data.getCode() == 1) {
                    AppHelper.getInstance().setUserDetails(data.getData());
                    AppHelper.getInstance().setUserToken(data.getToken());
                    if (data.getVerify()) {
                        AppHelper.getInstance().setLogin(true);
                        openHomeActivity();
                    } else {
                        openOTPActivity(Screens.SIGNUP, data.getData());
                    }

                } else {
                    if (socialData.getRegisterType() == "0") {
                        if (data.getRegistred())
                            if (!data.getVerify()) {
                                openOTPActivity(Screens.SIGNUP, data.getData());
                            }
                    }
                    showSnackBar(btnGo, data.getMessage());
//                    if (socialData.getRegisterType() != "0") {
//                        if (!data.getRegistred())
//                            openSignupActivity(socialData);
//                        else
//                            showSnackBar(btnGo, data.getMessage());
//                    } else {
//                        showSnackBar(btnGo, data.getMessage());
//                        if (data.getRegistred())
//                            if (!data.getVerify()) {
//                                openOTPActivity(Screens.SIGNUP, data.getData());
//                            }
//                    }
                }

            }

            @Override
            public void onFailure(BaseModel baseModel) {
                hideProgressDialog();
                showSnackBar(btnGo, baseModel.getMessage());
            }

            @Override
            public void onError(Call<UserPojo> responseCall, Throwable T) {
                hideProgressDialog();
                showSnackBar(btnGo, getString(R.string.error_message));
            }
        });

    }

    private HashMap<String, String> getLoginParam(String token, SocialData socialData) {
        HashMap<String, String> param = new HashMap<>();
        param.put(Parameter.device_id, token);
        if (socialData.getLoginType() == LoginType.NORMAL) {
            param.put(Parameter.email, edtemail.getText().toString().trim());
            param.put(Parameter.password, edtPassword.getText().toString().trim());
        } else {
            param.put(Parameter.social_id, socialData.getSocialId());
        }
        param.put(Parameter.register_type, socialData.getRegisterType());
        param.put(Parameter.os, "0");
        param.put(Parameter.app_version, "1.1.0.s1mIT8Xdm91MnP");
        param.put(Parameter.mobile_type, AppConstant.MOBILE_TYPE);
        param.put(Parameter.user_type, AppConstant.USER_TYPE);
        String country = "SG";
        if (AppHelper.getInstance().getCountry().contains("Malaysia")) {
            country = "MY";
        }
        param.put(Parameter.country, country);
        return param;
    }

    private void initGoogleLogin() {

        GoogleApiClient.OnConnectionFailedListener listener = connectionResult -> {

        };

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        if (mGoogleApiClient == null || !mGoogleApiClient.isConnected()) {
            mGoogleApiClient = new GoogleApiClient.Builder(LoginActivity.this)
                    .enableAutoManage(LoginActivity.this /* FragmentActivity */, listener /* OnConnectionFailedListener */)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
        }


        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN_GOOGLE);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (FacebookSdk.isFacebookRequestCode(requestCode)) {
            if (callbackManager != null)
                callbackManager.onActivityResult(requestCode, resultCode, data);
        } else if (requestCode == RC_SIGN_IN_GOOGLE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            getGoogleData(result);

            if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                Auth.GoogleSignInApi.signOut(mGoogleApiClient);
            }

            mGoogleApiClient.stopAutoManage(LoginActivity.this);
            mGoogleApiClient.disconnect();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void getGoogleData(GoogleSignInResult result) {

        if (result.isSuccess()) {
            // Signed in successfolly, show authenticated UI.
            final GoogleSignInAccount acct = result.getSignInAccount();
            Log.e("dccsxcsxdsxdscdcdcd", result.getSignInAccount().getPhotoUrl() + "");

            SocialData socialData = new SocialData();

            String name = "";
            if (acct.getDisplayName() != null) {
                name = acct.getDisplayName();
                if (name.contains("@")) {
                    name = name.split("@")[0];
                    name = name.replace(".", " ");
                    name = name.replace("_", " ");
                }
            } else
                name = acct.getEmail().split("@")[0];


            Log.e("Dispay Name :", acct.getDisplayName() + "...");
            Log.e("email :", acct.getEmail() + "...");
            Log.e("acc id :", acct.getId().toString() + "...");

            String finalName = name;
            socialData.setName(finalName);
            socialData.setEmail(acct.getEmail());
            socialData.setSocialId(acct.getId());
            socialData.setProfilePicUrl(acct.getPhotoUrl() + "");
            socialData.setLoginType(LoginType.GOOGLE);
            socialData.setRegisterType("1");

//            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(instanceIdResult -> callSocialLoginApi(instanceIdResult.getToken(), socialData));
            callSocialLoginApi("", socialData);
        } else {
            Log.e("Google sign result :", result.getStatus().getStatusCode()+"");
            // Signed out, show unauthenticated UI.
        }

    }

    private void initFacebookSDK() {
//        btnFacebookLogin.setLoginBehavior(LoginBehavior.WEB_ONLY);
        callbackManager = CallbackManager.Factory.create();

//        btnFacebookLogin.setReadPermissions(Arrays.asList("email", "user_gender", "user_birthday"));
        btnFacebookLogin.setReadPermissions(Arrays.asList("email"));
//        btnFacebookLogin.setFragment(this);
        //LoginManager.getInstance().setLoginBehavior(LoginBehavior.WEB_ONLY);
        btnFacebookLogin.setLoginBehavior(LoginBehavior.WEB_ONLY);
        //callbacl registration
        btnFacebookLogin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.e("onSuccess", AccessToken.getCurrentAccessToken().toString());
                getFacebookData();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });


        if (AccessToken.getCurrentAccessToken() == null)
            btnFacebookLogin.performClick();
        else
            getFacebookData();
    }

    private void getFacebookData() {

        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                Log.e("Facebook response :", object.toString());

                Gson gson = new Gson();
                final FacebookData facebookData = gson.fromJson(object.toString(), FacebookData.class);

                SocialData socialData = new SocialData();
                socialData.setName(facebookData.getName());
                socialData.setEmail(facebookData.getEmail());
                socialData.setSocialId(facebookData.getId());
                if (facebookData.getPicture() != null)
                    if (facebookData.getPicture().getData() != null)
                        socialData.setProfilePicUrl(facebookData.getPicture().getData().getUrl() + "");
                socialData.setLoginType(LoginType.FACEBOOK);
                socialData.setRegisterType("2");

//                FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(instanceIdResult -> callSocialLoginApi(instanceIdResult.getToken(), socialData));

                callSocialLoginApi("", socialData);
                LoginManager.getInstance().logOut();
            }
        });


        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,birthday,email,gender,first_name,last_name,picture.type(large)");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void initLinkedInLogin() {
        Linkedin.Companion.initialize(getApplicationContext(),
                "81ye5hhli441c5",
                "Kjz4mdwAzOtm4RMk",
                "https://www.thewirelesspost.com",
                "RANDOM_STRING",
                Arrays.asList("r_liteprofile", "r_emailaddress"));

        Linkedin.Companion.login(this, new LinkedinLoginViewResponseListener() {
            @Override
            public void linkedinDidLoggedIn(@NotNull LinkedinToken linkedinToken) {
                // Success
                Log.e("Success toke", linkedinToken.getAccessToken());
                accessToken = linkedinToken.getAccessToken();

                CheckNetworkListener callback = () -> {
                    if (isValidate()) {
                        showProgressDialog();
                        callLinkedinProfileApi(accessToken);
                    }
                };

                if (isNetworkAvailable(btnGo, callback)) {
                    callback.onRetryClick();
                }

            }

            @Override
            public void linkedinLoginDidFail(@NotNull String error) {
                // Fail
                Log.e("fail", error + ".....");
            }
        });
    }

    private void callLinkedinProfileApi(String token) {

        NetworkCall.getInstance().linkedinProfile(getLinkedinHeaderParam(token), new IResponseCallback<LinkedinProfilePojo>() {
            @Override
            public void success(LinkedinProfilePojo data) {
                Log.e("Linkedin success", data.getId());

                SocialData socialData = new SocialData();
                socialData.setName(data.getFirstName().getLocalized().getEnUS());
//                socialData.setEmail(facebookData.getEmail());
                socialData.setSocialId(data.getId());
                if (data.getProfilePicture() != null)
                    socialData.setProfilePicUrl(data.getProfilePicture().getDisplayImage().getElements().get(0).getIdentifiers().get(0).getIdentifier());
                socialData.setLoginType(LoginType.LINKEDIN);
                socialData.setRegisterType("3");

                callLinkedinEmailApi(token, socialData);
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                hideProgressDialog();
                showSnackBar(btnGo, baseModel.getMessage());
            }

            @Override
            public void onError(Call<LinkedinProfilePojo> responseCall, Throwable T) {
                hideProgressDialog();
                showSnackBar(btnGo, getString(R.string.error_message));
            }
        });

    }

    private void callLinkedinEmailApi(String token, SocialData socialData) {

        NetworkCall.getInstance().linkedinEmail(getLinkedinHeaderParam(token), new IResponseCallback<LinkedinEmailPojo>() {
            @Override
            public void success(LinkedinEmailPojo data) {
                Log.e("Linkedin Email success", data.getElements().get(0).getHandle().getEmailAddress());
                socialData.setEmail(data.getElements().get(0).getHandle().getEmailAddress());

//                FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(instanceIdResult -> callSocialLoginApi(instanceIdResult.getToken(), socialData));
                callSocialLoginApi("", socialData);


            }

            @Override
            public void onFailure(BaseModel baseModel) {
                hideProgressDialog();
                showSnackBar(btnGo, baseModel.getMessage());
            }

            @Override
            public void onError(Call<LinkedinEmailPojo> responseCall, Throwable T) {
                hideProgressDialog();
                showSnackBar(btnGo, getString(R.string.error_message));
            }
        });

    }

    private HashMap<String, String> getLinkedinHeaderParam(String token) {
        HashMap<String, String> param = new HashMap<>();
        param.put("Authorization", "Bearer " + token);
        param.put("cache-control", "no-cache");
        param.put("X-Restli-Protocol-Version", "2.0.0");

        return param;
    }

}