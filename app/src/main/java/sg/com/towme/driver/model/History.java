package sg.com.towme.driver.model;

public class History {
    private int id;
    private int amount;
    private int driver_id;
    private int user_id;
    private int booking_id;
    private String payment_id;
    private String currency;

    private String payment_method_types;
    private String receipt_email;
    private String payment_response;

    private String payment_status;
    private String created_at;
    private String updated_at;
    private String payment_type;

    public int getId() {
        return id;
    }

    public int getAmount() {
        return amount;
    }

    public int getDriver_id() {
        return driver_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public int getBooking_id() {
        return booking_id;
    }

    public String getPayment_id() {
        return payment_id;
    }

    public String getCurrency() {
        return currency;
    }

    public String getPayment_method_types() {
        return payment_method_types;
    }

    public String getReceipt_email() {
        return receipt_email;
    }

    public String getPayment_response() {
        return payment_response;
    }

    public String getPayment_status() {
        return payment_status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String getPayment_type() {
        return payment_type;
    }
}
