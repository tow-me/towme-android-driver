package sg.com.towme.driver.utility.media;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.net.Uri;

import java.io.File;

public class MediaScannerClient implements MediaScannerConnection.MediaScannerConnectionClient {
    private File file;
    private MediaScannerConnection mediaScannerConnection;

    public MediaScannerClient(Context context, File file) {
        this.file = file;
        mediaScannerConnection = new MediaScannerConnection(context, this);
    }

    @Override
    public void onMediaScannerConnected() {
        if (mediaScannerConnection == null) return;
        mediaScannerConnection.scanFile(file.getPath(), "*/*");
    }

    @Override
    public void onScanCompleted(String path, Uri uri) {
        if (mediaScannerConnection == null) return;
        mediaScannerConnection.disconnect();
    }

    public void setScanner(MediaScannerConnection mediaScannerConnection) {
        this.mediaScannerConnection = mediaScannerConnection;
    }

    public void connect() {
        if (mediaScannerConnection == null) return;
        mediaScannerConnection.connect();
    }


}
