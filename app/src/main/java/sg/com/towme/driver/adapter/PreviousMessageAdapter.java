package sg.com.towme.driver.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import sg.com.towme.driver.R;
import sg.com.towme.driver.enumeration.Screens;
import sg.com.towme.driver.listener.RecyclerViewClickListener;
import sg.com.towme.driver.model.booking.BookingData;
import sg.com.towme.driver.utility.DateTimeUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class PreviousMessageAdapter extends RecyclerView.Adapter<PreviousMessageAdapter.ViewHolder> {

    Context context;
    List<BookingData> listBooking;
    Screens screens;

    public void setCallback(RecyclerViewClickListener callback) {
        this.callback = callback;
    }

    RecyclerViewClickListener callback;

    public PreviousMessageAdapter(Context context, List<BookingData> listBooking, Screens screens) {
        this.context = context;
        this.listBooking = listBooking;
        this.screens = screens;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.row_messages, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        BookingData data = listBooking.get(position);
        holder.llRow.setBackgroundColor(context.getResources().getColor(R.color.colorBgGray));
        if (data.getUser() != null) {
            Glide.with(context)
                    .load(data.getUser().getUserImage().trim())
                    .placeholder(R.drawable.ic_user_placeholder)
                    .transform(new CropCircleTransformation())
                    .into(holder.imgProfile);

            holder.txtName.setText(data.getUser().getName());

        }

        if (data.getLastMessage() != null) {
            holder.txtMessage.setText(data.getLastMessage().getMessage());
            holder.txtMessageTime.setText(DateTimeUtil.convertUTCIntoLocal(data.getLastMessage().getCreatedAt(), "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "EEE, dd MMM yyyy hh:mm a"));
        }

        holder.llRow.setOnClickListener(view -> {
            callback.onClick(holder.llRow, position, null);
        });

    }

    @Override
    public int getItemCount() {
        return listBooking.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.llRow)
        LinearLayout llRow;
        @BindView(R.id.imgProfilePic)
        CircleImageView imgProfile;
        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.txtCompanyName)
        AppCompatTextView txtCompanyName;
        @BindView(R.id.txtMessage)
        AppCompatTextView txtMessage;

        @BindView(R.id.txt_message_time)
        TextView txtMessageTime;
        @BindView(R.id.ratingbar)
        AppCompatRatingBar ratingbar;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

}