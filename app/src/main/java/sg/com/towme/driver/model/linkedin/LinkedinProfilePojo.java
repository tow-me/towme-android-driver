
package sg.com.towme.driver.model.linkedin;

import sg.com.towme.driver.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LinkedinProfilePojo extends BaseModel {

    @SerializedName("firstName")
    @Expose
    private FirstName firstName;
    @SerializedName("lastName")
    @Expose
    private LastName lastName;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("profilePicture")
    @Expose
    private ProfilePicture profilePicture;

    public FirstName getFirstName() {
        return firstName;
    }

    public void setFirstName(FirstName firstName) {
        this.firstName = firstName;
    }

    public LastName getLastName() {
        return lastName;
    }

    public void setLastName(LastName lastName) {
        this.lastName = lastName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ProfilePicture getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(ProfilePicture profilePicture) {
        this.profilePicture = profilePicture;
    }
}
