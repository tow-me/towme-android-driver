package sg.com.towme.driver.fragment;


import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import sg.com.towme.driver.activity.AppNavigationActivity;
import sg.com.towme.driver.activity.BaseActivity;
import sg.com.towme.driver.activity.HomeActivity;
import sg.com.towme.driver.activity.ToolBarActivity;

import java.util.ArrayList;
import java.util.List;


public class BaseFragment extends Fragment {


    protected HomeActivity homeActivity;
    protected AppNavigationActivity appNavigationActivity;
    public BaseActivity activity;
    protected ToolBarActivity toolbarActivity;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof BaseActivity) {
            activity = (BaseActivity) context;
        }

        if (context instanceof ToolBarActivity) {
            toolbarActivity = (ToolBarActivity) context;
        }

        if (context instanceof HomeActivity) {
            homeActivity = (HomeActivity) context;
        }
        if (context instanceof AppNavigationActivity) {
            appNavigationActivity = (AppNavigationActivity) context;
        }
    }

    public void showSnackBar(String str) {
        ((AppNavigationActivity) getActivity()).showSnackBar(getView(), str);
    }

    public List<String> getDummyList(int length) {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < length; i++) {
            list.add("test " + i);
        }

        return list;
    }

}
