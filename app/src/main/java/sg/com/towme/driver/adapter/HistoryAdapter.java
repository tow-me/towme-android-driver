package sg.com.towme.driver.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import sg.com.towme.driver.R;
import sg.com.towme.driver.constant.AppConstant;
import sg.com.towme.driver.enumeration.Screens;
import sg.com.towme.driver.listener.RecyclerViewClickListener;
import sg.com.towme.driver.model.booking.BookingData;
import sg.com.towme.driver.utility.AppHelper;
import sg.com.towme.driver.utility.DateTimeUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {

    Context context;
    List<BookingData> listBooking;
    Screens screens;

    public void setCallback(RecyclerViewClickListener callback) {
        this.callback = callback;
    }

    RecyclerViewClickListener callback;

    public HistoryAdapter(Context context, List<BookingData> listBooking, Screens screens) {
        this.context = context;
        this.listBooking = listBooking;
        this.screens = screens;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.row_history_new, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (listBooking.size() > 0) {
            BookingData data = listBooking.get(position);

            if (data.getUser() != null) {
                Glide.with(context)
                        .load(data.getUser().getUserImage().trim())
                        .transform(new CropCircleTransformation())
                        .placeholder(R.drawable.ic_user_placeholder)
                        .into(holder.imgProfile);

                holder.txtName.setText(data.getUser().getName());
                holder.txtLocation.setText(data.getSource());
                holder.txtAgency.setText(data.getDestination());
                holder.txtDate.setText(DateTimeUtil.convertWithoutChangingTimezone(data.getBookingDate(), "yyyy-MM-dd HH:mm:ss", "EEE, dd MMM yyyy hh:mm a"));
                holder.txtCompanyName.setText(data.getUser().getCompanyName());
                String curr = "SGD";
                if (data.getCountry().contains("MY")) {
                    curr = "RM";
                }
                holder.txtPrice.setText(String.format("%s%s", curr, data.getTotalCost()));
//                holder.ratingbar.setRating(Float.valueOf(data.getUser().getAvgRating()));

                String time = DateTimeUtil.convertWithoutChangingTimezone(data.getStartedAt(), "yyyy-MM-dd HH:mm:ss", "hh.mm a") + "-"
                        + DateTimeUtil.convertWithoutChangingTimezone(data.getStoppedAt(), "yyyy-MM-dd HH:mm:ss", "hh.mm a");
                holder.txtTime.setText(time);

                if (data.getBookingStatus()==4 || data.getBookingStatus()==6){
                    holder.txtCardNumber.setVisibility(View.GONE);
                    holder.txtCancelText.setVisibility(View.VISIBLE);
                    holder.txtCancelText.setTextColor(Color.parseColor(AppHelper.getInstance().getStatusColor(data.getBookingStatus(),context)));
                    holder.txtCancelText.setText(AppHelper.getInstance().getStatusText(data.getBookingStatus(),context));
                }else {
                    holder.txtCardNumber.setVisibility(View.VISIBLE);
                    holder.txtCancelText.setVisibility(View.GONE);
                    holder.txtCardNumber.setText(AppHelper.getInstance().getStatusText(data.getBookingStatus(),context));
                    holder.txtCardNumber.setTextColor(Color.parseColor(AppHelper.getInstance().getStatusColor(data.getBookingStatus(),context)));
                }

            }
            holder.llRow.setOnClickListener(view -> {
                callback.onClick(holder.llRow, position, null);
            });
        }
    }

    @Override
    public int getItemCount() {
        return listBooking.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.llRow)
        LinearLayout llRow;
        @BindView(R.id.imgProfilePic)
        CircleImageView imgProfile;
        @BindView(R.id.txtName)
        AppCompatTextView txtName;
        @BindView(R.id.txt_location)
        AppCompatTextView txtLocation;
        @BindView(R.id.txt_agency)
        AppCompatTextView txtAgency;
        @BindView(R.id.txt_date)
        AppCompatTextView txtDate;
        @BindView(R.id.txt_time)
        AppCompatTextView txtTime;
        @BindView(R.id.txtCardNumber)
        AppCompatTextView txtCardNumber;
        @BindView(R.id.txtPrice)
        AppCompatTextView txtPrice;
        @BindView(R.id.txtCompanyName)
        AppCompatTextView txtCompanyName;
        @BindView(R.id.ratingbar)
        AppCompatRatingBar ratingbar;
        @BindView(R.id.txtCancelText)
        AppCompatTextView txtCancelText;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}
