package sg.com.towme.driver.utility;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import sg.com.towme.driver.R;
import sg.com.towme.driver.activity.AppNavigationActivity;
import sg.com.towme.driver.activity.BaseActivity;
import sg.com.towme.driver.listener.IObjectCallback;

public class BaseBottomSheet extends BottomSheetDialogFragment implements DialogInterface.OnShowListener {
    protected Context context;
    protected BaseActivity activity;
    protected AppNavigationActivity appNavigation;

    private boolean expanded;
    private boolean showKeyboard = false;
    private BottomSheetBehavior bottomSheetBehavior;

    private IObjectCallback<Boolean> dismissIObjectCallback;
    private View bottomSheet;

    public void setDismissIObjectCallback(IObjectCallback<Boolean> dismissIObjectCallback) {
        this.dismissIObjectCallback = dismissIObjectCallback;
    }

    private BottomSheetBehavior.BottomSheetCallback bottomSheetCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

    @Override
    public void onStart() {
        super.onStart();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setOnShowListener(this);
        Window window = dialog.getWindow();
        if (window != null) {
            window.setDimAmount(0.25f);
            if (showKeyboard)
                window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
        return dialog;

    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    public void setKeyboard(boolean isVisible) {
        this.showKeyboard = isVisible;
    }

    public void hideBottomSheet() {
        if (bottomSheetBehavior == null) return;
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        dismiss();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
        if (context instanceof BaseActivity)
            activity = (BaseActivity) context;
        if (context instanceof AppNavigationActivity)
            appNavigation = (AppNavigationActivity) context;
    }

    @Override
    public void onShow(DialogInterface dialogInterface) {
        bottomSheetBehavior = getBottomSheetBehaviour((Dialog) dialogInterface);
        if (bottomSheetBehavior == null) return;
        bottomSheetBehavior.addBottomSheetCallback(bottomSheetCallback);

        if (!expanded) return;
        bottomSheetBehavior.setPeekHeight(Resources.getSystem().getDisplayMetrics().heightPixels);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    public BottomSheetBehavior getBottomSheetBehaviour(Dialog dialog) {
        if (dialog == null) return null;
        bottomSheet = dialog.findViewById(R.id.design_bottom_sheet);
        if (bottomSheet == null) return null;
        return BottomSheetBehavior.from(bottomSheet);
    }

    public void requestLayout() {
        if (bottomSheet == null) return;
        bottomSheet.requestLayout();
    }

  /*  public void showProgressDialog() {
        if (activity == null) return;
        activity.showProgressDialog();
    }

    public void hideProgressDialog() {
        if (activity == null) return;
        activity.hideProgressDialog();
    }*/


    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        if (dismissIObjectCallback != null) dismissIObjectCallback.response(true);
    }

    @Override
    public void onCancel(@NonNull DialogInterface dialog) {
        super.onCancel(dialog);
        if (dismissIObjectCallback != null) dismissIObjectCallback.response(true);
    }

    @Override
    public void dismiss() {
        dismissIObjectCallback = null;
        super.dismiss();

    }
}
