package sg.com.towme.driver.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import sg.com.towme.driver.R;
import sg.com.towme.driver.constant.AppConstant;
import sg.com.towme.driver.listener.RecyclerViewClickListener;
import sg.com.towme.driver.model.booking.CreateBooking;
import sg.com.towme.driver.utility.AppHelper;
import sg.com.towme.driver.utility.DateTimeUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RequestAdapter extends RecyclerView.Adapter<RequestAdapter.ViewHolder> {

    Context context;

    public void setCallback(RecyclerViewClickListener callback) {
        this.callback = callback;
    }

    RecyclerViewClickListener callback;
    List<CreateBooking> listBooking;

    public RequestAdapter(Context context, List<CreateBooking> listBooking) {
        this.context = context;
        this.listBooking = listBooking;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.row_request, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CreateBooking data = listBooking.get(position);
        String type = "";
        String title = "";
//        if (data.getAccidentType().length() > 0) {
//            type = "(" + data.getAccidentType() + ")";
//            title = context.getString(R.string.accident);
//        } else {
//            type = "(" + data.getBreakdownType() + ")";
//            title = context.getString(R.string.breakdown);
//        }

        if (data.getPaymentType() == 0)
            holder.txtPaymentType.setText(context.getString(R.string.cash));
        else
            holder.txtPaymentType.setText(context.getString(R.string.card));
        if (data.getBookingType() == null) {
            holder.txtDateTime.setText(context.getString(R.string.towme_now));
        } else {
            if (data.getBookingType() == 0)
                holder.txtDateTime.setText(context.getString(R.string.towme_now));
            else {
                holder.txtDateTime.setText(DateTimeUtil.convertDateFormate(data.getBookingDate(), "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "EEE, dd MMM hh:mm a"));
            }
        }
        String vhcType = data.getVehicle().getVehicleTypeName();
        String vehicletype = "";
        if (vhcType != null) {
            vehicletype = vhcType.substring(0, 1).toUpperCase() + vhcType.substring(1).toLowerCase();
        }
        holder.txtTitle.setText(data.getVehicle().getVehicleNo() + " (" + vehicletype+" - "+ data.getTowPriceType() + ")");
        holder.txtType.setText("(" + data.getTowPriceType() + ")");
        holder.txtSource.setText(data.getSource());
        holder.txtDestination.setText(data.getDestination());
        double d= Double.parseDouble(data.getTotalCost());
        String curr = "SGD";
        if (data.getCountry() != null && data.getCountry().contains("MY")) {
            curr = "RM";
        }
        holder.txtPrice.setText(curr + " " + AppHelper.getInstance().formatCurrency(d));

        holder.btnAccept.setOnClickListener(view -> {
            callback.onClick(holder.btnAccept, position, null);
        });
        holder.btnReject.setOnClickListener(view -> {
            callback.onClick(holder.btnReject, position, null);
        });
        holder.imgInfo.setOnClickListener(view -> {
            callback.onClick(holder.imgInfo, position, null);
        });

    }

    @Override
    public int getItemCount() {
        return listBooking.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.llRow)
        LinearLayout llRow;
        @BindView(R.id.btnAccept)
        Button btnAccept;
        @BindView(R.id.btnReject)
        Button btnReject;
        @BindView(R.id.txtTitle)
        TextView txtTitle;
        @BindView(R.id.txtSource)
        TextView txtSource;
        @BindView(R.id.txtDestination)
        TextView txtDestination;
        @BindView(R.id.txtPrice)
        TextView txtPrice;
        @BindView(R.id.txtType)
        TextView txtType;
        @BindView(R.id.txtPaymentType)
        TextView txtPaymentType;
        @BindView(R.id.txtDateTime)
        TextView txtDateTime;
        @BindView(R.id.imgInfo)
        ImageView imgInfo;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }


}
