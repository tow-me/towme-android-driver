package sg.com.towme.driver.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.core.widget.ContentLoadingProgressBar;

import sg.com.towme.driver.R;
import sg.com.towme.driver.listener.iDialogButtonClick;
import sg.com.towme.driver.utility.AppHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import sg.com.towme.driver.network.NetworkURL;


public class ConnectAccountActivity extends ToolBarActivity {
    private static final String TAG = ConnectAccountActivity.class.getSimpleName();

    @BindView(R.id.webView)
    WebView webView;
    @BindView(R.id.progressBar)
    ContentLoadingProgressBar progressBar;

    private String url;

    private static final String STRIPE_CONNECT = NetworkURL.ROOT_STRIPE + "connect?drive_id=";
    private static final String STRIPE_CONNECT_SUCESS = NetworkURL.ROOT_STRIPE + "stripeSuccess";
    private static final String STRIPE_CONNECT_ERROR = NetworkURL.ROOT_STRIPE + "stripeError";

    public void setTopBar() {
        setToolbarTitle(getString(R.string.payment_setup));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_web);
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);
        setTopBar();
        String link = "https://api.towme.com.sg/api/";
        if (AppHelper.getInstance().getEnvironment() != null) {
            link = AppHelper.getInstance().getEnvironment().getStripeUrl();
        }
        link = link + "connect?drive_id=";
        loadWebView(url = link + +AppHelper.getInstance().getUserDetails().getUserid());
    }

    private void loadWebView(String url) {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewController());
        webView.loadUrl(url);
    }

    private class WebViewController extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            boolean shouldOverrideUrlLoading = super.shouldOverrideUrlLoading(view, request);
            String url = request.getUrl().getPath();
            checkFlag(url);
            return shouldOverrideUrlLoading;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);

        }

        private void checkFlag(String callback) {
            String link = "https://api.towme.com.sg/api/";
            if (AppHelper.getInstance().getEnvironment() != null) {
                link = AppHelper.getInstance().getEnvironment().getStripeUrl();
            }
            link = link + "stripeSuccess";
            String link2 = "https://api.towme.com.sg/api/";
            if (AppHelper.getInstance().getEnvironment() != null) {
                link2 = AppHelper.getInstance().getEnvironment().getStripeUrl();
            }
            link2 = link2 + "stripeError";
            if (link.contains(callback)) {
                openConfirmationDialog(new iDialogButtonClick() {
                    @Override
                    public void negativeClick() {

                    }

                    @Override
                    public void positiveClick(String str) {
                        setResult(RESULT_OK);
                        ConnectAccountActivity.this.finish();

                    }
                }, getString(R.string.stripe_connect_success), getString(R.string.ok), null);
            } else if (link2.contains(callback)) {
                openConfirmationDialog(new iDialogButtonClick() {
                    @Override
                    public void negativeClick() {
                        setResult(RESULT_CANCELED);
                        ConnectAccountActivity.this.finish();
                    }

                    @Override
                    public void positiveClick(String str) {
                        loadWebView(url);
                    }
                }, getString(R.string.stripe_connect_failed), getString(R.string.retry), getString(R.string.cancel));
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
