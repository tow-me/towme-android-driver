package sg.com.towme.driver.utility.media;

import android.content.Context;
import android.location.Geocoder;
import android.os.Parcel;
import android.os.Parcelable;


import sg.com.towme.driver.Controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Dakshay Sanghvi on 9/21/2016.
 */

public class Address implements Parcelable {


    public static final String SECONDARY = "secondary";
    public static final String PRIMARY = "primary";
    public static final String TYPE_HOUSE = "H";
    public static final String TYPE_BUILDING = "B";

    private int id;
    private String type;
    private String city;
    private String address_title;
    private String block;
    private String street;
    private String avenue;
    private String house_building_number;
    private String floor_number;
    private String apartment_number;
    private String direction;

    private String area;

    private String latitude;
    private String longitude;

    private boolean is_default;
    private boolean deleted = false;

    private HashMap<String, String> addressMap;

    public Address() {
    }

    public Address(Context context, HashMap<String, String> map) {
        super();
        setAddressMap(context, map);
    }


    protected Address(Parcel in) {
        id = in.readInt();
        type = in.readString();
        city = in.readString();
        address_title = in.readString();
        block = in.readString();
        street = in.readString();
        avenue = in.readString();
        house_building_number = in.readString();
        floor_number = in.readString();
        apartment_number = in.readString();
        direction = in.readString();
        is_default = in.readByte() != 0;
        area = in.readString();
        latitude = in.readString();
        longitude = in.readString();
        int size = in.readInt();

        if (addressMap != null) {
            for (int i = 0; i < size; i++) {
                String key = in.readString();
                String value = in.readString();
                addressMap.put(key, value);
            }
        }
    }

    public static final Creator<Address> CREATOR = new Creator<Address>() {
        @Override
        public Address createFromParcel(Parcel in) {
            return new Address(in);
        }

        @Override
        public Address[] newArray(int size) {
            return new Address[size];
        }
    };

    public void setAddressMap(Context context, HashMap<String, String> addressMap) {
//        if (addressMap == null) return;
//        this.addressMap = addressMap;
//        this.street = addressMap.get(Geocode.street_number);
//        String route = addressMap.get(Geocode.route);
//
//        if (isValidLine(route)) {
//            if (isValidLine(street)) {
//                street = street + ", ";
//            }
//            street = route;
//            street = street.replaceAll(" St ", " Street ");
//            if (street.contains("Lane")) {
//                street = street.replaceAll(" Street ", " Street - ");
//            }
//        }
//
//        this.house_building_number = addressMap.get(Geocode.premise);
//        this.city = addressMap.get(Geocode.locality);
//
//        this.latitude = addressMap.get(Geocode.lat);
//        this.longitude = addressMap.get(Geocode.lng);
//
//        block = append(addressMap.get(Geocode.sublocality_level_3), addressMap.get(Geocode.sublocality_level_2), addressMap.get(Geocode.sublocality_level_1));
//        avenue = append(addressMap.get(Geocode.administrative_area_level_3), addressMap.get(Geocode.administrative_area_level_2), addressMap.get(Geocode.administrative_area_level_1));
//
//
////        String sub_area = addressMap.get(Geocode.sublocality_level_1);
////        if (isValidLine(sub_area)) {
////            avenue = sub_area + ", " + avenue;
////        }
//
////        String postal_code = addressMap.get(Geocode.postal_code);
////        if (isValidLine(postal_code)) {
////            avenue = avenue + " - " + postal_code;
////        }
    }

    private static String append(String... strings) {
        StringBuilder sb = new StringBuilder();
        if (strings != null) {
            for (String string : strings) {
                if (isValidLine(string)) {
                    if (sb.length() > 0) {
                        sb.append("");
                    }
                    sb.append(string);
                }
            }
        }
        return sb.toString();
    }


    private static boolean isValidLine(String line) {
        return line != null && !line.isEmpty();
    }

    public HashMap<String, String> getAddressMap() {
        if (addressMap == null) {
            return addressMap = new HashMap<>();
        }
        return addressMap;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getType() {
        if (type == null || type.isEmpty()) {
            return TYPE_HOUSE;
        }
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getAvenue() {
        return avenue;
    }

    public void setAvenue(String avenue) {
        this.avenue = avenue;
    }

    public String getBuilding() {
        return house_building_number;
    }

    public void setBuilding(String house_building_number) {
        this.house_building_number = house_building_number;
    }

    public String getFloor() {
        return floor_number;
    }

    public void setFloor(String floor_number) {
        this.floor_number = floor_number;
    }

    public String getApartment() {
        return apartment_number;
    }

    public void setApartment(String apartment_number) {
        this.apartment_number = apartment_number;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }


    public double getLatitude() {
        return getValidDouble(latitude);
    }

    public static String getFormattedLatLng(double lat, double lng) {
        return String.format(Locale.ENGLISH, "%.4f, %.4f ", lat, lng);
    }

    public void setLatitude(double latitude) {
        this.latitude = String.valueOf(latitude);
    }

    public double getLongitude() {
        return getValidDouble(longitude);
    }

    public void setLongitude(double longitude) {
        this.longitude = String.valueOf(longitude);
    }

    public static double getValidDouble(String value) {
        if (value == null || value.trim().isEmpty()) {
            return 0;
        }
        try {
            return Double.parseDouble(value);
        } catch (Exception e) {
//            e.printStackTrace();
            return 0;
        }
    }

    public String getAddress_title() {
        return address_title;
    }

    public void setAddress_title(String address_title) {
        this.address_title = address_title;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public boolean is_default() {
        return is_default;
    }

    public void setIs_default(boolean is_default) {
        this.is_default = is_default;
    }

    public boolean isHouse() {
        return TYPE_HOUSE.equalsIgnoreCase(TYPE_HOUSE);
    }

    public String getFullAddress() {
        StringBuilder sb = new StringBuilder();
//        if (isHouse()) {
//            sb.append(house_building_number).append(", ").append(block).append(" ")
//                    .append(street).append(", ").append(avenue).append("\n")
//                    .append(direction.isEmpty() ? "" : direction + "\n")
//                    .append(city).append(", ").append(country.getName());
//        } else {
//            sb.append(apartment_number).append(", ").append(floor_number).append(" Floor, ")
//                    .append(house_building_number).append(block).append("\n")
//                    .append(street).append(", ").append(avenue).append("\n")
//                    .append(direction.isEmpty() ? "" : direction + "\n")
//                    .append(city).append(", ").append(country.getName());
//        }


        if (isHouse()) {
            sb.append("Kuwait").append(" ").append(city).append(" ").append(block).append("-").append(street).append(" ");
            if (avenue != null && !avenue.isEmpty()) {
                sb.append(avenue).append("-");
            }
            sb.append(house_building_number);
            if (direction != null && !direction.isEmpty()) {
                sb.append("\n").append(direction);
            }
        } else {
            sb.append("Kuwait").append(" ").append(city).append(" ").append(block).append("-").append(street).append(" ");
            if (avenue != null && !avenue.isEmpty()) {
                sb.append(avenue).append("-");
            }
            sb.append(apartment_number).append(" ").append(floor_number).append(" ").append(house_building_number);
            if (direction != null && !direction.isEmpty()) {
                sb.append("\n").append(direction);
            }
        }
        return sb.toString();

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(type);
        dest.writeString(city);
        dest.writeString(address_title);
        dest.writeString(block);
        dest.writeString(street);
        dest.writeString(avenue);
        dest.writeString(house_building_number);
        dest.writeString(floor_number);
        dest.writeString(apartment_number);
        dest.writeString(direction);
        dest.writeByte((byte) (is_default ? 1 : 0));
        dest.writeString(area);
        dest.writeString(latitude);
        dest.writeString(longitude);


        if (addressMap == null) {
            dest.writeInt(0);
        } else {
            dest.writeInt(addressMap.size());
            for (Map.Entry<String, String> entry : addressMap.entrySet()) {
                dest.writeString(entry.getKey());
                dest.writeString(entry.getValue());
            }
        }

    }

    public String myShortAddress() {
        StringBuilder sb = new StringBuilder();
        if (isHouse()) {
            sb.append(house_building_number).append(", ").append(block).append(",").append(street).append(", ").append(avenue).append("\n").append(direction.isEmpty() ? "" : direction + "\n").append(city).append(", ").append("Kuwait");
        } else {
            sb.append(apartment_number).append(", ").append(floor_number).append(", ").append(house_building_number).append(block).append(", ").append(street).append(" Street, ").append(avenue.isEmpty() ? "" : avenue + ", ").append(direction.isEmpty() ? "" : direction + "\n").append(city).append(", ").append("Kuwait");
        }

        return sb.toString();
    }

    public String myShortEncodeAddress() {
        StringBuilder sb = new StringBuilder();
        if (isHouse()) {
            sb.append(house_building_number).append("+").append(block).append("+").append(street).append("+").append(avenue).append("+").append(direction.isEmpty() ? "" : direction + "+").append(city).append("+").append("Kuwait");
        } else {
            sb.append(apartment_number).append("+").append(floor_number).append("+").append(house_building_number).append(block).append("+").append(street).append("+").append(avenue.isEmpty() ? "" : avenue + "+").append(direction.isEmpty() ? "" : direction + "+").append(city).append("+").append("Kuwait");
        }

        return sb.toString();
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Address{");
        sb.append("id=").append(id);
        sb.append(", type='").append(type).append('\'');
        sb.append(", country=").append("Kuwait");
        sb.append(", city='").append(city).append('\'');
        sb.append(", address_title='").append(address_title).append('\'');
        sb.append(", block='").append(block).append('\'');
        sb.append(", street='").append(street).append('\'');
        sb.append(", avenue='").append(avenue).append('\'');
        sb.append(", house_building_number='").append(house_building_number).append('\'');
        sb.append(", floor_number='").append(floor_number).append('\'');
        sb.append(", apartment_number='").append(apartment_number).append('\'');
        sb.append(", direction='").append(direction).append('\'');
        sb.append(", is_default=").append(is_default);
        sb.append(", latitude=").append(latitude);
        sb.append(", longitude=").append(longitude);
        sb.append(", addressMap=").append(addressMap);
        sb.append('}');
        return sb.toString();
    }


    private void geCodeAddress(double lat, double lng) {
        try {
            Geocoder geocoder = new Geocoder(Controller.getInstance(), Locale.getDefault());
            List<android.location.Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            if (addresses != null && addresses.size() > 0) {
                String line = "l";
                android.location.Address newAdress = addresses.get(0);
                if (newAdress.getMaxAddressLineIndex() > -1) {
                    line = newAdress.getAddressLine(0);
                }
                String city = newAdress.getLocality();
                String state = newAdress.getAdminArea();
                String country = newAdress.getCountryName();
                String postalCode = newAdress.getPostalCode();
                String knownName = newAdress.getFeatureName();
//                Logger.e("Geocode Address", line + " " + city + " " + state + " " + country + " " + postalCode + " " + knownName);
//                Logger.e("Full Geocode", newAdress.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean hasLatLng() {
        return getLatitude() != 0 || getLongitude() != 0;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }
}
