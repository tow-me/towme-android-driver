package sg.com.towme.driver.activity;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;


import sg.com.towme.driver.R;

import butterknife.ButterKnife;

public class DummyActivity extends ToolBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);

        iniUI();
    }

    private void iniUI() {
//        setHomeIcon(R.drawable.ic_back_white);
        setToolbarTitle("");
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
