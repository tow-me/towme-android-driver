package sg.com.towme.driver.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import sg.com.towme.driver.fragment.BaseFragment;

import java.util.List;

/**
 * Created by Dhara Golakiya.
 */

public class VPagerAdapter extends FragmentStatePagerAdapter {
    private List<BaseFragment> baseFragments;

    public VPagerAdapter(FragmentManager fm, List<BaseFragment> baseFragments) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.baseFragments = baseFragments;
    }

    @NonNull
    @Override
    public Fragment getItem(int i) {
        return baseFragments.get(i);
    }

    @Override
    public int getCount() {
        return baseFragments.size();
    }
}
