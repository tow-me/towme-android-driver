
package sg.com.towme.driver.model.booking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class User implements Serializable {

    @SerializedName("userid")
    @Expose
    private Integer userid;
    @SerializedName("invite_code")
    @Expose
    private String inviteCode;
    @SerializedName("social_id")
    @Expose
    private String socialId;
    @SerializedName("user_image")
    @Expose
    private String userImage;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("is_verified")
    @Expose
    private String isVerified;
    @SerializedName("mobile_v_code")
    @Expose
    private String mobileVCode;
    @SerializedName("email_v_code")
    @Expose
    private Object emailVCode;
    @SerializedName("vehicle_no")
    @Expose
    private Object vehicleNo;
    @SerializedName("ic_no")
    @Expose
    private Object icNo;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("uen")
    @Expose
    private String uen;
    @SerializedName("register_type")
    @Expose
    private Integer registerType;
    @SerializedName("user_type")
    @Expose
    private Integer userType;
    @SerializedName("user_status")
    @Expose
    private Integer userStatus;
    @SerializedName("mobile_type")
    @Expose
    private Integer mobileType;
    @SerializedName("tow_truck_details")
    @Expose
    private Object towTruckDetails;
    @SerializedName("flatebed_details")
    @Expose
    private Object flatebedDetails;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("profile_pic")
    @Expose
    private String profilePic;
    @SerializedName("country_code")
    @Expose
    private String countryCode;
    @SerializedName("stripe_customer_id")
    @Expose
    private Object stripeCustomerId;
    public String getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(String avgRating) {
        this.avgRating = avgRating;
    }

    @SerializedName("avg_rating")
    @Expose
    private String avgRating;
    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getInviteCode() {
        return inviteCode;
    }

    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
    }

    public String getSocialId() {
        return socialId;
    }

    public void setSocialId(String socialId) {
        this.socialId = socialId;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(String isVerified) {
        this.isVerified = isVerified;
    }

    public String getMobileVCode() {
        return mobileVCode;
    }

    public void setMobileVCode(String mobileVCode) {
        this.mobileVCode = mobileVCode;
    }

    public Object getEmailVCode() {
        return emailVCode;
    }

    public void setEmailVCode(Object emailVCode) {
        this.emailVCode = emailVCode;
    }

    public Object getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(Object vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public Object getIcNo() {
        return icNo;
    }

    public void setIcNo(Object icNo) {
        this.icNo = icNo;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getUen() {
        return uen;
    }

    public void setUen(String uen) {
        this.uen = uen;
    }

    public Integer getRegisterType() {
        return registerType;
    }

    public void setRegisterType(Integer registerType) {
        this.registerType = registerType;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public Integer getMobileType() {
        return mobileType;
    }

    public void setMobileType(Integer mobileType) {
        this.mobileType = mobileType;
    }

    public Object getTowTruckDetails() {
        return towTruckDetails;
    }

    public void setTowTruckDetails(Object towTruckDetails) {
        this.towTruckDetails = towTruckDetails;
    }

    public Object getFlatebedDetails() {
        return flatebedDetails;
    }

    public void setFlatebedDetails(Object flatebedDetails) {
        this.flatebedDetails = flatebedDetails;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getStripeCustomerId() {
        return stripeCustomerId;
    }

    public void setStripeCustomerId(Object stripeCustomerId) {
        this.stripeCustomerId = stripeCustomerId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
}
