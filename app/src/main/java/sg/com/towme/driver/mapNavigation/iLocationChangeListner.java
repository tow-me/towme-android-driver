package sg.com.towme.driver.mapNavigation;

import android.location.Location;

public interface iLocationChangeListner {
    void onLocationChange(String address, double latitude, double longitude, Location location);
}
