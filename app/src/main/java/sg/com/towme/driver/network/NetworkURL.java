package sg.com.towme.driver.network;

public interface NetworkURL {

    //    String ROOT_URL = "http://13.126.131.239/towme/api/v1/";
//    String ROOT_URL = "http://3.6.41.156/towme/api/v1/";
//    String ROOT_URL = "http://172.105.35.50/towme/api/v1/";
//    String ROOT_URL = "https://app.towme.com.sg/api/v1/";
//    String ROOT_URL = "http://devapp.towme.com.sg/api/v1/";
    String ROOT_URL = "https://devapp.towme.com.sg/api/v1/";
//    String ROOT_URL = "https://app.towme.com.sg/api/v1/";
    String ROOT_URL_LINKEDIN = "https://api.linkedin.com/v2/";
//    String ROOT_STRIPE = "http://172.105.35.50:3400/api/stripe/";
//    String ROOT_STRIPE = "https://api.towme.com.sg:3400/api/";
//    String ROOT_STRIPE = "http://devapi.towme.com.sg:3400/api/";

    String ROOT_STRIPE = "https://devapi.towme.com.sg/api/";
//    String ROOT_STRIPE = "https://api.towme.com.sg/api/";


    String LINKEDIN_PROFILE_DETAILS = "me?projection=(id,firstName,lastName,profilePicture(displayImage~:playableStreams))";
    String LINKEDIN_EMAIL = "emailAddress?q=members&projection=(elements*(handle~))";

    String LOGIN = "login";
    String REGISTER = "driver/register";

    String OTP_VERIFICATION = "driver/otpverify";
    String RESEND_OTP = "driver/resendotp";
    String FORGOT_PASSWORD = "driver/forgotpassword";
    String RESET_PASSWORD = "driver/resetpassword";

    String USER_PROFILE = "user/profile";
    String GET_PROFILE = "driver/profile";
    String UPDATE_PROFILE = "update/driver/profile";
    String UPDATE_TOWTRUCK = "update/driver/towtruck";
    String UPDATE_FLATEBED = "update/driver/flatebed";
    String CHANGE_COUNTRY = "country/switch";

    String CHANGE_PASSWORD = "change/password";
    String GET_NOTIFICATION = "get/notification";
    String LOGOUT = "logout";
    String GET_INVITE_CODE = "get/invitation/detail";

    //chat
    String BOOKING_LIST = "booking/list";
    String UPLOAD = "upload";

    //Stripe
    String BALANCE = "stripe/balance";
    String TRANSFER = "payment/request";
    String HISTORY = "history";
    String TRANSACTION_HISTORY = "stripe/paymentHistory";

    String EPHEMERAL_KEY = "stripe/ephemeral_keys";
    String PAYMENT_INTENT = "stripe/paymentIntent";
    String CAPTURE_INTENT = "stripe/captureIntent";
    String CONFIRM_INTENT = "stripe/confirmIntent";

    String GET_BRANDS = "get/brands";
    String CONFIGURATION = "get/configuration";
    String CHANGE_TOW_TYPE = "towtype/change";
    String GET_CALENDAR = "get/job/by/month";

    String REFRESH_DEVICE = "common/refresh-device";

    String ACCEPT_MALAYSIA = "malaysia/tow/accept/{booking}";
    String REJECT_MALAYSIA = "malaysia/tow/reject/{booking}";
    String CANCEL_MALAYSIA = "malaysia/tow/cancel/driver/{booking}";
    String START_MALAYSIA = "malaysia/tow/start/{booking}";
    String COMPLETE_MALAYSIA = "malaysia/tow/complete/{booking}";

    String ACCEPT_SG = "singapore/tow/accept/{booking}";
    String REJECT_SG = "singapore/tow/reject/{booking}";
    String CANCEL_SG = "singapore/tow/cancel/driver/{booking}";
    String START_SG = "singapore/tow/start/{booking}";
    String COMPLETE_SG = "singapore/tow/complete/{booking}";

    String UPDATE_LOCATION = "location/update";
    String CHANGE_ONLINE_STATUS = "online/status";

}
