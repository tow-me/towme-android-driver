package sg.com.towme.driver.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import sg.com.towme.driver.R;
import sg.com.towme.driver.adapter.NotificationAdapter;
import sg.com.towme.driver.constant.AppConstant;
import sg.com.towme.driver.listener.CheckNetworkListener;
import sg.com.towme.driver.listener.RecyclerViewClickListener;
import sg.com.towme.driver.model.BaseModel;
import sg.com.towme.driver.model.notification.NotificationData;
import sg.com.towme.driver.model.notification.NotificationPojo;
import sg.com.towme.driver.network.IResponseCallback;
import sg.com.towme.driver.network.NetworkCall;
import sg.com.towme.driver.network.Parameter;


public class NotificationActivity extends ToolBarActivity {
    NotificationAdapter adapter;
    List<NotificationData> listNotification;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    public static final String EXTRA_NOTIFICATION = "EXTRA_NOTIFICATION";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.fragment_notification);
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);
        iniUI();
    }


    private void iniUI() {
//        setHomeIcon(R.drawable.ic_back_white);
        setToolbarTitle(getString(R.string.notification));
        if (listNotification == null) {
            setAdapter();
            CheckNetworkListener callback = () -> {
                showProgressDialog();
                callGetNotificationAPI();
            };

            if (isNetworkAvailable(recyclerview, callback)) {
                callback.onRetryClick();
            }
        } else
            setAdapter();
    }

    private void setAdapter() {
        if (listNotification == null)
            listNotification = new ArrayList<>();
        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        if (adapter == null)
            adapter = new NotificationAdapter(this, listNotification);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.line_horizontal));
        recyclerview.addItemDecoration(dividerItemDecoration);
        adapter.setCallback(new RecyclerViewClickListener() {
            @Override
            public void onClick(View view, int position, Object object) {
                Intent intent = new Intent(NotificationActivity.this, NotificationDetailActivity.class);
                intent.putExtra(EXTRA_NOTIFICATION, Parcels.wrap(listNotification.get(position)));
                startActivity(intent);
            }
        });
        recyclerview.setAdapter(adapter);
    }

    private void callGetNotificationAPI() {
        NetworkCall.getInstance().getNotification(getParam(), new IResponseCallback<NotificationPojo>() {
            @Override
            public void success(NotificationPojo data) {
                hideProgressDialog();
                if (data.getCode() == 1) {
                    listNotification.clear();
                    listNotification.addAll(data.getData());
                    adapter.notifyDataSetChanged();
                    int notifId = getIntent().getIntExtra(AppConstant.notif_id, 0);
                    if (notifId != 0) {
                        for (int i = 0; i < data.getData().size(); i++) {
                            if (data.getData().get(i).getNotificationId() == notifId) {
                                Intent intent = new Intent(NotificationActivity.this, NotificationDetailActivity.class);
                                intent.putExtra(EXTRA_NOTIFICATION, Parcels.wrap(data.getData().get(i)));
                                startActivity(intent);
                                break;
                            }
                        }
                    }
                } else {
                    showSnackBar(recyclerview,data.getMessage());
                }
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                hideProgressDialog();
                showSnackBar(recyclerview,baseModel.getMessage());
            }

            @Override
            public void onError(Call<NotificationPojo> responseCall, Throwable T) {
                hideProgressDialog();
                showSnackBar(recyclerview,getString(R.string.error_message));
            }
        });

    }

    private HashMap<String, String> getParam() {
        HashMap<String, String> param = new HashMap<>();
        param.put(Parameter.notification_for, AppConstant.USER_TYPE);
        return param;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
