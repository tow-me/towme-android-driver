package sg.com.towme.driver.utility.media;

import android.content.Context;
import android.util.Size;

import java.io.File;

public class Thumb {
    public static final int THUMB_SIZE = 480;
    public static final Size SIZE = new Size(THUMB_SIZE, THUMB_SIZE);

    private MediaType mediaType;
    private File file;
    private File thumb;
    private byte[] bytes;

    public Thumb(MediaType mediaType, File file) {
        this.mediaType = mediaType;
        this.file = file;
    }


    public Thumb(MediaType mediaType, File file, File thumb, byte[] bytes) {
        this.mediaType = mediaType;
        this.file = file;
        this.thumb = thumb;
        this.bytes = bytes;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public File getThumb() {
        return thumb;
    }

    public void setThumb(File thumb) {
        this.thumb = thumb;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    public static Thumb generate(Context context, MediaType mediaType, File file) {
        return FileUtil.getThumb(context, mediaType, file);
    }

    public static Thumb generate(MediaType mediaType, File file) {
        return new Thumb(mediaType, file, null, null);
    }
}
