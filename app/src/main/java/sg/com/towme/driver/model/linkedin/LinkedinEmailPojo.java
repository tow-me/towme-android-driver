
package sg.com.towme.driver.model.linkedin;


import sg.com.towme.driver.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LinkedinEmailPojo extends BaseModel {

    @SerializedName("elements")
    @Expose
    private List<ElementEmail> elements = null;

    public List<ElementEmail> getElements() {
        return elements;
    }

    public void setElements(List<ElementEmail> elements) {
        this.elements = elements;
    }

}
