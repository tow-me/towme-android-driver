package sg.com.towme.driver.utility.media;

import android.os.Parcel;
import android.os.Parcelable;

public enum ActionState implements Parcelable {
    NONE(0, "NONE"), PAUSE(1, "PAUSED"),
    PROGRESS(2, "PROGRESS"), PROCESSING(3, "PROCESSING"),
    FINISH(4, "FINISH"), FAILED(5, "FAILED");

    private int id;
    private String name;

    ActionState(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public static final Creator<ActionState> CREATOR = new Creator<ActionState>() {
        @Override
        public ActionState createFromParcel(Parcel in) {
            return ActionState.values()[in.readInt()];
        }

        @Override
        public ActionState[] newArray(int size) {
            return new ActionState[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
    }
}

