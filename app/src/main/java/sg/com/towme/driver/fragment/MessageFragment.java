package sg.com.towme.driver.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.WorkManager;

import io.socket.client.Ack;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import com.google.gson.Gson;

import sg.com.towme.driver.Controller;
import sg.com.towme.driver.R;
import sg.com.towme.driver.adapter.MessageAdapter;
import sg.com.towme.driver.adapter.PreviousMessageAdapter;
import sg.com.towme.driver.constant.AppConstant;
import sg.com.towme.driver.enumeration.ChatType;
import sg.com.towme.driver.enumeration.Screens;
import sg.com.towme.driver.listener.iPageActive;
import sg.com.towme.driver.model.BaseModel;
import sg.com.towme.driver.model.booking.BookingData;
import sg.com.towme.driver.model.booking.BookingListPojo;
import sg.com.towme.driver.model.messages.MessagesPojo;
import sg.com.towme.driver.network.IResponseCallback;
import sg.com.towme.driver.network.NetworkCall;
import sg.com.towme.driver.network.Parameter;
import sg.com.towme.driver.socket.SocketConstant;
import sg.com.towme.driver.socket.SocketIOClient;
import sg.com.towme.driver.utility.AppHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import sg.com.towme.driver.utility.PreferanceHelper;

import static com.facebook.FacebookSdk.getApplicationContext;

public class MessageFragment extends BaseFragment implements iPageActive {

    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    MessageAdapter messagesAdapter;
    PreviousMessageAdapter previousMessagesAdapter;
    List<BookingData> listBooking;
    List<BookingData> listPrevioudBooking;

    Socket mSocket;
    @BindView(R.id.rvPreviousTrip)
    RecyclerView rvPreviousTrip;

    public static MessageFragment newInstance() {
        MessageFragment fragment = new MessageFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        mSocket = SocketIOClient.getInstance();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_messages, container, false);
        ButterKnife.bind(this, view);

        initUI();
        return view;
    }

    private void initUI() {
    }

    private void setAdapter() {

        if (listBooking == null)
            listBooking = new ArrayList<>();

        recyclerview.setLayoutManager(new LinearLayoutManager(getContext()));
        if (messagesAdapter == null)
            messagesAdapter = new MessageAdapter(getContext(), listBooking, Screens.MESSAGES);
//        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerview.getContext(), DividerItemDecoration.VERTICAL);
//
//        recyclerview.addItemDecoration(dividerItemDecoration);

        messagesAdapter.setCallback((view, position, object) -> {
            switch (view.getId()) {
                case R.id.llRow:

                    String startedImages = "";
                    String stoppedImages = "";
                    if (listBooking.get(position).getStartedImages() != null && listBooking.get(position).getStartedImages().size() > 0)
                        startedImages = listBooking.get(position).getStartedImages().get(0);

                    if (listBooking.get(position).getStoppedImages() != null && listBooking.get(position).getStoppedImages().size() > 0)
                        stoppedImages = listBooking.get(position).getStoppedImages().get(0);

                    homeActivity.openChatActivity(listBooking.get(position).getBookingId(), startedImages, stoppedImages, ChatType.CURRENT, listBooking.get(position).getUser(), AppConstant.REQUEST_CHAT);

                    break;
            }

        });

        recyclerview.setAdapter(messagesAdapter);
    }

    private void setPreviousAdapter() {

        if (listPrevioudBooking == null)
            listPrevioudBooking = new ArrayList<>();

        if (previousMessagesAdapter == null)
            previousMessagesAdapter = new PreviousMessageAdapter(getContext(), listPrevioudBooking, Screens.MESSAGES);

        previousMessagesAdapter.setCallback((view, position, object) -> {
            switch (view.getId()) {
                case R.id.llRow:

                    String startedImages = "";
                    String stoppedImages = "";

                    if (listPrevioudBooking.get(position).getStartedImages() != null && listPrevioudBooking.get(position).getStartedImages().size() > 0)
                        startedImages = listPrevioudBooking.get(position).getStartedImages().get(0);

                    if (listPrevioudBooking.get(position).getStoppedImages() != null && listPrevioudBooking.get(position).getStoppedImages().size() > 0)
                        stoppedImages = listPrevioudBooking.get(position).getStoppedImages().get(0);

                    homeActivity.openChatActivity(listPrevioudBooking.get(position).getBookingId(), startedImages, stoppedImages, ChatType.PREVIOUS, listPrevioudBooking.get(position).getUser(), 0);

                    break;
            }

        });

        rvPreviousTrip.setLayoutManager(new LinearLayoutManager(getContext()));
        rvPreviousTrip.setAdapter(previousMessagesAdapter);
    }

    @Override
    public void onPageActive(String str) {
        setAdapter();
        setPreviousAdapter();
        getBookingList();

//        callBookingListApi();
       /* if (listBooking == null || messagesAdapter == null) {
            setAdapter();
            callBookingListApi();
        } else
            setAdapter();*/

    }

    private void getBookingList() {
        appNavigationActivity.showProgressDialog();
        JSONObject object = new JSONObject();
        try {
            object.put(SocketConstant.USER_ID, AppHelper.getInstance().getUserDetails().getUserid());
            object.put(SocketConstant.user_type, AppConstant.USER_TYPE);
            if (AppHelper.getInstance().getUserToken() != null) {
                object.put(SocketConstant.TOKEN, AppHelper.getInstance().getUserToken());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("Booking list param", object.toString() + "...");
        mSocket.emit(SocketConstant.GET_CHAT_LIST_BOOKINGS, object, new Ack() {
            @Override
            public void call(Object... args) {
                getActivity().runOnUiThread(() -> {
                    appNavigationActivity.hideProgressDialog();
                    int code = 400;

                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(args[0].toString());
                        code = obj.getInt("status_code");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (code == 401) {
                        try {
                            if (getApplicationContext() != null) {
                                PreferanceHelper.getInstance().clearPreference();
                                WorkManager.getInstance(Controller.getInstance()).cancelAllWork();
                                homeActivity.openLoginActivity();
                            }
                        } catch (Exception e) {}
                    } else {
                        Log.e("Booking list response", args[0].toString());

               /* Gson gson = new Gson();

                Type collectionType = new TypeToken<ArrayList<CreateBooking>>() {
                }.getType();
                List<CreateBooking> list = gson.fromJson(args[0].toString(), collectionType);*/

                        Gson gson = new Gson();
                        MessagesPojo messagesPojo = gson.fromJson(args[0].toString(), MessagesPojo.class);

                        Log.e("Booking list size", messagesPojo.getData().getPreviousRides().size() + "...");

                        if (messagesPojo.getData().getCurrentRides().size() > 0) {
                            listBooking.clear();
                            listBooking.addAll(messagesPojo.getData().getCurrentRides());
                            messagesAdapter.notifyDataSetChanged();
                        } else {
                            listBooking.clear();
                            messagesAdapter.notifyDataSetChanged();
                        }

                        if (messagesPojo.getData().getPreviousRides().size() > 0) {
                            listPrevioudBooking.clear();
                            listPrevioudBooking.addAll(messagesPojo.getData().getPreviousRides());
                            previousMessagesAdapter.notifyDataSetChanged();
                        } else {
                            listPrevioudBooking.clear();
                            previousMessagesAdapter.notifyDataSetChanged();
                        }
                    }
                });

            }
        });

    }

    private void callBookingListApi() {

        homeActivity.showProgressDialog();
        List<Integer> listStatus = new ArrayList<>();
        listStatus.add(1);
        listStatus.add(2);
        listStatus.add(3);
        listStatus.add(4);
        listStatus.add(6);
        listStatus.add(7);

        NetworkCall.getInstance().bookingList(listStatus, getParam(), new IResponseCallback<BookingListPojo>() {
            @Override
            public void success(BookingListPojo data) {
                homeActivity.hideProgressDialog();

                if (data.getCode() == 1) {
                    listBooking.clear();
                    listBooking.addAll(data.getData());
                    messagesAdapter.notifyDataSetChanged();
                } else {
                    showSnackBar(data.getMessage());
                }

            }

            @Override
            public void onFailure(BaseModel baseModel) {
                homeActivity.hideProgressDialog();
                showSnackBar(baseModel.getMessage());
            }

            @Override
            public void onError(Call<BookingListPojo> responseCall, Throwable T) {
                homeActivity.hideProgressDialog();
                showSnackBar(getString(R.string.error_message));
            }
        });

    }

    private HashMap<String, String> getParam() {
        HashMap<String, String> param = new HashMap<>();
        param.put(Parameter.user_id, String.valueOf(AppHelper.getInstance().getUserDetails().getUserid()));
        param.put(Parameter.user_type, AppConstant.USER_TYPE);
      /*  param.put("booking_status[]", "1");
        param.put("booking_status[]", "2");
        param.put("booking_status[]", "3");
        param.put("booking_status[]", "7");
*/
        return param;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppConstant.REQUEST_CHAT) {
            getBookingList();
        }
    }
}
