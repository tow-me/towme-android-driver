package sg.com.towme.driver.utility;

import android.os.Handler;
import android.os.Looper;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.res.ResourcesCompat;

import sg.com.towme.driver.Controller;
import sg.com.towme.driver.R;

public class Util {

    public static void executeOnMain(Runnable runnable) {
        new Handler(Looper.getMainLooper()).post(runnable);
    }

    public static void TOAST(String message) {
        executeOnMain(() -> {
            Toast toast = Toast.makeText(Controller.getInstance(), message, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);

            View view = toast.getView();
            TextView textView = view.findViewById(android.R.id.message);
            textView.setGravity(Gravity.CENTER);
            textView.setTypeface(ResourcesCompat.getFont(Controller.getInstance(), R.font.helvetica_bold));
            textView.setTextSize(18f);
            toast.show();

        });
    }


}
