package sg.com.towme.driver.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.work.WorkManager;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import jp.wasabeef.glide.transformations.CropCircleTransformation;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import sg.com.towme.driver.Controller;
import sg.com.towme.driver.PicImage.ImagePicker;
import sg.com.towme.driver.R;
import sg.com.towme.driver.genericbottomsheet.GenericBottomModel;
import sg.com.towme.driver.genericbottomsheet.GenericBottomSheetDialog;
import sg.com.towme.driver.listener.CheckNetworkListener;
import sg.com.towme.driver.model.BaseModel;
import sg.com.towme.driver.model.brand.BrandData;
import sg.com.towme.driver.model.brand.BrandPojo;
import sg.com.towme.driver.model.user.UserData;
import sg.com.towme.driver.model.user.UserPojo;
import sg.com.towme.driver.network.IResponseCallback;
import sg.com.towme.driver.network.NetworkCall;
import sg.com.towme.driver.network.Parameter;
import sg.com.towme.driver.utility.AppHelper;
import sg.com.towme.driver.utility.PreferanceHelper;

public class ProfileActivity extends ToolBarActivity {


    @BindView(R.id.imgProfilePic)
    CircleImageView imgProfilePic;
    @BindView(R.id.txtName)
    AppCompatTextView txtName;
    @BindView(R.id.edtName)
    AppCompatEditText edtName;
    @BindView(R.id.edtPhoneNumber)
    AppCompatEditText edtPhoneNumber;
    @BindView(R.id.edtPersonalEmail)
    AppCompatEditText edtPersonalEmail;

    private static final int PICK_IMAGE = 1;
    @BindView(R.id.edtAddress)
    AppCompatEditText edtAddress;
    @BindView(R.id.btnSaveTowTruck)
    AppCompatButton btnSaveTowTruck;

    @BindView(R.id.btnSave)
    AppCompatButton btnSave;
    @BindView(R.id.edtYourEmail)
    AppCompatEditText edtYourEmail;
    @BindView(R.id.edtTouTruckNo)
    AppCompatEditText edtTouTruckNo;
    @BindView(R.id.edtTouTruckModel)
    AppCompatEditText edtTouTruckModel;
    @BindView(R.id.edtTouTruckBrand)
    AppCompatEditText edtTouTruckBrand;
    @BindView(R.id.btnSaveFlatebad)
    AppCompatButton btnSaveFlatebad;
    @BindView(R.id.edtFlatebadNo)
    AppCompatEditText edtFlatebadNo;
    @BindView(R.id.edtFlatebadModel)
    AppCompatEditText edtFlatebadModel;
    @BindView(R.id.edtFlatebadBrand)
    AppCompatEditText edtFlatebadBrand;
    @BindView(R.id.rbDaily)
    AppCompatRadioButton rbDaily;
    @BindView(R.id.rbWeekly)
    AppCompatRadioButton rbWeekly;
    @BindView(R.id.rbMonthly)
    AppCompatRadioButton rbMonthly;
    @BindView(R.id.chkNotification)
    AppCompatCheckBox chkNotification;
    @BindView(R.id.chkEmail)
    AppCompatCheckBox chkEmail;
    @BindView(R.id.chkSMS)
    AppCompatCheckBox chkSMS;
    @BindView(R.id.tvCountryCode)
    TextView tvCountryCode;
    UserData userData;
    File filePath = null;
    private static final int PLACE_PICKER = 111;

    List<BrandData> towtrucks;
    List<BrandData> flatbeds;

    Double latitude = 0.0;
    Double longitude = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);

        Places.initialize(getApplicationContext(), getString(R.string.google_maps_key));

        iniUI();
      /*  userData = AppHelper.getInstance().getUserDetails();
        setUserdata(userData);*/
        CheckNetworkListener callback = () -> {
            showProgressDialog();
            callGetProfileApi();
        };

        if (isNetworkAvailable(imgProfilePic, callback)) {
            callback.onRetryClick();
        }
        getTowTruckBrands();

    }

    private void iniUI() {
        setHomeIcon(R.drawable.ic_back_white);
        setToolbarTitle(getString(R.string.profile));

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
//        menu.findItem(R.id.action_overflow).setVisible(false);
        return true;
    }

    @OnClick({R.id.imgProfilePic, R.id.btnSaveTowTruck, R.id.btnSaveFlatebad, R.id.btnSave, R.id.edtAddress, R.id.edtFlatebadBrand, R.id.edtTouTruckBrand})
    public void onViewClicked(View view) {
        hideKeyBoard();
        switch (view.getId()) {
            case R.id.imgProfilePic:
                if (isReadStorageAllowed(PICK_IMAGE))
                    pickImage();
                break;
            case R.id.btnSaveTowTruck:
                CheckNetworkListener callback1 = () -> {
                    showProgressDialog();
                    callUpdateTowtruckApi();

                };
                if (isNetworkAvailable(imgProfilePic, callback1)) {
                    callback1.onRetryClick();
                }
                break;
            case R.id.btnSaveFlatebad:
                CheckNetworkListener callback2 = () -> {
                    showProgressDialog();
                    callUpdateFlatebedApi();

                };
                if (isNetworkAvailable(imgProfilePic, callback2)) {
                    callback2.onRetryClick();
                }
                break;
            case R.id.btnSave:
                CheckNetworkListener callback = () -> {
                    showProgressDialog();
                    callUpdateProfileAPI(filePath);

                };
                if (isNetworkAvailable(imgProfilePic, callback)) {
                    callback.onRetryClick();
                }
                break;
            case R.id.edtAddress:
                openPlacePicker();
                break;
            case R.id.edtTouTruckBrand:
                if (towtrucks != null)
                    openBrandSelectionDialog(getString(R.string.select_brand), 1);
                break;
            case R.id.edtFlatebadBrand:
                if (flatbeds != null)
                    openBrandSelectionDialog(getString(R.string.select_brand), 2);
                break;
        }
    }

    private void openBrandSelectionDialog(String header, int which) {
        List<GenericBottomModel> modelList = new ArrayList<>();
        if (which == 1) {
            for (int i = 0; i < towtrucks.size(); i++) {
                GenericBottomModel model = new GenericBottomModel();
                model.setId(towtrucks.get(i).getId() + "");
                model.setItemText(towtrucks.get(i).getBrand());
                modelList.add(model);
            }
        } else {
            for (int i = 0; i < flatbeds.size(); i++) {
                GenericBottomModel model = new GenericBottomModel();
                model.setId(flatbeds.get(i).getId() + "");
                model.setItemText(flatbeds.get(i).getBrand());
                modelList.add(model);
            }
        }
        openBottomSheet(header, modelList, new GenericBottomSheetDialog.RecyclerItemClick() {
            @Override
            public void onItemClick(GenericBottomModel genericBottomModel) {
                if (which == 1)
                    edtTouTruckBrand.setText(genericBottomModel.getItemText());
                else
                    edtFlatebadBrand.setText(genericBottomModel.getItemText());
            }
        });
    }

    private void pickImage() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
    }

    private boolean isReadStorageAllowed(int permissionCode) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, permissionCode);
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == PICK_IMAGE)
                pickImage();
        } else {
            showSnackBar(imgProfilePic, getResources().getString(R.string.you_have_to_allow_storage_permission));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE) {
            if (resultCode == RESULT_OK) {
                if (data.getData() != null) {
                    Bitmap bitmap = ImagePicker.getImageFromResult(this, resultCode, data);
                    if (bitmap != null) {
                        filePath = AppHelper.saveToInternalStorage(this, bitmap);
                        AppHelper.loadImage(this, imgProfilePic, R.drawable.ic_user_placeholder, filePath.getPath());
                    }
                }
            }
        } else if (requestCode == PLACE_PICKER) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                latitude = place.getLatLng().latitude;
                longitude = place.getLatLng().longitude;
                edtAddress.setText(place.getAddress());

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.e("Place error", status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    private void callGetProfileApi() {
        NetworkCall.getInstance().getProfile(new IResponseCallback<UserPojo>() {
            @Override
            public void success(UserPojo data) {
                hideProgressDialog();
                userData = data.getData();
                if (userData.getIsBlocked() == 1) {
                    CheckNetworkListener callback1 = () -> {
                        showProgressDialog();
                        callLogoutApi();
                    };
                    if (isNetworkAvailable(btnSave, callback1)) {
                        callback1.onRetryClick();
                    }
                } else
                    setUserdata(data.getData());
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                hideProgressDialog();
                showSnackBar(imgProfilePic, baseModel.getMessage());
            }

            @Override
            public void onError(Call<UserPojo> responseCall, Throwable T) {
                hideProgressDialog();
                showSnackBar(imgProfilePic, getString(R.string.error_message));
            }
        });
    }

    private void callLogoutApi() {

        NetworkCall.getInstance().logout(new IResponseCallback<BaseModel>() {
            @Override
            public void success(BaseModel data) {
                hideProgressDialog();
                if (data.getCode() == 1) {
                    PreferanceHelper.getInstance().clearPreference();
                    WorkManager.getInstance(Controller.getInstance()).cancelAllWork();
                    openLoginActivity();

                } else {
                    showSnackBar(btnSave,data.getMessage());
                }
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                hideProgressDialog();
                showSnackBar(btnSave,baseModel.getMessage());
            }

            @Override
            public void onError(Call<BaseModel> responseCall, Throwable T) {
                hideProgressDialog();
                showSnackBar(btnSave,getString(R.string.error_message));
            }
        });

    }

    private void setUserdata(UserData userData) {
        Glide.with(ProfileActivity.this)
                .load(userData.getUserImage())
                .transform(new CropCircleTransformation())
                .into(imgProfilePic);
        txtName.setText(userData.getName());
        edtName.setText(userData.getName());
        tvCountryCode.setText("+"+userData.getCountryCode());

        edtPhoneNumber.setText(userData.getMobile());
        edtPersonalEmail.setText(userData.getEmail());
        edtAddress.setText(userData.getAddress());

        edtFlatebadNo.setText(userData.getFlatebadVehicleno());
        edtFlatebadModel.setText(userData.getFlatebadModel());
        edtFlatebadBrand.setText(userData.getFlatebadBrand());

        edtTouTruckNo.setText(userData.getTowtruckVehicleno());
        edtTouTruckModel.setText(userData.getTowtruckModel());
        edtTouTruckBrand.setText(userData.getTowtruckBrand());

        if (userData.getType() != null) {
            if (userData.getType().equalsIgnoreCase("0")) {
                rbDaily.setChecked(true);
            } else if (userData.getType().equalsIgnoreCase("1")) {
                rbWeekly.setChecked(true);
            } else if (userData.getType().equalsIgnoreCase("2")) {
                rbMonthly.setChecked(true);
            }
        }
        if (userData.getPushNotificationSubscribe() != null)
            if (userData.getPushNotificationSubscribe().equalsIgnoreCase("1"))
                chkNotification.setChecked(true);
        if (userData.getEmailSubscribe() != null)
            if (userData.getEmailSubscribe().equalsIgnoreCase("1"))
                chkEmail.setChecked(true);
        if (userData.getSmsSubscribe() != null)
            if (userData.getSmsSubscribe().equalsIgnoreCase("1"))
                chkSMS.setChecked(true);

        latitude = Double.parseDouble(userData.getLatitude());
        longitude = Double.parseDouble(userData.getLongitude());

    }

    private void callUpdateProfileAPI(File fileProfileImage) {

        MultipartBody.Part profilePic = null;
        if (fileProfileImage != null) {
            RequestBody requestFile =
                    RequestBody.create(
                            MediaType.parse("image/*"),
                            fileProfileImage
                    );
            profilePic = MultipartBody.Part.createFormData(Parameter.user_image, fileProfileImage.getName(), requestFile);
        }

        NetworkCall.getInstance().updateProfile(profilePic, getUpdateProfileParam(), new IResponseCallback<UserPojo>() {
            @Override
            public void success(UserPojo data) {
                hideProgressDialog();
                showSnackBar(btnSave, data.getMessage());
                if (data.getCode() == 1) {
                   /* UserData userData = AppHelper.getInstance().getUserDetails();
                    userData.setName(data.getData().getName());
                    if (data.getData().getUserImage() != "") {
                        userData.setUserImage(data.getData().getUserImage());
                    }
                    AppHelper.getInstance().setUserDetails(userData);*/
                    AppHelper.getInstance().setUserDetails(data.getData());
                } else {

                }
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                hideProgressDialog();
                showSnackBar(btnSave, baseModel.getMessage());
            }

            @Override
            public void onError(Call<UserPojo> responseCall, Throwable T) {
                hideProgressDialog();
                showSnackBar(btnSave, getString(R.string.error_message));
            }
        });

    }

    public HashMap<String, RequestBody> getUpdateProfileParam() {
        HashMap<String, RequestBody> param = new HashMap<>();

        param.put(Parameter.name, RequestBody.create(MediaType.parse("text/plain"), edtName.getText().toString().trim()));
        param.put(Parameter.mobile, RequestBody.create(MediaType.parse("text/plain"), edtPhoneNumber.getText().toString().trim()));
        param.put(Parameter.address, RequestBody.create(MediaType.parse("text/plain"), edtAddress.getText().toString().trim()));
        param.put(Parameter.longitude, RequestBody.create(MediaType.parse("text/plain"), "20.1013"));
        param.put(Parameter.latitude, RequestBody.create(MediaType.parse("text/plain"), "20.1013"));

        String type = "";
        if (rbDaily.isChecked())
            type = "0";
        else if (rbWeekly.isChecked())
            type = "1";
        else if (rbMonthly.isChecked())
            type = "2";
//        param.put(Parameter.type, RequestBody.create(MediaType.parse("text/plain"), type));

//        param.put(Parameter.push_notification_subscribe, RequestBody.create(MediaType.parse("text/plain"), String.valueOf((chkNotification.isChecked()) ? 1 : 0)));
//        param.put(Parameter.sms_subscribe, RequestBody.create(MediaType.parse("text/plain"), String.valueOf((chkSMS.isChecked()) ? 1 : 0)));
//        param.put(Parameter.email_subscribe, RequestBody.create(MediaType.parse("text/plain"), String.valueOf((chkEmail.isChecked()) ? 1 : 0)));
        param.put(Parameter.towtruck_vehicleno, RequestBody.create(MediaType.parse("text/plain"), edtTouTruckNo.getText().toString().trim()));
        param.put(Parameter.towtruck_model, RequestBody.create(MediaType.parse("text/plain"), edtTouTruckModel.getText().toString().trim()));
        param.put(Parameter.towtruck_brand, RequestBody.create(MediaType.parse("text/plain"), edtTouTruckBrand.getText().toString().trim()));
        param.put(Parameter.flatebad_vehicleno, RequestBody.create(MediaType.parse("text/plain"), edtFlatebadNo.getText().toString().trim()));
        param.put(Parameter.flatebad_model, RequestBody.create(MediaType.parse("text/plain"), edtFlatebadModel.getText().toString().trim()));
        param.put(Parameter.flatebad_brand, RequestBody.create(MediaType.parse("text/plain"), edtFlatebadBrand.getText().toString().trim()));

//        param.put(Parameter.company_name, RequestBody.create(MediaType.parse("text/plain"), userData.getCompanyName()));
//        param.put(Parameter.uen, RequestBody.create(MediaType.parse("text/plain"), userData.getUen()));
        param.put(Parameter.email, RequestBody.create(MediaType.parse("text/plain"), userData.getEmail()));

      /*  if (latitude > 0 || longitude > 0) {
            param.put(Parameter.company_longitude, RequestBody.create(MediaType.parse("text/plain"), String.valueOf(longitude)));
            param.put(Parameter.company_latitude, RequestBody.create(MediaType.parse("text/plain"), String.valueOf(latitude)));
        }*/
        return param;
    }

    private void callUpdateTowtruckApi() {

        NetworkCall.getInstance().updateTowtruck(getTowtruckParam(), new IResponseCallback<BaseModel>() {
            @Override
            public void success(BaseModel data) {
                hideProgressDialog();
                showSnackBar(btnSave, data.getMessage());
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                hideProgressDialog();
                showSnackBar(imgProfilePic, baseModel.getMessage());
            }

            @Override
            public void onError(Call<BaseModel> responseCall, Throwable T) {
                hideProgressDialog();
                showSnackBar(imgProfilePic, getString(R.string.error_message));
            }
        });

    }

    private HashMap<String, String> getTowtruckParam() {
        HashMap<String, String> param = new HashMap<>();
        param.put(Parameter.towtruck_vehicleno, edtTouTruckNo.getText().toString().trim());
        param.put(Parameter.towtruck_model, edtTouTruckModel.getText().toString().trim());
        param.put(Parameter.towtruck_brand, edtTouTruckBrand.getText().toString().trim());
        return param;
    }

    private void callUpdateFlatebedApi() {

        NetworkCall.getInstance().updateFlatebed(getFlatebedParam(), new IResponseCallback<BaseModel>() {
            @Override
            public void success(BaseModel data) {
                hideProgressDialog();
                showSnackBar(btnSave, data.getMessage());
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                hideProgressDialog();
                showSnackBar(imgProfilePic, baseModel.getMessage());
            }

            @Override
            public void onError(Call<BaseModel> responseCall, Throwable T) {
                hideProgressDialog();
                showSnackBar(imgProfilePic, getString(R.string.error_message));
            }
        });

    }

    private HashMap<String, String> getFlatebedParam() {
        HashMap<String, String> param = new HashMap<>();
        param.put(Parameter.flatebad_vehicleno, edtFlatebadNo.getText().toString().trim());
        param.put(Parameter.flatebad_model, edtFlatebadModel.getText().toString().trim());
        param.put(Parameter.flatebad_brand, edtFlatebadBrand.getText().toString().trim());
        return param;
    }

    private void openPlacePicker() {

        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.PHONE_NUMBER, Place.Field.ADDRESS_COMPONENTS, Place.Field.ADDRESS, Place.Field.LAT_LNG);
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields)
                .build(this);
        startActivityForResult(intent, PLACE_PICKER);
    }

    private void getTowTruckBrands() {
        showProgressDialog();
        NetworkCall.getInstance().getBrands(new IResponseCallback<BrandPojo>() {
            @Override
            public void success(BrandPojo data) {
                if (data.getCode() == 1) {
                    if (towtrucks == null)
                        towtrucks = new ArrayList<>();
                    else
                        towtrucks.clear();
                    towtrucks.addAll(data.getData());
                } else {
                    showSnackBar(btnSave, data.getMessage());
                }
                getFlatBedBrands();
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                hideProgressDialog();
                showSnackBar(btnSave, baseModel.getMessage());
            }

            @Override
            public void onError(Call<BrandPojo> responseCall, Throwable T) {
                hideProgressDialog();
                showSnackBar(btnSave, getString(R.string.error_message));
            }
        }, "4");
    }

    private void getFlatBedBrands() {
        NetworkCall.getInstance().getBrands(new IResponseCallback<BrandPojo>() {
            @Override
            public void success(BrandPojo data) {
                hideProgressDialog();
                if (data.getCode() == 1) {
                    if (flatbeds == null)
                        flatbeds = new ArrayList<>();
                    else
                        flatbeds.clear();
                    flatbeds.addAll(data.getData());
                } else
                    showSnackBar(btnSave, data.getMessage());
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                hideProgressDialog();
                showSnackBar(btnSave, baseModel.getMessage());
            }

            @Override
            public void onError(Call<BrandPojo> responseCall, Throwable T) {
                hideProgressDialog();
                showSnackBar(btnSave, getString(R.string.error_message));
            }
        }, "5");
    }

}
