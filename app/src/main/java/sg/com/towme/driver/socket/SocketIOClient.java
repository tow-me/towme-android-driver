package sg.com.towme.driver.socket;


import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import io.socket.engineio.client.transports.WebSocket;
import okhttp3.OkHttpClient;
import sg.com.towme.driver.model.chat.Chat;
import sg.com.towme.driver.network.Parameter;
import sg.com.towme.driver.utility.AppHelper;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by Dhara Golakiya (email:dharagolakiya4@gmail.com) on 03/July/2020
 */
public class SocketIOClient {

    private static Socket mSocket;

    public static Socket getInstance() {
        if (mSocket != null) {
            return mSocket;
        } else {
            initSocket();
            return mSocket;
        }
    }

    private static final TrustManager[] trustAllCerts= new TrustManager[] { new X509TrustManager() {
        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
            return new java.security.cert.X509Certificate[] {};
        }

        public void checkClientTrusted(X509Certificate[] chain,
                                       String authType) throws CertificateException {
        }

        public void checkServerTrusted(X509Certificate[] chain,
                                       String authType) throws CertificateException {
        }
    } };

    private static void initSocket() {
        try {
            HostnameVerifier hostnameVerifier = new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
            SSLContext mySSLContext = SSLContext.getInstance("TLS");
            mySSLContext.init(null, trustAllCerts, null);
            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .hostnameVerifier(hostnameVerifier)
                    .sslSocketFactory(mySSLContext.getSocketFactory(), new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {

                        }

                        @Override
                        public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {

                        }

                        @Override
                        public X509Certificate[] getAcceptedIssuers() {
                            return new X509Certificate[0];
                        }
                    })
                    .build(); // default settings for all sockets

            IO.setDefaultOkHttpWebSocketFactory(okHttpClient);
            IO.setDefaultOkHttpCallFactory(okHttpClient);
            // set as an option
            IO.Options opts = new IO.Options();
            String[] tp = new String[1];
            tp[0] = WebSocket.NAME;
            opts.secure = true;
            opts.callFactory = okHttpClient;
            opts.webSocketFactory = okHttpClient;
            opts.transports = tp;
            String url = "https://api.towme.com.sg";
            if (AppHelper.getInstance().getEnvironment() != null) {
                url = AppHelper.getInstance().getEnvironment().getSocketUrl();
            }
            mSocket = IO.socket(url, opts);

            mSocket.connect();
            mSocket.on(SocketConstant.CONNECT, args -> {
                Log.e("connect success", args.toString());
                loginToSocket();
            });
        } catch (URISyntaxException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
    }

    private static void loginToSocket() {
        if (AppHelper.getInstance().getUserDetails() != null) {
            JSONObject param = new JSONObject();
            try {
                param.put(SocketConstant.USER_ID, String.valueOf(AppHelper.getInstance().getUserDetails().getUserid()));
                param.put(SocketConstant.USER_TYPE, String.valueOf(AppHelper.getInstance().getUserDetails().getType()));
                param.put(Parameter.os, "0");
                param.put(Parameter.app_version, "1.1.0.s1mIT8Xdm91MnP");

            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("Userid", param.toString());

            mSocket.emit(SocketConstant.LOGIN, param, new Ack() {
                @Override
                public void call(Object... args) {
                    Log.e("Login success", args[0].toString());
                }
            });

            mSocket.on(SocketConstant.GET_MESSAGE, onNewMessage);
            mSocket.on(SocketConstant.GET_BOOKING_INVITES, onNewRequest);
        }
//        JSONObject userId = SocketUtils.getInstance().getParameter(SocketConstant.USER_ID, String.valueOf(AppHelper.getInstance().getUserDetails().getUserid()));
    }

    static Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.e("Socket Io onNewMessage", args[0].toString());

            Gson gson = new Gson();
            List<Chat> list = Arrays.asList(gson.fromJson(args[0].toString(), Chat[].class));
            Chat chat = list.get(0);

//            NotificationHelper.getInstance().show(getRequestCode(), chat.getSender().getName(), chat.getMessage());


        }
    };

    private static int getRequestCode() {
        Random rnd = new Random();
        return 100 + rnd.nextInt(900000);
    }

    static void ringtone() {
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static Emitter.Listener onNewRequest = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.e("New Request success", "Paly sound");
            ringtone();
        }
    };


}


