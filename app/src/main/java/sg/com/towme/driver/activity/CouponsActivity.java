package sg.com.towme.driver.activity;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import sg.com.towme.driver.R;
import sg.com.towme.driver.adapter.CouponsAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CouponsActivity extends ToolBarActivity {

    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;

    CouponsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_coupons);
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);

        iniUI();
        setAdapter();
    }

    private void iniUI() {
//        setHomeIcon(R.drawable.ic_back_white);
        setToolbarTitle(getString(R.string.coupons));
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setAdapter() {
        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CouponsAdapter(CouponsActivity.this);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(CouponsActivity.this, DividerItemDecoration.VERTICAL);
        recyclerview.addItemDecoration(dividerItemDecoration);
        recyclerview.setAdapter(adapter);
    }

}
