package sg.com.towme.driver.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import sg.com.towme.driver.R;
import sg.com.towme.driver.constant.AppConstant;
import sg.com.towme.driver.listener.RecyclerViewClickListener;
import sg.com.towme.driver.model.booking.EndRideExtraCharge;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import sg.com.towme.driver.utility.AppHelper;

public class SummeryChargesAdapter extends RecyclerView.Adapter<SummeryChargesAdapter.ViewHolder> {

    Context context;

    public void setCallback(RecyclerViewClickListener callback) {
        this.callback = callback;
    }

    RecyclerViewClickListener callback;
    List<EndRideExtraCharge> extraChargesList;
    String currency;

    public SummeryChargesAdapter(Context context, List<EndRideExtraCharge> extraChargesList, String currency) {
        this.context = context;
        this.extraChargesList = extraChargesList;
        this.currency = currency;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.row_charges_summery, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        EndRideExtraCharge data = extraChargesList.get(position);
        holder.txtDescription.setText(data.getDesc());
        double d= Double.parseDouble(data.getPrice());
        holder.txtPrice.setText(currency  + AppHelper.getInstance().formatCurrency(d) +"");
    }

    @Override
    public int getItemCount() {
        return extraChargesList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtDescription)
        TextView txtDescription;
        @BindView(R.id.txtPrice)
        TextView txtPrice;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

}
