package sg.com.towme.driver.Dialog;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

import sg.com.towme.driver.R;
import sg.com.towme.driver.enumeration.Info;
import sg.com.towme.driver.listener.iInfoDialogListner;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Dhara Golakiya on 17-04-2018.
 */

public class InfoDialog extends BaseDialog {


    Unbinder unbinder;
    @BindView(R.id.txtMessage)
    AppCompatTextView txtMessage;
    @BindView(R.id.txtOk)
    AppCompatTextView txtOk;
    private iInfoDialogListner callBack;

    private String message;

    public void setInfo(Info info) {
        this.info = info;
    }

    private Info info;

    public void setCallBack(iInfoDialogListner callBack) {
        this.callBack = callBack;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static InfoDialog newInstance(iInfoDialogListner callBack, String message,Info info) {
        InfoDialog fragment = new InfoDialog();
        fragment.setMessage(message);
        fragment.setCallBack(callBack);
        fragment.setInfo(info);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_info, container, false);

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        txtMessage.setText(message);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.txtOk})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txtOk:
                callBack.onOkClick(info);
                dismiss();
                break;

        }
    }
}
