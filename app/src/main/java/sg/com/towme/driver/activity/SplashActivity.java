package sg.com.towme.driver.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.widget.LinearLayout;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.ActivityCompat;

import com.bumptech.glide.Glide;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.stripe.android.PaymentConfiguration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import sg.com.towme.driver.R;
import sg.com.towme.driver.model.BaseModel;
import sg.com.towme.driver.model.Environment;
import sg.com.towme.driver.model.config_.ConfigPojo;
import sg.com.towme.driver.network.IResponseCallback;
import sg.com.towme.driver.network.NetworkCall;
import sg.com.towme.driver.network.Parameter;
import sg.com.towme.driver.utility.AppHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;


public class SplashActivity extends AppNavigationActivity implements Runnable {

    @BindView(R.id.llMain)
    LinearLayout llMain;
    @BindView(R.id.imageview)
    AppCompatImageView imageview;
    private Handler handler;

    boolean isHandlerFinish = false;
    boolean isConfigSuccess = false;
    private String env = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
            finish();
            return;
        }
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        clearNotification();
        Glide.with(this)
                .load(R.raw.driver_gif)
                .into(imageview);

        callGetConfigurationData();
        LocationRequest mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(60000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationCallback mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        Geocoder geocoder = new Geocoder(SplashActivity.this, Locale.getDefault());
                        List<Address> addresses = new ArrayList<>();
                        try {
                            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        if (addresses.size() > 0) {
                            String country = addresses.get(0).getCountryName();
                            AppHelper.getInstance().setCountry(country);
                        }
                    }
                }
            }
        };
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.getFusedLocationProviderClient(this).requestLocationUpdates(mLocationRequest, mLocationCallback, null);

    }

    @Override
    protected void onResume() {
        super.onResume();
        startHandler();
    }

    private void startHandler() {
        handler = new Handler();
        handler.postDelayed(this, 3000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (handler != null) handler.removeCallbacks(this);
    }

    @Override
    public void run() {
        isHandlerFinish = true;
        if (isConfigSuccess) {
            if (AppHelper.getInstance().isLogin()){
                FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(instanceIdResult -> refreshDevice(instanceIdResult.getToken()));
            }
            else {
                if (env.toLowerCase().contains("staging")) {
                    Environment environment = new Environment();
                    environment.setRootUrl("https://devapp.towme.com.sg/api/v1/");
                    environment.setSocketUrl("https://devapi.towme.com.sg");
                    environment.setStripeKey("pk_test_asEfactFvsDRRWILkKcn9EJk00GGDsKM2V");
                    environment.setStripeUrl("https://devapi.towme.com.sg/api/");
                    AppHelper.getInstance().setEnvironment(environment);
                    String key = "pk_live_dtkwEhkGEsaXLjZlH4qMhJlK00k8ktUWu6";
                    if (AppHelper.getInstance().getEnvironment() != null) {
                        key = AppHelper.getInstance().getEnvironment().getStripeKey();
                    }
                    PaymentConfiguration.init(SplashActivity.this, key);
                } else if (env.toLowerCase().contains("production")) {
                    Environment environment = new Environment();
                    environment.setRootUrl("https://app.towme.com.sg/api/v1/");
                    environment.setSocketUrl("https://api.towme.com.sg");
                    environment.setStripeKey("pk_live_dtkwEhkGEsaXLjZlH4qMhJlK00k8ktUWu6");
                    environment.setStripeUrl("https://api.towme.com.sg/api/");
                    AppHelper.getInstance().setEnvironment(environment);
                    String key = "pk_live_dtkwEhkGEsaXLjZlH4qMhJlK00k8ktUWu6";
                    if (AppHelper.getInstance().getEnvironment() != null) {
                        key = AppHelper.getInstance().getEnvironment().getStripeKey();
                    }
                    PaymentConfiguration.init(SplashActivity.this, key);
                }
                NetworkCall.getInstance().getConfiguration(new IResponseCallback<ConfigPojo>() {
                    @Override
                    public void success(ConfigPojo data) {
//                hideProgressDialog();
                        if (data.getCode() == 1) {
                            isConfigSuccess = true;
                            AppHelper.getInstance().setConfigDetails(data.getData());
                            if (isHandlerFinish) {
                                openLoginActivity();
                            }
                        } else {
                            Snackbar snackbar = Snackbar.make(llMain, data.getMessage(), Snackbar.LENGTH_LONG);
                            snackbar.show();
                        }

                    }

                    @Override
                    public void onFailure(BaseModel baseModel) {
//                hideProgressDialog();
                        Snackbar snackbar = Snackbar.make(llMain, baseModel.getMessage(), Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }

                    @Override
                    public void onError(Call<ConfigPojo> responseCall, Throwable T) {
//                hideProgressDialog();
                        Snackbar snackbar = Snackbar.make(llMain, getString(R.string.error_message), Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                });
            }
        }

    }

    private HashMap<String, String> getDeviceParam(String token) {
        HashMap<String, String> param = new HashMap<>();
        param.put(Parameter.device_id, token);
        param.put(Parameter.os, "0");
        return param;
    }

    private void refreshDevice(String token) {
        NetworkCall.getInstance().refreshDevice(getDeviceParam(token), new IResponseCallback<BaseModel>() {
            @Override
            public void success(BaseModel data) {
                Intent i = getIntent();
                Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                if (i.hasExtra("notification_type")) {
                    Bundle extras = i.getExtras();
                    if (extras != null) {
                        String type = extras.getString("notification_type");
                        if (type != null) {
                            if (type.equals("chat")) {
                                String senderId = extras.getString("sender_id");
                                if (senderId != null) {
                                    intent.putExtra(HomeActivity.EXTRA_SENDER_ID, extras.getString("sender_id"));
                                }
                            }
                            intent.putExtra(HomeActivity.EXTRA_TYPE, type);
                        }
                    }
                }
                startActivity(intent);
                finishAffinity();
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                Snackbar snackbar = Snackbar.make(llMain, baseModel.getMessage(), Snackbar.LENGTH_LONG);
                snackbar.show();
                if (baseModel.getMessage().contains("Unauthenticated Access")) {
                    openLoginActivity();
                }
            }

            @Override
            public void onError(Call<BaseModel> responseCall, Throwable T) {
                Snackbar snackbar = Snackbar.make(llMain, getString(R.string.error_message), Snackbar.LENGTH_LONG);
                snackbar.show();
                openLoginActivity();
            }
        });
    }

    private void callGetConfigurationData() {
//        showProgressDialog();
        NetworkCall.getInstance().getConfiguration(new IResponseCallback<ConfigPojo>() {
            @Override
            public void success(ConfigPojo data) {
//                hideProgressDialog();
                if (data.getCode() == 1) {
                    isConfigSuccess = true;
                    if (data.getData().getEnvironment() != null) {
                        env = data.getData().getEnvironment();
                    }
                    AppHelper.getInstance().setConfigDetails(data.getData());
                    if (isHandlerFinish) {
                        if (AppHelper.getInstance().isLogin()) {
                            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(instanceIdResult -> refreshDevice(instanceIdResult.getToken()));
                        }
                        else {
                            if (env.toLowerCase().contains("staging")) {
                                Environment environment = new Environment();
                                environment.setRootUrl("https://devapp.towme.com.sg/api/v1/");
                                environment.setSocketUrl("https://devapi.towme.com.sg");
                                environment.setStripeKey("pk_test_asEfactFvsDRRWILkKcn9EJk00GGDsKM2V");
                                environment.setStripeUrl("https://devapi.towme.com.sg/api/");
                                AppHelper.getInstance().setEnvironment(environment);
                                String key = "pk_live_dtkwEhkGEsaXLjZlH4qMhJlK00k8ktUWu6";
                                if (AppHelper.getInstance().getEnvironment() != null) {
                                    key = AppHelper.getInstance().getEnvironment().getStripeKey();
                                }
                                PaymentConfiguration.init(SplashActivity.this, key);
                            } else if (env.toLowerCase().contains("production")) {
                                Environment environment = new Environment();
                                environment.setRootUrl("https://app.towme.com.sg/api/v1/");
                                environment.setSocketUrl("https://api.towme.com.sg");
                                environment.setStripeKey("pk_live_dtkwEhkGEsaXLjZlH4qMhJlK00k8ktUWu6");
                                environment.setStripeUrl("https://api.towme.com.sg/api/");
                                AppHelper.getInstance().setEnvironment(environment);
                                String key = "pk_live_dtkwEhkGEsaXLjZlH4qMhJlK00k8ktUWu6";
                                if (AppHelper.getInstance().getEnvironment() != null) {
                                    key = AppHelper.getInstance().getEnvironment().getStripeKey();
                                }
                                PaymentConfiguration.init(SplashActivity.this, key);
                            }
                            NetworkCall.getInstance().getConfiguration(new IResponseCallback<ConfigPojo>() {
                                @Override
                                public void success(ConfigPojo data) {
//                hideProgressDialog();
                                    if (data.getCode() == 1) {
                                        isConfigSuccess = true;
                                        AppHelper.getInstance().setConfigDetails(data.getData());
                                        if (isHandlerFinish) {
                                            openLoginActivity();
                                        }
                                    } else {
                                        Snackbar snackbar = Snackbar.make(llMain, data.getMessage(), Snackbar.LENGTH_LONG);
                                        snackbar.show();
                                    }

                                }

                                @Override
                                public void onFailure(BaseModel baseModel) {
//                hideProgressDialog();
                                    Snackbar snackbar = Snackbar.make(llMain, baseModel.getMessage(), Snackbar.LENGTH_LONG);
                                    snackbar.show();
                                }

                                @Override
                                public void onError(Call<ConfigPojo> responseCall, Throwable T) {
//                hideProgressDialog();
                                    Snackbar snackbar = Snackbar.make(llMain, getString(R.string.error_message), Snackbar.LENGTH_LONG);
                                    snackbar.show();
                                }
                            });
                        }
                    }
                } else {
                    showSnackBar(llMain, data.getMessage());
                }

            }

            @Override
            public void onFailure(BaseModel baseModel) {
//                hideProgressDialog();
                showSnackBar(llMain, baseModel.getMessage());
            }

            @Override
            public void onError(Call<ConfigPojo> responseCall, Throwable T) {
//                hideProgressDialog();
                showSnackBar(llMain, T.toString());
            }
        });

    }

}
