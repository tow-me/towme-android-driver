package sg.com.towme.driver.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;

import sg.com.towme.driver.R;

import sg.com.towme.driver.model.config_.ConfigDetails;
import sg.com.towme.driver.utility.AppHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HelpWebviewActivity extends ToolBarActivity {

    @BindView(R.id.webView)
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_help_webview);
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);

        iniUI();
        callWebviewMethod();
    }

    private void iniUI() {
//        setHomeIcon(R.drawable.ic_back_white);
        setToolbarTitle(getString(R.string.help));
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void callWebviewMethod() {
        webView.setWebViewClient(new MyBrowser());
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        ConfigDetails configDetails = AppHelper.getInstance().getConfigDetails();
        webView.loadUrl(configDetails.getHelpUser());
    }


    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
           /* view.loadUrl(url);
            return true;*/

            if(url.contains("tel:"))
            {
//                tel:+6578 2444
                Log.e("url",url);
                //make the substring with the telephone number, and invoke intent for dialer
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(url));
                startActivity(intent);
                return true;

            }
            else
                view.loadUrl(url);
            return true;
        }
    }

 /*   private void callWebviewMethod() {
        webView.setWebViewClient(new myWebClient());
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setSaveFormData(true);
        webView.getSettings().setAllowContentAccess(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setAllowFileAccessFromFileURLs(true);
        webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        webView.getSettings().setSupportZoom(true);
        webView.setClickable(true);
        webView.setWebChromeClient(new WebChromeClient() {
            public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
                callback.invoke(origin, true, false);
            }
        });

        ConfigDetails configDetails = AppHelper.getInstance().getConfigDetails();
//        webView.loadUrl(AppConstant.help_url);
        webView.loadUrl(configDetails.getHelpDriver().trim());
    }

    public class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

        }
    }*/

}
