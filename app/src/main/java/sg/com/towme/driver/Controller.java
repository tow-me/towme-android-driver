package sg.com.towme.driver;

import android.app.Application;

import com.stripe.android.PaymentConfiguration;
import sg.com.towme.driver.payment.StripeUtil;
import sg.com.towme.driver.utility.PreferanceHelper;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;


public class Controller extends Application {
    private static Controller application;
    private PreferanceHelper preferenceHelper;

    public static Controller getInstance() {
        return application;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
        preferenceHelper = PreferanceHelper.getInstance();
        PaymentConfiguration.init(this, StripeUtil.STRIPE_PUBLISHER_KEY);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

    }

    public PreferanceHelper getPreferenceHelper() {
        return preferenceHelper;
    }


}
