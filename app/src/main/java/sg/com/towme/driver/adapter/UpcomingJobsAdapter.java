package sg.com.towme.driver.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import sg.com.towme.driver.R;
import sg.com.towme.driver.listener.RecyclerViewClickListener;
import sg.com.towme.driver.model.booking.CreateBooking;
import sg.com.towme.driver.utility.DateTimeUtil;


import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class UpcomingJobsAdapter extends RecyclerView.Adapter<UpcomingJobsAdapter.ViewHolder> {

    Context context;

    public void setCallback(RecyclerViewClickListener callback) {
        this.callback = callback;
    }

    RecyclerViewClickListener callback;
    List<CreateBooking> bookingList;

    public UpcomingJobsAdapter(Context context, List<CreateBooking> upcomingBookingList) {
        this.context = context;
        this.bookingList = upcomingBookingList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.row_upcoming_jobs, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CreateBooking data = bookingList.get(position);
        holder.txtDate.setText(DateTimeUtil.convertDateFormate(data.getBookingDate(), "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "EEE, dd MMM hh:mm a"));

     /*   if (bookingList.size() == 1) {
            holder.expandable.expand();
        } else {
            holder.expandable.collapse();
        }*/

        if (data.getDriver() != null) {

            if (data.getDriver() instanceof List) {

            } else {
             /*   LinkedTreeMap<String, String> driver = (LinkedTreeMap<String, String>) data.getDriver();
                holder.txtName.setText(driver.get("name"));
                holder.txtCompanyName.setText(driver.get("company_name"));

                Glide.with(context)
                        .load(driver.get("user_image"))
                        .transform(new CropCircleTransformation())
                        .into(holder.imgProfilePic);*/
            }
        }


        holder.txtName.setText(data.getUser().getName());
        holder.txtCompanyName.setText(data.getUser().getCompanyName());

        Glide.with(context)
                .load(data.getUser().getUserImage())
                .transform(new CropCircleTransformation())
                .into(holder.imgProfilePic);

//        if (bookingList.size() > 1)
            holder.txtDate.setOnClickListener(view -> {
                if (holder.expandable.isExpanded()) {
                    holder.expandable.collapse();
                    holder.txtDate.setBackgroundResource(R.drawable.top_corner_white);
                    holder.txtDate.setTextColor(context.getResources().getColor(R.color.colorTextLightGreen));
                } else {
                    holder.expandable.expand();
                    holder.txtDate.setBackgroundResource(R.drawable.top_corner_green);
                    holder.txtDate.setTextColor(context.getResources().getColor(R.color.colorTextWhite));
                }
            });

        holder.txtCancelJob.setOnClickListener(view -> callback.onClick(holder.txtCancelJob,position,data));
        holder.txtNavigateJob.setOnClickListener(view -> callback.onClick(holder.txtNavigateJob, position, data));

        holder.expandable.setOnExpansionUpdateListener(new ExpandableLayout.OnExpansionUpdateListener() {
            @Override
            public void onExpansionUpdate(float expansionFraction, int state) {
                Log.e("state",state+"...");
            }
        });
    }

    @Override
    public int getItemCount() {
        return bookingList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtDate)
        TextView txtDate;
        @BindView(R.id.imgProfilePic)
        ImageView imgProfilePic;
        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.txtCompanyName)
        TextView txtCompanyName;
        @BindView(R.id.txtNavigateJob)
        TextView txtNavigateJob;
        @BindView(R.id.txtCancelJob)
        TextView txtCancelJob;
        @BindView(R.id.expandable)
        ExpandableLayout expandable;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }


}
