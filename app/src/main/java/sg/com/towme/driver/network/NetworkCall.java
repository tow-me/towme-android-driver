package sg.com.towme.driver.network;


import android.util.Log;

import okhttp3.ResponseBody;
import sg.com.towme.driver.model.BaseModel;
import sg.com.towme.driver.model.OnOffResponse;
import sg.com.towme.driver.model.UploadPojo;
import sg.com.towme.driver.model.booking.BookingListPojo;
import sg.com.towme.driver.model.brand.BrandPojo;
import sg.com.towme.driver.model.calendar.CalendarPojo;
import sg.com.towme.driver.model.config_.ConfigPojo;
import sg.com.towme.driver.model.invite.InvitePojo;
import sg.com.towme.driver.model.linkedin.LinkedinEmailPojo;
import sg.com.towme.driver.model.linkedin.LinkedinProfilePojo;
import sg.com.towme.driver.model.notification.NotificationPojo;
import sg.com.towme.driver.model.stripe.Balance;
import sg.com.towme.driver.model.transaction.TransactionData;
import sg.com.towme.driver.model.user.UserPojo;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sg.com.towme.driver.utility.AppHelper;

public class NetworkCall {

    private static NetworkCall instance;
    private ApiService apiService;
    private ApiService apiServiceLinkedin;
    private ApiService apiServiceStripe;

    private NetworkCall() {
        apiService = RetroClient.getApiService();
        apiServiceLinkedin = RetroClient.getApiServiceLinkedin();
        apiServiceStripe = RetroClient.getApiServiceStripe();
    }

    public static NetworkCall getInstance() {
        instance = new NetworkCall();
        return instance;
    }


    public void postLoginData(HashMap<String, String> param, IResponseCallback<UserPojo> callback) {
        Call<UserPojo> call = apiService.postLoginData(param);
        call.enqueue(new Callback<UserPojo>() {
            @Override
            public void onResponse(@NotNull Call<UserPojo> call, @NotNull Response<UserPojo> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<UserPojo> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }


    public void postRegistrationData(MultipartBody.Part profilePic, HashMap<String, RequestBody> param, IResponseCallback<UserPojo> callback) {
        Call<UserPojo> call = apiService.registration(profilePic, param);
        call.enqueue(new Callback<UserPojo>() {
            @Override
            public void onResponse(@NotNull Call<UserPojo> call, @NotNull Response<UserPojo> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<UserPojo> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }


    public void updateProfile(MultipartBody.Part profilePic, HashMap<String, RequestBody> param, IResponseCallback<UserPojo> callback) {
        Call<UserPojo> call = apiService.updateProfile(HeaderHelper.getInstance().getAuthTokenParam(), profilePic, param);
        call.enqueue(new Callback<UserPojo>() {
            @Override
            public void onResponse(@NotNull Call<UserPojo> call, @NotNull Response<UserPojo> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<UserPojo> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }


    public void changePassword(HashMap<String, String> param, IResponseCallback<BaseModel> callback) {
        Call<BaseModel> call = apiService.changePassword(HeaderHelper.getInstance().getAuthTokenParam(), param);
        call.enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(@NotNull Call<BaseModel> call, @NotNull Response<BaseModel> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<BaseModel> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void changeCountry(HashMap<String, String> param, IResponseCallback<BaseModel> callback) {
        Call<BaseModel> call = apiService.changeCountry(HeaderHelper.getInstance().getAuthTokenParam(), param);
        call.enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseModel> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void linkedinProfile(HashMap<String, String> param, IResponseCallback<LinkedinProfilePojo> callback) {
        Call<LinkedinProfilePojo> call = apiServiceLinkedin.getLinkedinProfile(param);
        call.enqueue(new Callback<LinkedinProfilePojo>() {
            @Override
            public void onResponse(@NotNull Call<LinkedinProfilePojo> call, @NotNull Response<LinkedinProfilePojo> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                 /*   BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }*/
                }
            }

            @Override
            public void onFailure(@NotNull Call<LinkedinProfilePojo> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }


    public void linkedinEmail(HashMap<String, String> param, IResponseCallback<LinkedinEmailPojo> callback) {
        Call<LinkedinEmailPojo> call = apiServiceLinkedin.getLinkedinEmail(param);
        call.enqueue(new Callback<LinkedinEmailPojo>() {
            @Override
            public void onResponse(@NotNull Call<LinkedinEmailPojo> call, @NotNull Response<LinkedinEmailPojo> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                 /*   BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }*/
                }
            }

            @Override
            public void onFailure(@NotNull Call<LinkedinEmailPojo> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void updateLocation(HashMap<String, String> param, IResponseCallback<BaseModel> callback) {
        Call<BaseModel> call = apiService.updateLocation(HeaderHelper.getInstance().getAuthTokenParam(), param);
        call.enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseModel> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void logout(IResponseCallback<BaseModel> callback) {
        Call<BaseModel> call = apiService.logout(HeaderHelper.getInstance().getAuthTokenParam());
        call.enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(@NotNull Call<BaseModel> call, @NotNull Response<BaseModel> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<BaseModel> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void getNotification(HashMap<String, String> param, IResponseCallback<NotificationPojo> callback) {
        Call<NotificationPojo> call = apiService.getNotification(HeaderHelper.getInstance().getAuthTokenParam(), param);
        call.enqueue(new Callback<NotificationPojo>() {
            @Override
            public void onResponse(@NotNull Call<NotificationPojo> call, @NotNull Response<NotificationPojo> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<NotificationPojo> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void OTPVerification(HashMap<String, String> param, IResponseCallback<UserPojo> callback) {
        Call<UserPojo> call = apiService.OTPVerification(param);
        call.enqueue(new Callback<UserPojo>() {
            @Override
            public void onResponse(@NotNull Call<UserPojo> call, @NotNull Response<UserPojo> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<UserPojo> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void resendOTP(HashMap<String, String> param, IResponseCallback<BaseModel> callback) {
        Call<BaseModel> call = apiService.resendOTP(param);
        call.enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(@NotNull Call<BaseModel> call, @NotNull Response<BaseModel> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<BaseModel> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void forgotPassword(HashMap<String, String> param, IResponseCallback<UserPojo> callback) {
        Call<UserPojo> call = apiService.forgotPassword(param);
        call.enqueue(new Callback<UserPojo>() {
            @Override
            public void onResponse(@NotNull Call<UserPojo> call, @NotNull Response<UserPojo> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<UserPojo> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void resetPassword(HashMap<String, String> param, IResponseCallback<UserPojo> callback) {
        Call<UserPojo> call = apiService.resetPassword(param);
        call.enqueue(new Callback<UserPojo>() {
            @Override
            public void onResponse(@NotNull Call<UserPojo> call, @NotNull Response<UserPojo> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<UserPojo> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void userProfile(IResponseCallback<UserPojo> callback) {
        Call<UserPojo> call = apiService.userProfile(HeaderHelper.getInstance().getAuthTokenParam());
        call.enqueue(new Callback<UserPojo>() {
            @Override
            public void onResponse(@NotNull Call<UserPojo> call, @NotNull Response<UserPojo> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<UserPojo> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void getProfile(IResponseCallback<UserPojo> callback) {
        Call<UserPojo> call = apiService.getProfile(HeaderHelper.getInstance().getAuthTokenParam());
        call.enqueue(new Callback<UserPojo>() {
            @Override
            public void onResponse(@NotNull Call<UserPojo> call, @NotNull Response<UserPojo> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<UserPojo> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void updateTowtruck(HashMap<String, String> param, IResponseCallback<BaseModel> callback) {
        Call<BaseModel> call = apiService.updateTowtruck(HeaderHelper.getInstance().getAuthTokenParam(), param);
        call.enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(@NotNull Call<BaseModel> call, @NotNull Response<BaseModel> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<BaseModel> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void updateFlatebed(HashMap<String, String> param, IResponseCallback<BaseModel> callback) {
        Call<BaseModel> call = apiService.updateFlatebed(HeaderHelper.getInstance().getAuthTokenParam(), param);
        call.enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(@NotNull Call<BaseModel> call, @NotNull Response<BaseModel> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<BaseModel> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void getInviteCode(IResponseCallback<InvitePojo> callback) {
        Call<InvitePojo> call = apiService.getInviteCode(HeaderHelper.getInstance().getAuthTokenParam());
        call.enqueue(new Callback<InvitePojo>() {
            @Override
            public void onResponse(@NotNull Call<InvitePojo> call, @NotNull Response<InvitePojo> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<InvitePojo> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void bookingList(List<Integer> status, HashMap<String, String> param, IResponseCallback<BookingListPojo> callback) {
        Call<BookingListPojo> call = apiService.bookingList(status, param, HeaderHelper.getInstance().getAuthTokenParam());
        call.enqueue(new Callback<BookingListPojo>() {
            @Override
            public void onResponse(@NotNull Call<BookingListPojo> call, @NotNull Response<BookingListPojo> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<BookingListPojo> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void upload(MultipartBody.Part file, IResponseCallback<UploadPojo> callback) {
        Call<UploadPojo> call = apiService.upload(file, HeaderHelper.getInstance().getAuthTokenParam());
        call.enqueue(new Callback<UploadPojo>() {
            @Override
            public void onResponse(@NotNull Call<UploadPojo> call, @NotNull Response<UploadPojo> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<UploadPojo> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void getCalendar(HashMap<String, String> param, IResponseCallback<CalendarPojo> callback) {
        Call<CalendarPojo> call = apiService.getCalendar(HeaderHelper.getInstance().getAuthTokenParam(), param);
        call.enqueue(new Callback<CalendarPojo>() {
            @Override
            public void onResponse(@NotNull Call<CalendarPojo> call, @NotNull Response<CalendarPojo> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<CalendarPojo> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void getBalance(IResponseCallback<Balance> callback) {
        Call<Balance> call = apiServiceStripe.balance(HeaderHelper.getInstance().getAuthTokenParam());
        call.enqueue(new Callback<Balance>() {
            @Override
            public void onResponse(@NotNull Call<Balance> call, @NotNull Response<Balance> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<Balance> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void transfer(HashMap<String, String> param, IResponseCallback<BaseModel> callback) {
        Call<BaseModel> call = apiService.transfer(HeaderHelper.getInstance().getAuthTokenParam(), param);
        call.enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(@NotNull Call<BaseModel> call, @NotNull Response<BaseModel> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<BaseModel> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void changeTowType(HashMap<String, String> param, IResponseCallback<BaseModel> callback) {
        Call<BaseModel> call = apiService.changeTowType(HeaderHelper.getInstance().getAuthTokenParam(), param);
        call.enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(@NotNull Call<BaseModel> call, @NotNull Response<BaseModel> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<BaseModel> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void changeOnlineStatus(HashMap<String, String> param, IResponseCallback<OnOffResponse> callback) {
        Call<OnOffResponse> call = apiService.changeOnlineStatus(HeaderHelper.getInstance().getAuthTokenParam(), param);
        call.enqueue(new Callback<OnOffResponse>() {
            @Override
            public void onResponse(@NotNull Call<OnOffResponse> call, @NotNull Response<OnOffResponse> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<OnOffResponse> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void getBrands(IResponseCallback<BrandPojo> callback, String categoryId) {
        Call<BrandPojo> call = apiService.getBrandList(categoryId);
        call.enqueue(new Callback<BrandPojo>() {
            @Override
            public void onResponse(@NotNull Call<BrandPojo> call, @NotNull Response<BrandPojo> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<BrandPojo> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void getTransactionHistory(HashMap<String, String> param, IResponseCallback<List<TransactionData>> callback) {
        Call<List<TransactionData>> call = apiServiceStripe.getTransactionHistory(HeaderHelper.getInstance().getAuthTokenParam(), param);
        call.enqueue(new Callback<List<TransactionData>>() {
            @Override
            public void onResponse(@NotNull Call<List<TransactionData>> call, @NotNull Response<List<TransactionData>> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<List<TransactionData>> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void refreshDevice(HashMap<String, String> param, IResponseCallback<BaseModel> callback) {
        Call<BaseModel> call = apiService.refreshDevice(HeaderHelper.getInstance().getAuthTokenParam(), param);
        call.enqueue(new Callback<BaseModel>() {
            @Override
            public void onResponse(Call<BaseModel> call, Response<BaseModel> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseModel> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void acceptMalaysia(String param, IResponseCallback<ResponseBody> callback) {
        Call<ResponseBody> call = apiService.acceptMalaysia(HeaderHelper.getInstance().getAuthTokenParam(), param);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void acceptSG(String param, IResponseCallback<ResponseBody> callback) {
        Call<ResponseBody> call = apiService.acceptSG(HeaderHelper.getInstance().getAuthTokenParam(), param);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void rejectMalaysia(String param, IResponseCallback<ResponseBody> callback) {
        Call<ResponseBody> call = apiService.rejectMalaysia(HeaderHelper.getInstance().getAuthTokenParam(), param);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void rejectSG(String param, IResponseCallback<ResponseBody> callback) {
        Call<ResponseBody> call = apiService.rejectSG(HeaderHelper.getInstance().getAuthTokenParam(), param);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void cancelMalaysia(String param, IResponseCallback<ResponseBody> callback) {
        Call<ResponseBody> call = apiService.cancelMalaysia(HeaderHelper.getInstance().getAuthTokenParam(), param);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void cancelSG(String param, IResponseCallback<ResponseBody> callback) {
        Call<ResponseBody> call = apiService.cancelSG(HeaderHelper.getInstance().getAuthTokenParam(), param);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void startMalaysia(String param, HashMap<String, String> params, IResponseCallback<ResponseBody> callback) {
        Call<ResponseBody> call = apiService.startMalaysia(HeaderHelper.getInstance().getAuthTokenParam(), param, params);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void startSG(String param, HashMap<String, String> params, IResponseCallback<ResponseBody> callback) {
        Call<ResponseBody> call = apiService.startSG(HeaderHelper.getInstance().getAuthTokenParam(), param, params);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void completeMalaysia(String param, HashMap<String, String> params, IResponseCallback<ResponseBody> callback) {
        Call<ResponseBody> call = apiService.completeMalaysia(HeaderHelper.getInstance().getAuthTokenParam(), param, params);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void completeSG(String param, HashMap<String, String> params, IResponseCallback<ResponseBody> callback) {
        Call<ResponseBody> call = apiService.completeSG(HeaderHelper.getInstance().getAuthTokenParam(), param, params);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

    public void getConfiguration(IResponseCallback<ConfigPojo> callback) {
        Call<ConfigPojo> call = apiService.getConfiguration();
        call.enqueue(new Callback<ConfigPojo>() {
            @Override
            public void onResponse(Call<ConfigPojo> call, Response<ConfigPojo> response) {
                if (response.isSuccessful()) {
                    callback.success(response.body());
                } else {
                    BaseModel baseModel = null;
                    try {
                        baseModel = ParsingHelper.getInstance().baseModelParsing(response.errorBody().string());
                        if (baseModel != null)
                            callback.onFailure(baseModel);
                        else
                            callback.onError(call, new Throwable());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ConfigPojo> call, Throwable t) {
                t.printStackTrace();
                callback.onError(call, t);
            }
        });
    }

}
