package sg.com.towme.driver.socket;

/**
 * Created by Dhara Golakiya (email:dharagolakiya4@gmail.com) on 03/July/2020
 */

public class SocketConstant {

//    public static final String URL = "http://172.105.35.50:3400";
//    public static final String URL = "http://devapi.towme.com.sg";
    public static final String URL = "https://devapi.towme.com.sg";
//    public static final String URL = "https://api.towme.com.sg";
    public static final String CONNECT = "connect";
    public static final String USER_ID = "user_id";
    public static final String USER_TYPE = "user_type";
    public static final String LOGIN = "login";
    public static final String LOGOUT = "logout";
    public static final String BOOKING_ID = "booking_id";
    public static final String MESSAGES = "messages";
    public static final String GET_MESSAGE = "get_message";
    public static final String SENDER_ID = "sender_id";
    public static final String MESSAGE = "message";
    public static final String TYPE = "type";
    public static final String TEXT = "text";
    public static final String IMAGE = "image";
    public static final String UPDATE_LOCATION = "update_location";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String DRIVER_BOOKING_INVITES = "driver_booking_invites";
    public static final String DRIVER_ID = "driver_id";
    public static final String COMPANY_CODE = "company_code";
    public static final String ONLINE_OFFLINE_DRIVERS = "online_offline_drivers";
    public static final String SERVICE_ID = "service_id";
    public static final String ONLINE_OFFLINE_STATUS = "online_offline_status";
    public static final String GET_BOOKING_INVITES = "get_booking_invites";
    public static final String LISTEN_CANCEL_BOOKING = "listen_cancel_booking";
    public static final String DELETE_BOOKING_INVITES = "delete_booking_invites";
    public static final String BOOKING_ACTION_BY_DRIVER = "booking_action_by_driver";
    public static final String BOOKING_STATUS = "booking_status";
    public static final String booking = "booking";
    public static final String distance_matrix_res = "distance_matrix_res";
    public static final String START_RIDE = "start_ride";
    public static final String IMAGES = "images";
    public static final String START_RIDE_DESCRIPTION = "start_ride_description";
    public static final String END_RIDE_DESCRIPTION = "end_ride_description";
    public static final String CANCEL_BOOKING = "cancel_booking";
    public static final String CANCEL_BY = "cancel_by";
    public static final String STOP_RIDE = "stop_ride";
    public static final String END_RIDE_EXTRA_CHARGE = "end_ride_extra_charge";
    public static final String DESC = "desc";
    public static final String PRICE = "price";
    public static final String BOOKING_CHARGE = "booking_charge";
    public static final String SERVICE_CHARGE = "service_charge";
    public static final String TOTAL_COST = "total_cost";
    public static final String LIMIT = "limit";
    public static final String PAGE = "page";
    public static final String RECEIVER_ID = "receiver_id";
    public static final String TOKEN = "token";
    public static final String GET_DRIVER_BOOKING = "get_driver_booking";
    public static final String GET_AVAILABLE_JOBS = "get_available_jobs";
    public static final String UPCOMING_BOOKING_REQUEST = "upcoming_booking_request";
    public static final String user_type = "user_type";
    public static final String start_ride = "start_ride";
    public static final String end_ride = "end_ride";
    public static final String JOB_NOT_PERFORMED_BY_DRIVER = "job_not_performed_by_driver";
    public static final String GET_CHAT_LIST_BOOKINGS = "get_chat_list_bookings";
    public static final String DISTANCE = "distance";

}
