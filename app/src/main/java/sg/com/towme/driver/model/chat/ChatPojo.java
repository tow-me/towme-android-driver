package sg.com.towme.driver.model.chat;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dhara Golakiya (email:dharagolakiya4@gmail.com) on 25/July/2020
 */
public class ChatPojo {
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("pages")
    @Expose
    private Integer pages;
    @SerializedName("current_page")
    @Expose
    private Integer currentPage;
    @SerializedName("row")
    @Expose
    private List<Chat> row = null;

    public Integer getCount() {
        return count;
    }

    public Integer getPages() {
        return pages;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public List<Chat> getRow() {
        if (row == null)
            return new ArrayList<Chat>();
        else
            return row;
    }

    public void setRow(List<Chat> row) {
        this.row = row;
    }
}
