
package sg.com.towme.driver.model.linkedin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LastName {

    @SerializedName("localized")
    @Expose
    private Localized_ localized;
    @SerializedName("preferredLocale")
    @Expose
    private PreferredLocale_ preferredLocale;

    public Localized_ getLocalized() {
        return localized;
    }

    public void setLocalized(Localized_ localized) {
        this.localized = localized;
    }

    public PreferredLocale_ getPreferredLocale() {
        return preferredLocale;
    }

    public void setPreferredLocale(PreferredLocale_ preferredLocale) {
        this.preferredLocale = preferredLocale;
    }

}
