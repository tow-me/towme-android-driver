package sg.com.towme.driver.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;

import com.applandeo.materialcalendarview.CalendarView;

import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.listeners.OnCalendarPageChangeListener;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;
import sg.com.towme.driver.R;
import sg.com.towme.driver.enumeration.Screens;
import sg.com.towme.driver.genericbottomsheet.GenericBottomModel;
import sg.com.towme.driver.genericbottomsheet.GenericBottomSheetDialog;
import sg.com.towme.driver.listener.iPageActive;
import sg.com.towme.driver.model.BaseModel;
import sg.com.towme.driver.model.calendar.CalendarPojo;
import sg.com.towme.driver.network.IResponseCallback;
import sg.com.towme.driver.network.NetworkCall;
import sg.com.towme.driver.network.Parameter;
import sg.com.towme.driver.utility.AppHelper;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;


public class CalenderFragmentTwo extends BaseFragment implements iPageActive {
    @BindView(R.id.calendarView)
    CalendarView calendarView;
    @BindView(R.id.llDetails)
    LinearLayout llDetails;
    @BindView(R.id.imgDown)
    AppCompatImageView imgDown;
    @BindView(R.id.txtMonthName)
    AppCompatTextView txtMonthName;
    @BindView(R.id.imgCollapse)
    AppCompatImageView imgCollapse;
    @BindView(R.id.txt_total_rides)
    AppCompatTextView txtTotalRides;
    @BindView(R.id.txt_total_commission)
    AppCompatTextView txtTotalCommission;
    @BindView(R.id.txt_average)
    AppCompatTextView txtAverage;
    String totalRides;
    String totalCommission;
    String avgPerDay;
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    @BindView(R.id.imgFilter)
    AppCompatImageView imgFilter;


    String firstDate = "";
    String secondDate = "";
    String towType = "";
    Date lastDate;
    @BindView(R.id.txtFilter)
    AppCompatTextView txtFilter;

    public static CalenderFragmentTwo newInstance() {
        CalenderFragmentTwo fragment = new CalenderFragmentTwo();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_calender_two, container, false);
        ButterKnife.bind(this, view);
        initUI();
//        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Singapore"));

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        callGetCalendarAPI(firstDate, secondDate, lastDate, towType);
//        calendarView.clearSelection();
    }

    private void initUI() {

        calendarView.setHeaderColor(Color.parseColor("#46aa50"));

        calendarView.setHeaderLabelColor(Color.WHITE);

        calendarView.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {
                Log.e("onDateSelected", eventDay.getCalendar().getTime().toString() + "...");
//                API FOrmate : "yyyy-MM-dd"
                String apiDate = eventDay.getCalendar().get(Calendar.YEAR) + "-" + String.format("%02d", (eventDay.getCalendar().get(Calendar.MONTH)) + 1) + "-" + String.format("%02d", eventDay.getCalendar().get(Calendar.DAY_OF_MONTH));
                Log.e("Selected date", apiDate);

                homeActivity.openHistoryActivity(Screens.CALENDER, apiDate);
            }
        });

        calendarView.setOnForwardPageChangeListener(new OnCalendarPageChangeListener() {
            @Override
            public void onChange() {
                Log.e("forword", "page cange::" + calendarView.getCurrentPageDate().getTime());

                Calendar cal3 = Calendar.getInstance();

                Date convertedDate = null;
                try {
                    Calendar calendarForword = calendarView.getCurrentPageDate();
//                    calendarForword.add(Calendar.MONTH, 1);
                    convertedDate = dateFormat.parse(calendarForword.get(Calendar.YEAR) + "-" + (calendarForword.get(Calendar.MONTH) + 1) + "-" + calendarForword.get(Calendar.DAY_OF_MONTH));
                    Log.e("Converted date", convertedDate + "...");
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                cal3.setTime(convertedDate);
                cal3.set(Calendar.DAY_OF_MONTH, 1);
                firstDate = dateFormat.format(cal3.getTime());
                cal3.set(Calendar.DAY_OF_MONTH, cal3.getActualMaximum(Calendar.DAY_OF_MONTH));
                secondDate = dateFormat.format(cal3.getTime());
                lastDate = cal3.getTime();
                callGetCalendarAPI(firstDate, secondDate, lastDate, towType);

            }
        });

        calendarView.setOnPreviousPageChangeListener(new OnCalendarPageChangeListener() {
            @Override
            public void onChange() {
                Log.e("previus", "page cange::" + calendarView.getCurrentPageDate().getTime());

                Calendar cal3 = Calendar.getInstance();

                Date convertedDate = null;
                try {
                    Calendar calendarPrev = calendarView.getCurrentPageDate();
//                    calendarPrev.add(Calendar.MONTH, -1);
                    convertedDate = dateFormat.parse(calendarPrev.get(Calendar.YEAR) + "-" + (calendarPrev.get(Calendar.MONTH) + 1) + "-" + calendarPrev.get(Calendar.DAY_OF_MONTH));
                    Log.e("Converted date", convertedDate + "...");
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                cal3.setTime(convertedDate);
                cal3.set(Calendar.DAY_OF_MONTH, 1);
                firstDate = dateFormat.format(cal3.getTime());
                cal3.set(Calendar.DAY_OF_MONTH, cal3.getActualMaximum(Calendar.DAY_OF_MONTH));
                secondDate = dateFormat.format(cal3.getTime());
                lastDate = cal3.getTime();
                callGetCalendarAPI(firstDate, secondDate, lastDate, towType);
            }
        });


        Calendar cal3 = Calendar.getInstance();
        cal3.set(Calendar.DAY_OF_MONTH, 1);
        firstDate = dateFormat.format(cal3.getTime());
        cal3.set(Calendar.DAY_OF_MONTH, cal3.getActualMaximum(Calendar.DAY_OF_MONTH));
        secondDate = dateFormat.format(cal3.getTime());
        lastDate = cal3.getTime();

        callGetCalendarAPI(firstDate, secondDate, lastDate, towType);

    }

    private void callGetCalendarAPI(String firstDate, String secondDate, Date lastDate, String towType) {

        NetworkCall.getInstance().getCalendar(getParam(firstDate, secondDate, towType), new IResponseCallback<CalendarPojo>() {
            @Override
            public void success(CalendarPojo data) {
                homeActivity.hideProgressDialog();
                if (data.getCode() == 1) {
                    try {
                        Calendar cal3 = Calendar.getInstance();
                        String todayDateString = dateFormat.format(cal3.getTime());
                        Date todayDate = dateFormat.parse(todayDateString);

                        double totalCommission = 0.0;
                        int totalTrip = 0;

                        List<EventDay> events = new ArrayList<>();

                        Log.e("Calender size", data.getData().size() + "...");
                        for (int i = 0; i < data.getData().size(); i++) {

//                            2020-11-04 17:24:33

                            Date secondDate = dateFormat.parse(data.getData().get(i).getBookingDate());
//                            Date secondDate = DateTimeUtil.convertDateFormate(data.getData().get(i).getBookingDate(),"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd");

                            if (data.getData().get(i).getBookingStatus() == 3 || data.getData().get(i).getBookingStatus() == 7) {
                                totalCommission = totalCommission + data.getData().get(i).getTowAmount();
                                totalTrip++;
                                Log.e("pink date", secondDate.toString() + "");
                                cal3.setTime(secondDate);
                                Calendar calendar = Calendar.getInstance();
                                calendar.setTime(secondDate);
                                events.add(new EventDay(calendar, R.drawable.pink_dot, Color.parseColor("#E17382")));
                            } else if (data.getData().get(i).getBookingStatus() == 1 ||
                                    data.getData().get(i).getBookingStatus() == 2 ||
                                    data.getData().get(i).getBookingStatus() == 5 ||
                                    data.getData().get(i).getBookingStatus() == 8) {
                                Log.e("Green date", secondDate.toString() + "");
                                cal3.setTime(secondDate);
                                Calendar calendar = Calendar.getInstance();
                                calendar.setTime(secondDate);
                                events.add(new EventDay(calendar, R.drawable.green_dot, Color.parseColor("#46AA50")));
                            }

                        }

                        Calendar calCurrent = Calendar.getInstance();
                        events.add(new EventDay(calCurrent, R.drawable.blue_dot, Color.parseColor("#1877F2")));

                        calendarView.setEvents(events);
                        String curr = "SGD";
                        if (data.getCountry().contains("MY")) {
                            curr = "RM";
                        }
                        txtTotalCommission.setText(curr + AppHelper.getInstance().formatCurrency(data.getEarning()));

                        DateFormat dateFormatForOnlyDay = new SimpleDateFormat("dd");
                        String day = dateFormatForOnlyDay.format(lastDate);
                        double avg = totalCommission / totalTrip;
                        if (avg > 0)
                            txtAverage.setText("$" + String.format("%.2f", avg));
                        else
                            txtAverage.setText("$0.0");
                        txtTotalRides.setText(String.valueOf(totalTrip));

                        DateFormat dateFormat = new SimpleDateFormat("MMMM yyyy");
                        String monthofday = dateFormat.format(lastDate);
                        txtMonthName.setText(monthofday);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    showSnackBar(data.getMessage());
                }
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                homeActivity.hideProgressDialog();
                showSnackBar(baseModel.getMessage());
            }

            @Override
            public void onError(Call<CalendarPojo> responseCall, Throwable T) {
                homeActivity.hideProgressDialog();
                showSnackBar(getString(R.string.error_message));
            }
        });
    }

    private HashMap<String, String> getParam(String firstDate, String secondDate, String towType) {
        HashMap<String, String> param = new HashMap<>();
        param.put(Parameter.userid, String.valueOf(AppHelper.getInstance().getUserDetails().getUserid()));
        param.put(Parameter.date_from, firstDate);
        param.put(Parameter.date_to, secondDate);
        if (towType.length() > 0)
            param.put(Parameter.booking_type, towType);
        return param;
    }

    @OnClick({R.id.imgCollapse, R.id.imgDown, R.id.imgFilter})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgCollapse:
                llDetails.setVisibility(View.GONE);
                break;
            case R.id.imgDown:
                llDetails.setVisibility(View.VISIBLE);
                break;
            case R.id.imgFilter:
                openBottomSheetDialog(getString(R.string.filter));
                break;
        }
    }

    @Override
    public void onPageActive(String str) {

    }


    private void openBottomSheetDialog(String header) {
        List<GenericBottomModel> modelList = new ArrayList<>();
        List<String> listStatusItem = Arrays.asList(getResources().getStringArray(R.array.towType));
        for (int i = 0; i < listStatusItem.size(); i++) {
            GenericBottomModel model = new GenericBottomModel();
            model.setId(i + "");
            model.setItemText(listStatusItem.get(i));
            modelList.add(model);
        }
        homeActivity.openBottomSheet(header, modelList, new GenericBottomSheetDialog.RecyclerItemClick() {
            @Override
            public void onItemClick(GenericBottomModel genericBottomModel) {
                if (genericBottomModel.getItemText().trim().equalsIgnoreCase("All")) {
                    txtFilter.setText(genericBottomModel.getItemText());
                    towType = "";
                } else {
                    txtFilter.setText(genericBottomModel.getItemText());
                    towType = genericBottomModel.getItemText().toLowerCase().trim();
                }
//                calendarView.clearSelection();
                homeActivity.showProgressDialog();
                callGetCalendarAPI(firstDate, secondDate, lastDate, towType);
            }
        });
    }
}
