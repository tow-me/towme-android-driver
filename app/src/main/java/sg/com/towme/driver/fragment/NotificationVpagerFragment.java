package sg.com.towme.driver.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import sg.com.towme.driver.R;
import sg.com.towme.driver.adapter.NotificationAdapter;
import sg.com.towme.driver.constant.AppConstant;
import sg.com.towme.driver.listener.CheckNetworkListener;
import sg.com.towme.driver.listener.iPageActive;
import sg.com.towme.driver.model.BaseModel;
import sg.com.towme.driver.model.notification.NotificationData;
import sg.com.towme.driver.model.notification.NotificationPojo;
import sg.com.towme.driver.network.IResponseCallback;
import sg.com.towme.driver.network.NetworkCall;
import sg.com.towme.driver.network.Parameter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

public class NotificationVpagerFragment extends BaseFragment implements iPageActive {


    NotificationAdapter adapter;
    List<NotificationData> listNotification;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.lblPreviousTrip)
    AppCompatTextView lblPreviousTrip;
    @BindView(R.id.rvPreviousTrip)
    RecyclerView rvPreviousTrip;

    public static NotificationVpagerFragment newInstance() {
        NotificationVpagerFragment fragment = new NotificationVpagerFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_messages, container, false);
        ButterKnife.bind(this, view);
        initUI();

        onPageActive("");

        return view;
    }


    @Override
    public void onPageActive(String str) {
        if (listNotification == null) {
            setAdapter();
            CheckNetworkListener callback = () -> {
                appNavigationActivity.showProgressDialog();
                callGetNotificationAPI();
            };

            if (appNavigationActivity.isNetworkAvailable(recyclerview, callback)) {
                callback.onRetryClick();
            }
        } else
            setAdapter();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void initUI() {
        rvPreviousTrip.setVisibility(View.GONE);
        lblPreviousTrip.setVisibility(View.GONE);
    }

    private void setAdapter() {
        if (listNotification == null)
            listNotification = new ArrayList<>();
        recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        if (adapter == null)
            adapter = new NotificationAdapter(getActivity(), listNotification);
        /*DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.line_horizontal));
        recyclerview.addItemDecoration(dividerItemDecoration);*/
       /* adapter.setCallback((view, position) -> {
            switch (view.getId()) {

            }

        });
*/
        recyclerview.setAdapter(adapter);
    }

    private void callGetNotificationAPI() {

        NetworkCall.getInstance().getNotification(getParam(), new IResponseCallback<NotificationPojo>() {
            @Override
            public void success(NotificationPojo data) {
                appNavigationActivity.hideProgressDialog();
                if (data.getCode() == 1) {
                    listNotification.clear();
                    listNotification.addAll(data.getData());
                    adapter.notifyDataSetChanged();
                } else {
                    showSnackBar(data.getMessage());
                }
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                appNavigationActivity.hideProgressDialog();
                showSnackBar(baseModel.getMessage());
            }

            @Override
            public void onError(Call<NotificationPojo> responseCall, Throwable T) {
                appNavigationActivity.hideProgressDialog();
                showSnackBar(getString(R.string.error_message));
            }
        });

    }

    private HashMap<String, String> getParam() {
        HashMap<String, String> param = new HashMap<>();
        param.put(Parameter.notification_for, AppConstant.USER_TYPE);
        return param;
    }

}
