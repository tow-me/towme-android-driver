
package sg.com.towme.driver.model.booking;


import sg.com.towme.driver.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BookingListPojo extends BaseModel {

    @SerializedName("data")
    @Expose
    private List<BookingData> data = null;

    public List<BookingData> getData() {
        return data;
    }

    public void setData(List<BookingData> data) {
        this.data = data;
    }

}
