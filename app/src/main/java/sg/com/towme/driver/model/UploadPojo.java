
package sg.com.towme.driver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UploadPojo extends BaseModel{

    @SerializedName("status_code")
    @Expose
    private Integer statusCode;

    @SerializedName("data")
    @Expose
    private List<String> data = null;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public List<String> getData() {
        return data;
    }

    public void setData(List<String> data) {
        this.data = data;
    }

}
