package sg.com.towme.driver.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Handler;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;

import sg.com.towme.driver.activity.AppNavigationActivity;
import sg.com.towme.driver.activity.HomeActivity;

/**
 * Created by vibhiksha on 21/2/20
 */
public class BaseDialog extends DialogFragment {

    private ConnectivityManager connectivityManager;
    protected AppNavigationActivity appNavigationActivity;

    protected HomeActivity homeActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof AppNavigationActivity) {
            appNavigationActivity = (AppNavigationActivity) context;
        }
        if (context instanceof HomeActivity) {
            homeActivity = (HomeActivity) context;
        }
    }

    public BaseDialog show(FragmentActivity activity) {
        if (activity.isFinishing() || activity.isDestroyed()) return null;
        try {
            new Handler().post(() -> showNow(activity.getSupportFragmentManager(), BaseDialog.class.getSimpleName()));
            return this;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean isShowing() {
        Dialog dialog = getDialog();
        if (dialog == null) return false;
        return dialog.isShowing() && !isRemoving();
    }
}
