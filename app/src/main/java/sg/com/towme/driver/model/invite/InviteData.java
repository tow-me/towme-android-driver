package sg.com.towme.driver.model.invite;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InviteData {

    @SerializedName("invitation_code")
    @Expose
    private String inviteCode;

    public String getInviteCode() {
        return inviteCode;
    }

    public void setInviteCode(String inviteCode) {
        this.inviteCode = inviteCode;
    }

}
