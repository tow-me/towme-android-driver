package sg.com.towme.driver.utility.media;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;

import com.google.android.material.bottomsheet.BottomSheetBehavior;

import sg.com.towme.driver.Controller;
import sg.com.towme.driver.R;
import sg.com.towme.driver.utility.BaseBottomSheet;
import sg.com.towme.driver.utility.Util;

import java.io.File;
import java.util.List;

import butterknife.ButterKnife;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by Dakshay Sanghvi
 */

public class BottomSheetFilePicker extends BaseBottomSheet implements View.OnClickListener {

    private static String[] PERMISSIONS = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private static final int REQUEST_PERMISSION = 101;

    private static final int VIDEO_LIMIT = 10;

    public static final int IMAGE = 1;
    public static final int VIDEO = 2;
    public static final int TEXT = 3;

    public static final int TAKE_PHOTO = 1;
    public static final int CHOOSE_IMAGE_FROM_GALLERY = 2;
    public static final int CROP_REQUEST = 3;
    public static final int TAKE_VIDEO = 4;
    public static final int CHOOSE_VIDEO_FROM_GALLERY = 5;
    public static final int TRIM_VIDEO = 6;
    public static final int PICK_IMAGE_VIDEO = 7;
    public static final int TAKE_ALL = 8;
    public static final int PICK_DOCUMENT = 9;
    public static final int PICK_CONTACT = 10;


    private Button btn_cancel, btn_take_image, btn_take_video;
    private Button btn_choose_image, btn_choose_video;

    private File file;
    private ImageView imageView;
    private int type = IMAGE;
    private int action = TAKE_PHOTO;

    private boolean direct_action = false;
    private MediaPickerCallback mediaPickerCallback;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.bottom_sheet_file_picker, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        mapping(view);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (direct_action) {
            BottomSheetBehavior bottomSheetBehavior = getBottomSheetBehaviour(getDialog());
            if (bottomSheetBehavior != null) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                bottomSheetBehavior.setPeekHeight(10);
                requestLayout();
            }
//            selectFile();
        }
    }

    private void mapping(View view) {
        btn_take_image = view.findViewById(R.id.btn_take_photo);
        btn_choose_image = view.findViewById(R.id.btn_choose_image);
        btn_take_video = view.findViewById(R.id.btn_take_video);
        btn_choose_video = view.findViewById(R.id.btn_choose_video);
        btn_cancel = view.findViewById(R.id.btn_edit_cancel);

       /* btn_take_image.setText(R.string.take_a_photo);
        btn_choose_image.setText(R.string.choose_image_from_gallery);
        btn_take_video.setText(R.string.take_a_video);
        btn_choose_video.setText(R.string.choose_video_from_gallery);*/
        btn_cancel.setText(R.string.cancel);


        btn_take_image.setOnClickListener(this);
        btn_choose_image.setOnClickListener(this);
        btn_take_video.setOnClickListener(this);
        btn_choose_video.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);


        if (type == TAKE_ALL) {
            btn_take_image.setVisibility(View.VISIBLE);
            btn_choose_image.setVisibility(View.VISIBLE);
            btn_take_video.setVisibility(View.VISIBLE);
            btn_choose_video.setVisibility(View.VISIBLE);
        } else if (type == PICK_IMAGE_VIDEO) {
            btn_take_image.setVisibility(View.GONE);
            btn_take_video.setVisibility(View.GONE);
            btn_choose_image.setVisibility(View.VISIBLE);
            btn_choose_video.setVisibility(View.VISIBLE);
        } else if (type == IMAGE) {
            btn_take_image.setVisibility(View.VISIBLE);
            btn_take_video.setVisibility(View.GONE);
            btn_choose_video.setVisibility(View.GONE);
        } else if (type == VIDEO) {
            btn_take_image.setVisibility(View.GONE);
            btn_choose_image.setVisibility(View.GONE);
            btn_take_video.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_edit_cancel:
                hideBottomSheet();
                break;
            case R.id.btn_take_photo:
                action = TAKE_PHOTO;
                selectFile();
                break;
            case R.id.btn_take_video:
                action = TAKE_VIDEO;
                selectFile();
                break;
            case R.id.btn_choose_image:
                action = CHOOSE_IMAGE_FROM_GALLERY;
                selectFile();
                break;
            case R.id.btn_choose_video:
                action = CHOOSE_VIDEO_FROM_GALLERY;
                selectFile();
                break;
        }
    }


    private boolean requestPermission() {
        if (EasyPermissions.hasPermissions(Controller.getInstance(), PERMISSIONS)) return true;
        EasyPermissions.requestPermissions(this, getString(R.string.permission_camera_and_storage_access), REQUEST_PERMISSION, PERMISSIONS);
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @AfterPermissionGranted(REQUEST_PERMISSION)
    private void selectFile() {
        if (!requestPermission()) return;

        if (action == TAKE_PHOTO || action == TAKE_VIDEO) {
            Intent intent;
            if (action == TAKE_PHOTO) {
                intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                file = FileUtil.createNewFile(context, MediaType.IMAGE);
            } else {
                intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                file = FileUtil.createNewFile(context, MediaType.VIDEO);
            }
            Uri uri = FileUtil.getURI(context, file);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            List<ResolveInfo> resInfoList = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
            for (ResolveInfo resolveInfo : resInfoList) {
                String packageName = resolveInfo.activityInfo.packageName;
                context.grantUriPermission(packageName, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }
            startActivityForResult(Intent.createChooser(intent, "Capture Using"), action);
        } else if (action == CHOOSE_IMAGE_FROM_GALLERY) {
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
            startActivityForResult(Intent.createChooser(intent, "Select Photo"), action);
        } else if (action == CHOOSE_VIDEO_FROM_GALLERY) {
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setDataAndType(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, "video/*");
            startActivityForResult(Intent.createChooser(intent, "Select Video"), action);
        } else if (action == PICK_DOCUMENT) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("*/*");
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
            startActivityForResult(Intent.createChooser(intent, "Pick File"), action);
        } else if (action == PICK_CONTACT) {
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
            startActivityForResult(intent, PICK_CONTACT);
        }
    }


    public static boolean isCorrectLimit(Context context, Uri uri) {
        try {
            MediaPlayer mp = MediaPlayer.create(context, uri);
            int duration = mp.getDuration();
            mp.release();
            return duration <= VIDEO_LIMIT;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            Log.i("onActivityResult ", "Request Code: " + requestCode + " Result Code: " + resultCode);
            if (direct_action) hideBottomSheet();
            return;
        }
        new Thread(() -> {
            showProgressBar(true);
            processActivityResult(requestCode, data);
            showProgressBar(false);
        }).start();
    }

    private void showProgressBar(boolean visible) {
        if (mediaPickerCallback == null) return;

        Util.executeOnMain(() -> mediaPickerCallback.showProgressBar(visible));
    }

    private void processActivityResult(int requestCode, Intent intent) {
        Media media = null;
        switch (requestCode) {
            case TAKE_PHOTO:
                if (file == null) return;
                media = Media.create(Thumb.generate(context, MediaType.IMAGE, file));
                if (imageView != null)
                    Glide.with(context).load(FileUtil.getURI(context, file)).into(imageView);
                break;
            case CHOOSE_IMAGE_FROM_GALLERY: {
//                file = FileUtil.getNewPath(context, intent.getLocks(), MediaType.IMAGE);
                file = FileUtil.getFileFromUri(context, intent.getData(), MediaType.IMAGE);
                if (file == null) return;
                media = Media.create(Thumb.generate(context, MediaType.IMAGE, file));
                if (imageView != null)
                    Glide.with(context).load(FileUtil.getURI(context, file)).into(imageView);
                break;
            }
            case TAKE_VIDEO:
                if (file != null)
                    media = Media.create(Thumb.generate(context, MediaType.VIDEO, file));
//                    if (isVideoTrimEnable) trimRequest(Uri.fromFile(file));
//                    else if (file != null) filePickerListener.onPicked(VIDEO, Uri.fromFile(file), file);
                break;
            case CHOOSE_VIDEO_FROM_GALLERY:
//              trimRequest(data.getUser());
//                file = FileUtil.getNewPath(context, intent.getLocks(), MediaType.VIDEO);
                file = FileUtil.getFileFromUri(context, intent.getData(), MediaType.VIDEO);
                if (file == null) return;
                Media mMedia = Media.create(Thumb.generate(context, MediaType.VIDEO, file));
                if (mMedia.isValid()) media = mMedia;
                break;
            case CROP_REQUEST:
                if (file == null) return;
                media = Media.create(Thumb.generate(context, MediaType.VIDEO, file));
                if (imageView != null)
                    Glide.with(context).load(FileUtil.getURI(context, file)).into(imageView);
                break;
            case PICK_DOCUMENT:
                if (intent.getData() == null) return;
                file = FileUtil.getFileFromUri(context, intent.getData(), MediaType.DOCUMENT);
                if (file == null) return;
                media = Media.create(Thumb.generate(MediaType.DOCUMENT, file));
                break;
            case PICK_CONTACT:
                if (intent.getData() == null) return;
//                media = Media.create(Ezvcard.write(readContactFromUri(context, intent.getLocks())).version(VCardVersion.V4_0).go());
        }

        if (mediaPickerCallback == null) return;
        final Media final_media = media;
        Util.executeOnMain(() -> {
            showProgressBar(false);
            if (final_media == null) mediaPickerCallback.onPickedError(null);
            else mediaPickerCallback.onPickedSuccess(final_media);
            hideBottomSheet();
        });
    }

    public static Uri getFileUri(File file) {
        return Uri.fromFile(file);
    }

    public static Uri getFileUri(String path) {
        return getFileUri(new File(path));
    }

    public void setMediaListenerCallback(int type, MediaPickerCallback mediaPickerCallback) {
        this.type = type;
        this.mediaPickerCallback = mediaPickerCallback;
    }

    public void setAction(int action) {
        this.action = action;
        direct_action = true;
    }
}
