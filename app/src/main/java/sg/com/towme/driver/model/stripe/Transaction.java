package sg.com.towme.driver.model.stripe;

import com.google.gson.annotations.SerializedName;
import sg.com.towme.driver.model.user.UserData;
import sg.com.towme.driver.utility.TextUtil;


public class Transaction {


    private static final String SUBSCRIPTION_PURCHASED = "subscription_purchase";
    private static final String PACKAGE_PURCHASED = "package_purchase";
    private static final String PACKAGE_PAYMENT = "package_payment";

    private String _id;
    private String type;
    @SerializedName("updatedAt")
    private String updated_at;
    @SerializedName("createdAt")
    private String created_at;
    @SerializedName(value = "stripe_response", alternate = {"stripe_intent_response"})
    private StripeResponse settlement;
    private String transaction_type;
    @SerializedName("operator_id")
    private UserData operator;
    private UserData from_user;


    public String get_id() {
        return _id;
    }

    public String getType() {
        return type;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public StripeResponse getSettlement() {
        return settlement;
    }

    public String getTransaction_type() {
        return transaction_type;
    }

    public UserData getOperator() {
        return operator;
    }

    public UserData getFrom_user() {
        return from_user;
    }

    public String getTitle() {
        if (transaction_type == null) return "";

        if (transaction_type.equalsIgnoreCase(SUBSCRIPTION_PURCHASED)) {
            return "Subscription Purchase";
        } else if (transaction_type.equalsIgnoreCase(PACKAGE_PURCHASED)) {
            return "Package Purchase";
        } else if (transaction_type.equalsIgnoreCase(PACKAGE_PAYMENT)) {
            return "Receive Payment";
        } else {
            return transaction_type;
        }
    }

    public boolean isDeducted() {
        if (type == null) return true;
        return !type.equalsIgnoreCase("credit");
    }

    public double getAmount() {
        StripeResponse settlement = getSettlement();
        if (settlement == null) return 0;
        return settlement.getTransactionAmount();
    }

    public String getCurrency() {
        StripeResponse settlement = getSettlement();
        if (settlement == null) return "";
        return settlement.getCurrency().toUpperCase();
    }

    public String getSummary() {
        StripeResponse settlement = getSettlement();
        if (settlement == null) return "";
        return hasError() ? settlement.getError().getMessage() : settlement.getDescription();
    }

    public boolean hasError() {
        return settlement != null && settlement.getError() != null;
    }

    public String getStatus() {
        StripeResponse settlement = getSettlement();
        if (settlement == null) return "";
        return settlement.getStatus();
    }

    public boolean hasReceiptUrl() {
        return settlement != null && !TextUtil.isNullOrEmpty(settlement.getReceiptUrl());
    }

    public String getReceiptUrl() {
        return hasReceiptUrl() ? settlement.getReceiptUrl() : "";
    }

}
