package sg.com.towme.driver.activity;

import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import sg.com.towme.driver.R;
import sg.com.towme.driver.adapter.ImageAdapter;
import sg.com.towme.driver.model.notification.NotificationData;

public class NotificationDetailActivity extends ToolBarActivity {

    @BindView(R.id.txtTitle)
    AppCompatTextView txtTitle;
    @BindView(R.id.txtDescription)
    AppCompatTextView txtDescription;
    @BindView(R.id.listImages)
    RecyclerView listImages;
    ImageAdapter adapter;
    List<String> images = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_notification_detail);
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);

        iniUI();
    }

    private void iniUI() {
        setHomeIcon(R.drawable.ic_back_white);
        setToolbarTitle("Notification");
        NotificationData notification = Parcels.unwrap(getIntent().getParcelableExtra(NotificationActivity.EXTRA_NOTIFICATION));
        if (notification != null) {
            if (notification.getImages() != null) {
                images = notification.getImages();
            }
            txtTitle.setText(notification.getNotificationTitle());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                txtDescription.setText(Html.fromHtml(notification.getNotificationDescription(), Html.FROM_HTML_MODE_COMPACT));
            } else {
                txtDescription.setText(Html.fromHtml(notification.getNotificationDescription()));
            }
        }
        setAdapter();
    }

    private void setAdapter() {
        listImages.setLayoutManager(new LinearLayoutManager(this));
        if (adapter == null)
            adapter = new ImageAdapter(this, images);
        listImages.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
//        menu.findItem(R.id.action_overflow).setVisible(false);
        return true;
    }
}