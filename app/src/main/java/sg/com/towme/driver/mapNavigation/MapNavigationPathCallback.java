package sg.com.towme.driver.mapNavigation;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;

public interface MapNavigationPathCallback {
    void onSuccessGetPolyLine(PolylineOptions lineOptions, ArrayList<LatLng> points);

    void onFailedToGetPolyLine();
}
