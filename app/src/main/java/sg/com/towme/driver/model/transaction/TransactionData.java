package sg.com.towme.driver.model.transaction;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TransactionData {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("payment_id")
    @Expose
    private String paymentId;
    @SerializedName("booking_id")
    @Expose
    private Integer bookingId;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("driver_id")
    @Expose
    private Integer driverId;
    @SerializedName("amount")
    @Expose
    private Double amount;
    @SerializedName("commission")
    @Expose
    private Double commission;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("payment_method_types")
    @Expose
    private Object paymentMethodTypes;
    @SerializedName("receipt_email")
    @Expose
    private Object receiptEmail;
    @SerializedName("payment_response")
    @Expose
    private String paymentResponse;
    @SerializedName("payment_status")
    @Expose
    private String paymentStatus;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("payment_type")
    @Expose
    private String paymentType;
    @SerializedName("user_cancellation")
    @Expose
    private int userCancellation;
    @SerializedName("is_redeemed")
    @Expose
    private int isRedeem;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public Integer getBookingId() {
        return bookingId;
    }

    public void setBookingId(Integer bookingId) {
        this.bookingId = bookingId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Object getPaymentMethodTypes() {
        return paymentMethodTypes;
    }

    public void setPaymentMethodTypes(Object paymentMethodTypes) {
        this.paymentMethodTypes = paymentMethodTypes;
    }

    public Object getReceiptEmail() {
        return receiptEmail;
    }

    public void setReceiptEmail(Object receiptEmail) {
        this.receiptEmail = receiptEmail;
    }

    public String getPaymentResponse() {
        return paymentResponse;
    }

    public void setPaymentResponse(String paymentResponse) {
        this.paymentResponse = paymentResponse;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public Double getCommission() {
        return commission;
    }

    public void setCommission(Double commission) {
        this.commission = commission;
    }

    public int getUserCancellation() {
        return userCancellation;
    }

    public void setUserCancellation(int userCancellation) {
        this.userCancellation = userCancellation;
    }

    public int getIsRedeem() {
        return isRedeem;
    }

    public void setIsRedeem(int isRedeem) {
        this.isRedeem = isRedeem;
    }
}
