package sg.com.towme.driver.model.stripe;

import java.util.List;

public class Balance {

    private List<Pending> pending;
    private boolean livemode;
    private List<Connect_reserved> connect_reserved;
    private List<Available> available;
    private String object;

    public List<Pending> getPending() {
        return pending;
    }

    public boolean getLivemode() {
        return livemode;
    }

    public List<Connect_reserved> getConnect_reserved() {
        return connect_reserved;
    }

    public List<Available> getAvailable() {
        return available;
    }

    public String getObject() {
        return object;
    }

    public static class Pending {
        private Source_types source_types;
        private String currency;
        private int amount;

        public Source_types getSource_types() {
            return source_types;
        }

        public String getCurrency() {
            return currency;
        }

        public int getAmount() {
            return amount;
        }
    }

    public static class Source_types {
        private int card;

        public int getCard() {
            return card;
        }
    }

    public static class Connect_reserved {
        private String currency;
        private int amount;

        public String getCurrency() {
            return currency;
        }

        public int getAmount() {
            return amount;
        }
    }

    public static class Available {
        private Source_types source_types;
        private String currency;
        private int amount;

        public Source_types getSource_types() {
            return source_types;
        }

        public String getCurrency() {
            return currency;
        }

        public int getAmount() {
            return amount;
        }
    }


    public double availableBalance() {
        double total = 0;
        if (available == null) return total;
        for (Available available : available) {
            total = total + available.amount;
        }
        return total / 100;
    }

    public double pendingBalance() {
        double total = 0;
        if (pending == null) return total;
        for (Pending pending : pending) {
            total = total + pending.amount;
        }
        return total / 100;
    }

    public double totalBalance() {
        return availableBalance() + pendingBalance();
    }

    public String getCurrency() {
        if (connect_reserved == null || connect_reserved.size() <= 0) return "SGD";
        return connect_reserved.get(0).getCurrency();
    }
}
