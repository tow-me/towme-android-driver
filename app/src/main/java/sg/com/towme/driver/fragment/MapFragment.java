package sg.com.towme.driver.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.WorkManager;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.PlaceLikelihood;
import com.google.android.libraries.places.api.net.FindCurrentPlaceRequest;
import com.google.android.libraries.places.api.net.FindCurrentPlaceResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.socket.client.Ack;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import okhttp3.ResponseBody;
import retrofit2.Call;
import sg.com.towme.driver.Controller;
import sg.com.towme.driver.R;
import sg.com.towme.driver.adapter.RequestAdapter;
import sg.com.towme.driver.adapter.UpcomingJobsAdapter;
import sg.com.towme.driver.constant.AppConstant;
import sg.com.towme.driver.enumeration.ChatType;
import sg.com.towme.driver.enumeration.NavigationType;
import sg.com.towme.driver.listener.iDialogButtonClick;
import sg.com.towme.driver.listener.iNavigationTypeListner;
import sg.com.towme.driver.listener.iPageActive;
import sg.com.towme.driver.mapNavigation.GPSTracker;
import sg.com.towme.driver.mapNavigation.iLocationChangeListner;
import sg.com.towme.driver.model.AbstractCallback;
import sg.com.towme.driver.model.BaseModel;
import sg.com.towme.driver.model.OnOffResponse;
import sg.com.towme.driver.model.OnOffStatus;
import sg.com.towme.driver.model.booking.BookingData;
import sg.com.towme.driver.model.booking.CreateBooking;
import sg.com.towme.driver.model.booking.User;
import sg.com.towme.driver.model.user.UserData;
import sg.com.towme.driver.network.IResponseCallback;
import sg.com.towme.driver.network.NetworkCall;
import sg.com.towme.driver.network.Parameter;
import sg.com.towme.driver.network.StripeCall;
import sg.com.towme.driver.payment.PaymentSessionHandler;
import sg.com.towme.driver.payment.StripeUtil;
import sg.com.towme.driver.socket.SocketConstant;
import sg.com.towme.driver.socket.SocketIOClient;
import sg.com.towme.driver.utility.AppHelper;
import sg.com.towme.driver.utility.DebugLog;
import sg.com.towme.driver.utility.PreferanceHelper;

import static com.facebook.FacebookSdk.getApplicationContext;


public class MapFragment extends GoogleMapFragment implements OnMapReadyCallback, iPageActive, iNavigationTypeListner, iLocationChangeListner/*, SensorEventListener */  /*,GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener */ {

    @BindView(R.id.edtCurrentLocation)
    AppCompatEditText edtCurrentLocation;
    @BindView(R.id.rlYourLocation)
    RelativeLayout rlYourLocation;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.chkSwitch)
    AppCompatCheckBox chkSwitch;
    RequestAdapter requestAdapter;
    Socket mSocket;
    List<CreateBooking> listBooking;
    @BindView(R.id.bottomSheetAcceptReject)
    FrameLayout bottomSheetAcceptReject;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.imgMessage)
    AppCompatImageView imgMessage;
    @BindView(R.id.imgCall)
    AppCompatImageView imgCall;
    @BindView(R.id.txtInfo)
    AppCompatTextView txtInfo;
    @BindView(R.id.txtEndob)
    AppCompatTextView txtEndob;
    @BindView(R.id.txtStartJob)
    AppCompatTextView txtStartJob;
    @BindView(R.id.txtCancelJob)
    AppCompatTextView txtCancelJob;
    @BindView(R.id.txtTitle)
    AppCompatTextView txtTitle;
    @BindView(R.id.txtSource)
    AppCompatTextView txtSource;
    @BindView(R.id.txtDestination)
    AppCompatTextView txtDestination;
    @BindView(R.id.txtPrice)
    AppCompatTextView txtPrice;
    @BindView(R.id.llConfirmedStarted)
    LinearLayout llConfirmedStarted;
    @BindView(R.id.flCurrentLocation)
    FrameLayout flCurrentLocation;
    @BindView(R.id.txtRecenter)
    AppCompatTextView txtRecenter;
    @BindView(R.id.txtPaymentType)
    AppCompatTextView txtPaymentType;
    @BindView(R.id.txtNavigation)
    AppCompatTextView txtNavigation;
    @BindView(R.id.imgBottomArrow)
    AppCompatImageView imgBottomArrow;
    @BindView(R.id.bottom_view)
    LinearLayout bottomView;
    @BindView(R.id.rvUpcomingJob)
    RecyclerView rvUpcomingJob;
    @BindView(R.id.bottom_sheetUpcomingJobs)
    RelativeLayout bottomSheetUpcomingJobs;
    List<CreateBooking> availableJobsList;
/*    private GoogleApiClient mGoogleApiClient;
    private boolean mRequestingLocationUpdates = false;
    private LocationRequest mLocationRequest;*/

    public static MapFragment newInstance() {
        MapFragment fragment = new MapFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        mSocket = SocketIOClient.getInstance();
//        Places.initialize(getApplicationContext(), getString(R.string.google_maps_key));
//        placesClient = Places.createClient(getActivity());
//        sensorManager = (SensorManager) getActivity().getSystemService(SENSOR_SERVICE);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        ButterKnife.bind(this, view);
/*
        if (getServicesAvailable()) {
            // Building the GoogleApi client
            buildGoogleApiClient();
            createLocationRequest();
        }*/

        gpsTracker = new GPSTracker(getActivity(), this);
//        changeCountry();
        initUI();
        initMap(this::onMapReady);
        return view;
    }

    private void changeCountry() {
        HashMap<String, String> param = new HashMap<>();
        String country = "SG";
        if (AppHelper.getInstance().getCountry().contains("Malaysia")) {
            country = "MY";
        }
        param.put("country", country);
        NetworkCall.getInstance().changeCountry(param, new IResponseCallback<BaseModel>() {
            @Override
            public void success(BaseModel data) {
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                showSnackBar(baseModel.getMessage());
            }

            @Override
            public void onError(Call<BaseModel> responseCall, Throwable T) {
                showSnackBar(getString(R.string.error_message));
            }
        });

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Log.e("onCreate", "Register all socket listener");
        mSocket.on(SocketConstant.GET_BOOKING_INVITES, onNewRequest);
        mSocket.on(SocketConstant.LISTEN_CANCEL_BOOKING, onCancelBooking);
        mSocket.on(SocketConstant.DELETE_BOOKING_INVITES, onDeleteBookingInvite);

      /*  if (requestAdapter == null || listBooking == null) {
            setAdapter();
            getBookingRequest();
        } else
            setAdapter();*/

       /* setAdapter();
        getBookingRequest();

        initBottomSheet();*/

    }

    private void initUI() {
        if (AppHelper.getInstance().getUserDetails().getOnlineOfflineStatus() != null)
            if (AppHelper.getInstance().getUserDetails().getOnlineOfflineStatus().equalsIgnoreCase("online"))
                chkSwitch.setChecked(true);
            else
                chkSwitch.setChecked(false);
//        changeOfflineAndOnlineStatus();
    }

    private void setAdapter() {
        if (listBooking == null)
            listBooking = new ArrayList<>();

        if (listBooking.size() <= 0)
            bottomSheetAcceptReject.setVisibility(View.GONE);
        else
            bottomSheetAcceptReject.setVisibility(View.VISIBLE);
        recyclerview.setLayoutManager(new LinearLayoutManager(getContext()));
        if (requestAdapter == null)
            requestAdapter = new RequestAdapter(getContext(), listBooking);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerview.getContext(), DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.line_horizontal));
        recyclerview.addItemDecoration(dividerItemDecoration);
        requestAdapter.setCallback((view, position, object) -> {
            // 0 = pending (default), 1 = accept booking, 2 = reject booking (Booking Status)
            switch (view.getId()) {
                case R.id.btnAccept:
//                    bookingActionByDriver(listBooking.get(position).getBookingId(), 1);

                    Log.e("Chargeable amount", listBooking.get(position).getChargeableAmount() + "...");
                    if (AppHelper.getInstance().getConfigDetails().getTowmeApplicationCommission() > 0) {
                        iDialogButtonClick callBack = new iDialogButtonClick() {
                            @Override
                            public void negativeClick() {
                            }

                            @Override
                            public void positiveClick(String s) {
                                startPaymentSession(listBooking.get(position));
                            }
                        };
                        homeActivity.openConfirmationDialog(callBack, getString(R.string.are_you_sure_you_want_to_pay_this_booking_request), getString(R.string.yes), getString(R.string.no));
                    } else {
                        if (listBooking.get(position).getCountry().contains("MY")) {
                            acceptBookingMalaysia(listBooking.get(position).getBookingId());
                        } else {
                            acceptBookingSG(listBooking.get(position).getBookingId());
//                            bookingActionByDriver(listBooking.get(position).getBookingId(), 1, null, null);
                        }
                    }
                    break;
                case R.id.btnReject:
//                    homeActivity.openEndJobctivity();
                    if (listBooking.get(position).getCountry().contains("MY")) {
                        rejectBookingMalaysia(listBooking.get(position).getBookingId());
                    } else {
                        rejectBookingSG(listBooking.get(position).getBookingId());
//                        bookingActionByDriver(listBooking.get(position).getBookingId(), 2, null, null);
                    }
                    break;
                case R.id.imgInfo:
//                    homeActivity.openEndJobctivity();
                    if (listBooking.get(position).getExtraNotes() == null || "".equals(listBooking.get(position).getExtraNotes())) {
                        appNavigationActivity.openBookingRemarkDialog("No booking remarks");
                    } else {
                        appNavigationActivity.openBookingRemarkDialog(listBooking.get(position).getExtraNotes());
                    }
                    break;
            }

        });

        recyclerview.setAdapter(requestAdapter);
    }

    private void acceptBookingMalaysia(int id) {
        homeActivity.showProgressDialog();
        NetworkCall.getInstance().acceptMalaysia(id+"", new IResponseCallback<ResponseBody>() {
            @Override
            public void success(ResponseBody data) {
                try {
                    JSONObject json = new JSONObject(data.string());
                    if (json.getBoolean("status")) {
                        bookingActionByDriver(id, 1, json.getJSONObject("data").getJSONObject("booking").toString(), json.getJSONObject("data").getJSONObject("distance_matrix_res").toString());
                    } else {
                        homeActivity.hideProgressDialog();
                        showSnackBar(json.getString("message"));
                    }
                } catch (Exception e) {}
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                homeActivity.hideProgressDialog();
                showSnackBar(baseModel.getMessage());
            }

            @Override
            public void onError(Call<ResponseBody> responseCall, Throwable T) {
                homeActivity.hideProgressDialog();
                showSnackBar(getString(R.string.error_message));
            }
        });
    }

    private void acceptBookingSG(int id) {
        homeActivity.showProgressDialog();
        NetworkCall.getInstance().acceptSG(id+"", new IResponseCallback<ResponseBody>() {
            @Override
            public void success(ResponseBody data) {
                try {
                    JSONObject json = new JSONObject(data.string());
                    if (json.getBoolean("status")) {
                        bookingActionByDriver(id, 1, json.getJSONObject("data").getJSONObject("booking").toString(), json.getJSONObject("data").getJSONObject("distance_matrix_res").toString());
                    } else {
                        homeActivity.hideProgressDialog();
                        showSnackBar(json.getString("message"));
                    }
                } catch (Exception e) {}
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                homeActivity.hideProgressDialog();
                showSnackBar(baseModel.getMessage());
            }

            @Override
            public void onError(Call<ResponseBody> responseCall, Throwable T) {
                homeActivity.hideProgressDialog();
                showSnackBar(getString(R.string.error_message));
            }
        });
    }

    private void rejectBookingMalaysia(int id) {
        homeActivity.showProgressDialog();
        NetworkCall.getInstance().rejectMalaysia(id+"", new IResponseCallback<ResponseBody>() {
            @Override
            public void success(ResponseBody data) {
                try {
                    JSONObject json = new JSONObject(data.string());
                    homeActivity.hideProgressDialog();
                    if (json.getBoolean("status")) {
                        removeRequestFromList(id);
                        getBookingRequest();
                        getAvailableJobsByDriver();
                    } else {
                        showSnackBar(json.getString("message"));
                    }
                } catch (Exception e) {}
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                homeActivity.hideProgressDialog();
                showSnackBar(baseModel.getMessage());
            }

            @Override
            public void onError(Call<ResponseBody> responseCall, Throwable T) {
                homeActivity.hideProgressDialog();
                showSnackBar(getString(R.string.error_message));
            }
        });
    }

    private void rejectBookingSG(int id) {
        homeActivity.showProgressDialog();
        NetworkCall.getInstance().rejectSG(id+"", new IResponseCallback<ResponseBody>() {
            @Override
            public void success(ResponseBody data) {
                try {
                    JSONObject json = new JSONObject(data.string());
                    homeActivity.hideProgressDialog();
                    if (json.getBoolean("status")) {
                        removeRequestFromList(id);
                        getBookingRequest();
                        getAvailableJobsByDriver();
                    } else {
                        showSnackBar(json.getString("message"));
                    }
                } catch (Exception e) {}
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                homeActivity.hideProgressDialog();
                showSnackBar(baseModel.getMessage());
            }

            @Override
            public void onError(Call<ResponseBody> responseCall, Throwable T) {
                homeActivity.hideProgressDialog();
                showSnackBar(getString(R.string.error_message));
            }
        });
    }

    private void startPaymentSession(CreateBooking createBooking) {
        PaymentSessionHandler paymentSessionHandler = StripeUtil.getPaymentSessionHandler(homeActivity);
        paymentSessionHandler.setType(PaymentSessionHandler.TYPE_ORDER);
        paymentSessionHandler.setTitle("Towme booking charges");
        paymentSessionHandler.setDescription("Order payment");
        paymentSessionHandler.setBookingDetails(String.valueOf(createBooking.getBookingId()), String.valueOf(AppHelper.getInstance().getUserDetails().getUserid()), String.valueOf(AppHelper.getInstance().getUserDetails().getUserid()));
        paymentSessionHandler.setDestination_stripe_account_id("");
        Log.e("Chargeable amount", "startPaymentSession..." + createBooking.getChargeableAmount() + "...");
        paymentSessionHandler.initTransaction(createBooking.getChargeableAmount());
        paymentSessionHandler.setPaymentSessionListener(new PaymentSessionHandler.PaymentSessionListener() {
            @Override
            public void onPaymentSuccess(String payment_intent_id, boolean captured) {
                Log.e("PaymentSessionHandler", "Success : " + payment_intent_id + " Captured - " + captured);

                callPaymentApi(payment_intent_id, createBooking);


            }

            @Override
            public void onPaymentFailed(String message) {
                Log.e("PaymentSessionHandler", message);
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void callPaymentApi(String payment_intent_id, CreateBooking createBooking) {
        homeActivity.showProgressDialog();
        HashMap<String, Object> param = new HashMap<>();
        param.put("intent_id", payment_intent_id);
        param.put("amount", "0");

        StripeCall.getInstance().captureIntent(param, new AbstractCallback<ResponseBody>() {
            @Override
            public void result(ResponseBody result) {
                homeActivity.hideProgressDialog();
                Log.e("Payment done", "...");
                if (AppHelper.getInstance().getCountry().contains("Malaysia")) {
                    acceptBookingMalaysia(createBooking.getBookingId());
                } else {
                    acceptBookingSG(createBooking.getBookingId());
//                    bookingActionByDriver(createBooking.getBookingId(), 1, null, null);
                }
                /*try {
                    JSONObject response = new JSONObject(result.string());

                    if (response.has("id")) {
                        String id = response.getString("id");
                        if (id != null) {
                            paymentDone();
                        } else
                            hideProgressDialog();

                    } else
                        hideProgressDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                    hideProgressDialog();
                }*/
            }
        });
    }

    @Override
    public void onPageActive(String str) {
        if (requestAdapter != null)
            requestAdapter.notifyDataSetChanged();
    }

    @OnClick({R.id.chkSwitch, R.id.txtRecenter})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.chkSwitch:
                changeOnlineOfflineStatus();
//                changeOfflineAndOnlineStatus();
                break;
            case R.id.txtRecenter:
                mapRecenter();
                recenterButtonManage();
                break;
        }
    }

    Emitter.Listener onNewRequest = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.e("New Request success", args[0].toString());


            Gson gson = new Gson();
            Type collectionType = new TypeToken<CreateBooking>() {
            }.getType();
            CreateBooking data = gson.fromJson(args[0].toString(), collectionType);

          /*  Gson gson = new Gson();
            CreateBooking data = gson.fromJson(args[0].toString(), CreateBooking.class);*/

            if (getActivity() != null) {
                getActivity().runOnUiThread(() -> {
                    Log.e("Booking size before", listBooking.size() + "...");
                    listBooking.add(0, data);
                    requestAdapter.notifyDataSetChanged();
                    bottomSheetAcceptReject.setVisibility(View.VISIBLE);
                    llConfirmedStarted.setVisibility(View.GONE);
                    bottomSheetUpcomingJobs.setVisibility(View.GONE);
                });
            } else {
                listBooking.add(0, data);
            }

        }
    };

    Emitter.Listener onCancelBooking = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.e("Cancel booking success", args[0].toString());

           /* Gson gson = new Gson();
            CreateBooking data = gson.fromJson(args[0].toString(), CreateBooking.class);*/

            Gson gson = new Gson();
            Type collectionType = new TypeToken<CreateBooking>() {
            }.getType();
            CreateBooking data = gson.fromJson(args[0].toString(), collectionType);


            if (getActivity() != null) {
                getActivity().runOnUiThread(() -> {
                    getBookingRequest();
//                    removeRequestFromList(data.getBookingId());
                });
            } else {
                getBookingRequest();
//                removeRequestFromList(data.getBookingId());
            }

        }
    };

    Emitter.Listener onDeleteBookingInvite = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.e("Delete b invite success", args[0].toString());

           /* Gson gson = new Gson();
            CreateBooking data = gson.fromJson(args[0].toString(), CreateBooking.class);*/

            Gson gson = new Gson();
            Type collectionType = new TypeToken<CreateBooking>() {
            }.getType();
            CreateBooking data = gson.fromJson(args[0].toString(), collectionType);


            if (getActivity() != null) {
                getActivity().runOnUiThread(() -> {
                    getBookingRequest();
//                    removeRequestFromList(data.getBookingId());
                });
            } else {
                getBookingRequest();
//                removeRequestFromList(data.getBookingId());
            }

        }
    };

    private void removeRequestFromList(Integer bookingId) {
        Log.e("Booking id compare", bookingId + "...");
        for (int i = 0; i < listBooking.size(); i++) {
            Log.e("Booking id loop", listBooking.get(i).getBookingId() + "...");

            if (listBooking.get(i).getBookingId() == bookingId) {
                listBooking.remove(i);
//                break;
            }
        }

        if (listBooking.size() <= 0)
            bottomSheetAcceptReject.setVisibility(View.GONE);

        requestAdapter.notifyDataSetChanged();
    }

    private void getBookingRequest() {

      /*  if (currentLatitude > 0 || currentLongitude > 0)
            getAvailableJobsByDriver();
        else
            getCurrentLatLongForAvailableJobsByDriver();*/

        homeActivity.showProgressDialog();

        String driverID = String.valueOf(AppHelper.getInstance().getUserDetails().getUserid());
        JSONObject param = new JSONObject();
        try {
            param.put(SocketConstant.DRIVER_ID, driverID);
            if (AppHelper.getInstance().getUserToken() != null) {
                param.put(SocketConstant.TOKEN, AppHelper.getInstance().getUserToken());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("Pending Booking param", param.toString());

        mSocket.emit(SocketConstant.DRIVER_BOOKING_INVITES, param, new Ack() {
            @Override
            public void call(Object... args) {
                int code = 400;

                JSONObject obj = null;
                try {
                    obj = new JSONObject(args[0].toString());
                    code = obj.getInt("status_code");

                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (code == 401) {
                    try {
                        if (getApplicationContext() != null) {
                            PreferanceHelper.getInstance().clearPreference();
                            WorkManager.getInstance(Controller.getInstance()).cancelAllWork();
                            homeActivity.openLoginActivity();
                        }
                    } catch (Exception e) {}
                } else {
                    Log.e("Pending Booking request", args[0].toString() + "...");


                    Gson gson = new Gson();
                    Type collectionType = new TypeToken<List<CreateBooking>>() {
                    }.getType();
                    List<CreateBooking> list = gson.fromJson(args[0].toString(), collectionType);

            /*    Gson gson = new Gson();
                List<CreateBooking> list = Arrays.asList(gson.fromJson(args[0].toString(), CreateBooking[].class));*/
                    Log.e("Pending Booking size", list.size() + "...");

                    getActivity().runOnUiThread(() -> {

                        homeActivity.hideProgressDialog();

                        if (list.size() > 0) {
                            listBooking.clear();
                            listBooking.addAll(list);
                            requestAdapter.notifyDataSetChanged();
                            bottomSheetAcceptReject.setVisibility(View.VISIBLE);
                            llConfirmedStarted.setVisibility(View.GONE);

                            showAvailableJobsOnMap();

                      /*  if (currentLatitude <= 0 || currentLongitude <= 0)
                            getCurrentLocation();
                        else
                            addMarker(currentLatitude, currentLongitude);*/
//                        addMarker(gpsTracker.getLatitude(), gpsTracker.getLongitude());
                        } else {
                            if (listBooking != null)
                                listBooking.clear();
                            bottomSheetAcceptReject.setVisibility(View.GONE);
                            getStartedCurrentBooking();

                        }
                    });
                }
            }
        });

    }

//    PlacesClient placesClient;

    private void showAvailableJobsOnMap() {
        if (currentLatitude > 0 || currentLongitude > 0)
            getAvailableJobsByDriver();
        else
            getCurrentLatLongForAvailableJobsByDriver();
    }

    private void getCurrentLocation() {
        if (currentLatitude > 0 && currentLongitude > 0) {
            addMarker(currentLatitude, currentLongitude);
            updateLocationAPI(currentLatitude, currentLongitude);
//            callUpdateLocationApi(currentLatitude, currentLongitude);
        }
        else {
            currentLatitude = gpsTracker.getLatitude();
            currentLongitude = gpsTracker.getLongitude();
            updateLocationAPI(currentLatitude, currentLongitude);
//            callUpdateLocationApi(currentLatitude, currentLongitude);
            addMarker(gpsTracker.getLatitude(), gpsTracker.getLongitude());
        }
//        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
//
//        FindCurrentPlaceRequest request =
//                FindCurrentPlaceRequest.newInstance(fields);
//
//        Task<FindCurrentPlaceResponse> placeResponse = placesClient.findCurrentPlace(request);
//
//        placeResponse.addOnCompleteListener(task -> {
//            if (task.isSuccessful()) {
//                FindCurrentPlaceResponse response = task.getResult();
//                PlaceLikelihood placeLikelihood = response.getPlaceLikelihoods().get(0);
//
//                Place place = placeLikelihood.getPlace();
//
//                currentLatitude = place.getLatLng().latitude;
//                currentLongitude = place.getLatLng().longitude;
//                callUpdateLocationApi(currentLatitude, currentLongitude);
//                addMarker(place.getLatLng().latitude, place.getLatLng().longitude);
//
//            } else {
//                Exception exception = task.getException();
//                Log.e("Current location", "Place not found: " + exception);
//                showSnackBar(exception.getLocalizedMessage());
//                if (exception instanceof ApiException) {
//                    ApiException apiException = (ApiException) exception;
//                    if (currentLatitude > 0 && currentLongitude > 0)
//                        addMarker(currentLatitude, currentLongitude);
//                    else {
//                        currentLatitude = gpsTracker.getLatitude();
//                        currentLongitude = gpsTracker.getLongitude();
//                        addMarker(gpsTracker.getLatitude(), gpsTracker.getLongitude());
//                    }
//                    Log.e("Current location", "Place not found: " + apiException.getStatusCode());
//                }
//            }
//        });
    }

    private void getCurrentLatLongForAvailableJobsByDriver() {
        if (currentLatitude > 0 && currentLongitude > 0)
            getAvailableJobsByDriver();
        else {
            currentLatitude = gpsTracker.getLatitude();
            currentLongitude = gpsTracker.getLongitude();
            getAvailableJobsByDriver();
        }
//        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
//
//        FindCurrentPlaceRequest request =
//                FindCurrentPlaceRequest.newInstance(fields);
//
//        Task<FindCurrentPlaceResponse> placeResponse = placesClient.findCurrentPlace(request);
//
//        placeResponse.addOnCompleteListener(task -> {
//            if (task.isSuccessful()) {
//                FindCurrentPlaceResponse response = task.getResult();
//                PlaceLikelihood placeLikelihood = response.getPlaceLikelihoods().get(0);
//
//                Place place = placeLikelihood.getPlace();
//
//                currentLatitude = place.getLatLng().latitude;
//                currentLongitude = place.getLatLng().longitude;
//
//                getAvailableJobsByDriver();
//
//            } else {
//                Exception exception = task.getException();
//                if (exception instanceof ApiException) {
//                    ApiException apiException = (ApiException) exception;
//                    if (currentLatitude > 0 && currentLongitude > 0)
//                        getAvailableJobsByDriver();
//                    else {
//                        currentLatitude = gpsTracker.getLatitude();
//                        currentLongitude = gpsTracker.getLongitude();
//                        getAvailableJobsByDriver();
//
//                    }
//                    Log.e("Current location", "Place not found: " + apiException.getStatusCode());
//                }
//            }
//        });
    }

    private void changeOfflineAndOnlineStatus() {

        homeActivity.showProgressDialog();
        JSONObject param = new JSONObject();
        try {
            param.put(SocketConstant.DRIVER_ID, AppHelper.getInstance().getUserDetails().getUserid());
            param.put(SocketConstant.SERVICE_ID, "");
            param.put(SocketConstant.ONLINE_OFFLINE_STATUS, chkSwitch.isChecked() ? 1 : 0);
            if (AppHelper.getInstance().getUserToken() != null) {
                param.put(SocketConstant.TOKEN, AppHelper.getInstance().getUserToken());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("on/off param", param.toString());

        mSocket.emit(SocketConstant.ONLINE_OFFLINE_DRIVERS, param, new Ack() {
            @Override
            public void call(Object... args) {

                getActivity().runOnUiThread(() -> {
                    homeActivity.hideProgressDialog();
                    int code = 400;

                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(args[0].toString());
                        code = obj.getInt("status_code");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (code == 401) {
                        try {
                            if (getApplicationContext() != null) {
                                PreferanceHelper.getInstance().clearPreference();
                                WorkManager.getInstance(Controller.getInstance()).cancelAllWork();
                                homeActivity.openLoginActivity();
                            }
                        } catch (Exception e) {}
                    } else {
                        Log.e("On/Off status success", args[0].toString() + "...");
                        Gson gson = new Gson();
                        OnOffStatus status = gson.fromJson(args[0].toString(), OnOffStatus.class);
                        UserData userData = AppHelper.getInstance().getUserDetails();
                        if (status.getOnlineOfflineStatus() == 1)
                            userData.setOnlineOfflineStatus("online");
                        else
                            userData.setOnlineOfflineStatus("offline");

                        AppHelper.getInstance().setUserDetails(userData);
                    }
                });

            }
        });
    }

    private HashMap<String, String> getOnlineStatusParam() {
        HashMap<String, String> param = new HashMap<>();
        param.put(Parameter.online_status, chkSwitch.isChecked() ? "1" : "0");
        return param;
    }

    private void changeOnlineOfflineStatus() {
        homeActivity.showProgressDialog();
        NetworkCall.getInstance().changeOnlineStatus(getOnlineStatusParam(), new IResponseCallback<OnOffResponse>() {
            @Override
            public void success(OnOffResponse data) {
                try {
                    homeActivity.hideProgressDialog();
                    if (data.isStatus()) {
                        UserData userData = AppHelper.getInstance().getUserDetails();
                        if (data.getData().getDriver().getOnlineStatus() == 1)
                            userData.setOnlineOfflineStatus("online");
                        else
                            userData.setOnlineOfflineStatus("offline");
                        AppHelper.getInstance().setUserDetails(userData);
                    } else {
                        showSnackBar(data.getMessage());
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                homeActivity.hideProgressDialog();
                showSnackBar(baseModel.getMessage());
            }

            @Override
            public void onError(Call<OnOffResponse> responseCall, Throwable T) {
                homeActivity.hideProgressDialog();
                showSnackBar(getString(R.string.error_message));
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("onDestroy", "Remove all socket listener");
        mSocket.off(SocketConstant.GET_BOOKING_INVITES, onNewRequest);
        mSocket.off(SocketConstant.LISTEN_CANCEL_BOOKING, onCancelBooking);
        mSocket.off(SocketConstant.DELETE_BOOKING_INVITES, onDeleteBookingInvite);
    }

    private void bookingActionByDriver(int bookingId, int status, String booking, String distance_matrix_res) {
//        0 = pending (default), 1 = accept booking, 2 = reject booking (Booking Status)

        homeActivity.showProgressDialog();
        JSONObject param = new JSONObject();
        try {
            param.put(SocketConstant.DRIVER_ID, AppHelper.getInstance().getUserDetails().getUserid());
            param.put(SocketConstant.COMPANY_CODE, AppHelper.getInstance().getUserDetails().getCompanyCode());
            param.put(SocketConstant.BOOKING_ID, bookingId);
            param.put(SocketConstant.BOOKING_STATUS, status);
            if (AppHelper.getInstance().getUserToken() != null) {
                param.put(SocketConstant.TOKEN, AppHelper.getInstance().getUserToken());
            }
            if (booking != null) {
                param.put(SocketConstant.booking, booking);
            }
            if (distance_matrix_res != null) {
                param.put(SocketConstant.distance_matrix_res, distance_matrix_res);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("Accept Reject param", param.toString());

        mSocket.emit(SocketConstant.BOOKING_ACTION_BY_DRIVER, param, new Ack() {
            @Override
            public void call(Object... args) {
                getActivity().runOnUiThread(() -> {
                    homeActivity.hideProgressDialog();
                    int code = 400;

                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(args[0].toString());
                        code = obj.getInt("status_code");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (code == 401) {
                        try {
                            if (getApplicationContext() != null) {
                                PreferanceHelper.getInstance().clearPreference();
                                WorkManager.getInstance(Controller.getInstance()).cancelAllWork();
                                homeActivity.openLoginActivity();
                            }
                        } catch (Exception e) {}
                    } else {
                        Log.e("Accept Reject success", args[0].toString() + "...");
                        int bookingId = 0;
                        try {
                            JSONObject jsnJsonObject = new JSONObject(args[0].toString());
                            JSONObject data = jsnJsonObject.getJSONObject("data");
                            bookingId = data.getInt("booking_id");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Log.e("Booking id ", bookingId + "...");
                        if (status == 1 || status == 2) {
                            removeRequestFromList(bookingId);
                            getBookingRequest();
                            getAvailableJobsByDriver();
                        } else if (status == 8) {
                            bottomSheetUpcomingJobs.setVisibility(View.GONE);
                            getStartedCurrentBooking();
                        }
                    }
                });

            }
        });

    }

    private BottomSheetBehavior mBottomSheetBehaviour;

    private void initBottomSheet() {
//        imgBottomArrow.setRotation(180);
        mBottomSheetBehaviour = BottomSheetBehavior.from(bottomSheetAcceptReject);
        if (getView() != null) {
            TypedValue tv = new TypedValue();

            if (getActivity().getTheme().resolveAttribute(R.attr.actionBarSize, tv, true)) {
                int actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
                mBottomSheetBehaviour.setPeekHeight(actionBarHeight * 6);
            }


        }

//        setUpcomingJobsAdapter();

//        onBottomSheetDragListener();
    }

    private void getStartedCurrentBooking() {

        homeActivity.showProgressDialog();

        String userID = String.valueOf(AppHelper.getInstance().getUserDetails().getUserid());
        JSONObject param = new JSONObject();
        try {
            param.put(SocketConstant.USER_ID, userID);
            if (AppHelper.getInstance().getUserToken() != null) {
                param.put(SocketConstant.TOKEN, AppHelper.getInstance().getUserToken());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("started/current param", param.toString());

        mSocket.emit(SocketConstant.GET_DRIVER_BOOKING, param, new Ack() {
            @Override
            public void call(Object... args) {
                if (getActivity() != null) {
                    getActivity().runOnUiThread(() -> {
                        homeActivity.hideProgressDialog();
                        int code = 400;

                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(args[0].toString());
                            code = obj.getInt("status_code");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (code == 401) {
                            try {
                                if (getApplicationContext() != null) {
                                    PreferanceHelper.getInstance().clearPreference();
                                    WorkManager.getInstance(Controller.getInstance()).cancelAllWork();
                                    homeActivity.openLoginActivity();
                                }
                            } catch (Exception e) {}
                        } else {
                            Log.e("started/current respnse", args[0].toString() + "...");

                            Gson gson = new Gson();
                            Type collectionType = new TypeToken<List<CreateBooking>>() {
                            }.getType();
                            List<CreateBooking> list = gson.fromJson(args[0].toString(), collectionType);

               /* Gson gson = new Gson();
                List<CreateBooking> list = Arrays.asList(gson.fromJson(args[0].toString(), CreateBooking[].class));*/
                            Log.e("started/current size", list.size() + "...");

                            if (list.size() > 0) {
                                removeCurrentLocationMarker();
                                setConfirmStartedLayout(list.get(0));
                            } else {
                                llConfirmedStarted.setVisibility(View.GONE);
                                getUpcomingBooking();
                                showAvailableJobsOnMap();
                            }
                        }

                    });
                }
            }
        });

    }

    private void setConfirmStartedLayout(CreateBooking createBooking) {
//        if (createBooking.getBookingType() == 0 && createBooking.getBookingStatus() == 1) {
        if (createBooking.getBookingStatus() == 1) {
            //After booking is confirmed
            llConfirmedStarted.setVisibility(View.VISIBLE);
            if (listBooking != null)
                listBooking.clear();
            bottomSheetAcceptReject.setVisibility(View.GONE);
            txtStartJob.setVisibility(View.VISIBLE);
            if (createBooking.getBookingType() == 0)
                txtCancelJob.setVisibility(View.VISIBLE);
            else
                txtCancelJob.setVisibility(View.GONE);
            txtEndob.setVisibility(View.GONE);

            setCustomerData(createBooking);
            if (currentLongitude <= 0 || currentLongitude <= 0)
                getCurrentLocationForPathOnMap(0, createBooking);
            else
                showOnMap(0, createBooking);

            txtStartJob.setOnClickListener(view -> {

                BookingData data = new BookingData();
                data.setBookingId(createBooking.getBookingId());
                data.setUser(createBooking.getUser());
                data.setTotalCost(createBooking.getTotalCost());
                data.setTowPrice(createBooking.getTowPrice());
                data.setVehicle(createBooking.getVehicle());
                data.setAccidentType(createBooking.getAccidentType());
                data.setBreakdownType(createBooking.getBreakdownType());
                data.setSource(createBooking.getSource());
                data.setDestination(createBooking.getDestination());
                data.setBookingTowType(createBooking.getBookingTowType());
                data.setDiscountedAmount(createBooking.getDiscountedAmount());
                data.setCountry(createBooking.getCountry());
                appNavigationActivity.openStartJobctivity(data);
            });
            txtCancelJob.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (createBooking.getCountry().contains("MY")) {
                        cancelBookingMalaysia(createBooking.getBookingId());
                    } else {
                        cancelBookingSG(createBooking.getBookingId());
//                        cancelBooking(createBooking.getBookingId(), null);
                    }
                }
            });

        } else if (createBooking.getBookingStatus() == 2) {
            //Ongoing Job
            llConfirmedStarted.setVisibility(View.VISIBLE);
            if (listBooking != null)
                listBooking.clear();
            bottomSheetAcceptReject.setVisibility(View.GONE);
            txtStartJob.setVisibility(View.GONE);
            txtCancelJob.setVisibility(View.GONE);
            txtEndob.setVisibility(View.VISIBLE);

            setCustomerData(createBooking);
            if (currentLongitude <= 0 || currentLongitude <= 0)
                getCurrentLocationForPathOnMap(1, createBooking);
            else
                showOnMap(1, createBooking);

            txtEndob.setOnClickListener(view -> {

                BookingData data = new BookingData();
                data.setBookingId(createBooking.getBookingId());
                data.setUser(createBooking.getUser());
                data.setTotalCost(createBooking.getTotalCost());
                data.setVehicle(createBooking.getVehicle());
                data.setAccidentType(createBooking.getAccidentType());
                data.setBreakdownType(createBooking.getBreakdownType());
                data.setSource(createBooking.getSource());
                data.setDestination(createBooking.getDestination());
                data.setBookingTowType(createBooking.getBookingTowType());
                data.setUser(createBooking.getUser());
                data.setDiscountedAmount(createBooking.getDiscountedAmount());
                data.setCountry(createBooking.getCountry());

                appNavigationActivity.openEndJobctivity(data);
            });

        }

        txtNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openNavigationDialog();
            }
        });

    }

    private void openNavigationDialog() {
        homeActivity.openNavigationTypeDialog(this);
    }

    @Override
    public void onNavigationTypeSelect(NavigationType navigationType) {
        if (navigationType == NavigationType.GOOGLE_MAP) {
            openGoogelMap();
        } else if (navigationType == NavigationType.WAZE_MAP) {
            openWazeMap();
        }

    }

    private void showOnMap(int i, CreateBooking data) {
        LinkedTreeMap<String, String> driver = (LinkedTreeMap<String, String>) data.getDriver();

        Double sourceLatitude;
        Double sourceLongitude;
        Double destLatitude;
        Double destLongitude;


        if (i == 0) {//after booking confirm
            sourceLatitude = currentLatitude;
            sourceLongitude = currentLongitude;

            destLatitude = Double.parseDouble(data.getLatitude());
            destLongitude = Double.parseDouble(data.getLongitude());
            drawPath(sourceLatitude, sourceLongitude, destLatitude, destLongitude, R.drawable.ic_driver_marker, R.drawable.marker);
        } else {
            sourceLatitude = Double.parseDouble(data.getLatitude());
            sourceLongitude = Double.parseDouble(data.getLongitude());
            destLatitude = Double.parseDouble(data.getDestLatitude());
            destLongitude = Double.parseDouble(data.getDestLongitude());
            drawPath(sourceLatitude, sourceLongitude, destLatitude, destLongitude, R.drawable.ic_driver_marker, R.drawable.ic_workshop);
        }
        Log.e("Draw path", "Source :" + sourceLatitude + "," + sourceLongitude + " Destination :" + destLatitude + "," + destLongitude);

    }

    private void getCurrentLocationForPathOnMap(int i, CreateBooking data) {
        if (currentLatitude > 0 && currentLongitude > 0)
            showOnMap(i, data);
        else {
            currentLatitude = gpsTracker.getLatitude();
            currentLongitude = gpsTracker.getLongitude();
            showOnMap(i, data);

        }
//        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
//
//        FindCurrentPlaceRequest request =
//                FindCurrentPlaceRequest.newInstance(fields);
//
//        Task<FindCurrentPlaceResponse> placeResponse = placesClient.findCurrentPlace(request);
//
//        placeResponse.addOnCompleteListener(task -> {
//            if (task.isSuccessful()) {
//                FindCurrentPlaceResponse response = task.getResult();
//                PlaceLikelihood placeLikelihood = response.getPlaceLikelihoods().get(0);
//
//                Place place = placeLikelihood.getPlace();
//
//                currentLatitude = place.getLatLng().latitude;
//                currentLongitude = place.getLatLng().longitude;
//
//                showOnMap(i, data);
//
//            } else {
//                Exception exception = task.getException();
//                if (exception instanceof ApiException) {
//                    ApiException apiException = (ApiException) exception;
//                    if (currentLatitude > 0 && currentLongitude > 0)
//                        showOnMap(i, data);
//                    else {
//                        currentLatitude = gpsTracker.getLatitude();
//                        currentLongitude = gpsTracker.getLongitude();
//                        showOnMap(i, data);
//
//                    }
//                    Log.e("Current location", "Place not found: " + apiException.getStatusCode());
//                }
//            }
//        });
    }

    private void setCustomerData(CreateBooking createBooking) {
        txtDestination.setText(createBooking.getDestination());
        double d = Double.parseDouble(createBooking.getTotalCost());
        String curr = "SGD";
        if (createBooking.getCountry() != null && createBooking.getCountry().contains("MY")) {
            curr = "RM";
        }
        txtPrice.setText(curr + "" +  AppHelper.getInstance().formatCurrency(d));
        txtSource.setText(createBooking.getSource());
        String vhcType = createBooking.getVehicle().getVehicleTypeName();
        String vehicletype = vhcType.substring(0, 1).toUpperCase() + vhcType.substring(1).toLowerCase();
        txtTitle.setText(createBooking.getVehicle().getVehicleNo()+" (" +vehicletype + "-" + createBooking.getTowPriceType()+")");

        if (createBooking.getPaymentType() == 0)
            txtPaymentType.setText(getString(R.string.cash));
        else
            txtPaymentType.setText(getString(R.string.card));

        imgMessage.setOnClickListener(view -> {
            String startedImages = "";
            String stoppedImages = "";
            if (createBooking.getStartedImages() != null && createBooking.getStartedImages().size() > 0)
                startedImages = (String) createBooking.getStartedImages().get(0);

            if (createBooking.getStoppedImages() != null && createBooking.getStoppedImages().size() > 0)
                stoppedImages = (String) createBooking.getStoppedImages().get(0);
            homeActivity.openChatActivity(createBooking.getBookingId(), startedImages, stoppedImages, ChatType.CURRENT, createBooking.getUser(), 0);

        });
        imgCall.setOnClickListener(view -> {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            if (createBooking.getUser().getCountryCode() != null) {
                if (createBooking.getUser().getCountryCode().contains("65")) {
                    intent.setData(Uri.parse("tel:" + createBooking.getUser().getCountryCode() + createBooking.getUser().getMobile()));
                } else if (createBooking.getUser().getCountryCode().contains("60")) {
                    intent.setData(Uri.parse("tel:0" + createBooking.getUser().getMobile()));
                } else {
                    intent.setData(Uri.parse("tel:" + createBooking.getUser().getMobile()));
                }
            } else {
                intent.setData(Uri.parse("tel:" + createBooking.getUser().getMobile()));
            }
            startActivity(intent);
        });
        txtInfo.setOnClickListener(view -> {
            if (createBooking.getExtraNotes() == null || "".equals(createBooking.getExtraNotes())) {
                appNavigationActivity.openBookingRemarkDialog("No booking remarks");
            } else {
                appNavigationActivity.openBookingRemarkDialog(createBooking.getExtraNotes());
            }
        });
    }

    private void getUpcomingBooking() {
//        showProgressDialog();
        JSONObject object = new JSONObject();
        try {
            object.put(SocketConstant.USER_ID, AppHelper.getInstance().getUserDetails().getUserid());
            object.put(SocketConstant.user_type, AppConstant.USER_TYPE);
            if (AppHelper.getInstance().getUserToken() != null) {
                object.put(SocketConstant.TOKEN, AppHelper.getInstance().getUserToken());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("UpcomingBooking param", object.toString() + "...");
        mSocket.emit(SocketConstant.UPCOMING_BOOKING_REQUEST, object, new Ack() {
            @Override
            public void call(Object... args) {
                getActivity().runOnUiThread(() -> {
                    int code = 400;

                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(args[0].toString());
                        code = obj.getInt("status_code");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (code == 401) {
                        try {
                            if (getApplicationContext() != null) {
                                PreferanceHelper.getInstance().clearPreference();
                                WorkManager.getInstance(Controller.getInstance()).cancelAllWork();
                                homeActivity.openLoginActivity();
                            }
                        } catch (Exception e) {}
                    } else {
                        Log.e("UpcomingBooking", args[0].toString());

                        Gson gson = new Gson();

                        Type collectionType = new TypeToken<ArrayList<CreateBooking>>() {
                        }.getType();
                        List<CreateBooking> list = gson.fromJson(args[0].toString(), collectionType);

                        Log.e("UpcomingBooking size", list.size() + "...");
                        if (list.size() > 0) {

                            if (upcomingBookingList == null)
                                upcomingBookingList = new ArrayList<>();
                            else
                                upcomingBookingList.clear();
                            upcomingBookingList.addAll(list);
                            if (upcomingBookingList != null && upcomingBookingList.size() > 0) {

                                initUpcomingBottomSheet();
                                bottomSheetUpcomingJobs.setVisibility(View.VISIBLE);
                                llConfirmedStarted.setVisibility(View.GONE);
                                adapter.notifyDataSetChanged();
                            }
//                        setConfirmStartedLayout(list.get(0));
                        } else {

                      /*  if (currentLatitude <= 0 || currentLongitude <= 0)
                            getCurrentLocation();
                        else
                            addMarker(currentLatitude, currentLongitude);*/
//                        showAvailableJobsOnMap();
                            llConfirmedStarted.setVisibility(View.GONE);
                        }
                    }
                });
            }
        });

    }

    private void cancelBookingMalaysia(int id) {
        homeActivity.showProgressDialog();
        NetworkCall.getInstance().cancelMalaysia(id+"", new IResponseCallback<ResponseBody>() {
            @Override
            public void success(ResponseBody data) {
                try {
                    JSONObject json = new JSONObject(data.string());
                    if (json.getBoolean("status")) {
                        cancelBooking(id, json.getJSONObject("data").getJSONObject("booking").toString());
                    } else {
                        homeActivity.hideProgressDialog();
                        showSnackBar(json.getString("message"));
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                homeActivity.hideProgressDialog();
                showSnackBar(baseModel.getMessage());
            }

            @Override
            public void onError(Call<ResponseBody> responseCall, Throwable T) {
                homeActivity.hideProgressDialog();
                showSnackBar(getString(R.string.error_message));
            }
        });
    }

    private void cancelBookingSG(int id) {
        homeActivity.showProgressDialog();
        NetworkCall.getInstance().cancelSG(id+"", new IResponseCallback<ResponseBody>() {
            @Override
            public void success(ResponseBody data) {
                try {
                    JSONObject json = new JSONObject(data.string());
                    if (json.getBoolean("status")) {
                        cancelBooking(id, json.getJSONObject("data").getJSONObject("booking").toString());
                    } else {
                        homeActivity.hideProgressDialog();
                        showSnackBar(json.getString("message"));
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(BaseModel baseModel) {
                homeActivity.hideProgressDialog();
                showSnackBar(baseModel.getMessage());
            }

            @Override
            public void onError(Call<ResponseBody> responseCall, Throwable T) {
                homeActivity.hideProgressDialog();
                showSnackBar(getString(R.string.error_message));
            }
        });
    }

    private void cancelBooking(int bookingID, String booking) {
        homeActivity.showProgressDialog();
        String userID = String.valueOf(AppHelper.getInstance().getUserDetails().getUserid());
        JSONObject param = new JSONObject();
        try {
            param.put(SocketConstant.BOOKING_ID, bookingID);
            param.put(SocketConstant.DRIVER_ID, userID);
            if (booking != null) {
                param.put(SocketConstant.booking, booking);
            }
            if (AppHelper.getInstance().getUserToken() != null) {
                param.put(SocketConstant.TOKEN, AppHelper.getInstance().getUserToken());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mSocket.emit(SocketConstant.JOB_NOT_PERFORMED_BY_DRIVER, param, new Ack() {
            @Override
            public void call(Object... args) {
                homeActivity.runOnUiThread(() -> {
                    homeActivity.hideProgressDialog();
                    int code = 400;

                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(args[0].toString());
                        code = obj.getInt("status_code");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (code == 401) {
                        try {
                            if (getApplicationContext() != null) {
                                PreferanceHelper.getInstance().clearPreference();
                                WorkManager.getInstance(Controller.getInstance()).cancelAllWork();
                                homeActivity.openLoginActivity();
                            }
                        } catch (Exception e) {}
                    } else {
                        Log.e("Cancel booking success", args[0].toString() + "...");
//                    openHomeActivity();
                        removePolyline();
                        getBookingRequest();
                        bottomSheetUpcomingJobs.setVisibility(View.GONE);
//                    getStartedCurrentBooking();
                    }
                });
            }
        });

    }

    public boolean getServicesAvailable() {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int isAvailable = api.isGooglePlayServicesAvailable(getActivity());
        if (isAvailable == ConnectionResult.SUCCESS) {
            return true;
        } else if (api.isUserResolvableError(isAvailable)) {

            Dialog dialog = api.getErrorDialog(getActivity(), isAvailable, 0);
            dialog.show();
        } else {
            Toast.makeText(getActivity(), "Play store service error", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

      /*  LatLng sydney = new LatLng(-34, 151);
        googleMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));*/

        map.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
            @Override
            public void onCameraMoveStarted(int i) {
                DebugLog.e("onCameraMoveStarted : isForceMove - " + isForceMapMove + "  isRecenter - " + isRecenter);
                isRecenter = !isForceMapMove;
                recenterButtonManage();
                isForceMapMove = true;
            }
        });
        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                if (!marker.getTag().toString().equalsIgnoreCase("Current Location")) {
                    int position = (int) (marker.getTag());
                    Log.e("marker position", position + "");

                    listBooking.clear();
                    listBooking.add(availableJobsList.get(position));
                    requestAdapter.notifyDataSetChanged();

                    bottomSheetUpcomingJobs.setVisibility(View.GONE);
                    bottomSheetAcceptReject.setVisibility(View.VISIBLE);
                    llConfirmedStarted.setVisibility(View.GONE);

                }
                return false;
            }
        });

        setAdapter();
        getBookingRequest();
        getCurrentLocation();
        initBottomSheet();


    }

    private void recenterButtonManage() {
        if (isRecenter) {
            txtRecenter.setVisibility(View.GONE);
        } else {
            txtRecenter.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onLocationChange(String address, double latitude, double longitude, Location location) {

        currentLatitude = latitude;
        currentLongitude = longitude;

        AppHelper.getInstance().setLatitude(currentLatitude + "");
        AppHelper.getInstance().setLongitude(currentLongitude + "");
        updateLocationAPI(latitude, longitude);
//        callUpdateLocationApi(latitude, longitude);
        Log.e("onLocationChange", "Latitude :" + latitude + "  Longitude :" + longitude);
        if (getActivity() != null)
            getActivity().runOnUiThread(() -> {
                addMarker(currentLatitude, currentLongitude);
                edtCurrentLocation.setText(address);
                againAndAgain(location);
                mLastLocation = location;
//            Toast.makeText(getActivity(), "Latitude :" + latitude + "  Longitude :" + longitude, Toast.LENGTH_LONG).show();
//            txtAddress.setText(address);
            });

    }

    private HashMap<String, String> getParam(Double latitude, Double longitude) {
        HashMap<String, String> param = new HashMap<>();
        param.put(Parameter.latitude, latitude+"");
        param.put(Parameter.longitude, longitude+"");
        return param;
    }

    private void updateLocationAPI(Double latitude, Double longitude) {
        NetworkCall.getInstance().updateLocation(getParam(latitude, longitude), new IResponseCallback<BaseModel>() {
            @Override
            public void success(BaseModel data) {

            }

            @Override
            public void onFailure(BaseModel baseModel) {
            }

            @Override
            public void onError(Call<BaseModel> responseCall, Throwable T) {
            }
        });
    }

    private void
    callUpdateLocationApi(Double latitude, Double longitude) {
        Log.e("Update location", "latitude:" + latitude + "..." + "longitude:" + longitude + "...");
        if (AppHelper.getInstance().getUserDetails() != null) {
            String userID = String.valueOf(AppHelper.getInstance().getUserDetails().getUserid());
            JSONObject param = new JSONObject();
            try {
                param.put(SocketConstant.USER_ID, userID);
                param.put(SocketConstant.LATITUDE, String.valueOf(latitude));
                param.put(SocketConstant.LONGITUDE, String.valueOf(longitude));
                if (AppHelper.getInstance().getUserToken() != null) {
                    param.put(SocketConstant.TOKEN, AppHelper.getInstance().getUserToken());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            mSocket.emit(SocketConstant.UPDATE_LOCATION, param, new Ack() {
                @Override
                public void call(Object... args) {
                    int code = 400;

                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(args[0].toString());
                        code = obj.getInt("status_code");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (code == 401) {
                        try {
                            if (getApplicationContext() != null) {
                                PreferanceHelper.getInstance().clearPreference();
                                WorkManager.getInstance(Controller.getInstance()).cancelAllWork();
                                homeActivity.openLoginActivity();
                            }
                        } catch (Exception e) {}
                    } else {
                        Log.e("Update location success", "latitude:" + latitude + "..." + "longitude:" + longitude + "...");
                    }
                }
            });
        }
    }

    private void initUpcomingBottomSheet() {
        bottomSheetUpcomingJobs.setVisibility(View.VISIBLE);

        imgBottomArrow.setRotation(180);
        mBottomSheetBehaviour = BottomSheetBehavior.from(bottomSheetUpcomingJobs);
        if (getView() != null) {
            TypedValue tv = new TypedValue();

            if (getActivity().getTheme().resolveAttribute(R.attr.actionBarSize, tv, true)) {
                int actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
                mBottomSheetBehaviour.setPeekHeight(actionBarHeight);
            }

        }

        setUpcomingJobsAdapter();

        onBottomSheetDragListener();


        mBottomSheetBehaviour.setState(BottomSheetBehavior.STATE_EXPANDED);


    }

    private int stateOfBottomSheet = 0;
    List<CreateBooking> upcomingBookingList;
    UpcomingJobsAdapter adapter;

    private void setUpcomingJobsAdapter() {
        if (upcomingBookingList == null)
            upcomingBookingList = new ArrayList<>();
        rvUpcomingJob.setLayoutManager(new LinearLayoutManager(getActivity()));
        if (adapter == null)
            adapter = new UpcomingJobsAdapter(getActivity(), upcomingBookingList);

        adapter.setCallback((view, position, object) -> {
            CreateBooking createBooking = (CreateBooking) object;
            switch (view.getId()) {
                case R.id.txtCancelJob:
                    if (createBooking.getCountry().contains("MY")) {
                        cancelBookingMalaysia(createBooking.getBookingId());
                    } else {
                        cancelBookingSG(createBooking.getBookingId());
//                        cancelBooking(createBooking.getBookingId(), null);
                    }
                    break;
                case R.id.txtNavigateJob:
//                    bookingActionByDriver(createBooking.getBookingId(), 8);
                    BookingData bookingData = new BookingData();
                    User driverData = new User();

                    if (createBooking.getDriver() != null) {

                        if (createBooking.getDriver() instanceof List) {

                        } else {
                            LinkedTreeMap<String, String> driver = (LinkedTreeMap<String, String>) createBooking.getDriver();
                            driverData.setUserImage(String.valueOf(driver.get("user_image")));
                            driverData.setName(String.valueOf(driver.get("name")));
                            double userID = Double.parseDouble(String.valueOf(driver.get("userid")));
                            driverData.setUserid((int) userID);
                        }
                    }

                    bookingData.setDriver(driverData);
                    bookingData.setTowPrice(createBooking.getTowPrice());
                    bookingData.setDiscountedAmount(createBooking.getDiscountedAmount());
                    bookingData.setVehicle(createBooking.getVehicle());
                    bookingData.setTowType(createBooking.getTowType());
                    bookingData.setSource(createBooking.getSource());
                    bookingData.setDestination(createBooking.getDestination());
                    bookingData.setBookingStatus(createBooking.getBookingStatus());
                       /* bookingData.setEndRideDescription(booking.getEndRideDescription());
                        bookingData.setStoppedImages(booking.getStoppedImages());*/
                    bookingData.setBookingId(createBooking.getBookingId());
                    bookingData.setUser(createBooking.getUser());
                    bookingData.setCouponId(createBooking.getCouponId());

                    appNavigationActivity.openJobDetailsActivity(bookingData);

                    break;
            }
        });
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);
        rvUpcomingJob.addItemDecoration(dividerItemDecoration);
        rvUpcomingJob.setAdapter(adapter);
    }

    public void onBottomSheetDragListener() {
        mBottomSheetBehaviour.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {

            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                // Called every time when the bottom sheet changes its state.

                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        imgBottomArrow.setRotation(0);
                        stateOfBottomSheet = 1;

                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        stateOfBottomSheet = 0;
                        imgBottomArrow.setRotation(180);

                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:

                        break;
                    case BottomSheetBehavior.STATE_SETTLING:


                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                // Called when the bottom sheet is being dragged


            }

        });
    }

    private void getAvailableJobsByDriver() {

        //homeActivity.showProgressDialog();

        String userID = String.valueOf(AppHelper.getInstance().getUserDetails().getUserid());
        JSONObject param = new JSONObject();
        try {
            param.put(SocketConstant.DRIVER_ID, userID);
            param.put(SocketConstant.LATITUDE, currentLatitude);
            param.put(SocketConstant.LONGITUDE, currentLongitude);
            param.put(SocketConstant.DISTANCE, String.valueOf(AppHelper.getInstance().getConfigDetails().getRadius()));
            if (AppHelper.getInstance().getUserToken() != null) {
                param.put(SocketConstant.TOKEN, AppHelper.getInstance().getUserToken());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("Available jobs param", param.toString());

        mSocket.emit(SocketConstant.GET_AVAILABLE_JOBS, param, new Ack() {
            @Override
            public void call(Object... args) {
                if (getActivity() != null)
                    getActivity().runOnUiThread(() -> {
                        homeActivity.hideProgressDialog();
                        int code = 400;

                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(args[0].toString());
                            code = obj.getInt("status_code");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (code == 401) {
                            try {
                                if (getApplicationContext() != null) {
                                    PreferanceHelper.getInstance().clearPreference();
                                    WorkManager.getInstance(Controller.getInstance()).cancelAllWork();
                                    homeActivity.openLoginActivity();
                                }
                            } catch (Exception e) {}
                        } else {
                            Log.e("Available jobs respnse", args[0].toString() + "...");

                            Gson gson = new Gson();
                            Type collectionType = new TypeToken<List<CreateBooking>>() {
                            }.getType();
                            List<CreateBooking> list = gson.fromJson(args[0].toString(), collectionType);

//                Gson gson = new Gson();
//                List<CreateBooking> list = Arrays.asList(gson.fromJson(args[0].toString(), CreateBooking[].class));
                            Log.e("Available jobs size", list.size() + "...");
                            if (availableJobsList == null)
                                availableJobsList = new ArrayList<>();
                            else
                                availableJobsList.clear();
                            availableJobsList.addAll(list);

                            addAvailableJobsMarker(availableJobsList);
                        }
                    });

            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        StripeUtil.getPaymentSessionHandler(homeActivity).onActivityResult(requestCode, resultCode, data);
    }

}