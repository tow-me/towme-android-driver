package sg.com.towme.driver.activity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import sg.com.towme.driver.R;
import sg.com.towme.driver.adapter.HistoryAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryActivity extends ToolBarActivity {

    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    HistoryAdapter historyAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_history);
        ButterKnife.bind(this);
        super.onCreate(savedInstanceState);

        iniUI();
        setAdapter();
    }

    private void iniUI() {
        setHomeIcon(R.drawable.ic_back_white);
        setToolbarTitle(getString(R.string.history));
    }

    private void setAdapter() {
        recyclerview.setLayoutManager(new LinearLayoutManager(this));
//        historyAdapter = new HistoryAdapter(HistoryActivity.this);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerview.getContext(), DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.line_horizontal));
        recyclerview.addItemDecoration(dividerItemDecoration);
        recyclerview.setAdapter(historyAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
//        menu.findItem(R.id.action_overflow).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
