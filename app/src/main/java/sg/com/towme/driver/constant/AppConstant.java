package sg.com.towme.driver.constant;

public class AppConstant {
    public static final String SCREEN = "screen";
    public static final String social_data = "social_data";
    public static final String user_data = "user_data";
    public static final String notif_id = "notif_id";
    public static final String USER_TYPE = "1";
    public static final String MOBILE_TYPE = "0";
    public static final String booking_id = "booking_id";
    public static final int REQUEST_CHECK_SETTINGS = 214;
    public static final int REQUEST_CHAT = 215;
    public static final String CURRENCY = "SGD";
    public static final String booking_data = "booking_data";
    public static final String help_url = "http://172.105.35.50/towme/help";

    public static int UPDATE_INTERVAL = 3000; // 3 sec
    public static int FATEST_INTERVAL = 3000; // 5 sec
    public static int DISPLACEMENT = 10; // 10 meters
}
