package sg.com.towme.driver.Dialog;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;

import sg.com.towme.driver.R;
import sg.com.towme.driver.activity.HomeActivity;
import sg.com.towme.driver.listener.iPersonalBusinessListner;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PersonalBusinessDialog extends BaseDialog {

    HomeActivity homeActivity;
    @BindView(R.id.btnBusiness)
    AppCompatButton btnBusiness;
    @BindView(R.id.btnPersonal)
    AppCompatButton btnPersonal;
    @BindView(R.id.llDialog)
    LinearLayout llDialog;

    public void setCallback(iPersonalBusinessListner callback) {
        this.callback = callback;
    }

    iPersonalBusinessListner callback;

    public static PersonalBusinessDialog newInstance(iPersonalBusinessListner callback) {
        PersonalBusinessDialog fragment = new PersonalBusinessDialog();
        fragment.setCallback(callback);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_personal_business, container, false);
        getDialog().requestWindowFeature(STYLE_NO_TITLE);
        setCancelable(false);
        ButterKnife.bind(this, view);

        return view;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof HomeActivity) {
            homeActivity = (HomeActivity) context;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }


    @OnClick({R.id.btnBusiness, R.id.btnPersonal})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnBusiness:
                callback.onBusinessClick();
                break;
            case R.id.btnPersonal:
                callback.onPersonalClick();
                break;
        }
        dismiss();

    }
}
