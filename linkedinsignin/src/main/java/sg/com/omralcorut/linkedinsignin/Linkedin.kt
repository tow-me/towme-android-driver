package sg.com.omralcorut.linkedinsignin

import android.content.Context
import android.content.Intent
import sg.com.omralcorut.linkedinsignin.helper.Constants
import sg.com.omralcorut.linkedinsignin.model.LinkedinToken
import sg.com.omralcorut.linkedinsignin.ui.LinkedinSignInActivity
import java.net.URLEncoder

class Linkedin {

    companion object {
        var linkedinLoginViewResponseListener: LinkedinLoginViewResponseListener? = null

        fun initialize(context: Context?, clientId: String, clientSecret: String, redirectUri: String, state: String, scopes: List<String>) {
            val editor = context?.getSharedPreferences(Constants.PREFS, Context.MODE_PRIVATE)?.edit()
            editor?.putString(Constants.CLIENT_ID, clientId)
            editor?.putString(Constants.CLIENT_SECRET, clientSecret)
            editor?.putString(Constants.REDIRECT_URI, redirectUri)
            editor?.putString(Constants.STATE, state)
            editor?.putStringSet(Constants.SCOPE, scopes.toMutableSet())
            editor?.apply()
        }

        fun login(context: Context?, listener: LinkedinLoginViewResponseListener) {
            linkedinLoginViewResponseListener = listener
            val loginActivity = Intent(context, LinkedinSignInActivity::class.java)
            context?.startActivity(loginActivity)
        }
    }
}

interface LinkedinLoginViewResponseListener {
    fun linkedinDidLoggedIn(linkedinToken: LinkedinToken)
    fun linkedinLoginDidFail(error: String)
}