package sg.com.omralcorut.linkedinsignin.model

data class LinkedinToken(val accessToken: String?,
                         val expiredTime: Long)